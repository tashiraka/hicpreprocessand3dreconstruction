# EcoRI
a <- read.table("E-Me-A.cfm.norm")
b <- read.table("E-Me-B.cfm.norm")
eme.cfm <- a + b

a <- read.table("E-Mp-A.cfm.norm")
b <- read.table("E-Mp-B.cfm.norm")
emp.cfm <- a + b

a <- read.table("E-Me-A.visibility.norm")
x <- mean(subset(a, V1>0)$V1)
b <- read.table("E-Me-B.visibility.norm")
y <- mean(subset(b, V1>0)$V1)
eme.visibility <- x + y

a <- read.table("E-Mp-A.visibility.norm")
x <- mean(subset(a, V1>0)$V1)
b <- read.table("E-Mp-B.visibility.norm")
y <- mean(subset(b, V1>0)$V1)
emp.visibility <- x + y

e.visibility.mean = mean(c(eme.visibility, emp.visibility))
e.cfm <- eme.cfm * e.visibility.mean / eme.visibility + emp.cfm * e.visibility.mean / emp.visibility

write.table(e.cfm, "E.cfm.norm", sep="\t", row.names=F, col.names=F)



# HindIII
a <- read.table("H-Me-A.cfm.norm")
b <- read.table("H-Me-B.cfm.norm")
hme.cfm <- a + b

a <- read.table("H-Mp-A.cfm.norm")
b <- read.table("H-Mp-B.cfm.norm")
hmp.cfm <- a + b

a <- read.table("H-Me-A.visibility.norm")
x <- mean(subset(a, V1>0)$V1)
b <- read.table("H-Me-B.visibility.norm")
y <- mean(subset(b, V1>0)$V1)
hme.visibility <- x + y

a <- read.table("H-Mp-A.visibility.norm")
x <- mean(subset(a, V1>0)$V1)
b <- read.table("H-Mp-B.visibility.norm")
y <- mean(subset(b, V1>0)$V1)
hmp.visibility <- x + y

h.visibility.mean = mean(c(hme.visibility, hmp.visibility))
h.cfm <- hme.cfm * h.visibility.mean / hme.visibility + hmp.cfm * h.visibility.mean / hmp.visibility

write.table(h.cfm, "H.cfm.norm", sep="\t", row.names=F, col.names=F)

