import std.stdio, std.conv, std.string, std.algorithm, std.array, std.typecons;

// The format of binInfo: [binID, contigName, startCoord, endCoord]
// If you want to get sub contact frequency matrix, you extract bin from binInfo file.
void main(string[] args)
{
  auto cfmFile = args[1];
  auto connectivityFile = args[2];       
  auto binInfoFile = args[3];
  auto outFile = args[4];

  auto sepChar = "-";

  auto gapInfo = readConnectivity(connectivityFile);
  double[][] connectedCfm;
  foreach(line; File(cfmFile).byLine) {
    connectedCfm ~= line.to!string.strip.split("\t").map!(x =>
                                                          x.to!double).array;
  }

  double[][] cfm;
  auto id = 0;
  foreach(isNotMissing; gapInfo) {
    auto tmp = new double[](gapInfo.length);
    tmp.fill(0);

    if(isNotMissing) {
      auto idCol = 0;
      
      foreach(i, isNotMissingCol; gapInfo) {
        if(isNotMissingCol) {
          tmp[i] = connectedCfm[id][idCol++];
        }
      }
      id++;
    }

    cfm ~= tmp;
  }

  string[] binInfo;
  foreach(line; File(binInfoFile).byLine) {
    binInfo ~= line.to!string.strip.replace("\t", sepChar);
  }

  auto fout = File(outFile, "w");
  fout.write("HiCMatrix\tRegions");
  foreach(info; binInfo) {
    fout.write("\t" ~ info);
  }
  fout.writeln;
  auto firstBinID = binInfo[0].split(sepChar)[0].to!uint;
  auto lastBinID = binInfo[$ - 1].split(sepChar)[0].to!uint;
  foreach(uint i, info; binInfo) {
    fout.write(info ~ "\t" ~ info);
    foreach(binID; firstBinID..(lastBinID+1)) {
      fout.write("\t", cfm[i + firstBinID][binID]);
    }
    fout.writeln;
  }
}

bool[] readConnectivity(string filename)
{
  bool[] gapInfo;
  foreach(line; File(filename, "r").byLine)
    gapInfo
      ~= line.to!string.strip.split("\t")[1] == "connected" ? true : false;
  return gapInfo;
}
