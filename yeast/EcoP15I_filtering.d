import std.stdio, std.string, std.conv, std.array, std.algorithm;


version(unittest) {}
 else {
   void main(string[] args)
   {
     auto DS1File = args[1];
     auto DS2File = args[2];
     auto SSFile = args[3];
     auto fragFile = args[4];
     auto EcoP15ICutLengthMin = args[5].to!uint;
     auto EcoP15ICutLengthMax = args[6].to!uint;
     auto outDS1File = args[7];
     auto outDS2File = args[8];
     auto outSSFile = args[9];

     auto frags = readFrag(fragFile);
     
     auto foutDS1 = File(outDS1File, "w");
     auto foutDS2 = File(outDS2File, "w");
     foreach(ref ds; readDS(DS1File, DS2File)) {
       if(ds.isValid(frags, EcoP15ICutLengthMin, EcoP15ICutLengthMax)) {
         foutDS1.writeln(ds.chrID(Pair.first), "\t",
                         ds.strand(Pair.first), "\t",
                         ds.pos(Pair.first), "\t",
                         ds.fragID(Pair.first));
         foutDS2.writeln(ds.chrID(Pair.second), "\t",
                         ds.strand(Pair.second), "\t",
                         ds.pos(Pair.second), "\t",
                         ds.fragID(Pair.second));
       }
     }

     auto foutSS = File(outSSFile, "w");
     foreach(ref ss; readSS(SSFile))
       if(ss.isValid(frags, EcoP15ICutLengthMin, EcoP15ICutLengthMax))
         foutSS.writeln(ss.chrID, "\t", ss.strand, "\t",
                        ss.pos, "\t", ss.fragID);
     
   }
 }


enum Pair {first, second}
enum Strand : char {plus='+', minus='-'}


DSread[] readDS(string file1, string file2)
{
  auto app = appender!(DSread[]);
  foreach(line; File(file1, "r").byLine) {
    if(line != "") {
      auto fields = line.to!string.split('\t');
      app.put(new DSread(fields[0].to!uint, fields[2].to!uint,
                         fields[1] == "+" ? Strand.plus : Strand.minus,
                         fields[3].to!uint));
    }
  }

  auto ds = app.data;
  auto i = 0;
  foreach(line; File(file2, "r").byLine) {
    if(line != "") {
      auto fields = line.to!string.split('\t');
      ds[i++].putSecond(fields[0].to!uint, fields[2].to!uint,
                        fields[1] == "+" ? Strand.plus : Strand.minus,
                        fields[3].to!uint);
    }
  }
  return ds;
}


SSread[] readSS(string file)
{
  auto app = appender!(SSread[]);
  foreach(line; File(file, "r").byLine) {
    if(line[0] != '@' && line != "") {
      auto fields = line.to!string.split('\t');
      app.put(new SSread(fields[0].to!uint, fields[2].to!uint,
                         fields[1] == "+" ? Strand.plus : Strand.minus,
                         fields[3].to!uint));
    }
  }
  return app.data;
}


class SSread
{
private:
  uint chrID_, pos_, fragID_;
  Strand strand_;
  
public:
  this(uint chrID, uint pos, Strand str, uint fragID) {
    chrID_=chrID; pos_=pos; strand_=str; fragID_=fragID;
  }

  @property {
    uint chrID() {return chrID_;}
    uint pos() {return pos_;}
    char strand() {return strand_;}
    uint fragID()  {return fragID_;}
  }

  bool isValid(Fragment[] frags,
               uint EcoP15ICutLengthMin, uint EcoP15ICutLengthMax) {
    if(frags[fragID].isValid) {
      auto cutLength = (strand == Strand.plus)
        ? frags[fragID].end - pos + 1 : pos - frags[fragID].start+ 1;

      return EcoP15ICutLengthMin <= cutLength
        && cutLength <= EcoP15ICutLengthMax;
    }
    else {
      return false;
    }
  }
}


class DSread
{
private:
  uint[2] chrID_, pos_, fragID_;;
  Strand[2] strand_;
  
public:
  this(uint chrID, uint pos, Strand str, uint fragID) {
    chrID_[0]=chrID; pos_[0]=pos; strand_[0]=str; fragID_[0]=fragID;
  }

  void putSecond(uint chrID, uint pos, Strand str, uint fragID) {
    chrID_[1]=chrID; pos_[1]=pos; strand_[1]=str; fragID_[1]=fragID;
  }
  
  @property {
    uint chrID(Pair p) { return p == Pair.first ? chrID_[0] : chrID_[1]; }
    uint pos(Pair p) { return p == Pair.first ? pos_[0] : pos_[1]; }
    char strand(Pair p) { return p == Pair.first ? strand_[0] : strand_[1]; }
    ulong fragID(Pair p)  { return p == Pair.first ? fragID_[0] : fragID_[1]; }
  }

  bool isValid(Fragment[] frags,
               uint EcoP15ICutLengthMin, uint EcoP15ICutLengthMax) {
    if(frags[fragID(Pair.first)].isValid && frags[fragID(Pair.second)].isValid) {
      auto cutLength1 = (strand(Pair.first) == Strand.plus)
        ? frags[fragID(Pair.first)].end - pos(Pair.first) + 1
        : pos(Pair.first) - frags[fragID(Pair.first)].start + 1;

      auto cutLength2 = (strand(Pair.second) == Strand.plus)
        ? frags[fragID(Pair.second)].end - pos(Pair.second) + 1
        : pos(Pair.second) - frags[fragID(Pair.second)].start + 1;

      return EcoP15ICutLengthMin <= cutLength1
        && cutLength1 <= EcoP15ICutLengthMax
        && EcoP15ICutLengthMin <= cutLength2
        && cutLength2 <= EcoP15ICutLengthMax;
    }
    else {
      return false;
    }
  }
}


class Fragment {
private:
  uint id_, chrID_, start_, end_;
  bool isValid_;
  
public:
  this(uint id, uint chrID, uint start, uint end, bool v) {
    id_=id; chrID_=chrID; start_=start; end_=end; isValid_=v;
  }

  @property {
    uint id() {return id_;}
    uint chrID() {return chrID_;}
    uint start() {return start_;}
    uint end() {return end_;}
    bool isValid() {return isValid_;}
  }
}


Fragment[] readFrag(string file)
{
  Fragment[] frags;  
  foreach(line; File(file, "r").byLine) {
    if(line != "") {
      auto fields = line.to!string.split("\t");
      frags ~= new Fragment(fields[0].to!uint, fields[1].to!uint,
                            fields[2].to!uint, fields[3].to!uint,
                            fields[4] == "valid" ? true : false);
    }
  }
  return frags;
}
