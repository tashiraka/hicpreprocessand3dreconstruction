#!/bio/bin/python
# -*- coding: utf-8 -*-
from Bio import SeqIO, Restriction, Seq, SeqUtils
from argparse import RawTextHelpFormatter
import numpy as np
import argparse


parser = argparse.ArgumentParser(
    description='''
  This command is tool to list up restriction cut sites from a genome.
  A site position is followed by 1-origin coordinate of the genome.
  A restriction fragment is represented as a closed interval, [start, end].
  The output format is as follows.
  <cut site ID>\t<chromosome ID>\t<start position>\t<end position>    
  ''',
    epilog="Author: Yuichi Motai",
    formatter_class=RawTextHelpFormatter)

parser.add_argument('-v', '--version', action='version', version='version 1.0')
parser.add_argument('genome', help='fasta file containing reference genome')
parser.add_argument('RE', help='restriction enzyme name')
parser.add_argument('out_file', help='output file')
args = parser.parse_args()


# This chromosome name parser is for sacCer3
def toID(chrName):
    if chrName == "chrI":
        return '0'
    elif chrName == "chrII":
        return '1'
    elif chrName == "chrIII":
        return '2'
    elif chrName == "chrIV":
        return '3'
    elif chrName == "chrV":
        return '4'
    elif chrName == "chrVI":
        return '5'
    elif chrName == "chrVII":
        return '6'
    elif chrName == "chrVIII":
        return '7'
    elif chrName == "chrIX":
        return '8'
    elif chrName == "chrX":
        return '9'
    elif chrName == "chrXI":
        return '10'
    elif chrName == "chrXII":
        return '11'
    elif chrName == "chrXIII":
        return '12'
    elif chrName == "chrXIV":
        return '13'
    elif chrName == "chrXV":
        return '14'
    elif chrName == "chrXVI":
        return '15'
    else:
        print "ERROR: invalid chromosome name, " + chrName
        quit()

re = getattr(Restriction, args.RE)
cut_position = re.search(Seq.Seq(re.site))[0]
right_len = cut_position - 1;
left_len = re.size - cut_position

fout = open(args.out_file, "w")
cut_site_id = 0
for chrom in SeqIO.parse(open(args.genome, 'r'), 'fasta') :
    if chrom.name != 'chrM':
        for rsite in re.search(chrom.seq) :
            fout.write(str(cut_site_id) + '\t' + toID(chrom.name) + '\t' + str(rsite - right_len) + '\t' + str(rsite + left_len) + '\n')
            cut_site_id += 1
