import std.stdio, std.string, std.conv, std.array, std.algorithm,
  std.parallelism, std.typecons, std.math, std.range;


void main(string[] args)
{
  auto coordsFile = args[1];
  auto filtrationFile = args[2];
  auto alphaValue = args[3].to!double;
  auto outFile = args[4];

  auto coords = readMat!(double)(coordsFile);
  auto edgeFiltration = readEdgeFiltration(filtrationFile);
  
  auto contactMat = createContactMat(coords, edgeFiltration, alphaValue);

  writeMat!(double)(contactMat, outFile);
}


void writeMat(T)(T[][] mat, string filename)
{
  auto fout = File(filename, "w");
  foreach(row; mat) {
    foreach(i, elem; row) {
         fout.write(elem);
         
         if(i < row.length - 1) {
           fout.write("\t");
         }
    }
    fout.writeln;
  }
}


double[][] createContactMat(double[][] coords,
                            Tuple!(double, double[], double[])[] filtrationEdge,
                            double alpha)
{
  auto contactMat = new double[][](coords.length, coords.length);
  foreach(ref cm; contactMat) {
    cm.fill(0);
  }

  foreach(fe; filtrationEdge) {
    if(fe[0] <= alpha) {
      ulong i = ulong.max;
      ulong j = ulong.max;
      foreach(k, coord; coords) {
        if(fe[1] == coord) i = k;
        if(fe[2] == coord) j = k;
      }
      
      if(i == ulong.max || j == ulong.max) {
        writeln(fe[1]);
        writeln(fe[2]);
        throw new Exception("ERROR: A coordinate is not found.");
      }

      auto d = euclideanDist(fe[1], fe[2]);
      contactMat[i][j] = d;
      contactMat[j][i] = d;
      //contactMat[i][j] = 1;
      //contactMat[j][i] = 1;
    }
  }

  return contactMat;
}


double euclideanDist(double[] u, double[] v)
{
  return reduce!((a, b) => a + (b[0] - b[1])^^2)(0.0, zip(u, v)).sqrt;
}



T[][] readMat(T)(string filename)
{
  auto matApp = appender!(T[][]);
  auto fin = File(filename, "r");
  foreach(line; fin.byLine) {
    if(line[0] != '#')
      matApp.put(line.to!string.strip.split("\t").map!(x => x.to!T).array);
  }
  return matApp.data;
}


Tuple!(double, double[], double[])[] readEdgeFiltration(string filename)
{
  Tuple!(double, double[], double[])[] filtration;
  auto fin = File(filename, "r");
  foreach(line; fin.byLine) {
    auto fields = line.to!string.strip.split("\t");
    if(fields.length == 3) {
      filtration ~= tuple(fields[0].to!double,
                          fields[1].split(" ").map!(x => x.to!double).array,
                          fields[2].split(" ").map!(x => x.to!double).array);
    }
  }
  return filtration;
}
