prefix=$1
RE1=$2
RE2=$3
EcoP15I_cut_length_min=$4
EcoP15I_cut_length_max=$5


path=~/Workspace/Hi-C_workbench/refined_hic_preprocess/yeast_visibility/
genome_path=/work/yuichi/S.cerevisiae/genome/
RE1_fragment=${genome_path}sacCer3.${RE1}_fragments
RE2_cut_site=${genome_path}sacCer3.${RE2}_cut_sites
filtered_fragment=${genome_path}sacCer3.${prefix}_${EcoP15I_cut_length_min}-${EcoP15I_cut_length_max}_fragments
mapped_DS1=${prefix}.DS1.mapped
mapped_DS2=${prefix}.DS2.mapped
mapped_SS=${prefix}.SS.mapped
filtered_DS1=${prefix}.DS1.filtered
filtered_DS2=${prefix}.DS2.filtered
filtered_SS=${prefix}.SS.filtered
contact_frequency_matrix=${prefix}.cfm
normalized_contact_frequency_matrix=${prefix}.cfm.norm
SS_visibility=${prefix}.SS.visivility
normalizedSS_visibility=${prefix}.SS.visivility.norm
visibility=${prefix}.visibility
normalized_visibility=${prefix}.visibility.norm



${path}RE2_filtering $RE1_fragment $RE2_cut_site $EcoP15I_cut_length_max $filtered_fragment

${path}EcoP15I_filtering $mapped_DS1 $mapped_DS2 $mapped_SS $filtered_fragment $EcoP15I_cut_length_min $EcoP15I_cut_length_max $filtered_DS1 $filtered_DS2 $filtered_SS

${path}convert_to_cfm $filtered_fragment $filtered_DS1 $filtered_DS2 $filtered_SS $contact_frequency_matrix

${path}convert_to_SS_cfv $filtered_fragment $filtered_SS $SS_visibility

${path}normalize_visibility $filtered_fragment $filtered_DS1 $filtered_DS2 $filtered_SS $normalized_contact_frequency_matrix $normalized_SS_visibility

${path}calc_visibility $contact_frequency_matrix $SS_visibility $visibility
${path}calc_visibility $normalized_contact_frequency_matrix $normalized_SS_visibility $normalized_visibility
