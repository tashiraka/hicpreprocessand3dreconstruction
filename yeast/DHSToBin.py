import sys

def toChr(id):
    if id == 0:
        return 'chrI'
    elif id == 1:
        return 'chrII'
    elif id == 2:
        return 'chrIII'
    elif id == 3:
        return 'chrIV'
    elif id == 4:
        return 'chrV'
    elif id == 5:
        return 'chrVI'
    elif id == 6:
        return 'chrVII'
    elif id == 7:
        return 'chrVIII'
    elif id == 8:
        return 'chrIX'
    elif id == 9:
        return 'chrX'
    elif id == 10:
        return 'chrXI'
    elif id == 11:
        return 'chrXII'
    elif id == 12:
        return 'chrXIII'
    elif id == 13:
        return 'chrXIV'
    elif id == 14:
        return 'chrXV'
    elif id == 15:
        return 'chrXVI'
    else:
        print 'NOOOOO'
        quit()

        
args = sys.argv

DHSFile = args[1]
chrInfoFile = args[2]
outFile = args[3]


DHSList = []
for line in open(DHSFile):
    DHSList.append(line.rstrip().split('\t'))

chrInfo = {}
for line in open(chrInfoFile):
    if line[0] != '#':
        fields = line.rstrip().split('\t')
        chrInfo[toChr(int(fields[0]))] = fields

# ASSUMPTION: The length of DHS <= 35
binnedDHSList = [0 for x in range(0, int(chrInfo['chrXVI'][-1]) + 1)]
binSizeList = [-1.0 for x in range(0, int(chrInfo['chrXVI'][-1]) + 1)]

for DHS in DHSList:
    chrName = DHS[0]
    start = int(DHS[1])
    end = int(DHS[2])
    startBin = int(start / int(chrInfo[chrName][2])) + int(chrInfo[chrName][3])
    endBin = int(end / int(chrInfo[chrName][2])) + int(chrInfo[chrName][3])
    
    for binID in range(startBin, endBin + 1):
        binSizeList[binID] = float(chrInfo[chrName][2])
        binnedDHSList[binID] += end - start + 1

for i, binnedDHS in enumerate(binnedDHSList):
    if binnedDHS != 0:        
        binnedDHSList[i] /= binSizeList[i]
        
fout = open(outFile, 'w')
for binnedDHS in binnedDHSList:
    fout.write(str(binnedDHS) + '\n')
