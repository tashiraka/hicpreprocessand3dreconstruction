import std.stdio, std.conv, std.string, std.algorithm, std.array, std.math, std.random;

version(unittest){}
 else{
   void main(string[] args)
   {
     auto fragFile = args[1];
     auto DS1File = args[2];
     auto DS2File = args[3];
     auto SSFile = args[4];
     // noiseRate * mean of contact frequencies
     auto noiseRate = args[5].to!double; 
     auto outMatrixFile = args[6];
     auto outVectorFile = args[7];

     auto frags = readFrag(fragFile);
     auto ODS = readDS(DS1File, DS2File, frags);
     auto OSS = readSS(SSFile, frags);

     // pertubate ODS and OSS
     int maxNoise = (ODS.mean * noiseRate).to!int;
     writeln("Max of ODS's noise: ", maxNoise);
     foreach(ref row; ODS) {
       foreach(ref elem; row) {
         elem += uniform(0, maxNoise);
       }
     }

     maxNoise = (OSS.mean * noiseRate).to!int;
     writeln("Max of OSS's noise: ", maxNoise);
     foreach(ref elem; OSS) {
       elem += uniform(0, maxNoise);
     }
     
     auto B = new double[](OSS.length);
     auto B0 = 1.0;
     auto SDS = new double[](OSS.length);
     auto SSS = new double[](OSS.length);
     auto dB = new double[](OSS.length);
     double[][] W = ODS.dup;
     auto initialMeanVisibility = W.visibility.mean;
     
     // init
     B.fill(1.0);
     B0 = 1.0;
     SDS.fill(0.0);
     SSS.fill(0.0);
     dB.fill(0.0);
     
     // iteration
     foreach(noUse; 0..1000) {
       SDS = W.visibility;
       SSS[] = OSS[] / (B[] * B0);
       dB[]= SDS[] + SSS[];
       dB[] /= dB.mean;
       dB = dB.map!(x => x<=0.0 ? 1.0 : x).array;

       auto flag = true;
       foreach(elem; dB) {
         if(abs(elem - 1.0) < 0.00001)
           flag = flag & true;
         else
           flag = false;
       }
       if(flag) {
         writeln(noUse);
         break;
       }
    
       foreach(i, noUse1; W) {
         foreach(j, noUse2; W[i]) {
           W[i][j] = W[i][j] / (dB[i] * dB[j]);
         }
       }
       B[] *= dB[];
       B0 = B.mean;
     }

     //TDS: W;
     //TSS: SSS[] = OSS[] / (B[] * B0);
     SSS[] = OSS[] / (B[] * B0);
     writeln("T: ", SSS.mean / W.mean);
     writeln("O: ", OSS.mean / ODS.mean);

     // scaling
     auto finalMeanVisibility = W.visibility.mean;
     foreach(i, noUse1; W) {
       foreach(j, noUse2; W[i]) {
         W[i][j] *= initialMeanVisibility / finalMeanVisibility;
       }
     }
     SSS[] *= initialMeanVisibility / finalMeanVisibility;
     
     auto fout = File(outMatrixFile, "w");
     foreach(row; W) {
       foreach(i, elem; row) {
         fout.write(elem);
         if(i < row.length - 1)
           fout.write("\t");
       }
       fout.write("\n");
     }

     fout = File(outVectorFile, "w");
     foreach(elem; SSS) {
       fout.writeln(elem);
     }
   }
 }


double[] visibility(double[][] mat) {
  return mat.map!(row => row.sum).array;
}


double mean(double[] arr) {
  auto farr = arr.filter!(x => x > 0.0);
  return farr.sum / farr.array.length;
}


double mean(double[][] mat) {
  return mat.map!(row => row.filter!(x => x > 0.0).sum).sum
    / mat.map!(row => row.filter!(x => x > 0.0).array.length).sum;
}


double[][] readDS(string file1, string file2, Fragment[] frags)
{
  auto freqMat = new double[][](frags.length, frags.length);
  foreach(ref row; freqMat)
    row.fill(0.0);
  
  auto fin1 = File(file1, "r");
  auto fin2 = File(file2, "r");
  
  while(true) {
    auto fields1 = fin1.readln.to!string.strip.split('\t');
    auto fields2 = fin2.readln.to!string.strip.split('\t');

    if(fields1.length != 4 || fields2.length !=4)
      break;

    freqMat[fields1[3].to!uint][fields2[3].to!uint]++;
    freqMat[fields2[3].to!uint][fields1[3].to!uint]++;
  }
  
  return freqMat;
}


double[] readSS(string file, Fragment[] frags)
{
  auto freqMat = new double[](frags.length);
  freqMat.fill(0.0);
    
  auto fin = File(file, "r");
  
  while(true) {
    auto fields = fin.readln.to!string.strip.split('\t');

    if(fields.length != 4)
      break;

    freqMat[fields[3].to!uint]++;
  }
  
  return freqMat;
}


class Fragment {
private:
  uint id_, chrID_, start_, end_;
  bool isValid_;
  
public:
  this(uint id, uint chrID, uint start, uint end, bool v) {
    id_=id; chrID_=chrID; start_=start; end_=end; isValid_=v;
  }

  @property {
    uint id() {return id_;}
    uint chrID() {return chrID_;}
    uint start() {return start_;}
    uint end() {return end_;}
    bool isValid() {return isValid_;}
  }
}


Fragment[] readFrag(string file)
{
  Fragment[] frags;  
  foreach(line; File(file, "r").byLine) {
    if(line != "") {
      auto fields = line.to!string.split("\t");
      frags ~= new Fragment(fields[0].to!uint, fields[1].to!uint,
                            fields[2].to!uint, fields[3].to!uint,
                            fields[4] == "valid" ? true : false);
    }
  }
  return frags;
}
