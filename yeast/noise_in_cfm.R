options(echo=TRUE) # if you want see commands in output file
args <- commandArgs(trailingOnly = TRUE)
connectedCfm <- args[1] 
noiseLevel <- args[2] # % of the standard deviation of contact frequency
outFile <- args[3]

cfm <- read.table(connectedCfm)
s <- sd(as.vector(cfm)[cfm > 0])
maxNoise <- s * noiseLevel
perturbedCFM <- sapply(cfm,  function(x) {return runif(1, min=0, max=maxNoise)})
write.table(perturbedCFM, outFile, row.names=F, col.names=F, sep="\t")
