import std.stdio, std.conv, std.string, std.algorithm, std.array, std.math;

version(unittest){}
 else{
   void main(string[] args)
   {
     auto fragFile = args[1];
     auto DS1File = args[2];
     auto DS2File = args[3];
     auto outMatrixFile = args[4];

     auto frags = readFrag(fragFile);
     auto cfm = readDS(DS1File, DS2File, frags);
  
     auto fout = File(outMatrixFile, "w");
     foreach(row; cfm) {
       foreach(i, elem; row) {
         fout.write(elem);
         if(i < row.length)
           fout.write("\t");
       }
       fout.write("\n");
     }
   }
 }




double[][] readDS(string file1, string file2, Fragment[] frags)
{
  auto freqMat = new double[][](frags.length, frags.length);
  foreach(ref row; freqMat)
    row.fill(0.0);
  
  auto fin1 = File(file1, "r");
  auto fin2 = File(file2, "r");
  
  while(true) {
    auto fields1 = fin1.readln.to!string.strip.split('\t');
    auto fields2 = fin2.readln.to!string.strip.split('\t');

    if(fields1.length != 4 || fields2.length !=4)
      break;

    freqMat[fields1[3].to!uint][fields2[3].to!uint]++;
    freqMat[fields2[3].to!uint][fields1[3].to!uint]++;
  }
  
  return freqMat;
}



class Fragment {
private:
  uint id_, chrID_, start_, end_;
  bool isValid_;
  
public:
  this(uint id, uint chrID, uint start, uint end, bool v) {
    id_=id; chrID_=chrID; start_=start; end_=end; isValid_=v;
  }

  @property {
    uint id() {return id_;}
    uint chrID() {return chrID_;}
    uint start() {return start_;}
    uint end() {return end_;}
    bool isValid() {return isValid_;}
  }
}


Fragment[] readFrag(string file)
{
  Fragment[] frags;  
  foreach(line; File(file, "r").byLine) {
    if(line != "") {
      auto fields = line.to!string.split("\t");
      frags ~= new Fragment(fields[0].to!uint, fields[1].to!uint,
                            fields[2].to!uint, fields[3].to!uint,
                            fields[4] == "valid" ? true : false);
    }
  }
  return frags;
}
