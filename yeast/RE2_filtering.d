import std.stdio, std.conv, std.string, std.array;

// output valid fragments
// threshold is b in EcoP15I cut site, [a, b];
version(unittest){}
 else{
   void main(string[] args)
   {
     auto RE1FragFile = args[1];
     auto RE2CutSiteFile = args[2];
     auto thresholdDistance = args[3].to!uint;
     auto outFile = args[4];

     auto frags = readFrag(RE1FragFile);
     auto cutSites = readCutSite(RE2CutSiteFile);
     frags.checkInclusion(cutSites);
     
     auto fout = File(outFile, "w");
     foreach(fragByChr; frags)
       foreach(frag; fragByChr)
         if(frag.isValid(cutSites, thresholdDistance))
           fout.writeln(frag.fragID, "\t", frag.chrID, "\t", frag.start,
                        "\t", frag.end, "\t", "valid");
         else
           fout.writeln(frag.fragID, "\t", frag.chrID, "\t", frag.start,
                        "\t", frag.end, "\t", "invalid");
   }
 }


void checkInclusion(Fragment[][16] frags, CutSite[][16] cutSites)
{
  uint firstCutSite=0, lastCutSite=0, prevFirst=0, j=0;

  foreach(i, ref fragByChr; frags) {
    firstCutSite=0; lastCutSite=0; prevFirst=0; j=0;

    foreach(ref frag; fragByChr) {
      j = prevFirst; // for overlap of overhang

      while(j < cutSites[i].length && cutSites[i][j].center < frag.start) {
        j++;
      }
      firstCutSite = j;
      prevFirst=j;
     
      while(j < cutSites[i].length && cutSites[i][j].center <= frag.end) {
        j++;
      }
      lastCutSite = j - 1;
     
      if(firstCutSite <= lastCutSite) {
        uint[] arr;
        foreach(x; firstCutSite..(lastCutSite + 1))
          arr ~= x;
        frag.cutSiteIDs(arr);
      }
    }
  }  
}


class Fragment
{
private:
  uint fragID_, chrID_, start_, end_;
  uint[] cutSiteIDs_;
  
public:
  this(uint id, uint chrID, uint start, uint end) {
    fragID_=id; chrID_=chrID; start_=start; end_=end;
  }

  @property {
    uint fragID() {return fragID_;}
    uint chrID() {return chrID_;}
    uint start() {return start_;}
    uint end() {return end_;}
    uint[] cutSiteIDs() {return cutSiteIDs_;}
    void cutSiteIDs(uint[] c) {cutSiteIDs_=c;}
  }

  bool isValid(CutSite[][16] cutSites, uint threshold) {
    // not have RE2 site or inside EcoP15I cut length
    if(cutSiteIDs_.length > 0
       && cutSites[chrID_][cutSiteIDs_[0]].start - start_ + 1 > threshold
       && end_ - cutSites[chrID_][cutSiteIDs_[$-1]].end  + 1 > threshold) 
      return true;
    else 
      return false;
  }
}


Fragment[][16] readFrag(string file)
{
  Fragment[][16] frags;  
  foreach(line; File(file, "r").byLine) {
    if(line != "") {
      auto fields = line.to!string.split("\t");
      frags[fields[1].to!uint] ~= new Fragment(fields[0].to!uint,
                                               fields[1].to!uint,
                                               fields[2].to!uint,
                                               fields[3].to!uint);
    }
  }
  return frags;
}


class CutSite
{
private:
  uint id_, chrID_, start_, end_;
  
public:
  this(uint id, uint chrID, uint start, uint end) {
    id_=id; chrID_=chrID; start_=start; end_=end;
  }

  @property {
    uint id() {return id_;}
    uint chrID() {return chrID_;}
    uint start() {return start_;}
    uint end() {return end_;}
    uint center() {return (start_ + end_) / 2;}
  }
}


CutSite[][16] readCutSite(string file)
{
  CutSite[][16] cutSites;  
  foreach(line; File(file, "r").byLine) {
    if(line != "") {
      auto fields = line.to!string.split("\t");
      cutSites[fields[1].to!uint] ~= new CutSite(fields[0].to!uint,
                                                 fields[1].to!uint,
                                                 fields[2].to!uint,
                                                 fields[3].to!uint);
    }
  }
  return cutSites;
}

