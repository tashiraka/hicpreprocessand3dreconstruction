import std.stdio, std.conv, std.string, std.algorithm, std.array;

void main(string[] args)
{
  auto filtrationFile = args[1];
  auto coordFile = args[2];
  auto connectivityFile = args[3];
  auto outFile = args[4];

  auto coords = readCoord(coordFile);
  auto missingInfo = readConnectivity(connectivityFile);

  auto coordsToBinID = coords.toBinID(missingInfo);

  auto fout = File(outFile, "w");
  foreach(line; File(filtrationFile).byLine) {
    auto fields = line.to!string.strip.split("\t");
    fout.write(fields[0], '\t');

    foreach(i; 1..fields.length) {
      auto xyz = fields[i].strip.split(' ');
      double[3] coord = [xyz[0].to!double, xyz[1].to!double, xyz[2].to!double];
      if(i < fields.length - 1) {
        fout.write(coordsToBinID[coord], '\t');
      }
      else {
        fout.writeln(coordsToBinID[coord]);
      }
    }
  }
}


ulong[double[3]] toBinID(double[3][] coords, bool[] missingInfo)
{
  ulong[double[3]] coordToBinID;
  auto coordID = 0;
  
  foreach(binID, isExist; missingInfo) {
    if(isExist) {
      coordToBinID[coords[coordID++]] = binID;
    }
  }

  return coordToBinID;
}


double[3][] readCoord(string filename)
{
  double[3][] coords;
  foreach(line; File(filename, "r").byLine) {
    auto fields = line.to!string.strip.split("\t");
    coords ~= [fields[0].to!double, fields[1].to!double, fields[2].to!double];
  }
  return coords;
}


bool[] readConnectivity(string filename)
{
  bool[] missingInfo;
  foreach(line; File(filename, "r").byLine)
    missingInfo
      ~= line.to!string.strip.split("\t")[1] == "connected" ? true : false;
  return missingInfo;
}


