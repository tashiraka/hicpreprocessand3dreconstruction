import std.stdio, std.conv, std.string, std.array, std.algorithm, std.range, std.math;


void main(string[] args)
{
  auto inFile = args[1];

  double[][] pos;
  foreach(line; File(inFile).byLine) {
    pos ~= line.strip.split("\t").map!(x => x.to!double).array;
  }

  auto max = 0.0;
  foreach(i, p0; pos) {
    writeln(i, " / ", pos.length);
    foreach(p1; pos) {
      auto d = reduce!((a, b) => a + (b[0] - b[1])^^2)(0.0, zip(p0, p1));
      if(max < d) {
        max = d;
      }
    }
  }

  writeln(max.sqrt);
}
