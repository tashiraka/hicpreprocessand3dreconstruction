#./checkConnection ../data/cfm.norm.binned ../data/cfm.connected ../data/bin.connectivity
#./CtoD ../data/cfm.connected ../data/distMat
#./DtoG ../data/distMat ../data/gramMat
#./GtoEig ../data/gramMat ../data/eigVal ../data/eigVec
./EigToX ../data/eigVal ../data/eigVec 5000 ../data/3Dcoord.xyz
./checkGOF ../data/eigVal ../data/3Dcoord.xyz ../data/distMat ../data/gof.txt
./xyz_to_pdb ../data/3Dcoord_tmp.xyz ../data/bin.connectivity ../data/sacCer3.binInfo ../data/3Dcoord.pdb
#./compute_filtration ../data/3Dcoord.xyz ../data/filtration
#./compute_persistent_homology ../data/filtration 0.1 ../data/persistence_intervals


#./EigToX ../data/eigVal ../data/eigVec 5000 ../data/3Dcoord_tmp.xyz
#./xyz_to_pdb ../data/3Dcoord_tmp.xyz ../data/bin.connectivity ../data/sacCer3.binInfo ../data/3Dcoord_tmp.pdb
#./compute_filtration ../data/3Dcoord_tmp.xyz ../data/filtration_tmp
#./compute_persistent_homology ../data/filtration_tmp 0.062 ../data/persistence_intervals_tmp


#./compute_persistent_homology ../data/filtration_alpha 1.239 ../data/persistence_intervals_alpha
#./recover_cfm_by_conv_func ../data/3Dcoord.xyz ../data/cfm.connected.recovered_by_conv_func
#./recover_cfm_by_simplicial_complex ../data/3Dcoord.xyz ../data/filtration 10000000 ../data/cfm.connected.recovered_by_simplicial_complex

#binInfo=../data/chrI.binInfo
#binInfo=../data/sacCer3.binInfo

#./cfm_to_cdt ../data/cfm.connected ../data/bin.connectivity $binInfo ../data/cfm.norm.binned.cdt
#./cfm_to_cdt ../data/cfm.connected.recovered_by_conv_func ../data/bin.connectivity $binInfo ../data/cfm.recovered_by_conv_func.cdt
#./cfm_to_cdt ../data/cfm.connected.recovered_by_simplicial_complex ../data/bin.connectivity $binInfo ../data/cfm.recovered_by_simplicial_complex.cdt

### chrII ###
#./checkConnection ../data/cfm.norm.binned.chrII ../data/cfm.connected.chrII ../data/bin.connectivity.chrII \
#    && ./CtoD ../data/cfm.connected.chrII ../data/distMat.chrII \
#    && ./DtoG ../data/distMat.chrII ../data/gramMat.chrII \
#    && ./GtoEig ../data/gramMat.chrII ../data/eigVal.chrII ../data/eigVec.chrII \
#    && ./EigToX ../data/eigVal.chrII ../data/eigVec.chrII 100000 ../data/3Dcoord.chrII.xyz \
#    && ./checkGOF ../data/eigVal.chrII ../data/3Dcoord.chrII.xyz ../data/distMat.chrII ../data/gof.chrII.txt \
#    && ./xyz_to_pdb ../data/3Dcoord.chrII.xyz ../data/bin.connectivity.chrII ../data/chrIntervals.chrII.txt ../data/3Dcoord.chrII.pdb \
#    && ./compute_filtration ../data/3Dcoord.chrII.xyz ../data/filtration.chrII \
#&& ./compute_persistent_homology ../data/filtration.chrII 0.1 ../data/persistence_intervals.chrII \

#./checkConnection ../data/cfm.norm.binned.3k ../data/cfm.connected.3k ../data/bin.connectivity.3k \
#    && ./CtoD ../data/cfm.connected.3k ../data/distMat.3k \
#    && ./DtoG ../data/distMat.3k ../data/gramMat.3k \
#    && ./GtoEig ../data/gramMat.3k ../data/eigVal.3k ../data/eigVec.3k \
#    && ./EigToX ../data/eigVal.3k ../data/eigVec.3k 100000 ../data/3Dcoord.3k.xyz \
#    && ./checkGOF ../data/eigVal.3k ../data/3Dcoord.3k.xyz ../data/distMat.3k ../data/gof.3k.txt \
#    && ./xyz_to_pdb ../data/3Dcoord.3k.xyz ../data/bin.connectivity.3k ../data/chrIntervals.3k.txt ../data/3Dcoord.3k.pd

#./checkConnection ../data/cfm.norm.binned.4k ../data/cfm.connected.4k ../data/bin.connectivity.4k \
#    && ./CtoD ../data/cfm.connected.4k ../data/distMat.4k \
#    && ./DtoG ../data/distMat.4k ../data/gramMat.4k \
#    && ./GtoEig ../data/gramMat.4k ../data/eigVal.4k ../data/eigVec.4k \
#    && ./EigToX ../data/eigVal.4k ../data/eigVec.4k 100000 ../data/3Dcoord.4k.xyz \
#    && ./checkGOF ../data/eigVal.4k ../data/3Dcoord.4k.xyz ../data/distMat.4k ../data/gof.4k.txt \
#    && ./xyz_to_pdb ../data/3Dcoord.4k.xyz ../data/bin.connectivity.4k ../data/chrIntervals.4k.txt ../data/3Dcoord.4k.pdb 

#&& ./compute_filtration ../data/3Dcoord.xyz ../data/filtration \
#./compute_persistent_homology ../data/filtration 1.239 ../data/persistence_intervals
#./compute_persistent_homology ../data/filtration_alpha 1.239 ../data/persistence_intervals_alpha
#./recover_cfm_by_conv_func ../data/3Dcoord.xyz ../data/cfm.connected.recovered_by_conv_func
#./recover_cfm_by_simplicial_complex ../data/3Dcoord.xyz ../data/filtration 10000000 ../data/cfm.connected.recovered_by_simplicial_complex

#binInfo=../data/chrI.binInfo
#binInfo=../data/sacCer3.binInfo

#./cfm_to_cdt ../data/cfm.connected ../data/bin.connectivity $binInfo ../data/cfm.norm.binned.cdt
#./cfm_to_cdt ../data/cfm.connected.recovered_by_conv_func ../data/bin.connectivity $binInfo ../data/cfm.recovered_by_conv_func.cdt
#./cfm_to_cdt ../data/cfm.connected.recovered_by_simplicial_complex ../data/bin.connectivity $binInfo ../data/cfm.recovered_by_simplicial_complex.cdt








### pack and unpack ###
#./compute_persistent_homology ../data/filtration_alpha 1.239 ../data/persistence_intervals_alpha
