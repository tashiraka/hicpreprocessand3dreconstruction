import sys

args = sys.argv

DHSFile = args[1]
PHFile = args[2]
outFile = args[3]


DHS = []
for line in open(DHSFile):
    DHS.append(float(line.rstrip()))

fout = open(outFile, 'w')
for line in open(PHFile):
    fields = line.rstrip().split('\t')
    fout.write(fields[0] + '\t' + fields[1] + '\t' + fields[2] + '\t')

    # additional
    localDHS = 0.0
    for i in range(3, len(fields)):
        localDHS += DHS[int(fields[i])]
    localDHS = localDHS / float(len(fields) - 3)

    # multiplicative
    #localDHS = 1.0
    #for i in range(3, len(fields)):
    #    localDHS *= DHS[int(fields[i])]
    #localDHS = localDHS ** (1 / float(len(fields) - 3))
        
    fout.write(str(localDHS) + '\n')
