import std.stdio, std.conv, std.string, std.algorithm, std.array;


version(unittest){}
 else{
   void main(string[] args)
   {
     auto cfmFile = args[1];
     auto fragFile = args[2];
     auto outCfmFile = args[3];
     auto outBinFile = args[4];

     uint[16] binSize = [5005, 4989, 5026, 4991, 4974, 5003, 5005, 4980,
                         4999, 5006, 5014, 4993, 4997, 4996, 5006, 4990];
     uint[16] binNum = [46, 163, 63, 307, 116, 54, 218, 113,
                        88, 149, 133, 216, 185, 157, 218, 190];
     
     auto cfm = readCfm(cfmFile);
     auto frags = readFrag(fragFile);
     auto fragToBin = frags.binningFrags(binSize, binNum);
     auto binnedCfm = cfm.binningCfm(fragToBin, binNum);
     
     auto fout = File(outCfmFile, "w");
     foreach(row; binnedCfm) {
       foreach(i, elem; row) {
         fout.write(elem);
         if(i < row.length) {
           fout.write("\t");
         }
       }
       fout.writeln;
     }

     fout = File(outBinFile, "w");
     foreach(i, elem; fragToBin) {
       fout.writeln(i, "\t", elem);
     }
   }
 }


uint[] binningFrags(Fragment[][16] frags, uint[16] binSize, uint[16] binNum)
{
  uint[] fragToBin;
  uint nextStart = 0;

  foreach(i, fragByChr; frags) {
    auto chrSize = fragByChr[$ - 1].end;
    foreach(frag; fragByChr) {
      fragToBin ~= (frag.center - 1) / binSize[frag.chrID] + nextStart;
    }
    nextStart += binNum[i];
  }

  return fragToBin;
}


double[][] binningCfm(double[][] cfm, uint[] fragToBin, uint[] binNum)
{
  auto totalBinNum = binNum.sum;
  auto binnedCfm = new double[][](totalBinNum, totalBinNum);

  foreach(ref row; binnedCfm)
    row.fill(0.0);

  foreach(i, row; cfm) {
    foreach(j, elem; row) {
      binnedCfm[fragToBin[i]][fragToBin[j]] += elem;
    }
  }
  return binnedCfm;
}


class Fragment {
private:
  uint id_, chrID_, start_, end_;
  
public:
  this(uint id, uint chrID, uint start, uint end) {
    id_=id; chrID_=chrID; start_=start; end_=end;
  }

  @property {
    uint id() {return id_;}
    uint chrID() {return chrID_;}
    uint start() {return start_;}
    uint end() {return end_;}
    uint center() {return (start_ + end_) / 2;}
  }
}


Fragment[][16] readFrag(string file)
{
  Fragment[][16] frags;
  
  foreach(line; File(file, "r").byLine) {
    if(line != "") {
      auto fields = line.to!string.split("\t");
      frags[fields[1].to!uint]
        ~= new Fragment(fields[0].to!uint, fields[1].to!uint,
                        fields[2].to!uint, fields[3].to!uint);
    }
  }
  return frags;
}


double[][] readCfm(string file)
{
  double[][] cfm;
  foreach(line; File(file, "r").byLine) 
    cfm ~= line.to!string.strip.split("\t").map!(x => x.to!double).array;
  return cfm;
}
