import std.stdio, std.string, std.conv;

version(unittest){}
 else{
   void main(string[] args)
   {
     auto sam1 = args[1];
     auto sam2 = args[2];
     auto DS1 = args[3];
     auto DS2 = args[4];
     auto SS = args[5];

     auto foutDS1 = File(DS1, "w");
     auto foutDS2 = File(DS2, "w");
     auto foutSS = File(SS, "w");

     auto fin1 = File(sam1, "r");
     auto fin2 = File(sam2, "r");
     string line1, line2, qname1, qname2, rname1, rname2;
     string[] fields1, fields2;
     uint flag1, flag2, MAPQ1, MAPQ2;
     bool mapped1, mapped2, uniq1, uniq2;

     while((line1 = fin1.readln) !is null
           && (line2 = fin2.readln) !is null
           && line1 != "\n" && line2 != "\n")
       {
       if(line1[0] == '@' && line2[0] == '@') {
         foutDS1.write(line1);
         foutDS2.write(line2);
         foutSS.write(line1);
       }
       else if((line1[0] == '@' && line2[0] != '@')
               || (line1[0] != '@' && line2[0] == '@')) {
         throw new Exception("ERROR: the numbers of header are diferent.");
       }
       else {
         fields1 = line1.to!string.strip.split("\t");
         fields2 = line2.to!string.strip.split("\t");
         qname1 = fields1[0];
         qname2 = fields2[0];
         rname1 = fields1[2];
         rname2 = fields2[2];

         if(qname1 != qname2)
           throw new Exception("ERROR: not paired.\n\n" ~ line1 ~ "\n" ~ line2);
         
         if(rname1 != "chrM" && rname2 != "chrM") {
           flag1 = fields1[1].to!uint;
           flag2 = fields2[1].to!uint;
           MAPQ1 = fields1[4].to!uint;
           MAPQ2 = fields2[4].to!uint;
           mapped1 = (flag1 & 0x0100) == 0x0 && (flag1 & 0x0004) == 0x0;
           mapped2 = (flag2 & 0x0100) == 0x0 && (flag2 & 0x0004) == 0x0;
           uniq1 = MAPQ1 >= 30;
           uniq2 = MAPQ2 >= 30;
           
           if(mapped1 && mapped2 && uniq1 && uniq2) {
             foutDS1.write(line1);
             foutDS2.write(line2);
           }
           else if(mapped1 && uniq1) {
             foutSS.write(line1);
           }
           else if(mapped2 && uniq2) {
             foutSS.write(line2);
           }
         }
       }
     }
   }
 }
