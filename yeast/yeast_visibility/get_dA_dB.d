import std.stdio, std.conv, std.string, std.array, std.math;


version(unittest) {}
 else {
   void main(string[] args)
   {     
     auto DS1File = args[1];
     auto DS2File = args[2];
     auto SSFile = args[3];
     auto fragFile = args[4];
     auto outFile = args[5];

     auto frags = readFrag(fragFile);
     auto fout = File(outFile, "w");
     foreach(ref read; readEntry(DS1File))
       fout.writeln(read.distance(frags));
     foreach(ref read; readEntry(DS2File))
       fout.writeln(read.distance(frags));
     foreach(ref read; readEntry(SSFile))
       fout.writeln(read.distance(frags));
   }
 }


class Fragment {
private:
  uint id_, chrID_, start_, end_;

public:
  this(uint id, uint chrID, uint start, uint end) {
    id_=id; chrID_=chrID; start_=start; end_=end;
  }

  @property {
    uint id() {return id_;}
    uint chrID() {return chrID_;}
    uint start() {return start_;}
    uint end() {return end_;}
  }
}


Fragment[] readFrag(string file)
{
  Fragment[] frags;  
  foreach(line; File(file, "r").byLine) {
    if(line != "") {
      auto fields = line.to!string.split("\t");
      frags ~= new Fragment(fields[0].to!uint, fields[1].to!uint,
                            fields[2].to!uint, fields[3].to!uint);
    }
  }
  return frags;
}


class Read
{
private:
  uint chrID_, pos_;
  char strand_;
  uint fragID_;
  
public:
  this(uint chrID, char str, uint pos, uint fragID) {
    chrID_=chrID; strand_=str; pos_=pos; fragID_=fragID;
  }

  @property {
    uint chrID() {return chrID_;}
    uint pos() {return pos_;}
    char strand() {return strand_;}
    uint fragID()  {return fragID_;}
  }

  uint distance(Fragment[] frags) {
    if(strand == '+') {
      if(frags[fragID].end < pos)
        throw new Exception("ERROR: frags[chrID][fragID].end < pos");
      return frags[fragID].end - pos + 1;
    }
    else {
      if(pos < frags[fragID].start)
        throw new Exception("ERROR: pos < frags[chrID][fragID].end");
      return pos - frags[fragID].start + 1;
    }
  }
}


Read[] readEntry(string file)
{
  auto app = appender!(Read[]);
  foreach(line; File(file, "r").byLine) {
    if(line != "") {
      auto fields = line.to!string.split('\t');
      app.put(new Read(fields[0].to!uint, fields[1].to!char,
                       fields[2].to!uint, fields[3].to!uint));
    }
  }
  return app.data;
}
