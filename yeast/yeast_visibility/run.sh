path=~/Workspace/Hi-C_workbench/refined_hic_preprocess/yeast_visibility/
genome_path=/work/yuichi/S.cerevisiae/genome/

#---------------------
# fragment
#---------------------
#python ${path}restrict_to_fragments.py ${genome_path}sacCer3.fa EcoRI ${genome_path}sacCer3.EcoRI_fragments
#python ${path}restrict_to_fragments.py ${genome_path}sacCer3.fa HindIII ${genome_path}sacCer3.HindIII_fragments


#python ${path}list_up_cut_site.py ${genome_path}sacCer3.fa MseI ${genome_path}sacCer3.MseI_cut_sites
#python ${path}list_up_cut_site.py ${genome_path}sacCer3.fa MspI ${genome_path}sacCer3.MspI_cut_sites


#---------------------
# read
#---------------------
#bowtie2 --very-sensitive -p 48 -x ${genome_path}bt2/sacCer3 -U E-Me-A_1.fastq -S E-Me-A_1.sam >E-Me-A_1.bowtie2.log 2>&1 
#bowtie2 --very-sensitive -p 48 -x ${genome_path}bt2/sacCer3 -U E-Me-B_1.fastq -S E-Me-B_1.sam >E-Me-B_1.bowtie2.log 2>&1
#bowtie2 --very-sensitive -p 48 -x ${genome_path}bt2/sacCer3 -U E-Mp-A_1.fastq -S E-Mp-A_1.sam >E-Mp-A_1.bowtie2.log 2>&1
#bowtie2 --very-sensitive -p 48 -x ${genome_path}bt2/sacCer3 -U E-Mp-B_1.fastq -S E-Mp-B_1.sam >E-Mp-B_1.bowtie2.log 2>&1
#bowtie2 --very-sensitive -p 48 -x ${genome_path}bt2/sacCer3 -U H-Me-A_1.fastq -S H-Me-A_1.sam >H-Me-A_1.bowtie2.log 2>&1
#bowtie2 --very-sensitive -p 48 -x ${genome_path}bt2/sacCer3 -U H-Me-B_1.fastq -S H-Me-B_1.sam >H-Me-B_1.bowtie2.log 2>&1
#bowtie2 --very-sensitive -p 48 -x ${genome_path}bt2/sacCer3 -U H-Mp-A_1.fastq -S H-Mp-A_1.sam >H-Mp-A_1.bowtie2.log 2>&1
#bowtie2 --very-sensitive -p 48 -x ${genome_path}bt2/sacCer3 -U H-Mp-B_1.fastq -S H-Mp-B_1.sam >H-Mp-B_1.bowtie2.log 2>&1
#bowtie2 --very-sensitive -p 48 -x ${genome_path}bt2/sacCer3 -U E-Me-A_2.fastq -S E-Me-A_2.sam >E-Me-A_2.bowtie2.log 2>&1
#bowtie2 --very-sensitive -p 48 -x ${genome_path}bt2/sacCer3 -U E-Me-B_2.fastq -S E-Me-B_2.sam >E-Me-B_2.bowtie2.log 2>&1
#bowtie2 --very-sensitive -p 48 -x ${genome_path}bt2/sacCer3 -U E-Mp-A_2.fastq -S E-Mp-A_2.sam >E-Mp-A_2.bowtie2.log 2>&1
#bowtie2 --very-sensitive -p 48 -x ${genome_path}bt2/sacCer3 -U E-Mp-B_2.fastq -S E-Mp-B_2.sam >E-Mp-B_2.bowtie2.log 2>&1
#bowtie2 --very-sensitive -p 48 -x ${genome_path}bt2/sacCer3 -U H-Me-A_2.fastq -S H-Me-A_2.sam >H-Me-A_2.bowtie2.log 2>&1
#bowtie2 --very-sensitive -p 48 -x ${genome_path}bt2/sacCer3 -U H-Me-B_2.fastq -S H-Me-B_2.sam >H-Me-B_2.bowtie2.log 2>&1
#bowtie2 --very-sensitive -p 48 -x ${genome_path}bt2/sacCer3 -U H-Mp-A_2.fastq -S H-Mp-A_2.sam >H-Mp-A_2.bowtie2.log 2>&1
#bowtie2 --very-sensitive -p 48 -x ${genome_path}bt2/sacCer3 -U H-Mp-B_2.fastq -S H-Mp-B_2.sam >H-Mp-B_2.bowtie2.log 2>&1
#bowtie2 --very-sensitive -p 48 -x ${genome_path}bt2/sacCer3 -U U-H-Me-A_1.fastq -S U-H-Me-A_1.sam >U-H-Me-A_1.bowtie2.log 2>&1
#bowtie2 --very-sensitive -p 48 -x ${genome_path}bt2/sacCer3 -U U-H-Me-B_1.fastq -S U-H-Me-B_1.sam >U-H-Me-B_1.bowtie2.log 2>&1
#bowtie2 --very-sensitive -p 48 -x ${genome_path}bt2/sacCer3 -U U-H-Me-A_2.fastq -S U-H-Me-A_2.sam >U-H-Me-A_2.bowtie2.log 2>&1
#bowtie2 --very-sensitive -p 48 -x ${genome_path}bt2/sacCer3 -U U-H-Me-B_2.fastq -S U-H-Me-B_2.sam >U-H-Me-B_2.bowtie2.log 2>&1


#samtools view -Shb E-Me-A_1.sam >tmp1.bam; samtools sort -n tmp1.bam >tmp1.sort; rm tmp1.bam; samtools view -h tmp1.sort.bam >E-Me-A_1.sort.sam; rm tmp1.sort.bam;
#samtools view -Shb E-Me-B_1.sam >tmp2.bam; samtools sort -n tmp2.bam >tmp2.sort; rm tmp2.bam; samtools view -h tmp2.sort.bam >E-Me-B_1.sort.sam; rm tmp2.sort.bam;
#samtools view -Shb E-Mp-A_1.sam >tmp3.bam; samtools sort -n tmp3.bam >tmp3.sort; rm tmp3.bam; samtools view -h tmp3.sort.bam >E-Mp-A_1.sort.sam; rm tmp3.sort.bam;
#samtools view -Shb E-Mp-B_1.sam >tmp4.bam; samtools sort -n tmp4.bam >tmp4.sort; rm tmp4.bam; samtools view -h tmp4.sort.bam >E-Mp-B_1.sort.sam; rm tmp4.sort.bam;
#samtools view -Shb H-Me-A_1.sam >tmp5.bam; samtools sort -n tmp5.bam >tmp5.sort; rm tmp5.bam; samtools view -h tmp5.sort.bam >H-Me-A_1.sort.sam; rm tmp5.sort.bam;
#samtools view -Shb H-Me-B_1.sam >tmp6.bam; samtools sort -n tmp6.bam >tmp6.sort; rm tmp6.bam; samtools view -h tmp6.sort.bam >H-Me-B_1.sort.sam; rm tmp6.sort.bam;
#samtools view -Shb H-Mp-A_1.sam >tmp7.bam; samtools sort -n tmp7.bam >tmp7.sort; rm tmp7.bam; samtools view -h tmp7.sort.bam >H-Mp-A_1.sort.sam; rm tmp7.sort.bam;
#samtools view -Shb H-Mp-B_1.sam >tmp8.bam; samtools sort -n tmp8.bam >tmp8.sort; rm tmp8.bam; samtools view -h tmp8.sort.bam >H-Mp-B_1.sort.sam; rm tmp8.sort.bam;
#samtools view -Shb E-Me-A_2.sam >tmp9.bam; samtools sort -n tmp9.bam >tmp9.sort; rm tmp9.bam; samtools view -h tmp9.sort.bam >E-Me-A_2.sort.sam; rm tmp9.sort.bam;
#samtools view -Shb E-Me-B_2.sam >tmp10.bam; samtools sort -n tmp10.bam >tmp10.sort; rm tmp10.bam; samtools view -h tmp10.sort.bam >E-Me-A_2.sort.sam; rm tmp10.sort.bam;
#samtools view -Shb E-Mp-A_2.sam >tmp11.bam; samtools sort -n tmp11.bam >tmp11.sort; rm tmp11.bam; samtools view -h tmp11.sort.bam >E-Mp-B_2.sort.sam; rm tmp11.sort.bam;
#samtools view -Shb E-Mp-B_2.sam >tmp12.bam; samtools sort -n tmp12.bam >tmp12.sort; rm tmp12.bam; samtools view -h tmp12.sort.bam >E-Mp-A_2.sort.sam; rm tmp12.sort.bam;
#samtools view -Shb H-Me-A_2.sam >tmp13.bam; samtools sort -n tmp13.bam >tmp13.sort; rm tmp13.bam; samtools view -h tmp13.sort.bam >H-Me-B_2.sort.sam; rm tmp13.sort.bam;
#samtools view -Shb H-Me-B_2.sam >tmp14.bam; samtools sort -n tmp14.bam >tmp14.sort; rm tmp14.bam; samtools view -h tmp14.sort.bam >H-Me-A_2.sort.sam; rm tmp14.sort.bam;
#samtools view -Shb H-Mp-A_2.sam >tmp15.bam; samtools sort -n tmp15.bam >tmp15.sort; rm tmp15.bam; samtools view -h tmp15.sort.bam >H-Mp-B_2.sort.sam; rm tmp15.sort.bam;
#samtools view -Shb H-Mp-B_2.sam >tmp16.bam; samtools sort -n tmp16.bam >tmp16.sort; rm tmp16.bam; samtools view -h tmp16.sort.bam >H-Mp-A_2.sort.sam; rm tmp16.sort.bam;
#samtools view -Shb U-H-Me-A_1.sam >tmp17.bam; samtools sort -n tmp17.bam >tmp17.sort; rm tmp17.bam; samtools view -h tmp17.sort.bam >U-H-Me-A_1.sort.sam; rm tmp17.sort.bam;
#samtools view -Shb U-H-Me-B_1.sam >tmp18.bam; samtools sort -n tmp18.bam >tmp18.sort; rm tmp18.bam; samtools view -h tmp18.sort.bam >U-H-Me-B_1.sort.sam; rm tmp18.sort.bam;
#samtools view -Shb U-H-Me-A_2.sam >tmp19.bam; samtools sort -n tmp19.bam >tmp19.sort; rm tmp19.bam; samtools view -h tmp19.sort.bam >U-H-Me-A_2.sort.sam; rm tmp19.sort.bam;
#samtools view -Shb U-H-Me-B_2.sam >tmp20.bam; samtools sort -n tmp10.bam >tmp20.sort; rm tmp20.bam; samtools view -h tmp20.sort.bam >U-H-Me-B_2.sort.sam; rm tmp20.sort.bam;


#${path}divide_DS_SS E-Me-A_1.sort.sam E-Me-A_2.sort.sam E-Me-A_1.DS.sam E-Me-A_2.DS.sam E-Me-A.SS.sam
#${path}divide_DS_SS E-Me-B_1.sort.sam E-Me-B_2.sort.sam E-Me-B_1.DS.sam E-Me-B_2.DS.sam E-Me-B.SS.sam
#${path}divide_DS_SS E-Mp-A_1.sort.sam E-Mp-A_2.sort.sam E-Mp-A_1.DS.sam E-Mp-A_2.DS.sam E-Mp-A.SS.sam
#${path}divide_DS_SS E-Mp-B_1.sort.sam E-Mp-B_2.sort.sam E-Mp-B_1.DS.sam E-Mp-B_2.DS.sam E-Mp-B.SS.sam
#${path}divide_DS_SS H-Me-A_1.sort.sam H-Me-A_2.sort.sam H-Me-A_1.DS.sam H-Me-A_2.DS.sam H-Me-A.SS.sam
#${path}divide_DS_SS H-Me-B_1.sort.sam H-Me-B_2.sort.sam H-Me-B_1.DS.sam H-Me-B_2.DS.sam H-Me-B.SS.sam
#${path}divide_DS_SS H-Mp-A_1.sort.sam H-Mp-A_2.sort.sam H-Mp-A_1.DS.sam H-Mp-A_2.DS.sam H-Mp-A.SS.sam
#${path}divide_DS_SS H-Mp-B_1.sort.sam H-Mp-B_2.sort.sam H-Mp-B_1.DS.sam H-Mp-B_2.DS.sam H-Mp-B.SS.sam
#${path}divide_DS_SS U-H-Me-A_1.sort.sam U-H-Me-A_2.sort.sam H-Me-A_1.DS.sam U-H-Me-A_2.DS.sam U-H-Me-A.SS.sam
#${path}divide_DS_SS U-H-Me-B_1.sort.sam U-H-Me-B_2.sort.sam H-Me-B_1.DS.sam U-H-Me-B_2.DS.sam U-H-Me-B.SS.sam


#python ${path}decide_read_pos.py E-Me-A_1.DS.sam E-Me-A_2.DS.sam E-Me-A.SS.sam E-Me-A_1.DS E-Me-A_2.DS E-Me-A.SS
#python ${path}decide_read_pos.py E-Me-B_1.DS.sam E-Me-B_2.DS.sam E-Me-B.SS.sam E-Me-B_1.DS E-Me-B_2.DS E-Me-B.SS
#python ${path}decide_read_pos.py E-Mp-A_1.DS.sam E-Mp-A_2.DS.sam E-Mp-A.SS.sam E-Mp-A_1.DS E-Mp-A_2.DS E-Mp-A.SS
#python ${path}decide_read_pos.py E-Mp-B_1.DS.sam E-Mp-B_2.DS.sam E-Mp-B.SS.sam E-Mp-B_1.DS E-Mp-B_2.DS E-Mp-B.SS
#python ${path}decide_read_pos.py H-Me-A_1.DS.sam H-Me-A_2.DS.sam H-Me-A.SS.sam H-Me-A_1.DS H-Me-A_2.DS H-Me-A.SS
#python ${path}decide_read_pos.py H-Me-B_1.DS.sam H-Me-B_2.DS.sam H-Me-B.SS.sam H-Me-B_1.DS H-Me-B_2.DS H-Me-B.SS
#python ${path}decide_read_pos.py H-Mp-A_1.DS.sam H-Mp-A_2.DS.sam H-Mp-A.SS.sam H-Mp-A_1.DS H-Mp-A_2.DS H-Mp-A.SS
#python ${path}decide_read_pos.py H-Mp-B_1.DS.sam H-Mp-B_2.DS.sam H-Mp-B.SS.sam H-Mp-B_1.DS H-Mp-B_2.DS H-Mp-B.SS
#python ${path}decide_read_pos.py U-H-Me-A_1.DS.sam U-H-Me-A_2.DS.sam U-H-Me-A.SS.sam U-H-Me-A_1.DS U-H-Me-A_2.DS U-H-Me-A.SS
#python ${path}decide_read_pos.py U-H-Me-B_1.DS.sam U-H-Me-B_2.DS.sam U-H-Me-B.SS.sam U-H-Me-B_1.DS U-H-Me-B_2.DS U-H-Me-B.SS


#${path}map_to_fragment E-Me-A_1.DS E-Me-A_2.DS E-Me-A.SS ${genome_path}sacCer3.EcoRI_fragments E-Me-A_1.DS.mapped E-Me-A_2.DS.mapped E-Me-A.SS.mapped
#${path}map_to_fragment E-Me-B_1.DS E-Me-B_2.DS E-Me-B.SS ${genome_path}sacCer3.EcoRI_fragments E-Me-B_1.DS.mapped E-Me-B_2.DS.mapped E-Me-B.SS.mapped
#${path}map_to_fragment E-Mp-A_1.DS E-Mp-A_2.DS E-Mp-A.SS ${genome_path}sacCer3.EcoRI_fragments E-Mp-A_1.DS.mapped E-Mp-A_2.DS.mapped E-Mp-A.SS.mapped
#${path}map_to_fragment E-Mp-B_1.DS E-Mp-B_2.DS E-Mp-B.SS ${genome_path}sacCer3.EcoRI_fragments E-Mp-B_1.DS.mapped E-Mp-B_2.DS.mapped E-Mp-B.SS.mapped
#${path}map_to_fragment H-Me-A_1.DS H-Me-A_2.DS H-Me-A.SS ${genome_path}sacCer3.HindIII_fragments H-Me-A_1.DS.mapped H-Me-A_2.DS.mapped H-Me-A.SS.mapped
#${path}map_to_fragment H-Me-B_1.DS H-Me-B_2.DS H-Me-B.SS ${genome_path}sacCer3.HindIII_fragments H-Me-B_1.DS.mapped H-Me-B_2.DS.mapped H-Me-B.SS.mapped
#${path}map_to_fragment H-Mp-A_1.DS H-Mp-A_2.DS H-Mp-A.SS ${genome_path}sacCer3.HindIII_fragments H-Mp-A_1.DS.mapped H-Mp-A_2.DS.mapped H-Mp-A.SS.mapped
#${path}map_to_fragment H-Mp-B_1.DS H-Mp-B_2.DS H-Mp-B.SS ${genome_path}sacCer3.HindIII_fragments H-Mp-B_1.DS.mapped H-Mp-B_2.DS.mapped H-Mp-B.SS.mapped
#${path}map_to_fragment U-H-Me-A_1.DS U-H-Me-A_2.DS U-H-Me-A.SS ${genome_path}sacCer3.HindIII_fragments U-H-Me-A_1.DS.mapped U-H-Me-A_2.DS.mapped U-H-Me-A.SS.mapped
#${path}map_to_fragment U-H-Me-B_1.DS U-H-Me-B_2.DS U-H-Me-B.SS ${genome_path}sacCer3.HindIII_fragments U-H-Me-B_1.DS.mapped U-H-Me-B_2.DS.mapped U-H-Me-B.SS.mapped


#${path}get_dA_dB E-Me-A_1.DS.mapped E-Me-A_2.DS.mapped E-Me-A.SS.mapped ${genome_path}sacCer3.EcoRI_fragments E-Me-A.dA_dB
#${path}get_dA_dB E-Me-B_1.DS.mapped E-Me-B_2.DS.mapped E-Me-B.SS.mapped ${genome_path}sacCer3.EcoRI_fragments E-Me-B.dA_dB
#${path}get_dA_dB E-Mp-A_1.DS.mapped E-Mp-A_2.DS.mapped E-Mp-A.SS.mapped ${genome_path}sacCer3.EcoRI_fragments E-Mp-A.dA_dB
#${path}get_dA_dB E-Mp-B_1.DS.mapped E-Mp-B_2.DS.mapped E-Mp-B.SS.mapped ${genome_path}sacCer3.EcoRI_fragments E-Mp-B.dA_dB
#${path}get_dA_dB H-Me-A_1.DS.mapped H-Me-A_2.DS.mapped H-Me-A.SS.mapped ${genome_path}sacCer3.HindIII_fragments H-Me-A.dA_dB
#${path}get_dA_dB H-Me-B_1.DS.mapped H-Me-B_2.DS.mapped H-Me-B.SS.mapped ${genome_path}sacCer3.HindIII_fragments H-Me-B.dA_dB
#${path}get_dA_dB H-Mp-A_1.DS.mapped H-Mp-A_2.DS.mapped H-Mp-A.SS.mapped ${genome_path}sacCer3.HindIII_fragments H-Mp-A.dA_dB
#${path}get_dA_dB H-Mp-B_1.DS.mapped H-Mp-B_2.DS.mapped H-Mp-B.SS.mapped ${genome_path}sacCer3.HindIII_fragments H-Mp-B.dA_dB
#${path}get_dA_dB U-H-Me-A_1.DS.mapped U-H-Me-A_2.DS.mapped U-H-Me-A.SS.mapped ${genome_path}sacCer3.HindIII_fragments U-H-Me-A.dA_dB
#${path}get_dA_dB U-H-Me-B_1.DS.mapped U-H-Me-B_2.DS.mapped U-H-Me-B.SS.mapped ${genome_path}sacCer3.HindIII_fragments U-H-Me-B.dA_dB


# (check EcoP15I cut sites from outputs of get_dA_dB)
EcoP15I_min1=25
EcoP15I_max1=27
EcoP15I_min2=26
EcoP15I_max2=28

#---------------------
# fragment 
#---------------------
#${path}RE2_filtering ${genome_path}sacCer3.EcoRI_fragments ${genome_path}sacCer3.MseI_cut_sites $EcoP15I_max1 ${genome_path}sacCer3.E-Me_25-27_fragments
#${path}RE2_filtering ${genome_path}sacCer3.EcoRI_fragments ${genome_path}sacCer3.MspI_cut_sites $EcoP15I_max1 ${genome_path}sacCer3.E-Mp_25-27_fragments
#${path}RE2_filtering ${genome_path}sacCer3.HindIII_fragments ${genome_path}sacCer3.MseI_cut_sites $EcoP15I_max1 ${genome_path}sacCer3.H-Me_25-27_fragments
#${path}RE2_filtering ${genome_path}sacCer3.HindIII_fragments ${genome_path}sacCer3.MspI_cut_sites $EcoP15I_max1 ${genome_path}sacCer3.H-Mp_25-27_fragments
#${path}RE2_filtering ${genome_path}sacCer3.HindIII_fragments ${genome_path}sacCer3.MseI_cut_sites $EcoP15I_max2 ${genome_path}sacCer3.H-Me_26-28_fragments
#${path}RE2_filtering ${genome_path}sacCer3.HindIII_fragments ${genome_path}sacCer3.MspI_cut_sites $EcoP15I_max2 ${genome_path}sacCer3.H-Mp_26-28_fragments


#---------------------
# read
#---------------------
#${path}EcoP15I_filtering E-Me-A_1.DS.mapped E-Me-A_2.DS.mapped E-Me-A.SS.mapped ${genome_path}sacCer3.E-Me_25-27_fragments 25 27 E-Me-A_1.DS.valid E-Me-A_2.DS.valid E-Me-A.SS.valid
#${path}EcoP15I_filtering E-Me-B_1.DS.mapped E-Me-B_2.DS.mapped E-Me-B.SS.mapped ${genome_path}sacCer3.E-Me_25-27_fragments 25 27 E-Me-B_1.DS.valid E-Me-B_2.DS.valid E-Me-B.SS.valid
#${path}EcoP15I_filtering E-Mp-A_1.DS.mapped E-Mp-A_2.DS.mapped E-Mp-A.SS.mapped ${genome_path}sacCer3.E-Mp_25-27_fragments 25 27 E-Mp-A_1.DS.valid E-Mp-A_2.DS.valid E-Mp-A.SS.valid
#${path}EcoP15I_filtering E-Mp-B_1.DS.mapped E-Mp-B_2.DS.mapped E-Mp-B.SS.mapped ${genome_path}sacCer3.E-Mp_25-27_fragments 25 27 E-Mp-B_1.DS.valid E-Mp-B_2.DS.valid E-Mp-B.SS.valid
#${path}EcoP15I_filtering H-Me-A_1.DS.mapped H-Me-A_2.DS.mapped H-Me-A.SS.mapped ${genome_path}sacCer3.H-Me_25-27_fragments 25 27 H-Me-A_1.DS.valid H-Me-A_2.DS.valid H-Me-A.SS.valid
#${path}EcoP15I_filtering H-Me-B_1.DS.mapped H-Me-B_2.DS.mapped H-Me-B.SS.mapped ${genome_path}sacCer3.H-Me_26-28_fragments 26 28 H-Me-B_1.DS.valid H-Me-B_2.DS.valid H-Me-B.SS.valid
#${path}EcoP15I_filtering H-Mp-A_1.DS.mapped H-Mp-A_2.DS.mapped H-Mp-A.SS.mapped ${genome_path}sacCer3.H-Mp_25-27_fragments 25 27 H-Mp-A_1.DS.valid H-Mp-A_2.DS.valid H-Mp-A.SS.valid
#${path}EcoP15I_filtering H-Mp-B_1.DS.mapped H-Mp-B_2.DS.mapped H-Mp-B.SS.mapped ${genome_path}sacCer3.H-Mp_26-28_fragments 26 28 H-Mp-B_1.DS.valid H-Mp-B_2.DS.valid H-Mp-B.SS.valid
#${path}EcoP15I_filtering U-H-Me-A_1.DS.mapped U-H-Me-A_2.DS.mapped U-H-Me-A.SS.mapped ${genome_path}sacCer3.H-Me_25-27_fragments 25 27 U-H-Me-A_1.DS.valid U-H-Me-A_2.DS.valid U-H-Me-A.SS.valid
#${path}EcoP15I_filtering U-H-Me-B_1.DS.mapped U-H-Me-B_2.DS.mapped U-H-Me-B.SS.mapped ${genome_path}sacCer3.H-Me_26-28_fragments 26 28 U-H-Me-B_1.DS.valid U-H-Me-B_2.DS.valid U-H-Me-B.SS.valid

#${path}convert_to_cfm ${genome_path}sacCer3.E-Me_25-27_fragments E-Me-A_1.DS.valid E-Me-A_2.DS.valid E-Me-A.cfm.no_norm
#${path}convert_to_cfm ${genome_path}sacCer3.E-Me_25-27_fragments E-Me-B_1.DS.valid E-Me-B_2.DS.valid E-Me-B.cfm.no_norm
#${path}convert_to_cfm ${genome_path}sacCer3.E-Mp_25-27_fragments E-Mp-A_1.DS.valid E-Mp-A_2.DS.valid E-Mp-A.cfm.no_norm
#${path}convert_to_cfm ${genome_path}sacCer3.E-Mp_25-27_fragments E-Mp-B_1.DS.valid E-Mp-B_2.DS.valid E-Mp-B.cfm.no_norm
#${path}convert_to_cfm ${genome_path}sacCer3.H-Me_25-27_fragments H-Me-A_1.DS.valid H-Me-A_2.DS.valid H-Me-A.cfm.no_norm
#${path}convert_to_cfm ${genome_path}sacCer3.H-Me_26-28_fragments H-Me-B_1.DS.valid H-Me-B_2.DS.valid H-Me-B.cfm.no_norm
#${path}convert_to_cfm ${genome_path}sacCer3.H-Mp_25-27_fragments H-Mp-A_1.DS.valid H-Mp-A_2.DS.valid H-Mp-A.cfm.no_norm
#${path}convert_to_cfm ${genome_path}sacCer3.H-Mp_26-28_fragments H-Mp-B_1.DS.valid H-Mp-B_2.DS.valid H-Mp-B.cfm.no_norm
#${path}convert_to_cfm ${genome_path}sacCer3.H-Me_25-27_fragments U-H-Me-A_1.DS.valid U-H-Me-A_2.DS.valid U-H-Me-A.cfm.no_norm
#${path}convert_to_cfm ${genome_path}sacCer3.H-Me_26-28_fragments U-H-Me-B_1.DS.valid U-H-Me-B_2.DS.valid U-H-Me-B.cfm.no_norm

#${path}convert_to_SS_cfv ${genome_path}sacCer3.E-Me_25-27_fragments E-Me-A.SS.valid E-Me-A.cfv.no_norm
#${path}convert_to_SS_cfv ${genome_path}sacCer3.E-Me_25-27_fragments E-Me-B.SS.valid E-Me-B.cfv.no_norm
#${path}convert_to_SS_cfv ${genome_path}sacCer3.E-Mp_25-27_fragments E-Mp-A.SS.valid E-Mp-A.cfv.no_norm
#${path}convert_to_SS_cfv ${genome_path}sacCer3.E-Mp_25-27_fragments E-Mp-B.SS.valid E-Mp-B.cfv.no_norm
#${path}convert_to_SS_cfv ${genome_path}sacCer3.H-Me_25-27_fragments H-Me-A.SS.valid H-Me-A.cfv.no_norm
#${path}convert_to_SS_cfv ${genome_path}sacCer3.H-Me_26-28_fragments H-Me-B.SS.valid H-Me-B.cfv.no_norm
#${path}convert_to_SS_cfv ${genome_path}sacCer3.H-Mp_25-27_fragments H-Mp-A.SS.valid H-Mp-A.cfv.no_norm
#${path}convert_to_SS_cfv ${genome_path}sacCer3.H-Mp_26-28_fragments H-Mp-B.SS.valid H-Mp-B.cfv.no_norm
#${path}convert_to_SS_cfv ${genome_path}sacCer3.H-Me_25-27_fragments U-H-Me-A.SS.valid U-H-Me-A.cfv.no_norm
#${path}convert_to_SS_cfv ${genome_path}sacCer3.H-Me_26-28_fragments U-H-Me-B.SS.valid U-H-Me-B.cfv.no_norm

#${path}normalize_visibility ${genome_path}sacCer3.E-Me_25-27_fragments E-Me-A_1.DS.valid E-Me-A_2.DS.valid E-Me-A.SS.valid E-Me-A.cfm.norm E-Me-A.cfv.norm
#${path}normalize_visibility ${genome_path}sacCer3.E-Me_25-27_fragments E-Me-B_1.DS.valid E-Me-B_2.DS.valid E-Me-B.SS.valid E-Me-B.cfm.norm E-Me-B.cfv.norm
#${path}normalize_visibility ${genome_path}sacCer3.E-Mp_25-27_fragments E-Mp-A_1.DS.valid E-Mp-A_2.DS.valid E-Mp-A.SS.valid E-Mp-A.cfm.norm E-Mp-A.cfv.norm
#${path}normalize_visibility ${genome_path}sacCer3.E-Mp_25-27_fragments E-Mp-B_1.DS.valid E-Mp-B_2.DS.valid E-Mp-B.SS.valid E-Mp-B.cfm.norm E-Mp-B.cfv.norm
#${path}normalize_visibility ${genome_path}sacCer3.H-Me_25-27_fragments H-Me-A_1.DS.valid H-Me-A_2.DS.valid H-Me-A.SS.valid H-Me-A.cfm.norm H-Me-A.cfv.norm
#${path}normalize_visibility ${genome_path}sacCer3.H-Me_26-28_fragments H-Me-B_1.DS.valid H-Me-B_2.DS.valid H-Me-B.SS.valid H-Me-B.cfm.norm H-Me-B.cfv.norm
#${path}normalize_visibility ${genome_path}sacCer3.H-Mp_25-27_fragments H-Mp-A_1.DS.valid H-Mp-A_2.DS.valid H-Mp-A.SS.valid H-Mp-A.cfm.norm H-Mp-A.cfv.norm
#${path}normalize_visibility ${genome_path}sacCer3.H-Mp_26-28_fragments H-Mp-B_1.DS.valid H-Mp-B_2.DS.valid H-Mp-B.SS.valid H-Mp-B.cfm.norm H-Mp-B.cfv.norm
#${path}normalize_visibility ${genome_path}sacCer3.H-Me_25-27_fragments U-H-Me-A_1.DS.valid U-H-Me-A_2.DS.valid U-H-Me-A.SS.valid U-H-Me-A.cfm.norm U-H-Me-A.cfv.norm
#${path}normalize_visibility ${genome_path}sacCer3.H-Me_26-28_fragments U-H-Me-B_1.DS.valid U-H-Me-B_2.DS.valid U-H-Me-B.SS.valid U-H-Me-B.cfm.norm U-H-Me-B.cfv.norm

#${path}calc_visibility E-Me-A.cfm.no_norm E-Me-A.cfv.no_norm E-Me-A.visibility.no_norm
#${path}calc_visibility E-Me-B.cfm.no_norm E-Me-B.cfv.no_norm E-Me-B.visibility.no_norm
#${path}calc_visibility E-Mp-A.cfm.no_norm E-Mp-A.cfv.no_norm E-Mp-A.visibility.no_norm
#${path}calc_visibility E-Mp-B.cfm.no_norm E-Mp-B.cfv.no_norm E-Mp-B.visibility.no_norm
#${path}calc_visibility H-Me-A.cfm.no_norm H-Me-A.cfv.no_norm H-Me-A.visibility.no_norm
#${path}calc_visibility H-Me-B.cfm.no_norm H-Me-B.cfv.no_norm H-Me-B.visibility.no_norm
#${path}calc_visibility H-Mp-A.cfm.no_norm H-Mp-A.cfv.no_norm H-Mp-A.visibility.no_norm
#${path}calc_visibility H-Mp-B.cfm.no_norm H-Mp-B.cfv.no_norm H-Mp-B.visibility.no_norm
#${path}calc_visibility U-H-Me-A.cfm.no_norm U-H-Me-A.cfv.no_norm U-H-Me-A.visibility.no_norm
#${path}calc_visibility U-H-Me-B.cfm.no_norm U-H-Me-B.cfv.no_norm U-H-Me-B.visibility.no_norm
#${path}calc_visibility E-Me-A.cfm.norm E-Me-A.cfv.norm E-Me-A.visibility.norm
#${path}calc_visibility E-Me-B.cfm.norm E-Me-B.cfv.norm E-Me-B.visibility.norm
#${path}calc_visibility E-Mp-A.cfm.norm E-Mp-A.cfv.norm E-Mp-A.visibility.norm
#${path}calc_visibility E-Mp-B.cfm.norm E-Mp-B.cfv.norm E-Mp-B.visibility.norm
#${path}calc_visibility H-Me-A.cfm.norm H-Me-A.cfv.norm H-Me-A.visibility.norm
#${path}calc_visibility H-Me-B.cfm.norm H-Me-B.cfv.norm H-Me-B.visibility.norm
#${path}calc_visibility H-Mp-A.cfm.norm H-Mp-A.cfv.norm H-Mp-A.visibility.norm
#${path}calc_visibility H-Mp-B.cfm.norm H-Mp-B.cfv.norm H-Mp-B.visibility.norm
#${path}calc_visibility U-H-Me-A.cfm.norm U-H-Me-A.cfv.norm U-H-Me-A.visibility.norm
#${path}calc_visibility U-H-Me-B.cfm.norm U-H-Me-B.cfv.norm U-H-Me-B.visibility.norm

#R --vanilla --slave < ${path}cat_norm_cfm.R

#${path}binning_cfm E.cfm.norm ${genome_path}sacCer3.EcoRI_fragments E.cfm.norm.binned E.norm.bin
#${path}binning_cfm H.cfm.norm ${genome_path}sacCer3.HindIII_fragments H.cfm.norm.binned H.norm.bin

#R --vanilla --slave < ${path}cat_binned_norm_cfm.R


#R --vanilla --slave < ${path}cat_no_norm_cfm.R

#${path}binning_cfm E.cfm.no_norm ${genome_path}sacCer3.EcoRI_fragments E.cfm.no_norm.binned E.bin
#${path}binning_cfm H.cfm.no_norm ${genome_path}sacCer3.HindIII_fragments H.cfm.no_norm.binned H.bin


#R --vanilla --slave < ${path}cor_cfm.R
