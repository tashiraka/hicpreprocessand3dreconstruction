import std.stdio, std.conv, std.string, std.algorithm, std.array, std.math;

version(unittest){}
 else{
   void main(string[] args)
   {
     auto fragFile = args[1];
     auto SSFile = args[2];
     auto outVectorFile = args[3];

     auto frags = readFrag(fragFile);
     auto ssVector = readSS(SSFile, frags);
  
     auto fout = File(outVectorFile, "w");
     foreach(elem; ssVector) {
       fout.writeln(elem);
     }
   }
 }


double[] readSS(string file, Fragment[] frags)
{
  auto freqMat = new double[](frags.length);
  freqMat.fill(0.0);
    
  auto fin = File(file, "r");
  
  while(true) {
    auto fields = fin.readln.to!string.strip.split('\t');

    if(fields.length != 4)
      break;

    freqMat[fields[3].to!uint]++;
  }
  
  return freqMat;
}


class Fragment {
private:
  uint id_, chrID_, start_, end_;
  bool isValid_;
  
public:
  this(uint id, uint chrID, uint start, uint end, bool v) {
    id_=id; chrID_=chrID; start_=start; end_=end; isValid_=v;
  }

  @property {
    uint id() {return id_;}
    uint chrID() {return chrID_;}
    uint start() {return start_;}
    uint end() {return end_;}
    bool isValid() {return isValid_;}
  }
}


Fragment[] readFrag(string file)
{
  Fragment[] frags;  
  foreach(line; File(file, "r").byLine) {
    if(line != "") {
      auto fields = line.to!string.split("\t");
      frags ~= new Fragment(fields[0].to!uint, fields[1].to!uint,
                            fields[2].to!uint, fields[3].to!uint,
                            fields[4] == "valid" ? true : false);
    }
  }
  return frags;
}
