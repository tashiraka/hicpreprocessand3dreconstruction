R --vanilla --slave < ${path}cat_norm_cfm.R
    
${path}binning_cfm E.cfm.norm ${genome_path}sacCer3.EcoRI_fragments E.cfm.norm.binned E.norm.bin
${path}binning_cfm H.cfm.norm ${genome_path}sacCer3.HindIII_fragments H.cfm.norm.binned H.norm.bin

R --vanilla --slave < ${path}cat_binned_norm_cfm.R


R --vanilla --slave < ${path}cat_no_norm_cfm.R

${path}binning_cfm E.cfm.no_norm ${genome_path}sacCer3.EcoRI_fragments E.cfm.no_norm.binned E.bin
${path}binning_cfm H.cfm.no_norm ${genome_path}sacCer3.HindIII_fragments H.cfm.no_norm.binned H.bin
    
    
R --vanilla --slave < ${path}cor_cfm.R
    
