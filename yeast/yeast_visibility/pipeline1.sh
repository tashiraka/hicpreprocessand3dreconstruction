prefix=$1
RE1=$2
RE2=$3


path=~/Workspace/Hi-C_workbench/refined_hic_preprocess/yeast_visibility/
genome_path=/work/yuichi/S.cerevisiae/genome/
genome=${genome_path}sacCer3.fa
bt2_index=${genome_path}bt2/sacCer3
RE1_fragment=${genome_path}sacCer3.${RE1}_fragments
RE2_cut_site=${genome_path}sacCer3.${RE2}_cut_sites
filtered_fragment=${genome_path}sacCer3.${prefix}_${EcoP15I_cut_length_min}-${EcoP15I_cut_length_max}_fragments
fastq1=${prefix}_1.fastq
fastq2=${prefix}_2.fastq
sam1=${prefix}_1.sam
sam2=${prefix}_2.sam
sorted_sam1=${prefix}_1.sorted.sam
sorted_sam2=${prefix}_2.sorted.sam
DS_sam1=${prefix}.DS1.sam
DS_sam2=${prefix}.DS2.sam
SS_sam=${prefix}.SS.sam
DS1=${prefix}.DS1
DS2=${prefix}.DS2
SS=${prefix}.SS
mapped_DS1=${prefix}.DS1.mapped
mapped_DS2=${prefix}.DS2.mapped
mapped_SS=${prefix}.SS.mapped
dA_dB=${prefix}.dA_dB


python ${path}restrict_to_fragments.py $genome $RE1 EcoRI $RE1_fragment
python ${path}list_up_cut_site.py $genome $RE2 $RE2_cut_site


bowtie2 --very-sensitive -p 48 -x $bt2_index -U $fastq1 -S $sam1 >${prefix}_1.bowtie2.log 2>&1 
bowtie2 --very-sensitive -p 48 -x $bt2_index -U $fastq2 -S $sam2 >${prefix}_2.bowtie2.log 2>&1


samtools view -Shb $sam1 >${prefix}_1.tmp.bam; samtools sort -n ${prefix}_1.tmp.bam >${prefix}_1.tmp.sort; rm ${prefix}_1.tmp.bam; samtools view -h ${prefix}_1.tmp.sort.bam >$sorted_sam1; rm ${prefix}_1.tmp.sort.bam;
samtools view -Shb $sam2 >${prefix}_2.tmp.bam; samtools sort -n ${prefix}_2.tmp.bam >${prefix}_2.tmp.sort; rm ${prefix}_2.tmp.bam; samtools view -h ${prefix}_2.tmp.sort.bam >$sorted_sam2; rm ${prefix}_2.tmp.sort.bam;


${path}divide_DS_SS $sorted_sam1 $sorted_sam2 $DS_sam1 $DS_sam2 $SS_sam 


python ${path}decide_read_pos.py $sorted_sam2 $DS_sam1 $DS_sam2 $SS_sam $sorted_sam2 $DS1 $DS2 $SS


${path}map_to_fragment $DS1 $DS2 $SS $RE1_fragment $mapped_DS1 $mapped_DS2 $mapped_SS


${path}get_dA_dB $mapped_DS1 $mapped_DS2 $mapped_SS $RE1_fragment $dA_dB

#------------STOP!!! and check RE1 cut length!!!!------------------------

