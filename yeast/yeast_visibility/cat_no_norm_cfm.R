# EcoRI
a <- read.table("E-Me-A.cfm.no_norm")
b <- read.table("E-Me-B.cfm.no_norm")
eme.cfm <- a + b
eme.max <- max(as.vector(data.matrix(eme.cfm)))

a <- read.table("E-Mp-A.cfm.no_norm")
b <- read.table("E-Mp-B.cfm.no_norm")
emp.cfm <- a + b
emp.max <- max(as.vector(data.matrix(emp.cfm)))

e.max.mean <- (eme.max + emp.max) / 2.0
e.cfm <- eme.cfm * e.max.mean / eme.max + emp.cfm * e.max.mean / emp.max

write.table(e.cfm, "E.cfm.no_norm", sep="\t", row.names=F, col.names=F)


# HindIII
a <- read.table("H-Me-A.cfm.no_norm")
b <- read.table("H-Me-B.cfm.no_norm")
hme.cfm <- a + b
hme.max <- max(as.vector(data.matrix(hme.cfm)))

a <- read.table("H-Mp-A.cfm.no_norm")
b <- read.table("H-Mp-B.cfm.no_norm")
hmp.cfm <- a + b
hmp.max <- max(as.vector(data.matrix(hmp.cfm)))

h.max.mean <- (hme.max + hmp.max) / 2.0
h.cfm <- hme.cfm * h.max.mean / hme.max + hmp.cfm * h.max.mean / hmp.max

write.table(h.cfm, "H.cfm.no_norm", sep="\t", row.names=F, col.names=F)

