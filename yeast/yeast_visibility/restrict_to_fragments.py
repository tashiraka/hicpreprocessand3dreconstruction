#!/bio/bin/python
# -*- coding: utf-8 -*-
from Bio import SeqIO, Restriction, Seq, SeqUtils
from argparse import RawTextHelpFormatter
import numpy as np
import argparse


parser = argparse.ArgumentParser(
    description='''
  This command is tool to cut out restriction fragments from a genome.
  A site position is followed by 1-origin coordinate of the genome.
  This command returns the positions where overhangs fullfilled.
  A restriction fragment is represented as a closed interval, [start, end].
  The output format is as follows.
  <fragment ID>\t<chromosome ID>\t<start position>\t<end position>
    
  For example:
  5\'-GAATTC-3\'
  5\'-CTTAAG-3\' is recognizad by a restriction enzyme EcoRI.
  EcoRI cuts this sequence to
  5\'-G  AATTC-3\'
  3\'-CTTAA  G-5\'.
  Then this command returns, (assuming the sequence is called chr1)
  chr1\t1\t5
  chr1\t6\t10
  ''',
    epilog="Author: Yuichi Motai",
    formatter_class=RawTextHelpFormatter)

parser.add_argument('-v', '--version', action='version', version='version 1.0')
parser.add_argument('genome', help='fasta file containing reference genome')
parser.add_argument('RE', help='restriction enzyme name')
parser.add_argument('out_file', help='output file')
args = parser.parse_args()


# This chromosome name parser is for sacCer3
def toID(chrName):
    if chrName == "chrI":
        return '0'
    elif chrName == "chrII":
        return '1'
    elif chrName == "chrIII":
        return '2'
    elif chrName == "chrIV":
        return '3'
    elif chrName == "chrV":
        return '4'
    elif chrName == "chrVI":
        return '5'
    elif chrName == "chrVII":
        return '6'
    elif chrName == "chrVIII":
        return '7'
    elif chrName == "chrIX":
        return '8'
    elif chrName == "chrX":
        return '9'
    elif chrName == "chrXI":
        return '10'
    elif chrName == "chrXII":
        return '11'
    elif chrName == "chrXIII":
        return '12'
    elif chrName == "chrXIV":
        return '13'
    elif chrName == "chrXV":
        return '14'
    elif chrName == "chrXVI":
        return '15'
    else:
        print "ERROR: invalid chromosome name, " + chrName
        quit()

re = getattr(Restriction, args.RE)
fout = open(args.out_file, "w")
fragment_id = 0

if(re.is_blunt()):
    for chrom in SeqIO.parse(open(args.genome, 'r'), 'fasta') :
        if chrom.name != 'chrM':
            prev_rsite = 1
            
            for rsite in re.search(chrom.seq) :
                fout.write(str(fragment_id) + "\t" + toID(chrom.name) + '\t' + str(prev_rsite) + '\t' + str(rsite - 1) + '\n')
                prev_rsite = rsite
                fragment_id += 1
            else:
                fout.write(str(fragment_id) + "\t" + toID(chrom.name) + "\t" + str(prev_rsite) + "\t" + str(len(chrom.seq)) + '\n')
                fragment_id += 1

elif(re.is_5overhang()):
    for chrom in SeqIO.parse(open(args.genome, 'r'), 'fasta') :
        if chrom.name != 'chrM':
            prev_rsite = 1
            
            for rsite in re.search(chrom.seq) :
                fout.write(str(fragment_id) + "\t" + toID(chrom.name) + '\t' + str(prev_rsite) + '\t' + str(rsite - 1 - re.ovhg) + '\n')
                prev_rsite = rsite
                fragment_id += 1
            else:
                fout.write(str(fragment_id) + "\t" + toID(chrom.name) + "\t" + str(prev_rsite) + "\t" + str(len(chrom.seq)) + '\n')
                fragment_id += 1
            
elif(re.is_3overhag()):
    for chrom in SeqIO.parse(open(args.genome, 'r'), 'fasta') :
        if chrom.name != 'chrM':
            prev_rsite = 1
        
            for rsite in re.search(chrom.seq) :
                fout.write(str(fragment_id) + "\t" + toID(chrom.name) + '\t' + str(prev_rsite) + '\t' + str(rsite - 1) + '\n')
                prev_rsite = rsite - re.ovhg
                fragment_id += 1
            else:
                fout.write(str(fragment_id) + "\t" + toID(chrom.name) + "\t" + str(prev_rsite) + "\t" + str(len(chrom.seq)) + '\n')
                fragment_id += 1

else:
    print 'ERROR: The type of overhang is \'unkown\''    
    quit
