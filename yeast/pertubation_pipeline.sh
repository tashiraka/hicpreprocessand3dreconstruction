noiseLevel=1 # % of standard deviation of contact frequency

${path}convert_to_cfm ${genome_path}sacCer3.E-Me_25-27_fragments E-Me-A_1.DS.valid E-Me-A_2.DS.valid E-Me-A.cfm.no_norm
${path}convert_to_cfm ${genome_path}sacCer3.E-Me_25-27_fragments E-Me-B_1.DS.valid E-Me-B_2.DS.valid E-Me-B.cfm.no_norm
${path}convert_to_cfm ${genome_path}sacCer3.E-Mp_25-27_fragments E-Mp-A_1.DS.valid E-Mp-A_2.DS.valid E-Mp-A.cfm.no_norm
${path}convert_to_cfm ${genome_path}sacCer3.E-Mp_25-27_fragments E-Mp-B_1.DS.valid E-Mp-B_2.DS.valid E-Mp-B.cfm.no_norm
${path}convert_to_cfm ${genome_path}sacCer3.H-Me_25-27_fragments H-Me-A_1.DS.valid H-Me-A_2.DS.valid H-Me-A.cfm.no_norm
${path}convert_to_cfm ${genome_path}sacCer3.H-Me_26-28_fragments H-Me-B_1.DS.valid H-Me-B_2.DS.valid H-Me-B.cfm.no_norm
${path}convert_to_cfm ${genome_path}sacCer3.H-Mp_25-27_fragments H-Mp-A_1.DS.valid H-Mp-A_2.DS.valid H-Mp-A.cfm.no_norm
${path}convert_to_cfm ${genome_path}sacCer3.H-Mp_26-28_fragments H-Mp-B_1.DS.valid H-Mp-B_2.DS.valid H-Mp-B.cfm.no_norm


Rscript noise_in_cfm.R E-Me-A.cfm.no_norm $noiseLevel E-Me-A.cfm.no_norm.perturbed_${noiseLevel}
Rscript noise_in_cfm.R E-Me-B.cfm.no_norm $noiseLevel E-Me-B.cfm.no_norm.perturbed_${noiseLevel}
Rscript noise_in_cfm.R E-Mp-A.cfm.no_norm $noiseLevel E-Mp-A.cfm.no_norm.perturbed_${noiseLevel}
Rscript noise_in_cfm.R E-Mp-B.cfm.no_norm $noiseLevel E-Mp-B.cfm.no_norm.perturbed_${noiseLevel}
Rscript noise_in_cfm.R H-Me-A.cfm.no_norm $noiseLevel H-Me-A.cfm.no_norm.perturbed_${noiseLevel}
Rscript noise_in_cfm.R H-Me-B.cfm.no_norm $noiseLevel H-Me-B.cfm.no_norm.perturbed_${noiseLevel}
Rscript noise_in_cfm.R H-Mp-A.cfm.no_norm $noiseLevel H-Mp-A.cfm.no_norm.perturbed_${noiseLevel}
Rscript noise_in_cfm.R H-Mp-B.cfm.no_norm $noiseLevel H-Mp-B.cfm.no_norm.perturbed_${noiseLevel}


#${path}normalize_visibility ${genome_path}sacCer3.E-Me_25-27_fragments E-Me-A_1.DS.valid E-Me-A_2.DS.valid E-Me-A.SS.valid E-Me-A.cfm.norm E-Me-A.cfv.norm
#${path}normalize_visibility ${genome_path}sacCer3.E-Me_25-27_fragments E-Me-B_1.DS.valid E-Me-B_2.DS.valid E-Me-B.SS.valid E-Me-B.cfm.norm E-Me-B.cfv.norm
#${path}normalize_visibility ${genome_path}sacCer3.E-Mp_25-27_fragments E-Mp-A_1.DS.valid E-Mp-A_2.DS.valid E-Mp-A.SS.valid E-Mp-A.cfm.norm E-Mp-A.cfv.norm
#${path}normalize_visibility ${genome_path}sacCer3.E-Mp_25-27_fragments E-Mp-B_1.DS.valid E-Mp-B_2.DS.valid E-Mp-B.SS.valid E-Mp-B.cfm.norm E-Mp-B.cfv.norm
#${path}normalize_visibility ${genome_path}sacCer3.H-Me_25-27_fragments H-Me-A_1.DS.valid H-Me-A_2.DS.valid H-Me-A.SS.valid H-Me-A.cfm.norm H-Me-A.cfv.norm
#${path}normalize_visibility ${genome_path}sacCer3.H-Me_26-28_fragments H-Me-B_1.DS.valid H-Me-B_2.DS.valid H-Me-B.SS.valid H-Me-B.cfm.norm H-Me-B.cfv.norm
#${path}normalize_visibility ${genome_path}sacCer3.H-Mp_25-27_fragments H-Mp-A_1.DS.valid H-Mp-A_2.DS.valid H-Mp-A.SS.valid H-Mp-A.cfm.norm H-Mp-A.cfv.norm
#${path}normalize_visibility ${genome_path}sacCer3.H-Mp_26-28_fragments H-Mp-B_1.DS.valid H-Mp-B_2.DS.valid H-Mp-B.SS.valid H-Mp-B.cfm.norm H-Mp-B.cfv.norm
#${path}normalize_visibility ${genome_path}sacCer3.H-Me_25-27_fragments U-H-Me-A_1.DS.valid U-H-Me-A_2.DS.valid U-H-Me-A.SS.valid U-H-Me-A.cfm.norm U-H-Me-A.cfv.norm
#${path}normalize_visibility ${genome_path}sacCer3.H-Me_26-28_fragments U-H-Me-B_1.DS.valid U-H-Me-B_2.DS.valid U-H-Me-B.SS.valid U-H-Me-B.cfm.norm U-H-Me-B.cfv.norm


R --vanilla --slave < ${path}cat_norm_cfm.R

${path}binning_cfm E.cfm.norm ${genome_path}sacCer3.EcoRI_fragments E.cfm.norm.binned E.norm.bin
${path}binning_cfm H.cfm.norm ${genome_path}sacCer3.HindIII_fragments H.cfm.norm.binned H.norm.bin

R --vanilla --slave < ${path}cat_binned_norm_cfm.R


R --vanilla --slave < ${path}cat_no_norm_cfm.R

#${path}binning_cfm E.cfm.no_norm ${genome_path}sacCer3.EcoRI_fragments E.cfm.no_norm.binned E.bin
#${path}binning_cfm H.cfm.no_norm ${genome_path}sacCer3.HindIII_fragments H.cfm.no_norm.binned H.bin


#R --vanilla --slave < ${path}cor_cfm.R
