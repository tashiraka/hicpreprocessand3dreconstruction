#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_3.h>

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <getopt.h>
#include <stdlib.h>
#include <math.h>


// types in CGAL package; Delaunay_triangulation_3
typedef CGAL::Exact_predicates_exact_constructions_kernel K;
typedef CGAL::Delaunay_triangulation_3<K> Dt;
typedef Dt::Point Point;
typedef Dt::Vertex Vertex;
typedef Dt::Edge Edge;
typedef Dt::Facet Facet;
typedef Dt::Cell Cell;
typedef Dt::Cell_handle Cell_handle;
typedef Dt::Vertex_handle Vertex_handle;


class Alpha_complex: Dt
{
private:
  using Dt::finite_facets_begin;
  using Dt::finite_facets_end;
  using Dt::finite_edges_begin;
  using Dt::finite_edges_end;
  using Dt::finite_vertices_begin;
  using Dt::finite_vertices_end;
  using Dt::finite_cells_begin;
  using Dt::finite_cells_end;
  using Dt::is_infinite;

  typedef std::multimap<double, Vertex_handle> Alpha_vertex_map;
  typedef std::multimap<double, Facet> Alpha_facet_map;
  typedef std::multimap<double, Edge> Alpha_edge_map;
  typedef std::multimap<double, Cell_handle> Alpha_cell_map;
  
  Alpha_vertex_map alpha_vertex_map;
  Alpha_edge_map alpha_edge_map;  
  Alpha_facet_map alpha_facet_map;
  Alpha_cell_map alpha_cell_map;

  //The p0 lies out the minimum circumsphere of p1 and p2 => true.
  //otherwise => false.
  bool is_Gabriel(const int& i0, const Cell_handle& ch,
                  const int& i1, const int& i2) const
  {
    auto p0 = ch->vertex(i0)->point();
    auto p1 = ch->vertex(i1)->point();
    auto p2 = ch->vertex(i2)->point();
    auto midp = CGAL::midpoint(p1, p2);
    return CGAL::squared_distance(midp, p0) > CGAL::squared_radius(p1, p2);
  }

  //The p0 lies out the minimum circumsphere of p1, p2 and p3 => true.
  //otherwise => false.
  bool is_Gabriel(const int& i0, const Cell_handle& ch,
                  const int& i1, const int& i2, const int& i3) const
  {
    auto p0 = ch->vertex(i0)->point();
    auto p1 = ch->vertex(i1)->point();
    auto p2 = ch->vertex(i2)->point();
    auto p3 = ch->vertex(i3)->point();
    auto cc = CGAL::circumcenter(p1, p2, p3);
    return CGAL::squared_distance(p0, cc) > CGAL::squared_radius(p1, p2, p3);
  }

  double alpha(const Vertex_handle& v) const
  {
    return 0;
  }

  double alpha(const Edge& e) const
  {
    return sqrt(CGAL::to_double
                (CGAL::squared_radius(e.first->vertex(e.second)->point(),
                                      e.first->vertex(e.third)->point())));
  }

  double alpha(const Cell_handle& ch, const int& i0, const int& i1) const
  {
    return sqrt(CGAL::to_double
                (CGAL::squared_radius(ch->vertex(i0)->point(),
                                      ch->vertex(i1)->point())));
  }
  
  double alpha(const Cell_handle& ch,
               const int& i0, const int& i1, const int& i2) const
  {
    return sqrt(CGAL::to_double
                (CGAL::squared_radius(ch->vertex(i0)->point(),
                                      ch->vertex(i1)->point(),
                                      ch->vertex(i2)->point())));
  }

  double alpha(const Cell_handle& ch) const
  {
    return sqrt(CGAL::to_double
                (CGAL::squared_radius(ch->vertex(0)->point(),
                                      ch->vertex(1)->point(),
                                      ch->vertex(2)->point(),
                                      ch->vertex(3)->point())));
  }
  
  void compute_vertex_alpha() {
    for(auto it=finite_vertices_begin(); it!=finite_vertices_end(); ++it) {
      if(is_infinite(it)) continue;
      alpha_vertex_map.insert(typename Alpha_vertex_map::value_type
                              (alpha(it), it));
    }
  }

  void compute_edge_alpha() {
    for(auto it=finite_edges_begin(); it!=finite_edges_end(); ++it) {
      if(is_infinite(*it)) continue;
      alpha_edge_map.insert(typename Alpha_edge_map::value_type
                            (alpha(*it), *it));
    } 
  }

  double select_facet_alpha(const Cell_handle& ch,
                            const std::vector<int>& index) const
  {
    if(!is_Gabriel(index[0], ch, index[1], index[2])) {
      return alpha(ch, index[1], index[2]);
    }
    else if(!is_Gabriel(index[1], ch, index[0], index[2])) {
      return alpha(ch, index[0], index[2]);
    }
    else if(!is_Gabriel(index[2], ch, index[0], index[1])) {
      return alpha(ch, index[0], index[1]);
    }
    else {
      return alpha(ch, index[0], index[1], index[2]);
    }       
  }

  double max_facet_alpha(const Cell_handle& ch) const
  {
    auto a0 = select_facet_alpha(ch, {1, 2, 3});
    auto a1 = select_facet_alpha(ch, {0, 2, 3});
    auto a2 = select_facet_alpha(ch, {0, 1, 3});
    auto a3 = select_facet_alpha(ch, {0, 1, 2});
    return std::max({a0, a1, a2, a3});
  }
  
  void compute_facet_alpha() {
     for(auto it=finite_facets_begin(); it!=finite_facets_end(); ++it) {
       if(is_infinite(*it)) continue;

       std::vector<int> index{0, 1, 2, 3};
       index.erase(index.begin() + it->second);

       alpha_facet_map.insert(typename Alpha_facet_map::value_type
                              (select_facet_alpha(it->first, index), *it));
     }
  }
  
  void compute_cell_alpha()
  {
    for(auto it=finite_cells_begin(); it!=finite_cells_end(); ++it) {
      if(is_infinite(it)) continue;
      
      if(!is_Gabriel(0, it, 1, 2, 3) || !is_Gabriel(1, it, 0, 2, 3)
         || !is_Gabriel(2, it, 0, 1, 3) || !is_Gabriel(3, it, 0, 1, 2))
        {
          alpha_cell_map.insert(typename Alpha_cell_map::value_type
                                (max_facet_alpha(it), it));
        }
      else {
        alpha_cell_map.insert(typename Alpha_cell_map::value_type
                              (alpha(it), it));          
      }
    }  
  }
  
public:
  Alpha_complex(std::vector<Point>& points) {
    //compute the Delaunay triangulation     
    insert(points.begin(), points.end());

    compute_vertex_alpha();
    compute_edge_alpha();
    compute_facet_alpha();
    compute_cell_alpha();
  }

  void output_filtration(const char* outFile) const
  {
    std::ofstream fout(outFile);
    
    //the alpha values of vertices are 0.
    for(auto vit=alpha_vertex_map.begin(); vit!=alpha_vertex_map.end(); ++vit) {
      fout << vit->first << "\t" << *vit->second << "\n";
    }

    auto eit = alpha_edge_map.begin();
    auto fit = alpha_facet_map.begin();
    auto cit = alpha_cell_map.begin();
    
    while(eit != alpha_edge_map.end()
          || fit != alpha_facet_map.end()
          || cit != alpha_cell_map.end())
      {
        if(eit != alpha_edge_map.end()
           && (fit == alpha_facet_map.end() || eit->first <= fit->first)
           && (cit == alpha_cell_map.end() || eit->first <= cit->first))
          {
            fout << eit->first  << "\t"
                 << *eit->second.first->vertex(eit->second.second) << "\t"
                 << *eit->second.first->vertex(eit->second.third) << "\n";
            ++eit;
          }
        if(fit != alpha_facet_map.end()
           && (eit == alpha_edge_map.end() || fit->first < eit->first)
           && (cit == alpha_cell_map.end() || fit->first <= cit->first))
          {
            std::vector<int> index{0, 1, 2, 3};
            index.erase(index.begin() + fit->second.second);
          
            fout << fit->first << "\t"
                 << *fit->second.first->vertex(index[0]) << "\t"
                 << *fit->second.first->vertex(index[1]) << "\t"
                 << *fit->second.first->vertex(index[2]) << "\n";
            ++fit;
          }
        if(cit != alpha_cell_map.end()
           && (eit == alpha_edge_map.end() || cit->first < eit->first)
           && (fit == alpha_facet_map.end() || cit->first < fit->first))
          {
            fout << cit->first << "\t"
                 << *cit->second->vertex(0) << "\t"
                 << *cit->second->vertex(1) << "\t"
                 << *cit->second->vertex(2) << "\t"
                 << *cit->second->vertex(3) << "\n";
            ++cit;
          }
      }
    
    fout.close();
  }
};



int main(int argc, char** argv) {
  auto pointsFile = argv[1];
  auto outFile = argv[2];
  
  // input points from pointsFile
  std::ifstream fin(pointsFile);
  std::vector<Point> points;
  Point p;
  while(fin >> p) {
    points.push_back(p);
  }
  
  Alpha_complex ac(points);
  ac.output_filtration(outFile);

  return 0;
}
