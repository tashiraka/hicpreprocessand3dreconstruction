/***
!!!Assumption!!!
The rule to name variables in this source code is depend on
Afra Zomoridian and Gunnar Carlsson, "Computing Persistent Homology",
Discrete and Computational Geometry.

Field of persistent homology group is only Z2[x].

input format

The line has four patterns.
Vertex
x1 y1 z1 alpha_value
Edge
x1 y1 z1 x2 y2 z2 alpha_value
Facet
x1 y1 z1 x2 y2 z2 x3 y3 z3 alpha_value
Cell
x1 y1 z1 x2 y2 z2 x3 y3 z3 x4 y4 z4 alpha_value
 
Dimension is lower than 4.
The lines are sorted by alpha_value.

***/

import std.stdio, std.typecons, std.array, std.conv, std.algorithm, std.string, std.parallelism;


version(unittest){}
 else {
   void main(string[] args)
   {
     auto filtrationFile = args[1];
     auto filtrationStepSize = args[2].to!double;
     auto outFile = args[3];

     auto simplices = readFiltration(filtrationFile, filtrationStepSize);
     simplices = simplices.boundaryOperator;
     auto intervals = computeIntervals(simplices);
     writeIntervals(outFile, intervals, simplices);
   }
 }


void writeIntervals(string filename, Interval[] intervals, Simplex[][] simplices) {
  auto fout = File(filename, "w");
  foreach(interval; intervals) {
    fout.write(interval);
    
    foreach(i, subCol; interval.simplex.subtractedColumns) 
      fout.write("\t" ~ simplices[interval.dim][subCol].toString);
    
    fout.writeln;
  }
}


Simplex[][] readFiltration(string filename, double filtrationStepSize)
{
  auto app = new Appender!(Simplex[])[](4);

  foreach(line; File(filename, "r").byLine) {
    auto fields = line.to!string.strip.split("\t");
    auto dim = fields.length - 2;
    auto deg = cast(uint)(fields[0].to!double / filtrationStepSize);
    auto points = new Point[](dim + 1);

    for(auto i=0; i<dim+1; i++)
      points[i] = new Point(fields[i+1].split(" ")
                            .map!(x => x.to!double).array);

    app[dim].put(new Simplex(points.dup, deg));
  }
  
  return [app[0].data, app[1].data, app[2].data, app[3].data];
}


Simplex[][] boundaryOperator(Simplex[][] simplices)
{
  foreach(dim, simplicesByDim; simplices) {
    if(dim != 0) {
      foreach(ref simplex; simplicesByDim.parallel) {
        for(auto i=0; i<dim+1; i++) {
          //Dlang's remove function modifies original array...!!
          auto dupPoints = simplex.points.dup;
          simplex.boundaries ~= searchIndex(simplices[dim - 1],
                                            dupPoints.remove(dim - i));
        }

        if(simplex.boundaries.length != dim + 1) 
          throw new Exception("ERROR: simplex.boundaries.length != dim + 1");
      }
    }
  }
  return simplices;
}


unittest {
  auto p0 = new Point([0, 0, 0]);
  auto p1 = new Point([1, 0, 0]);
  auto p2 = new Point([0, 1, 0]);
  auto p3 = new Point([0, 0, 1]);

  auto s0 = [new Simplex([p0], 0), new Simplex([p1], 0),
             new Simplex([p2], 0), new Simplex([p3], 0)];
  auto s1 = [new Simplex([p0, p1], 1), new Simplex([p0, p2], 1),
             new Simplex([p0, p3], 1), new Simplex([p1, p2], 2),
             new Simplex([p2, p3], 2), new Simplex([p3, p1], 2)];
  auto s2 = [new Simplex([p0, p1, p2], 2), new Simplex([p0, p1, p3], 2),
             new Simplex([p0, p2, p3], 2), new Simplex([p1, p2, p3], 3)];
  auto s3 = [new Simplex([p0, p1, p2, p3], 3)];
  
  Simplex[][] s = [s0, s1, s2, s3];
  s = s.boundaryOperator;
  
  assert(s[1][0].boundaries == [0, 1], s[1][0].boundaries.to!string);
  assert(s[1][1].boundaries == [0, 2], s[1][1].boundaries.to!string);
  assert(s[1][2].boundaries == [0, 3], s[1][2].boundaries.to!string);
  assert(s[1][3].boundaries == [1, 2], s[1][3].boundaries.to!string);
  assert(s[1][4].boundaries == [2, 3], s[1][4].boundaries.to!string);
  assert(s[1][5].boundaries == [3, 1], s[1][5].boundaries.to!string);
  assert(s[2][0].boundaries == [0, 1, 3], s[2][0].boundaries.to!string);
  assert(s[2][1].boundaries == [0, 2, 5], s[2][1].boundaries.to!string);
  assert(s[2][2].boundaries == [1, 2, 4], s[2][2].boundaries.to!string);
  assert(s[2][3].boundaries == [3, 5, 4], s[2][3].boundaries.to!string);
  assert(s[3][0].boundaries == [0, 1, 2, 3], s[3][0].boundaries.to!string);
}


ulong searchIndex(Simplex[] simplices, Point[] targetPoints)
{
  foreach(i, simplex; simplices) 
    if(compPoints(simplex.points, targetPoints)) 
      return i;

  writeln(targetPoints);
  
  throw new Exception("k-simplex's boundary is not included in (k-1)-simplex");
}


// compare the two sets of points 
bool compPoints(Point[] points1, Point[] points2)
{
  int result = 0;
  foreach(p1; points1)
    foreach(p2; points2)
      if(p1.coords == p2.coords)
        result++;

  return result == points1.length;
}


unittest {
  auto p0 = new Point([0, 0, 0]);
  auto p1 = new Point([1, 0, 0]);
  auto p2 = new Point([0, 1, 0]);
  auto p3 = new Point([0, 0, 1]);

  auto s0 = [new Simplex([p0], 0), new Simplex([p1], 0),
             new Simplex([p2], 0), new Simplex([p3], 0)];
  auto s1 = [new Simplex([p0, p1], 1), new Simplex([p0, p2], 1),
             new Simplex([p0, p3], 1), new Simplex([p1, p2], 2),
             new Simplex([p2, p3], 2), new Simplex([p3, p1], 2)];
  auto s2 = [new Simplex([p0, p1, p2], 2), new Simplex([p0, p1, p3], 2),
             new Simplex([p0, p2, p3], 2), new Simplex([p1, p2, p3], 3)];
  auto s3 = [new Simplex([p0, p1, p2, p3], 3)];

  assert(searchIndex(s0, [p0]) == 0, searchIndex(s0, [p0]).to!string);
  assert(searchIndex(s0, [p1]) == 1, searchIndex(s0, [p1]).to!string);
  assert(searchIndex(s0, [p2]) == 2, searchIndex(s0, [p2]).to!string);
  assert(searchIndex(s0, [p3]) == 3, searchIndex(s0, [p3]).to!string);
  assert(searchIndex(s1, [p0, p1]) == 0, searchIndex(s1, [p0, p1]).to!string);
  assert(searchIndex(s1, [p0, p2]) == 1, searchIndex(s1, [p0, p2]).to!string);
  assert(searchIndex(s1, [p0, p3]) == 2, searchIndex(s1, [p0, p3]).to!string);
  assert(searchIndex(s1, [p1, p2]) == 3, searchIndex(s1, [p1, p2]).to!string);
  assert(searchIndex(s1, [p2, p3]) == 4, searchIndex(s1, [p2, p3]).to!string);
  assert(searchIndex(s1, [p3, p1]) == 5, searchIndex(s1, [p3, p1]).to!string);
  assert(searchIndex(s2, [p0, p1, p2]) == 0,
         searchIndex(s2, [p0, p1, p2]).to!string);
  assert(searchIndex(s2, [p0, p1, p3]) == 1,
         searchIndex(s2, [p0, p1, p3]).to!string);
  assert(searchIndex(s2, [p0, p2, p3]) == 2,
         searchIndex(s2, [p0, p2, p3]).to!string);
  assert(searchIndex(s2, [p1, p2, p3]) == 3,
         searchIndex(s2, [p1, p2, p3]).to!string);
}


Interval[] computeIntervals(Simplex[][] simplices)
{
  auto T = new Tuple!(ulong,"j",
                      ulong[],"d",
                      bool,"mark")[][](simplices.length, 0);
  foreach(i, noUse1; T) {
    T[i].length = simplices[i].length;
    T[i].fill(tuple(0, null, false));
  }
  auto L = appender!(Interval[]);

  foreach(dim, ref simplicesByDim; simplices) {
    foreach(j, ref simplex; simplicesByDim) {
      ulong[] d = null;
      if(dim == 0) {
        d = removePivotRow(simplex, null);
      } else {
        d = removePivotRow(simplex, T[dim - 1]);
      }

      if(dim == 0 && d != null)
        throw new Exception("ERROR: dim == 0 && d != null");
      
      if(d == null) {
        T[dim][j].mark = true;

      } else {
        auto i = maxIndex(d);
        T[dim - 1][i].j = j;
        T[dim - 1][i].d = d;

        L.put(new Interval(simplex, simplices[dim - 1][i].deg, simplex.deg));
      }
    }
  }

  foreach(dim, ref simplicesByDim; simplices) 
    foreach(j, ref simplex; simplicesByDim)
      if(dim != 0 && T[dim][j].mark && T[dim][j].d == null)
        L.put(new Interval(simplex, simplex.deg, uint.max)); 
  
  return L.data;
}


ulong[] removePivotRow(ref Simplex simplex,
                       Tuple!(ulong, "j", ulong[], "d", bool, "mark")[] T)
{
  foreach(boundary; simplex.boundaries)
    if(!T[boundary].mark)
      simplex.removeBoundary(boundary); //row transform, not trace
  
  while(simplex.boundaries != null) {
    auto i = maxIndex(simplex.boundaries);
    if(T[i].d == null)
      break;

    simplex.subtractFromBoundary(T[i].d); //column transform, trace
    simplex.subtractedColumns ~= T[i].j;
  }
  
  return simplex.boundaries;
}


ulong maxIndex(ulong[] boundaries)
{
  return reduce!((a, b) => max(a, b))(ulong.min, boundaries);
}


class Point
{
  double[] coords = null;
  this(double[] c) {coords = c;}
  override string toString() {
    return reduce!((a, b) => a ~ "," ~ b.to!string)(coords[0].to!string,
                                                    coords[1..$]);
  }
}


class Simplex
{
  Point[] points = null;
  uint deg = 0;
  ulong[] boundaries = null;
  ulong[] subtractedColumns = null;
  
  this(Point[] p, uint d) {
    points = p; deg = d;
  }

  @property ulong dim() { return points.length - 1; }

  ulong[] removeBoundary(ulong boundary) {
    return boundaries = boundaries.filter!(x => x != boundary).array;
  }
  
  ulong[] subtractFromBoundary(ulong[] subtractingBoundaries)
  {
    foreach(subtractingBoundary; subtractingBoundaries) {
      auto finder = boundaries.find(subtractingBoundary);

      if(finder.empty)
        boundaries ~= subtractingBoundary;
      else
        boundaries = boundaries.remove(boundaries.length - finder.length);
    }
    
    return boundaries;
  }

  override string toString() {
    string s;
    foreach(i, point; points) {
      s ~= point.toString;
      if(i < points.length - 1)
        s ~= "\t";
    }
    return s;
  }
}


class Interval
{
  Simplex simplex;
  uint start = 0;
  uint end = 0;

  this(ref Simplex s, uint a, uint b) {
    simplex = s;
    start = a;
    end = b;
  }

  @property ulong dim() { return simplex.dim; }

  override string toString() {
    return dim.to!string ~ "\t" ~ start.to!string ~ "\t" ~ end.to!string
      ~ "\t" ~ simplex.toString;
  }
}
