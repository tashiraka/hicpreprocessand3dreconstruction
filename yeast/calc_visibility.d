import std.stdio, std.conv, std.string, std.algorithm, std.array;

void main(string[] args)
{
  auto cfmFile = args[1];
  auto cfvFile = args[2];
  auto outFile = args[3];

  double[] visibility;
  
  foreach(line; File(cfmFile, "r").byLine) {
    visibility ~= line.to!string.strip.split("\t").map!(x => x.to!double).sum;
  }

  auto i = 0;
  foreach(line; File(cfvFile, "r").byLine) {
    visibility[i++] += line.to!double;
  }
    
  auto fout = File(outFile, "w");
  foreach(v; visibility) {
    fout.writeln(v);
  }
}
