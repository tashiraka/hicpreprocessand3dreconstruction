#!/bin/bash


usage() {
    cat <<EOF
 
 DESCRIPTION:
     $(basename $0) is tool to extract valid alignments from a SAM file.
 
 USAGE:
     sh $(basename $0) [-vh] <in SAM file> <out SAM file>
 
 OPTIONS:
     -v
         print $(basename $0) version.
     -h
         print this.
 
 EXAMPLES:
     sh $(basename $0) in.sam out.sam >out.log 2>err.log
 
 AUTHOR:
     written by Yuichi Motai.
 
EOF
}


version() {
    echo "version 1.0"
}


while getopts "m:vh" opt;
do
    case $opt in
        v)  version
            exit 0
            ;;
        h)  usage
            exit 0
            ;;
        :)
            echo "ERROR: $opt needs value." 1>&2
            usage
            exit 1

            ;;
        /?)
            echo "ERROR: invalid option" 1>&2
            usage
            exit 1
            ;;
    esac
done


shift `expr $OPTIND - 1`
if [ $# -ne 2 ]; then
    echo "ERROR: the number of argment is $#, but needs 2." 1>&2
    usage
    exit 1
fi


in=$1
out=$2

echo -en "Extraction of valid alignments from $in"
echo -en "\tinput reads: "
samtools view -Sc $in

# removing unmapped (do not have a valid alignment) and secondary-mapped reads
samtools view -Sh -F 0x0104 $in >$out

echo -en "\tnot-secondary valid aligned reads: "
samtools view -Sc $out
