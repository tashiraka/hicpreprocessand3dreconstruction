import argparse, operator

parser = argparse.ArgumentParser(
    description='''
    This script is tool to make mappability table.\n
    ''',
    epilog='''
    Author: Yuichi Motai
    ''')
parser.add_argument('-v', '--version', action='version', version='version 1.0')
parser.add_argument('sam', help='SAM file, mapped-back simulated reads.')
parser.add_argument('rfrag_file', help='rfrag_table file.')
parser.add_argument('chr_num', help='the number of chromosome.')
parser.add_argument('MAPQ_threshold', help='MAPQ thresold, default 30.', type=int)
parser.add_argument('window_size', help='window size to calculate mappability.', type=int)
parser.add_argument('out_file', help='output file')
args = parser.parse_args()


# This chromosome name parser is for hg38 and oryLat2.
def chrname_to_id(name):
    id = name.split('chr')[1]
    if id == 'X':
        return '23'
    elif id == 'Y':
        return '24'
    else:
        return id


class Fend:
    def __init__(self, a, b, c, d):
        self.fendID = a
        self.fragID = b
        self.chrID = c
        self.coord = d
        self.mappability = float()
        self.mapped_total = 0
        self.mapped_unique = 0


class Read:
    def __init__(self, a, b, c):
        self.chrID = a
        self.position = b # 1-origin leftmost
        self.mapq = c
        

fout = open(args.out_file, 'w')

# input fends.
# I assumed the rfrag file entry is sorted by fragment positions.
fends = [[] for i in range(args.chr_num)]
for rfrag in open(args.rfrag_file, 'r'):
    fields = line.rstrip('\n').split('\t') # <fragment ID>\t<chromosome ID>\t<start position>\t<end position>
    chrID = int(fields[1]) - 1

    if(fends[chrID][-1].coord >= int(fields[1])):
        print 'ERROR: rfrag is not sorted'
        quit()
        
    fends[chrID] += [Fend(int(fields[0]) * 2 - 1, fields[0], int(fields[1]), fields[2])]
    fends[chrID] += [Fend(int(fields[0]) * 2, fields[0], int(fields[1]), fields[3])]
    

# input SAM file.
# I assumed the SAM file entry is sorted by thier positions.
# eg. samtools sort <in.bam> <out.prefix>
reads = [[] for i in range(args.chr_num)]
for read in open(args.sam, 'r'):
    fields = line.rstrip('\n').split('\t') # SAM format
    chrID = chrname_to_id(fields[2])

    if(reads[chrID][-1].position >= int(fields[3])):
        print 'ERROR: rfrag is not sorted'
        quit()
    
    reads[chrID] += Reads(chrID, int(fields[3]), fields[4])
    

# calculate mappability.
# It is necessary to associate reads to nearest fends.
for chrID in range(args.chr_num):
    fend_id = 0
    for read in reads[chrID]:
        while(read.position < fends[chrID][fend_id])
