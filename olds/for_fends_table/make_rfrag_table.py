# -*- coding: utf-8 -*-
from Bio import SeqIO, Restriction, Seq, SeqUtils
import numpy as np
import argparse


parser = argparse.ArgumentParser(
    description='''
    This script is tool to making a restriction-fragments table for hicpipe.\n
    A half open interval, [a, b], represents a restriction fragment.\n
    Output file column: <fragment ID>\t<chromosome ID>\t<start position>\t<end position>\n
    Caution: fragment ID is 1-origin, the coodinate system is 0-origin.
    ''',
    epilog='''
    Author: Yuichi Motai
    ''')
parser.add_argument('-v', '--version', action='version', version='version 1.0')
parser.add_argument('refGenome', help='fasta file containing reference genome')
parser.add_argument('RE', help='restriction enzyme name')
parser.add_argument('out_file', help='output file')
args = parser.parse_args()


# This chromosome name parser is for UCSC human (eg. hg38).
def chrname_to_id(name):
    id = name.split('chr')[1]
    if id == 'X':
        return '23'
    elif id == 'Y':
        return '24'
    else:
        return id


re = getattr(Restriction, args.RE)
fout = open(args.out_file, "w")
fragID = 1

# Restriction cut site position, e.g. EcoRI cut 'GAATTC' to 'G' and 'AATTC',
# then cutting position is 2 (1-origin and base after the cut site).
for chrom in SeqIO.parse(open(args.refGenome, 'r'), 'fasta') :
    chrID = chrname_to_id(chrom.name)
    prevRSite = 1
    
    for rsite in re.search(chrom.seq) :
        fout.write(str(fragID) + '\t' + chrID + '\t' + str(prevRsite) + '\t' + str(rsite - 1))
        prevRSite = rsite
        fragID += 1
    else:
        fout.write(str(fragID) + '\t' + chrID + "\t" + str(prevRsite) + "\t" + str(len(chrom.seq)))
        fragID += 1
