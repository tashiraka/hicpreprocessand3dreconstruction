from Bio import SeqIO, Seq, SeqUtils
import numpy as np
import argparse


parser = argparse.ArgumentParser(
    description='''
    This script is tool to make simulated reads.\n
    simulated reads are uniformly generated from a reference genome.
    ''',
    epilog='''
    Author: Yuichi Motai
    ''')
parser.add_argument('-v', '--version', action='version', version='version 1.0')
parser.add_argument('refGenome', help='fasta file containing reference genome')
parser.add_argument('interval', help='interval between starting points of simulated reads. If start each base, set 1.', type=int)
parser.add_argument('length', help='length of an simulated read.', type=int)
parser.add_argument('out_file', help='output FASTA file')
args = parser.parse_args()


# This chromosome name parser is for UCSC human (eg. hg38).
def chrname_to_id(name):
    id = name.split('chr')[1]
    if id == 'X':
        return '23'
    elif id == 'Y':
        return '24'
    else:
        return id


fout = open(args.out_file, "w")

for chrom in SeqIO.parse(open(args.refGenome, 'r'), 'fasta') :
    chr_len = len(chrom.seq)
    start_point = 0

    while start_point + args.length < chr_len:
        fout.write('>' + chrom.name + '_' + str(start_point + 1)  + '\n')
        fout.write(str(chrom.seq[start_point : start_point + args.length]) + '\n')
        start_point += args.interval
