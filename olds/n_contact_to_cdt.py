'''
Format for TreeView
'''
import sys, math

args=sys.argv
if (len(args) !=4):
        print "Usage: >python %s <contact_frequency_matrix> <out_file>" \
                % args[0]
        quit()

cfmFile = args[1]
outFile = args[2]

cbins_fin = open(cbins_file, 'r')
cbins_fin.readline()
bins = []
prev = 0
for line in cbins_fin:
	fields = line.rstrip().split('\t')
	bins += [fields[1] + '-' + fields[2]]
	if(int(fields[0]) != prev + 1):
		print 'ERROR : The cbin index are not continous.'
		quit()
	prev = int(fields[0])

n_contact_fin = open(n_contact_file, 'r')
n_contact_fin.readline()
contact_matrix = [[0 for j in range(len(bins))] for i in range(len(bins))]
for line in n_contact_fin:
	fields = line.rstrip().split('\t')
	contact_matrix[int(fields[0]) - 1][int(fields[1]) - 1] = math.log10((float(fields[3]) + 1.0) / (float(fields[2]) + 1.0))
	#contact_matrix[int(fields[0]) - 1][int(fields[1]) - 1] = float(fields[3]) / float(fields[2])
	#contact_matrix[int(fields[0]) - 1][int(fields[1]) - 1] = float(fields[3])
	contact_matrix[int(fields[1]) - 1][int(fields[0]) - 1] = contact_matrix[int(fields[0]) - 1][int(fields[1]) - 1]

fout = open(out_file, 'w')
fout.write('HiCMatrix\tRegions')
for b in bins:
	fout.write('\t' + b)
fout.write('\n')

for i, b in enumerate(bins):
	fout.write(b + '\t' + b)
	for contact in contact_matrix[i]:
		fout.write('\t' + str(contact))
	fout.write('\n')

