# -*- coding: utf-8 -*-
import argparse

parser = argparse.ArgumentParser(
    description='''
    This script is tool to making a paired-read table for hicpipe.\n
    Output coodinate system is 1-origin same as SAM format.
    ''',
    epilog='''
    Author: Yuichi Motai
    ''')
parser.add_argument('-v', '--version', action='version', version='version 1.0')
parser.add_argument('SAM1', help='an input SAM file 1')
parser.add_argument('SAM2', help='an input SAM file 2')
parser.add_argument('out_file', help='an output file')
args = parser.parse_args()


# This chromosome name parser is for UCSC human (eg. hg38).
def chrname_to_id(name):
    id = name.split('chr')[1]
    if id == 'X':
        return '23'
    elif id == 'Y':
        return '24'
    else:
        return id

fin1 = open(args.SAM1, "r")
fin2 = open(args.SAM2, 'r')
fout = open(args.out_file, "w")

fout.write("chr1\tcoord1\tstrand1\tchr2\tcoord2\tstrand2\n")
line1 = fin1.readline()
line2 = fin2.readline()


while line1 and line2:
    fields1 = line1.split('\t')
    fields2 = line2.split('\t')
    strand1 = '-' if int(fields1[1]) & 16 == 16 else '+'
    strand2 = '-' if int(fields2[1]) & 16 == 16 else '+'
    fout.write(chrname_to_id(fields1[2]) + '\t' + fields1[3] + '\t' + strand1 + '\t' + \
               chrname_to_id(fields2[2]) + '\t' + fields2[3] + '\t' + strand2 + '\n')
    line1 = fin1.readline()
    line2 = fin2.readline()

