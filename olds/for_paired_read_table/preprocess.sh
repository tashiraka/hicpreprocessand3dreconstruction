#!/bin/bash


usage() {
    cat <<EOF
 
 DESCRIPTION:
     $(basename $0) is tool for preprocess SAM files from Hi-C.
 
 USAGE:
     sh $(basename $0) [-vh] [-m MAPQ-threshold] <in SAM file 1> <in SAM file 2> <out SAM file 1> <out SAM file 2>
 
 OPTIONS:
     -m MAPQ-Threshold
         threshold of MAPQ, output MAPQ>=(this value) reads, default 30.
     -v
         print $(basename $0) version.
     -h
         print this.
 
 EXAMPLES:
     sh $(basename $0) -m 10 in1.sam in2.sam out1.sam out2.sam >out.log 2>err.log
 
 AUTHOR:
     written by Yuichi Motai.
 
EOF
}


version() {
    echo "version 1.0"
}


# parsing argments
MAPQThreshold=30 #default

while getopts "m:vh" opt;
do
    case $opt in
        m)  MAPQThreshold=$OPTARG
            ;;
        v)  version
            exit 0
            ;;
        h)  usage
            exit 0
            ;;
        :)
            echo "ERROR: $opt needs value." 1>&2
            usage
            exit 1

            ;;
        /?)
            echo "ERROR: invalid option" 1>&2
            usage
            exit 1
            ;;
    esac
done


shift `expr $OPTIND - 1`
if [ $# -ne 4 ]; then
    echo "ERROR: the number of argment is $#, but needs 4." 1>&2
    usage
    exit 1
fi

in1=$1
in2=$2
out1=$3
out2=$4
dir1=$(cd $(dirname $in1); pwd)


# temporary directory and files
tmpdir=`mktemp -d -p $dir1`
tmp1=`mktemp -p $tmpdir`
tmp2=`mktemp -p $tmpdir`
tmp3=`mktemp -p $tmpdir`
trap "rm -rf $tmpdir" EXIT


echo "The log of preprocess for $in1 and $in2"


#############################################################
# The following processes extracts "uniquely" mapped reads. #
#############################################################
echo -en "\tinput mapped reads in $in1: "
samtools view -Sc $in1

# removing unmapped (do not have a valid alignment) and secondary-mapped reads
samtools view -hbS -F 0x0104 $in1 >$tmp1
echo -en "\tnot-secondary valid aligned reads in $in1: "
samtools view -c $tmp1

# removing low-MAPQ(<$MAPQThreshold) reads
samtools view -q $MAPQThreshold $tmp1 >$tmp2
echo "" > $tmp1
echo -en "\tMAPQ>=$MAPQThreshold reads in $in1: "
cat $tmp2 | wc -l


echo -en "\tinput mapped reads in $in2: "
samtools view -Sc $in2

# removing unmapped (do not have a valid alignment) and secondary-mapped reads
samtools view -hbS -F 0x04 $in2 \
    | samtools view -bh -F 0x100 - >$tmp1
echo -en "\tnot-secondary valid aligned reads in $in2: "
samtools view -c $tmp1

# removing low-MAPQ(<$MAPQThreshold) reads
samtools view -q $MAPQThreshold $tmp1 >$tmp3
echo "" > $tmp1
echo -en "\tMAPQ>=$MAPQThreshold reads in $in2: "
cat $tmp3 | wc -l


##################################################
# The following processes extracts paired reads. #
##################################################
touch $out1 $out2
cat $tmp2 $tmp3 \
    | sort -T/grid2 -t"	" -k1,1 \
    | awk -F "\t" -v out1=$out1 -v out2=$out2 '
          NR==1 {
              prev=$0
              prevName=$1
          } 
          NR>1 { 
              if($1==prevName) {
                  print prev > out1
                  print $0 > out2
              }
              prev=$0
              prevName=$1
          }'
echo "" > $tmp2
echo "" > $tmp3

echo -en "\tpaired reads : "
cat $out1 | wc -l
