import std.stdio;
import std.getopt;
import std.conv;
import core.stdc.stdlib;
import std.array;
import std.string;
import std.algorithm;

void usage()
{
  writeln(r"
 DESCRIPTION:
     This command is a tool to map paired reads to restriction fragments
     and get the distribution of dA + dB. The output is the list of dA and dB.

 USAGE:
     command [-vh] <in SAM1> <in SAM2> <in fragment file> <genome size file> <out file>
 
 ARGMENTS:
     in SAM1, in SAM2
         paired SAM files for input.
     in fragment file
         restriction fragment file, format: <fragmentID>\t<chrName>\t<start>\t<end>
     genome size file
         <chrName>\t<size>
     out file
         file for output.

 OPTIONS:
     -v
         print version.
     -h
         print this.
 
 AUTHOR:
     written by Yuichi Motai.
");

  exit(0);
}


void thisVersion() {
  writeln("version 1.0");
  exit(0);
}


/*-------------main----------------*/
version(unittest) {}
 else {
   void main(string[] args)
   {
     /*--------parsing option---------------*/
     try {
       getopt(args, "version|v", &thisVersion, "help|h", &usage);

       if(args.length != 6)
         throw new Exception("ERROR: the number of argments illeagal.");
     }
     catch(GetOptException e) {
       writeln("ERROR: invalid option\n", e.toString);
       usage();
     }
     catch(Exception e) {
       writeln(e.toString);
       usage();
     }

     auto samFile1 = args[1];
     auto samFile2 = args[2];
     auto fragmentFile = args[3];
     auto sizeFile = args[4];
     auto outFile = args[5];
     
     /*--------main processes---------------*/
     auto pairedReads = readSam(samFile1, samFile2);
     pairedReads = pairedReads.filter!(x => x.chrName(Pair.first) != "chrM"
                                       && x.chrName(Pair.second) != "chrM").array;

     uint[string] size;
     foreach(line; File(sizeFile, "r").byLine) {
       auto fields = line.to!string.strip.split("\t");
       size[fields[0]] = fields[1].to!uint;
     }

     auto fragmentEndForMinusStrand = readFragmentStart(fragmentFile, size); 
     auto fragmentEndForPlusStrand = readFragmentEnd(fragmentFile, size);
     
     foreach(ref pairedRead; pairedReads)
       pairedRead.searchFragment(fragmentEndForMinusStrand,
                                 fragmentEndForPlusStrand);

     auto fout = File(outFile, "w");
     foreach(pairedRead; pairedReads) {
       fout.writeln(pairedRead.dA);
       fout.writeln(pairedRead.dB);
     }
   }
 }


uint[][string] readFragmentStart(string file, uint[string] size)
{
  uint[][string] fragmentEndForMinusStrand;
  foreach(key; size.byKey) {
    fragmentEndForMinusStrand[key] = new uint[](size[key]);
    fragmentEndForMinusStrand[key].fill(0);
  }

  foreach(line; File(file, "r").byLine) {
    auto fields = line.to!string.strip.split("\t");
    fragmentEndForMinusStrand[fields[1]][fields[2].to!uint] = fields[2].to!uint;
  }
  
  foreach(ref fragmentEnds; fragmentEndForMinusStrand) {
    auto prev = 0;
    foreach(ref elem; fragmentEnds) {
      if(elem == 0) elem = prev;
      else prev = elem;
    }
  }
  return fragmentEndForMinusStrand;  
}


uint[][string] readFragmentEnd(string file, uint[string] size)
{
  uint[][string] fragmentEndForPlusStrand;
  foreach(key; size.byKey) {
    fragmentEndForPlusStrand[key] = new uint[](size[key]);
    fragmentEndForPlusStrand[key].fill(0);
  }
  
  foreach(line; File(file, "r").byLine) {
    auto fields = line.to!string.strip.split("\t");
    fragmentEndForPlusStrand[fields[1]][fields[3].to!uint] = fields[3].to!uint;
  }
  foreach(ref fragmentEnds; fragmentEndForPlusStrand) {
    auto prev = 0;
    foreach_reverse(ref elem; fragmentEnds) {
      if(elem == 0) elem = prev;
      else prev = elem;
    }
  }
  return fragmentEndForPlusStrand;
}


PairedRead[] readSam(string file1, string file2)
{
  auto app = appender!(PairedRead[]);
  foreach(line; File(file1, "r").byLine) {
    if(line[0] != '@') {
      auto samEntry = new SamParser(line);
      app.put(new PairedRead(samEntry.rname, samEntry.pos,
                             samEntry.seq.length, samEntry.strand));
    }
  }
  
  auto pairedReads = app.data;
  auto i = 0;
  foreach(line; File(file2, "r").byLine) {
    if(i >= pairedReads.length)
      throw new Exception("Error: the number of entries in paired SAM files are different.");
      
    if(line[0] != '@') {
      auto samEntry = new SamParser(line);
      pairedReads[i++].putSecond(samEntry.rname, samEntry.pos,
                                 samEntry.seq.length, samEntry.strand);
    }
  }
  return pairedReads;
}


class SamParser
{
private:
  string qname_;
  uint flag_;
  string rname_;
  uint pos_;
  uint mapq_;
  string cigar_;
  string rnext_;
  uint pnext_;
  int tlen_;
  string seq_;
  string qual_;
  string[] optional_;

public:
  this(char[] entry) {
    auto fields = entry.to!string.strip.split('\t');
    qname_ = fields[0];
    flag_ = fields[1].to!uint;
    rname_ = fields[2];
    pos_ = fields[3].to!uint;
    mapq_ = fields[4].to!uint;
    cigar_ = fields[5];
    rnext_ = fields[6];
    pnext_ = fields[7].to!uint;
    tlen_ = fields[8].to!int;
    seq_ = fields[9];
    qual_ = fields[10];
    if(fields.length > 11)
      optional_ = fields[11..$];
  }

  @property {
    string qname() {return qname_;}
    uint flag() {return flag_;}
    string rname() {return rname_;}
    uint pos() {return pos_;}
    uint mapq() {return mapq_;};
    string cigar() {return cigar_;};
    string rnext() {return rnext_;};
    uint pnext() {return pnext_;};
    int tlen() {return tlen_;};
    string seq() {return seq_;};
    string qual() {return qual_;};
    string[] optional() {return optional_;};
    char strand() {
      if((flag & 0x10) == 0x10) return '-';
      else return '+'; 
    }
  }
}


enum Pair { first, second }


class PairedRead
{
private:
  string[2] chrName_ = ["chrI", "chrI"];
  uint[2] position_ = [0, 0];
  ulong[2] length_ = [0, 0];
  char[2] strand_ = ['+', '+'];
  uint[2] fragmentEndPosition_;
  
public:
  this(string chr, uint pos, ulong len, char str) {
    chrName_[0]=chr; position_[0]=pos; length_[0]=len; strand_[0]=str;
  }

  void putSecond(string chr, uint pos, ulong len, char str) {
    chrName_[1]=chr; position_[1]=pos; length_[1]=len; strand_[1]=str;
  }
  
  @property {
    string chrName(Pair p) {
      if(p == Pair.first) return chrName_[0];
      else return chrName_[1];
    }    
    uint position(Pair p) {
      if(p == Pair.first) return position_[0];
      else return position_[1];
    }
    ulong len(Pair p) {
      if(p == Pair.first) return length_[0];
      else return length_[1];
    }
    char strand(Pair p) {
      if(p == Pair.first) return strand_[0];
      else return strand_[1];
    }
    uint dA() {
      return abs(position_[0] - fragmentEndPosition_[0]);
    }
    uint dB() {
      return abs(position_[1] - fragmentEndPosition_[1]);
    }
  }
  
  void searchFragment(uint[][string] fragmentEndForMinusStrand,
                      uint[][string] fragmentEndForPlusStrand) {
    if(this.strand(Pair.first) == '+') {
      fragmentEndPosition_[0]
        = fragmentEndForPlusStrand[this.chrName(Pair.first)][this.position(Pair.first)];
    }
    else {
      fragmentEndPosition_[0]
        = fragmentEndForMinusStrand[this.chrName(Pair.first)][this.position(Pair.first)];
    }
    
    if(this.strand(Pair.second) == '+') {
      fragmentEndPosition_[1]
        = fragmentEndForPlusStrand[this.chrName(Pair.second)][this.position(Pair.second)];
    }
    else {
      fragmentEndPosition_[1]
        = fragmentEndForMinusStrand[this.chrName(Pair.second)][this.position(Pair.second)];
    }
  }
}

class Fragment {
private:
  uint id_ = 0;
  string chrName_ = "chrI";
  uint start_ = 0;
  uint end_ = 0;

public:
  this(uint id, string chr, uint start, uint end) {
    id_=id; chrName_=chr; start_=start; end_=end;
  }

  @property {
    uint id() {return id_;}
    string chrName() {return chrName_;}
    uint start() {return start_;}
    uint end() {return end_;}
  }

  override string toString() {
    return chrName ~ "\t" ~ start_.to!string ~ "\t" ~ end_.to!string;
  }
}
  
  
