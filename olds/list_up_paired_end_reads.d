import std.stdio, std.getopt, std.conv, std.array, std.string, core.stdc.stdlib;


void usage()
{
  writeln(r"
 DESCRIPTION:
     This command is a tool to change paired SAM format to the format \n
     for downstream analysis.
     The output format is:
     <ID>\t<chrName1>\t<position1>\t<length1>\t<strand>\t<chrName2>\t<position2>\t<length2>\t<strand>

 USAGE:
     command [-vh] <in SAM1> <in SAM2> <out file>
 
 ARGMENTS:
     in SAM1, in SAM2
         paired SAM files for input.
     out file
         file for output.

 OPTIONS:
     -v
         print version.
     -h
         print this.
 
 AUTHOR:
     written by Yuichi Motai.
");

  exit(0);
}


void thisVersion() {
  writeln("version 1.0");
  exit(0);
}


/*-------------main----------------*/
version(unittest) {}
 else {
   void main(string[] args)
   {
     /*--------parsing option---------------*/
     try {
       getopt(args, "version|v", &thisVersion, "help|h", &usage);

       if(args.length != 4)
         throw new Exception("ERROR: the number of argments illeagal.");
     }
     catch(GetOptException e) {
       writeln("ERROR: invalid option\n", e.toString);
       usage();
     }
     catch(Exception e) {
       writeln(e.toString);
       usage();
     }

     auto samFile1 = args[1];
     auto samFile2 = args[2];
     auto outFile = args[3];
     
     /*--------main processes---------------*/
     auto pairedReads = readSam(samFile1, samFile2);
     auto fout = File(outFile, "w");
     foreach(id, pairedRead; pairedReads)
       fout.writeln(id.to!string ~ "\t" ~ pairedRead.toDownstreamFormat);
   }
 }


PairedRead[] readSam(string file1, string file2)
{
  auto app = appender!(PairedRead[]);
  
  foreach(line; File(file1, "r").byLine) {
    auto samEntry = new SamParser(line);
    app.put(new PairedRead(samEntry.rname, samEntry.pos,
                           samEntry.seq.length, samEntry.strand));
  }

  auto pairedReads = app.data;
  auto i = 0;
  foreach(line; File(file2, "r").byLine) {
    auto samEntry = new SamParser(line);
    pairedReads[i++].putSecond(samEntry.rname, samEntry.pos,
                               samEntry.seq.length, samEntry.strand);
  }

  return pairedReads;
}


class SamParser
{
private:
  string qname_;
  uint flag_;
  string rname_;
  uint pos_;
  uint mapq_;
  string cigar_;
  string rnext_;
  uint pnext_;
  int tlen_;
  string seq_;
  string qual_;
  string[] optional_;

public:
  this(char[] entry) {
    auto fields = entry.to!string.strip.split('\t');
    qname_ = fields[0];
    flag_ = fields[1].to!uint;
    rname_ = fields[2];
    pos_ = fields[3].to!uint;
    mapq_ = fields[4].to!uint;
    cigar_ = fields[5];
    rnext_ = fields[6];
    pnext_ = fields[7].to!uint;
    tlen_ = fields[8].to!int;
    seq_ = fields[9];
    qual_ = fields[10];
    optional_ = fields[11..$];
  }

  @property {
    string qname() {return qname_;}
    uint flag() {return flag;}
    string rname() {return rname;}
    uint pos() {return pos_;}
    uint mapq() {return mapq_;};
    string cigar() {return cigar_;};
    string rnext() {return rnext_;};
    uint pnext() {return pnext_;};
    int tlen() {return tlen_;};
    string seq() {return seq_;};
    string qual() {return qual_;};
    string[] optional() {return optional_;};
    char strand() {
      if((flag & 0x10) == 0x10) return '-';
      else return '+'; 
    }
  }
}


enum Pair { first, second }


class PairedRead
{
private:
  string[2] chrName_ = ["chrI", "chrI"];
  uint[2] position_ = [0, 0];
  ulong[2] length_ = [0, 0];
  char[2] strand_ = ['+', '+'];

public:
  this(string chr, uint pos, ulong len, char str) {
    chrName_[0]=chr; position_[0]=pos; length_[0]=len; strand_[0]=str;
  }

  void putSecond(string chr, uint pos, ulong len, char str) {
    chrName_[1]=chr; position_[1]=pos; length_[1]=len; strand_[1]=str;
  }
  
  @property {
    string chrName(Pair p) {
      if(p == Pair.first) return chrName_[0];
      else return chrName_[1];
    }    
    uint position(Pair p) {
      if(p == Pair.first) return position_[0];
      else return position_[1];
    }
    ulong len(Pair p) {
      if(p == Pair.first) return length_[0];
      else return length_[1];
    }
    char strand(Pair p) {
      if(p == Pair.first) return strand_[0];
      else return strand_[1];
    }
  }


  string toDownstreamFormat() {
    return this.chrName(Pair.first) ~ "\t" ~ this.position(Pair.first).to!string
      ~ "\t" ~ this.len(Pair.first).to!string ~ "\t" ~ this.strand(Pair.first)
      ~ "\t" ~ this.chrName(Pair.second) ~ "\t" ~ this.position(Pair.second).to!string
      ~ "\t" ~ this.len(Pair.second).to!string ~ "\t" ~ this.strand(Pair.second);
  }
}

