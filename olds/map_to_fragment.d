import std.stdio;
import std.getopt;
import std.conv;
import core.stdc.stdlib;
import std.array;
import std.string;

void usage()
{
  writeln(r"
 DESCRIPTION:
     This command is a tool to map paired reads to restriction fragments.
     The output format is:
     <chrName1>\t<position1>\t<length1>\t<strand>\t<fragmentChrName1>\t<fragmentStart1>\t<fragmentEnd1>\t<chrName2>\t<position2>\t<length2>\t<strand>\t<fragmentChrName2>\t<fragmentStart2>\t<fragmentEnd2>

 USAGE:
     command [-vh] <in SAM1> <in SAM2> <in fragment file> <out file>
 
 ARGMENTS:
     in SAM1, in SAM2
         paired SAM files for input.
     in fragment file
         restriction fragment file, format: <chrName>\t<start>\t<end>
     out file
         file for output.

 OPTIONS:
     -v
         print version.
     -h
         print this.
 
 AUTHOR:
     written by Yuichi Motai.
");

  exit(0);
}


void thisVersion() {
  writeln("version 1.0");
  exit(0);
}


/*-------------main----------------*/
version(unittest) {}
 else {
   void main(string[] args)
   {
     /*--------parsing option---------------*/
     try {
       getopt(args, "version|v", &thisVersion, "help|h", &usage);

       if(args.length != 5)
         throw new Exception("ERROR: the number of argments illeagal.");
     }
     catch(GetOptException e) {
       writeln("ERROR: invalid option\n", e.toString);
       usage();
     }
     catch(Exception e) {
       writeln(e.toString);
       usage();
     }

     auto samFile1 = args[1];
     auto samFile2 = args[2];
     auto fragmentFile = args[3];
     auto outFile = args[4];
     
     /*--------main processes---------------*/
     auto pairedReads = readSam(samFile1, samFile2);
     auto fragments = readFragment(fragmentFile); // assume sorted fragments
     foreach(ref pairedRead; pairedReads)
       pairedRead.searchFragment(fragments);
     auto fout = File(outFile, "w");
     foreach(pairedRead; pairedReads)
       fout.writeln(pairedRead.toString);
   }
 }


Fragment[][string] readFragment(string file)
{
  Fragment[][string] fragments;
  
  foreach(line; File(file, "r").byLine) {
    auto fields = line.to!string.strip.split("\t");
    fragments[fields[0]] ~= [new Fragment(fields[0],
                                          fields[1].to!uint,
                                          fields[2].to!uint)];
  }
  
  return fragments;
}


PairedRead[] readSam(string file1, string file2)
{
  auto app = appender!(PairedRead[]);
  
  foreach(line; File(file1, "r").byLine) {
    auto samEntry = new SamParser(line);
    app.put(new PairedRead(samEntry.rname, samEntry.pos,
                           samEntry.seq.length, samEntry.strand));
  }

  auto pairedReads = app.data;
  auto i = 0;
  foreach(line; File(file2, "r").byLine) {
    auto samEntry = new SamParser(line);
    pairedReads[i++].putSecond(samEntry.rname, samEntry.pos,
                               samEntry.seq.length, samEntry.strand);
  }

  return pairedReads;
}


class SamParser
{
private:
  string qname_;
  uint flag_;
  string rname_;
  uint pos_;
  uint mapq_;
  string cigar_;
  string rnext_;
  uint pnext_;
  int tlen_;
  string seq_;
  string qual_;
  string[] optional_;

public:
  this(char[] entry) {
    auto fields = entry.to!string.strip.split('\t');
    qname_ = fields[0];
    flag_ = fields[1].to!uint;
    rname_ = fields[2];
    pos_ = fields[3].to!uint;
    mapq_ = fields[4].to!uint;
    cigar_ = fields[5];
    rnext_ = fields[6];
    pnext_ = fields[7].to!uint;
    tlen_ = fields[8].to!int;
    seq_ = fields[9];
    qual_ = fields[10];
    optional_ = fields[11..$];
  }

  @property {
    string qname() {return qname_;}
    uint flag() {return flag;}
    string rname() {return rname;}
    uint pos() {return pos_;}
    uint mapq() {return mapq_;};
    string cigar() {return cigar_;};
    string rnext() {return rnext_;};
    uint pnext() {return pnext_;};
    int tlen() {return tlen_;};
    string seq() {return seq_;};
    string qual() {return qual_;};
    string[] optional() {return optional_;};
    char strand() {
      if((flag & 0x10) == 0x10) return '-';
      else return '+'; 
    }
  }
}


enum Pair { first, second }


class PairedRead
{
private:
  string[2] chrName_ = ["chrI", "chrI"];
  uint[2] position_ = [0, 0];
  ulong[2] length_ = [0, 0];
  char[2] strand_ = ['+', '+'];
  Fragment[2] fragment_;
  
public:
  this(string chr, uint pos, ulong len, char str) {
    chrName_[0]=chr; position_[0]=pos; length_[0]=len; strand_[0]=str;
  }

  void putSecond(string chr, uint pos, ulong len, char str) {
    chrName_[1]=chr; position_[1]=pos; length_[1]=len; strand_[1]=str;
  }
  
  @property {
    string chrName(Pair p) {
      if(p == Pair.first) return chrName_[0];
      else return chrName_[1];
    }    
    uint position(Pair p) {
      if(p == Pair.first) return position_[0];
      else return position_[1];
    }
    ulong len(Pair p) {
      if(p == Pair.first) return length_[0];
      else return length_[1];
    }
    char strand(Pair p) {
      if(p == Pair.first) return strand_[0];
      else return strand_[1];
    }
    Fragment fragment(Pair p) {
      if(p == Pair.first) return fragment_[0];
      else return fragment_[1];
    }
  }

  void searchFragment(Fragment[][string] fragments) {
    fragment_[0] = binarySearch(Pair.first,
                                fragments[this.chrName(Pair.first)]);
    fragment_[1] = binarySearch(Pair.first,
                                fragments[this.chrName(Pair.first)]);
  }

  override string toString() {
    return this.chrName(Pair.first) ~ "\t" ~ this.position(Pair.first).to!string
      ~ "\t" ~ this.len(Pair.first).to!string ~ "\t" ~ this.strand(Pair.first)
      ~ "\t" ~ this.fragment(Pair.first).toString ~ "\t"
      ~ this.chrName(Pair.second) ~ "\t" ~ this.position(Pair.second).to!string
      ~ "\t" ~ this.len(Pair.second).to!string ~ "\t" ~ this.strand(Pair.second)
      ~ "\t" ~ this.fragment(Pair.second).toString;
  }
  
private:
  Fragment binarySearch(Pair p, Fragment[] fragments) {
    ulong startID = 0;
    ulong endID = fragments.length - 1;
    
    if(this.strand(p) == '+') {
      while(startID < endID) {
        auto middleID = (startID + endID) / 2;
        if(this.position(p) < fragments[middleID].end) {
          endID = middleID;
        } else {
          startID = middleID;
        }
      }
      if(startID > endID)
        throw new Exception("ERROR: binary search startID > endID");

      return fragments[startID];
    }
    else {
      while(startID < endID) {
        auto middleID = (startID + endID) / 2;
        if(fragments[middleID].start < this.position(p)) {
          startID = middleID;
        } else {
          endID = middleID;
        }
      }
      if(startID > endID)
        throw new Exception("ERROR: binary search startID > endID");

      return fragments[startID];
    }
  }
}


class Fragment {
private:
  string chrName_ = "chrI";
  uint start_ = 0;
  uint end_ = 0;

public:
  this(string chr, uint start, uint end) {
    chrName_=chr; start_=start; end_=end;
  }

  @property {
    string chrName() {return chrName_;}
    uint start() {return start_;}
    uint end() {return end_;}
  }

  override string toString() {
    return chrName ~ "\t" ~ start_.to!string ~ "\t" ~ end_.to!string;
  }
}

