import std.stdio, std.conv, std.string;

version(unittest){}
 else{
   void main(string[] args)
   {
     auto samFile = args[1];
     auto outFile = args[2];

     auto fout = File(outFile, "w");
     foreach(line; File(samFile, "r").byLine) 
       if(line[0] == '@' ||
          line.to!string.strip.split("\t")[2] != "chrM") 
         fout.writeln(line);
   }
 }
