#!/bin/bash

#---------usage---------------------
usage() {
    cat <<EOF
 
 DESCRIPTION:
     $(basename $0) is tool to map reads truncated iteratively.
     This method improves the number of mapped reads 
     because Hi-C reads over ligated positions could be uniquely mapped.

 USAGE:
     sh $(basename $0) [-vh] [-p N_THREADS] [-m MIN_LEN] [-s LEN_STEP] [-l MAX_LEN] <genome> <FASTQ> <SAM>
 
 ARGMENTS:
     genome
         The basename of the index for the reference genome by bowtie2-build.
     FASTQ
         FASTQ file containing reads to be aligned.
     SAM
         File to write SAM alignments to.

 OPTIONS:
     -p N_THREADS
         launch NTHREADS parallel search threads (default: 1).
     -m MIN_LEN
         The truncation length at the first iteration of mapping (default: 25).
     -s LEN_STEP
         The increase in truncation length at each iteration (default: 5).
     -l MAX_LEN
         The maximum length of reads. (default: 50).
     -v
         print $(basename $0) version.
     -h
         print this.

 EXAMPLES:
     sh $(basename $0) -p 12 -m 20 -s 10 -l 70 genome.bt2 in.fastq out.sam >out.log 2>err.log
 
 AUTHOR:
     written by Yuichi Motai.
 
EOF
}


version() {
    echo "version 1.0"
}


#------------parsing arguments------------------
n_threds=1
min_len=25
len_step=5
read_len=50

while getopts ":p:m:s:l:vh" opt;
do
    case $opt in
        p)  n_threds=$OPTARG
            ;;
        m)  min_len=$OPTARG
            ;;
        s)  len_step=$OPTARG
            ;;
        l)  read_len=$OPTARG
            ;;

        v)  version
            exit 0
            ;;
        h)  usage
            exit 0
            ;;
        :)
            echo "ERROR: option -$OPTARG needs value." 1>&2
            usage
            exit 1

            ;;
        ?)
            echo "ERROR: invalid option -$OPTARG" 1>&2
            usage
            exit 1
            ;;
    esac
done


shift `expr $OPTIND - 1`
if [ $# -ne 3 ]; then
    echo "ERROR: the number of argment is $#, but needs 3." 1>&2
    usage
    exit 1
fi


genome=$1
fastq=$2
sam=$3


#--------temporary files----------------
dir=$(cd $(dirname $sam); pwd)
tmp1=`mktemp -p $dir`
tmp2=`mktemp -p $dir`
touch ${tmp1}.bam
touch ${tmp2}.bam
trap "rm $tmp1 $tmp2 ${tmp1}.bam ${tmp2}.bam" EXIT


#--------main process-------------------
trim_3=`expr $read_len - $min_len`
terminator=`expr 0 - $len_step`

echo "The length, `expr $read_len - $trim_3`, reads mapping:"
bowtie2 --very-sensitive -p $n_threds -3 $trim_3 -x $genome -U $fastq -S $sam
samtools view -uS $sam | samtools sort -n - $tmp1 \
    && samtools view -h ${tmp1}.bam > $tmp1

trim_3=`expr $trim_3 - $len_step`

while [ $trim_3 -gt $terminator ]
do
    # always mapping full length at last.
    if [ $trim_3 -lt 0 ]
    then
        echo "The length, $read_len, reads mapping:"
        bowtie2 --very-sensitive -p $n_threds -x $genome -U $fastq -S $tmp2
    else
        echo "The length, `expr $read_len - $trim_3`, reads mapping:"
        bowtie2 --very-sensitive -p $n_threds -3 $trim_3 -x $genome -U $fastq -S $tmp2
    fi
    
    # choose the entry which have bigger MAPQ from corresponding two entries.
    mv $sam $tmp1
    samtools view -uS $tmp2 | samtools sort -n - $tmp2 \
    && samtools view -h ${tmp2}.bam > $tmp2
    
    awk -F "\t" 'FILENAME == ARGV[1] && $1 == "@HD"
                 FILENAME == ARGV[1] && $1 == "@SQ"
                 FILENAME == ARGV[1] && $1 !~ /^@/ {
                     line1 = $0
                     mapq1 = $5
                     
                     while((getline < ARGV[2]) > 0 && $1 ~ /^@/) {}
                     
                     line2 = $0
                     mapq2 = $5

                     if(mapq1 > mapq2)
                         print line1
                     else
                         print line2
                 }' $tmp1 $tmp2 > $sam
    
    trim_3=`expr $trim_3 - $len_step`
done
