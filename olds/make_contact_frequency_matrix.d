import std.stdio, std.getopt, std.algorithm, std.conv, std.array, std.string, core.stdc.stdlib;


void usage()
{
  writeln(r"
 DESCRIPTION:
     This command is a tool to make a contact frequency matrix from paired SAM\n
     files, filtering invalid Hi-C reads. The row or column represents a \n
     end of restriction fragment.

 USAGE:
     command [-vh] <in SAM1> <in SAM2> <in ifragment file> <contact frequency matrix file> <fragment end list file>
 
 ARGMENTS:
     in SAM1, in SAM2
         paired SAM files for input.
     in fragment file
         restriction fragment file, format: <chrName>\t<start>\t<end>
     contact frequency matrix file
         file for output.
     fragment end list file
         file for output

 OPTIONS:
     -v
         print version.
     -h
         print this.
 
 AUTHOR:
     written by Yuichi Motai.
");

  exit(0);
}


void thisVersion() {
  writeln("version 1.0");
  exit(0);
}


/*-------------main----------------*/
version(unittest) {}
 else {
   void main(string[] args)
   {
     /*--------parsing option---------------*/
     try {
       getopt(args, "version|v", &thisVersion, "help|h", &usage);

       if(args.length != 6)
         throw new Exception("ERROR: the number of argments illeagal.");
     }
     catch(GetOptException e) {
       writeln("ERROR: invalid option\n", e.toString);
       usage();
     }
     catch(Exception e) {
       writeln(e.toString);
       usage();
     }

     auto samFile1 = args[1];
     auto samFile2 = args[2];
     auto fragmentFile = args[3];
     auto contactFile = args[4];
     auto fragmentEndFile = args[5];
       
     /*--------main processes---------------*/
     // assume sorted fragments
     auto fragments = readFragment(fragmentFile);
     auto C = new ContactFrequencyMatrix(fragments);
     foreach(ref pairedRead; readSAM(samFile1, samFile2)) {
       pairedRead.searchFragment(fragments);
       C.putPairedRead(pairedRead);
     }
     auto fout = File(contactFile, "w");
     foreach(row; C) {
       foreach(elem; row)
         fout.write(elem.count, "\t");
       fout.writeln();
     }
     
     fout = File(fragmentEndFile, "w");
     foreach(row; C)
       foreach(fragmentEnd; row.fragmentEnd)
         fout.writeln(fragmentEnd.toString);
   }
 }



uint yeastChrNameToID(string chrName)
{
  switch(chrName){
  case "chrI": return 0; break;
  case "chrII": return 1; break;
  case "chrIII": return 2; break;
  case "chrIV": return 3; break;
  case "chrV": return 4; break;
  case "chrVI": return 5; break;
  case "chrVII": return 6; break;
  case "chrVIII": return 7; break;
  case "chrIX": return 8; break;
  case "chrX": return 9; break;
  case "chrXI": return 10; break;
  case "chrXII": return 11; break;
  case "chrXIII": return 12; break;
  case "chrXIV": return 13; break;
  case "chrXV": return 14; break;
  case "chrXVI": return 15; break;
  default: throw new Exception("ERROR: invalid chromosome name, ", chrName);
  }
}

enum Pair{ first, second }


class ContactFrequencyMatrix
{
private:
  PairedRead[][][] matrix_;
  struct FragmentEnd {
    uint chrID;
    uint position;
  }
  FragmentEnd[] fragmentEnds_;
  
public:
  this(Fragment[][] fragments) {
    auto size = fragments.map!(x => x.length).sum;
    matrix_ = new PairedRead[][][](size, size, 0);

    auto app = Appender!(FragmentEnd[])([]);
    foreach(byChr; fragments)
      foreach(fragment; byChr){
        app.put(FragmentEnd(fragment.chrID, fragment.start));
        app.put(FragmentEnd(fragment.chrID, fragment.end)); 
      }
  }

  void putPairedRead(PairedRead pairedRead) {
    matrix_[pairedRead.fragmentEnd(Pair.first)]
      [pairedRead.fragmentEnd(Pair.second)]
      ~= pairedRead;
  }
}


class Fragment {
private:
  uint chrID_ = 0;
  uint start_ = 0;
  uint end_ = 0;

public:
  this(uint chrID, uint start, uint end) {
    chrID_=chrID; start_=start; end_=end;
  }

  @property {
    uint chrID() {return chrID_;}
    uint start() {return start_;}
    uint end() {return end_;}
  }
}


class PairedRead
{
private:
  uint[2] chrID_ = [0, 0];
  uint[2] position_ = [0, 0];
  ulong[2] length_ = [0, 0];
  char[2] strand_ = ['+', '+'];
  Fragment[2] fragment_;
  
public:
  this(uint chrID, uint pos, ulong len, char str) {
    chrID_[0]=chrID; position_[0]=pos; length_[0]=len; strand_[0]=str;
  }

  void putSecond(uint chrID, uint pos, ulong len, char str) {
    chrID_[1]=chrID; position_[1]=pos; length_[1]=len; strand_[1]=str;
  }
  
  @property {
    uint chrID(Pair p) {
      if(p == Pair.first) return chrID_[0];
      else return chrID_[1];
    }    
    uint position(Pair p) {
      if(p == Pair.first) return position_[0];
      else return position_[1];
    }
    ulong len(Pair p) {
      if(p == Pair.first) return length_[0];
      else return length_[1];
    }
    char strand(Pair p) {
      if(p == Pair.first) return strand_[0];
      else return strand_[1];
    }
    Fragment fragment(Pair p) {
      if(p == Pair.first) return fragment_[0];
      else return fragment_[1];
    }
  }

  void searchFragment(Fragment[][] fragments) {
    fragment_[0] = binarySearch(Pair.first,
                                fragments[this.chrID(Pair.first)]);
    fragment_[1] = binarySearch(Pair.first,
                                fragments[this.chrID(Pair.first)]);
  }

  
private:
  Fragment binarySearch(Pair p, Fragment[] fragments) {
    ulong startID = 0;
    ulong endID = fragments.length - 1;
    
    if(this.strand(p) == '+') {
      while(startID < endID) {
        auto middleID = (startID + endID) / 2;
        if(this.position(p) < fragments[middleID].end) {
          endID = middleID;
        } else {
          startID = middleID;
        }
      }
      if(startID > endID)
        throw new Exception("ERROR: binary search startID > endID");

      return fragments[startID];
    }
    else {
      while(startID < endID) {
        auto middleID = (startID + endID) / 2;
        if(fragments[middleID].start < this.position(p)) {
          startID = middleID;
        } else {
          endID = middleID;
        }
      }
      if(startID > endID)
        throw new Exception("ERROR: binary search startID > endID");

      return fragments[startID];
    }
  }
}



Fragment[][] readFragment(string file)
{
  Fragment[][] fragments;
  
  foreach(line; File(file, "r").byLine) {
    auto fields = line.to!string.strip.split("\t");
    fragments[yeastChrNameToID(fields[0])]
      ~= new Fragment(yeastChrNameToID(fields[0]),
                       fields[1].to!uint,
                       fields[2].to!uint);
  }
  
  return fragments;
}


PairedRead[] readSam(string file1, string file2)
{
  auto app = appender!(PairedRead[]);
  
  foreach(line; File(file1, "r").byLine) {
    auto samEntry = new SamParser(line);
    app.put(new PairedRead(samEntry.rname, samEntry.pos,
                           samEntry.seq.length, samEntry.strand));
  }

  auto pairedReads = app.data;
  auto i = 0;
  foreach(line; File(file2, "r").byLine) {
    auto samEntry = new SamParser(line);
    pairedReads[i++].putSecond(samEntry.rname, samEntry.pos,
                               samEntry.seq.length, samEntry.strand);
  }

  return pairedReads;
}


class SamParser
{
private:
  string qname_;
  uint flag_;
  string rname_;
  uint pos_;
  uint mapq_;
  string cigar_;
  string rnext_;
  uint pnext_;
  int tlen_;
  string seq_;
  string qual_;
  string[] optional_;

public:
  this(char[] entry) {
    auto fields = entry.to!string.strip.split('\t');
    qname_ = fields[0];
    flag_ = fields[1].to!uint;
    rname_ = fields[2];
    pos_ = fields[3].to!uint;
    mapq_ = fields[4].to!uint;
    cigar_ = fields[5];
    rnext_ = fields[6];
    pnext_ = fields[7].to!uint;
    tlen_ = fields[8].to!int;
    seq_ = fields[9];
    qual_ = fields[10];
    optional_ = fields[11..$];
  }

  @property {
    string qname() {return qname_;}
    uint flag() {return flag;}
    string rname() {return rname;}
    uint pos() {return pos_;}
    uint mapq() {return mapq_;};
    string cigar() {return cigar_;};
    string rnext() {return rnext_;};
    uint pnext() {return pnext_;};
    int tlen() {return tlen_;};
    string seq() {return seq_;};
    string qual() {return qual_;};
    string[] optional() {return optional_;};
    char strand() {
      if((flag & 0x10) == 0x10) return '-';
      else return '+'; 
    }
  }
}


