import std.stdio;
import std.getopt;
import std.typecons;
import std.conv;
import core.stdc.stdlib;


void usage() {
  writeln(r"
 DESCRIPTION:
     This command is a tool to truncate Ns from the ends of the reads
     in a FASTQ file.

 USAGE:
     command [-vh] <in FASTQ> <out FASTQ>
 
 ARGMENTS:
     in FASTQ
         FASTQ file for input.
     out FASTQ
         FASTQ file for output.

 OPTIONS:
     -v
         print version.
     -h
         print this.
 
 AUTHOR:
     written by Yuichi Motai.
");

  exit(0);
}


void thisVersion() {
  writeln("version 1.0");
  exit(0);
}


/*-------------main----------------*/
version(unittest) {}
 else {
   void main(string[] args) {
     /*--------parsing option---------------*/
     try {
       getopt(args, "version|v", &thisVersion, "help|h", &usage);
       if(args.length != 3)
         throw new Exception("ERROR: the number of argments illeagal.");
     }
     catch(GetOptException e) {
       writeln("ERROR: invalid option\n", e.toString);
       usage();
     }
     catch(Exception e) {
       writeln(e.toString);
       usage();
     }

     auto fastqFile = args[1];
     auto outFile = args[2];
     
     /*--------main processes---------------*/
     fastqTruncation(fastqFile, outFile);
   }
 }


void fastqTruncation(string fastqFile, string outFile) {
     auto fout = File(outFile, "w");
     auto truncationInfo = Tuple!(string, "seq",
                                  int, "leftTruncLen",
                                  int, "rightTruncLen")("", 0, 0);

     uint i = 0;
     foreach(line; File(fastqFile, "r").byLine) {
       auto strLine = line.to!string;

       switch(i % 4) {
       case 0:
         fout.writeln(strLine);
         break;
       case 1:
         truncationInfo = truncateEndNs(strLine);
         fout.writeln(truncationInfo.seq);
         break;
       case 2:
         fout.writeln(strLine);
         break;
       default:
         fout.writeln(strLine[truncationInfo.leftTruncLen
                              ..($ - truncationInfo.rightTruncLen)]);
         break;
       }
       i++;
     }
}

Tuple!(string, int, int) truncateEndNs(string seq) {
  auto leftTruncLen = 0, rightTruncLen = 0;
  
  foreach(base; seq) {
    if(base == 'N')
      leftTruncLen++;
    else
      break;
  }

  // Seq is all N.
  if(leftTruncLen == seq.length)
    return tuple(seq, 0, 0);
  
  foreach_reverse(base; seq) {
    if(base == 'N')
      rightTruncLen++;
    else
      break;
  }

  return tuple(seq[leftTruncLen..($ - rightTruncLen)],
               leftTruncLen, rightTruncLen);
}

