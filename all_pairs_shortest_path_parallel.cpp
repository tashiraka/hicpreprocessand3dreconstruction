#include <iostream>
#include <fstream>
#include <vector>
#include <cstdlib>
#include <string>
#include <boost/algorithm/string.hpp> // for boost:split
#include <boost/lexical_cast.hpp>

#include <boost/graph/use_mpi.hpp> // Enable PBGL interfaces to BGL algorithms
#include <boost/graph/distributed/mpi_process_group.hpp> // Communication via MPI
#include <boost/graph/distributed/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>


int main(int argc, char *argv[])
{
  char* cfmFile = argv[1];
  char* outFile = argv[2];
  
  // Using MPI
  using boost::graph::distributed::mpi_process_group;

  // Define the graph type
  // vecS is faster than listS and setS, by adjascency_list web page.
  typedef boost::adjacency_list<boost::vecS,
				boost::distributedS<mpi_process_group, boost::vecS>,
				boost::undirectedS,
				boost::no_property,                 // Vertex properties
				boost::property<boost::edge_weight_t, double> // Edge properties
				> Graph;
  typedef boost::graph_traits<Graph>::vertex_descriptor vertex_descriptor;
  typedef boost::graph_traits<Graph>::edge_descriptor edge_descriptor;

  // Construct the graph
  typedef std::pair<int, int> Edge;
  std::ifstream ifs(cfmFile);
  std::string line;
  std::vector<Edge> edges;
  std::vector<double> weights;
  int vertexNum = 0;
  
  while (getline(ifs, line)) {
    std::vector<std::string> fields;
    boost::split(fields, line, boost::is_any_of("\t"));
    int i = boost::lexical_cast<int>(fields[0]);
    int j = boost::lexical_cast<int>(fields[1]);
    double contactFrequency = boost::lexical_cast<double>(fields[2]);
    if (contactFrequency != 0) {
      edges.push_back(Edge(i, j));
      weights.push_back(1.0 / contactFrequency);
    }
    vertexNum = i; // The last entry indicates the matrix size.
  }
  
  Graph g(edges.begin(), edges.end(), weights.begin(), vertexNum);
  
  // Invoke Dijkstra's algorithm and output
  for (int i = 0; i < vertexNum; i++) {
    vertex_descriptor s = boost::vertex(i, g);
    std::vector<double> d(boost::num_vertices(g));
    boost::dijkstra_shortest_paths(g, s,
      boost::distance_map(boost::make_iterator_property_map(d.begin(), get(boost::vertex_index, g))));
     
    std::ofstream ofs(outFile);
    bool first = true;
    for (int j; j < d.size(); j++) {
      if (first) ofs << d[j];
      else ofs << "\t" << d[j];
      first = false;
    }
    ofs << "\n";
  }
}

