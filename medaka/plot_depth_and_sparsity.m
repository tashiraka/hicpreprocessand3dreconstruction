function plot_depth_and_sparsity(cfmFile, ssDepthFile, flag, minD, maxD, plotDepthFile, ...
                                 plotBothDepthFile, plotSparsityFile)
    cfm = load(cfmFile);
    cfm(:, 1:2) = cfm(:, 1:2) + sparse(1);
    cfm = spconvert(cfm);
    cfm = cfm + cfm.' - diag(diag(cfm));
    n = size(cfm, 1);

    ss_depth = load(ssDepthFile);
    ss_depth = ss_depth';
    
    depth = full(sum(cfm));
    sparsity = full(sum(logical(cfm))) ./ n;

    both = depth + ss_depth;
    both = log10(both(both > 0));
    
    depth = depth(depth > 0);
    depth = sort(depth);
    nd = length(depth);
    %depth = depth(floor(nd * 0.12):ceil(nd * 0.9994));
    if flag
        depth = depth(ceil(nd * minD)+1:floor(nd * maxD));
    else
        depth = depth(minD <= depth & depth <= maxD);
    end
    
    %depth = depth(100 <= depth & depth <= 1200);
    %sparsity = log10(sparsity(sparsity > 0));
    %sparsity = sparsity(0 < sparsity & sparsity < 0.005);

    %depth = depth ./ sparsity;
    
    saveas(histogram(depth), plotDepthFile, 'eps');
    saveas(histogram(both), plotBothDepthFile, 'eps');
    saveas(histogram(sparsity), plotSparsityFile, 'eps');
