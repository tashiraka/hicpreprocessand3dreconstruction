import std.stdio, std.string, std.conv, std.math;


void main(string[] args)
{
  auto minLibLen = args[1].to!uint;
  auto maxLibLen = args[2].to!uint;
  
  uint inter=0, intra=0;
  uint sameStr=0, diffStr=0;
  uint sameFrag=0, diffFrag=0;
  uint notLigation=0, selfLigation=0, reLigation=0;
  uint sameFragSameStrand=0, reLigationChain=0;
  uint total=0;
  
  foreach (line; stdin.byLineCopy) {
    total++;

    auto fields = line.strip.split;
    
    auto rname1 = fields[1];
    auto rname2 = fields[9];
    auto str1 = fields[2] == "+" ? Strand.fwd : Strand.rev;
    auto str2 = fields[10] == "+" ? Strand.fwd : Strand.rev;
    auto pos1 = fields[3].to!uint;
    auto pos2 = fields[11].to!uint;
    auto frag1 = fields[4].to!uint;
    auto frag2 = fields[12].to!uint;
    auto libLen = fields[7].to!ulong + fields[15].to!ulong;
    
    if (str1 == str2) sameStr++;
    else diffStr++;

    if (rname1 != rname2) inter++;
    else {
      intra++;

      if (frag1 == frag2) {
        sameFrag++;

        if (str1 == Strand.fwd && str2 == Strand.rev) {
          if (pos1 > pos2) selfLigation++;
          else notLigation++;
        }
        else if (str1 == Strand.rev && str2 == Strand.fwd) {
          if (pos2 > pos1) selfLigation++;
          else notLigation++;
        }
        else {
          sameFragSameStrand++;
        }
      }
      else {
        diffFrag++;

        if (abs(frag1 - frag2) == 1 && str1 != str2) reLigation++;

        if (str1 != str2 && (minLibLen <= abs(pos1 - pos2) || abs(pos1 - pos2) <= maxLibLen))
            reLigationChain++;
      }
    }
  }
  
  writeln("Intra-chromosomal: ", intra, " ", intra / total.to!double);
  writeln("Inter-chromosomal: ", inter, " ", inter / total.to!double);
  writeln("Same strand: ", sameStr, " ", sameStr / total.to!double);
  writeln("Different strand: ", diffStr, " ", diffStr / total.to!double);
  writeln("Same fragment: ", sameFrag, " ", sameFrag / intra.to!double);
  writeln("Different fragment: ", diffFrag, " ", diffFrag / intra.to!double);
  writeln("self-Ligation: ", selfLigation, " ", selfLigation / intra.to!double);
  writeln("re-Ligation: ", reLigation, " ", reLigation / intra.to!double);
  writeln("not Ligated: ", notLigation, " ", notLigation / intra.to!double);
  writeln("Same fragment Same strand: ", sameFragSameStrand, " ", sameFragSameStrand / intra.to!double);
  writeln("re-Ligation chain: ", reLigationChain, " ", reLigationChain / intra.to!double);
}


enum Strand: char {fwd=0, rev=1}

