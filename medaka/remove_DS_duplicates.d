import std.stdio, std.conv, std.string, std.typecons, std.algorithm, std.math;


/*
bool isDup(DSPair ds1, DSPair ds2)
{
  auto dupMargin = 4; // 1 is exactly same.
  return abs(ds1.pos1 - ds2.pos1) <= 4  && abs(ds1.pos2 - ds2.pos2) <= 4 ;
}
*/

void main(string[] args)
{
  auto DSFile = args[1];
  auto noDupFile = args[2];
  auto dupFile = args[3];


  DSPair[][string][string] ff, fr, rr;
  
  // qname1 rname1 strand1 pos1 fragID1 mapq1 mappedLen1 DNALen1        \
  // qname2 rname2 strand2 pos2 fragID2 mapq2 mappedLen2 DNALen2
  foreach (line; File(DSFile).byLine) {
    auto fields = line.to!string.strip.split;

    auto rname1 = fields[1];
    auto rname2 = fields[9];
    auto str1 = fields[2] == "+" ? Strand.fwd : Strand.rev;
    auto str2 = fields[10] == "+" ? Strand.fwd : Strand.rev;
    auto pos1 = fields[3].to!uint;
    auto pos2 = fields[11].to!uint;
    
    // DS pair must have tha order from "+" to "-".
    if (str1 == Strand.rev && str2 == Strand.fwd) {
      fields = fields[8..16] ~ fields[0..8];
      str1 = Strand.fwd;
      str2 = Strand.rev;
    }
    
    if (str1 == Strand.fwd) {
      if (str2 == Strand.fwd) {
        fields.sortRname(rname1, rname2, pos1, pos2);
        ff[rname1][rname2] ~= new DSPair(fields, line.to!string);
      }
      else
        fr[rname1][rname2] ~= new DSPair(fields, line.to!string);
    }
    else {
      fields.sortRname(rname1, rname2, pos1, pos2);
      rr[rname1][rname2] ~= new DSPair(fields, line.to!string);
    }
  }

  
  // Output
  auto noDupOut = File(noDupFile, "w");
  auto dupOut = File(dupFile, "w");

  sortDup(ff, noDupOut, dupOut);
  sortDup(fr, noDupOut, dupOut);
  sortDup(rr, noDupOut, dupOut);
}


DSPair[] sortPosition(ref DSPair[] pairs)
{
  pairs.sort!((a,b) => a.pos1 < b.pos1);
  pairs.sort!((a,b) => a.pos2 < b.pos2, SwapStrategy.stable);
  return pairs;
}



void sortDup(DSPair[][string][string] hash, File noDupOut, File dupOut)
{
  foreach (rname1; hash.byKey) {
    foreach (rname2; hash[rname1].byKey) {
      auto pairs = hash[rname1][rname2];

      if (pairs.length == 1) {
        noDupOut.writeln(pairs[0]);
      }
      else {
        // Already ordered by other fields from position
        // Sort by paired position
        pairs.sortPosition;
        pairs.sortPosition;
      
        DSPair[] dupPairs = [pairs[0]];
        
        foreach (pair; pairs[1..$]) {
          if (dupPairs[$-1].pos1 != pair.pos1 ||
              dupPairs[$-1].pos2 != pair.pos2) {
            if (dupPairs.length == 1)
              noDupOut.writeln(dupPairs[0]);
            else {
              // Remain pair with max sum of pairs' MAPQ.
              noDupOut.writeln(dupPairs.maxMapq);
              dupPairs.each!(x => dupOut.writeln(x));
            }

            dupPairs = [];
          }
          
          dupPairs ~= pair;
        }

        if (dupPairs.length == 1)
          noDupOut.writeln(dupPairs[0]);
        else {
          // Remain pair with max sum of pairs' MAPQ.
          noDupOut.writeln(dupPairs.maxMapq);
          dupPairs.each!(x => dupOut.writeln(x));
        }
      }
    }      
  }
}


DSPair maxMapq(DSPair[] pairs)
{
  if (pairs.length == 1) return pairs[0];
                                           
  DSPair maxPair = pairs[0];
  
  foreach (pair; pairs[1..$])
    if (maxPair.mapqSum < pair.mapqSum)
      maxPair = pair;
  
  return maxPair;
  
}

void sortRname(ref string[] fields, ref string rname1, ref string rname2,
               uint pos1, uint pos2)
{
  if (rname1 > rname2 || (rname1 == rname2 && pos1 > pos2)) {
    fields = fields[8..16] ~ fields[0..8];
    auto tmp = rname1;
    rname1 = rname2;
    rname2 = rname1;
  }
}


enum Strand: char {fwd=0, rev=1}

class DSPair
{
  uint pos1, pos2, mapq1, mapq2;
  string line;
  
  this (string[] fields, string l)
  {
    if (fields.length != 16) throw new Exception("Fields.length != 16");

    auto s1 = fields[2] == "+" ? Strand.fwd : Strand.rev;
    auto s2 = fields[10] == "+" ? Strand.fwd : Strand.rev;

    // DS pair must have tha order from "+" to "-".
    if (s1 == Strand.rev && s2 == Strand.fwd)
      throw new Exception("Rev to Fwd pair");

    // qname1 rname1 strand1 pos1 fragID1 mapq1 mappedLen1 DNALen1      \
    // qname2 rname2 strand2 pos2 fragID2 mapq2 mappedLen2 DNALen2
    pos1 = fields[3].to!uint;
    pos2 = fields[11].to!uint;
    mapq1 = fields[5].to!uint;
    mapq2 = fields[13].to!uint;

    line = l;
  }

  override string toString()
  {
    return line;
  }

  uint mapqSum()
  {
    return mapq1 + mapq2;
  }
}
