dataDir=../data/liver
#prefix=${dataDir}/liver.paired.mapped.noDup.fragMapped.filtered
prefix=${dataDir}/liver.cfm.1m.subMat
#dataDir=../data/st25
#prefix=${dataDir}/st25

./checkConnection ${prefix} ${prefix}.connected ${prefix}.connectivity
./CtoD ${prefix}.connected ${prefix}.connected.distMat
./DtoG ${prefix}.connected.distMat ${prefix}.connected.gramMat
./GtoEig ${prefix}.connected.gramMat ${prefix}.connected.eigVal ${prefix}.connected.eigVec
./EigToX ${prefix}.connected.eigVal ${prefix}.connected.eigVec 100 ${prefix}.connected.xyz
./checkGOF ${prefix}.connected.eigVal ${prefix}.connected.xyz ${prefix}.connected.distMat ${prefix}.connected.gof.txt
./xyz_to_pdb ${prefix}.connected.xyz ${prefix}.connectivity ${prefix}.binInfo  ${prefix}.connected.pdb

#./checkConnection ${prefix}.cfm.1m.normalized.full ${prefix}.cfm.1m.normalized.connected ${prefix}.cfm.1m.normalized.connectivity
#./CtoD ${prefix}.cfm.1m.normalized.connected ${prefix}.cfm.1m.normalized.connected.distMat
#./DtoG ${prefix}.cfm.1m.normalized.connected.distMat ${prefix}.cfm.1m.normalized.connected.gramMat
#./GtoEig ${prefix}.cfm.1m.normalized.connected.gramMat ${prefix}.cfm.1m.normalized.connected.eigVal ${prefix}.cfm.1m.normalized.connected.eigVec
#./EigToX ${prefix}.cfm.1m.normalized.connected.eigVal ${prefix}.cfm.1m.normalized.connected.eigVec 1 ${prefix}.cfm.1m.normalized.connected.xyz
#./checkGOF ${prefix}.cfm.1m.normalized.connected.eigVal ${prefix}.cfm.1m.normalized.connected.xyz ${prefix}.cfm.1m.normalized.connected.distMat ${prefix}.cfm.1m.normalized.connected.gof.txt
#./xyz_to_pdb ${prefix}.cfm.1m.normalized.connected.xyz ${prefix}.cfm.1m.normalized.connectivity ${prefix}.1m.binInfo  ${prefix}.cfm.1m.normalized.connected.pdb


#./checkConnection ${prefix}.cfm.1m.full ${prefix}.cfm.1m.connected ${prefix}.cfm.1m.connectivity
#./CtoD ${prefix}.cfm.1m.connected ${prefix}.cfm.1m.connected.distMat
#./DtoG ${prefix}.cfm.1m.connected.distMat ${prefix}.cfm.1m.connected.gramMat
#./GtoEig ${prefix}.cfm.1m.connected.gramMat ${prefix}.cfm.1m.connected.eigVal ${prefix}.cfm.1m.connected.eigVec
#./EigToX ${prefix}.cfm.1m.connected.eigVal ${prefix}.cfm.1m.connected.eigVec 1000 ${prefix}.cfm.1m.connected.xyz
#./checkGOF ${prefix}.cfm.1m.connected.eigVal ${prefix}.cfm.1m.connected.xyz ${prefix}.cfm.1m.connected.distMat ${prefix}.cfm.1m.connected.gof.txt
#./xyz_to_pdb ${prefix}.cfm.1m.connected.xyz ${prefix}.cfm.1m.connectivity ${prefix}.binInfo.1m  ${prefix}.cfm.1m.connected.pdb
