/***
 * Assume the input bam file is sorted by the names.
 ***/

// out format
// qname1 rname1 strand1 pos1 fragID1 mapq1 mappedLen1 DNALen1

import std.stdio, std.algorithm, std.conv, std.array, std.string, std.process, std.regex;


version(unittest) {}
 else {
   void main(string[] args)
   {     
     auto inFile = args[1];
     auto fragFile = args[2];
     auto nthreads = args[3].to!int;
     auto outFile = args[4];

     auto frags = readFrag(fragFile);
     auto fout = File(outFile, "w");

     File pipe;

     if (inFile.split(".")[$-1] == "bam") {
       pipe = pipeProcess(["sambamba", "view",
                           "-t", nthreads.to!string,
                           inFile]).stdout;
     }
     else pipe = File(inFile);

     string bufLine;

     foreach (line; pipe.byLine) {
       bufLine = line.to!string;
       auto record = new SamRecord(bufLine);

       if ((record.rname in frags) !is null) {
         record.searchFrag(frags);
         
         fout.writeln(record.qname, "\t",
                      record.rname, "\t",
                      record.strand.to!char, "\t",
                      record.pos, "\t",
                      record.fragID, "\t",
                      record.mapq, "\t",
                      record.mappedLen, "\t",
                      record.DNALen);
       }
     }
   }
 }

    
enum Strand : char {plus='+', minus='-'}


class SamRecord
{
  string qname, rname, mapq;
  uint pos, fragID, DNALen;
  ulong mappedLen;
  Strand strand;
  
  this(string record)
  {
    auto fields = record.split('\t');
    qname = fields[0];
    rname = fields[2];
    pos = fields[3].to!uint;
    mapq = fields[4];
    mappedLen = fields[9].length;
    
    auto flag = fields[1].to!uint;
    strand = (flag & 0x10) == 0 ? Strand.plus : Strand.minus;

    auto cigar = fields[5];
    if(strand == Strand.minus) {
      pos = calcReverse5primePos(cigar);
    }
  }

  void searchFrag(Fragment[][string] frags)
  {
    auto index = binarySearchFrag(frags[rname]);
    fragID = frags[rname][index].id;
    DNALen = estimateDNALength(frags[rname][index]);
  }

private:  
  uint estimateDNALength(Fragment frag)
  {
    return strand == Strand.plus ? frag.fragEnd - pos + 1 : pos - frag.fragStart  + 1;
  }

  
  uint binarySearchFrag(Fragment[] frags)
  {
    uint startID = 0;
    uint endID = (frags.length - 1).to!uint;
    
    if (strand == Strand.plus) { // search the template 5' ends of the fragments
      while (startID < endID) {
        auto middleID = (startID + endID) / 2;
        
        if (frags[middleID].end <= pos) startID = middleID + 1;
        else endID = middleID;
      }
    }
    else {
      while (startID < endID) { // search the reverse 5' ends of the fragments
        auto middleID = (startID + endID + 1) / 2;

        if(pos <= frags[middleID].start) endID = middleID - 1;
        else startID = middleID;
      }
    }
    
    if (startID > endID)
      throw new Exception("ERROR: binary search startID > endID");
    
    return startID;
  }
  
  uint calcReverse5primePos(string cigar) {
    auto cigarDict = parseCigar(cigar);

    if(cigarDict['='] != 0 || cigarDict['X'] != 0) {
      throw new Exception("ERROR: CIGAR strings has =/X instead of M.");
    }

    return pos - 1 + cigarDict['M'] + cigarDict['D']
      + cigarDict['N'] + cigarDict['S'] + cigarDict['H'];
  }

  int[char] parseCigar(string cigar)
  {
    auto cigarDict = ['M':0, 'I':0, 'D':0, 'N':0,
                      'S':0, 'H':0, 'P':0, '=':0, 'X':0];

    foreach (op; cigarDict.byKey) {
      auto capture = matchFirst(cigar, r"(\d+)" ~ op);
      cigarDict[op] = capture.empty ? 0 : capture.hit.split(op)[0].to!int;
    }

    return cigarDict;
  }
}


class Fragment
{
  uint id, start, end, fragStart, fragEnd;
  string rname;
  
  this(uint id_, string rname_, uint start_, uint end_,
       uint fragStart_, uint fragEnd_)
  {
    id = id_;
    rname = rname_;
    start = start_;
    end = end_;
    fragStart = fragStart_;
    fragEnd = fragEnd_;
  }
}


Fragment[][string] readFrag(string file)
{
  Fragment[][string] frags;
  
  foreach (line; File(file).byLine) {
    auto fields = line.to!string.split("\t");
    frags[fields[1]] ~= new Fragment(fields[0].to!uint, fields[1],
                                     fields[2].to!uint, fields[3].to!uint,
                                     fields[4].to!uint, fields[5].to!uint); 
  }
  
  return frags;
}
