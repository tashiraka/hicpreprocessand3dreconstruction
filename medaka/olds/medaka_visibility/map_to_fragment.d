import std.stdio, std.getopt, std.algorithm, std.conv, std.array, std.string;


version(unittest) {}
 else {
   void main(string[] args)
   {     
     auto DSfile1 = args[1];
     auto DSfile2 = args[2];
     auto SSfile = args[3];
     auto fragFile = args[4];
     auto outDS1File = args[5];
     auto outDS2File = args[6];
     auto outSSFile = args[7];

     auto frags = readFrag(fragFile);
     
     auto foutDS1 = File(outDS1File, "w");
     auto foutDS2 = File(outDS2File, "w");
     foreach(ref ds; readDS(DSfile1, DSfile2)) {
       ds.searchFrag(frags);
       foutDS1.writeln(ds.chrID(Pair.first), "\t",
                       ds.strand(Pair.first), "\t",
                       ds.pos(Pair.first), "\t",
                       ds.fragID(Pair.first));
       foutDS2.writeln(ds.chrID(Pair.second), "\t",
                       ds.strand(Pair.second), "\t",
                       ds.pos(Pair.second), "\t",
                       ds.fragID(Pair.second));
     }

     auto foutSS = File(outSSFile, "w");     
     foreach(ref ss; readSS(SSfile)) {
       ss.searchFrag(frags);
       foutSS.writeln(ss.chrID, "\t", ss.strand, "\t", ss.pos, "\t", ss.fragID);
     }     
   }
 }


enum Pair {first, second}
enum Strand : char {plus='+', minus='-'}


class Fragment {
private:
  uint id_, chrID_, start_, end_;

public:
  this(uint id, uint chrID, uint start, uint end) {
    id_=id; chrID_=chrID; start_=start; end_=end;
  }

  @property {
    uint id() {return id_;}
    uint chrID() {return chrID_;}
    uint start() {return start_;}
    uint end() {return end_;}
  }
}


Fragment[][24] readFrag(string file)
{
  Fragment[][24] frags;  
  foreach(line; File(file, "r").byLine) {
    if(line != "") {
      auto fields = line.to!string.split("\t");
      frags[fields[1].to!uint] ~= new Fragment(fields[0].to!uint,
                                               fields[1].to!uint,
                                               fields[2].to!uint,
                                               fields[3].to!uint);
    }
  }
  return frags;
}


SSread[] readSS(string file)
{
  auto app = appender!(SSread[]);
  foreach(line; File(file, "r").byLine) {
    if(line != "") {
      auto fields = line.to!string.split('\t');
      auto chrID = fields[0].to!uint;
      auto pos = fields[1].to!uint;
      auto strand = fields[2] == "+" ? Strand.plus : Strand.minus;
      app.put(new SSread(chrID, pos, strand));
    }
  }
  return app.data;
}


class SSread
{
private:
  uint chrID_, pos_;
  Strand strand_;
  ulong fragID_;
  
public:
  this(uint chrID, uint pos, Strand str) {
    chrID_=chrID; pos_=pos; strand_=str;
  }

  @property {
    uint chrID() {return chrID_;}
    uint pos() {return pos_;}
    char strand() {return strand_ == Strand.plus ? '+' : '-';}
    ulong fragID()  {return fragID_;}
  }

  void searchFrag(Fragment[][] frags) {
    fragID_ = binarySearch(frags[chrID]);
  }

  
private:
  ulong binarySearch(Fragment[] frags) {
    ulong startID = 0;
    ulong endID = frags.length - 1;
    
    if(strand == Strand.plus) {
      while(startID < endID) {
        auto middleID = (startID + endID) / 2;
        if(frags[middleID].end <= pos) {
          startID = middleID + 1;
        } else {
          endID = middleID;
        }
      }
      if(startID > endID)
        throw new Exception("ERROR: binary search startID > endID");

      return frags[startID].id;
    }
    else {
      while(startID < endID) {
        auto middleID = (startID + endID + 1) / 2;
        if(pos <= frags[middleID].start) {
          endID = middleID - 1;
        } else {
          startID = middleID;          
        }
      }
      if(startID > endID)
        throw new Exception("ERROR: binary search startID > endID");

      return frags[startID].id;
    }
  }
}


DSread[] readDS(string file1, string file2)
{
  auto app = appender!(DSread[]);
  foreach(line; File(file1, "r").byLine) {
    if(line != "") {
      auto fields = line.to!string.split('\t');
      auto chrID = fields[0].to!uint;
      auto pos = fields[1].to!uint;
      auto strand = fields[2] == "+" ? Strand.plus : Strand.minus;
      app.put(new DSread(chrID, pos, strand));
    }
  }

  auto ds = app.data;
  auto i = 0;
  foreach(line; File(file2, "r").byLine) {
    if(line != "") {
      auto fields = line.to!string.split('\t');
      auto chrID = fields[0].to!uint;
      auto pos = fields[1].to!uint;
      auto strand = fields[2] == "+" ? Strand.plus : Strand.minus;
      ds[i++].putSecond(chrID, pos, strand);
    }
  }
  return ds;
}


class DSread
{
private:
  uint[2] chrID_, pos_;
  Strand[2] strand_;
  ulong[2] fragID_;
  
public:
  this(uint chrID, uint pos, Strand str) {
    chrID_[0]=chrID; pos_[0]=pos; strand_[0]=str;
  }

  void putSecond(uint chrID, uint pos, Strand str) {
    chrID_[1]=chrID; pos_[1]=pos; strand_[1]=str;
  }
  
  @property {
    uint chrID(Pair p) { return p == Pair.first ? chrID_[0] : chrID_[1]; }
    uint pos(Pair p) { return p == Pair.first ? pos_[0] : pos_[1]; }
    char strand(Pair p) {
      if(p == Pair.first) 
        return strand_[0] == Strand.plus ? '+' : '-';
      else
        return strand_[1] == Strand.plus ? '+' : '-';
    }
    ulong fragID(Pair p)  { return p == Pair.first ? fragID_[0] : fragID_[1]; }
  }

  void searchFrag(Fragment[][] frags) {
    fragID_[0] = binarySearch(Pair.first, frags[chrID(Pair.first)]);
    fragID_[1] = binarySearch(Pair.second, frags[chrID(Pair.second)]);
  }

  
private:
  ulong binarySearch(Pair p, Fragment[] frags) {
    ulong startID = 0;
    ulong endID = frags.length - 1;
    
    if(strand(p) == Strand.plus) {
      while(startID < endID) {
        auto middleID = (startID + endID) / 2;
        if(frags[middleID].end <= pos(p)) {
          startID = middleID + 1;
        } else {
          endID = middleID;
        }
      }
      if(startID > endID)
        throw new Exception("ERROR: binary search startID > endID");

      return frags[startID].id;
    }
    else {
      while(startID < endID) {
        auto middleID = (startID + endID + 1) / 2;
        if(pos(p) <= frags[middleID].start) {
          endID = middleID - 1;
        } else {
          startID = middleID;          
        }
      }
      if(startID > endID)
        throw new Exception("ERROR: binary search startID > endID");

      return frags[startID].id;
    }
  }
}

