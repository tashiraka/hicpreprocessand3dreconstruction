import std.stdio, std.conv, std.string, std.algorithm, std.array, std.math;

version(unittest){}
 else{
   void main(string[] args)
   {
     auto fragFile = args[1];
     auto DS1File = args[2];
     auto DS2File = args[3];
     auto outMatrixFile = args[4];

     writeln(1);
     ulong matSize = 0;
     foreach(line; File(fragFile, "r").byLine) {
       if(line != "") {
         matSize++;
       }
     }

     writeln(2);
     auto cfm = readDS(DS1File, DS2File, matSize);

     writeln(3);
     auto fout = File(outMatrixFile, "w");
     foreach(row; cfm) {
       foreach(i, elem; row) {
         fout.write(elem);
         if(i < row.length)
           fout.write("\t");
       }
       fout.write("\n");
     }
   }
 }




double[][] readDS(string file1, string file2, ulong matSize)
{
  writeln(matSize);
  auto freqMat = new double[][](matSize, matSize);
  writeln(11111);
  foreach(ref row; freqMat)
    row.fill(0.0);
  
  auto fin1 = File(file1, "r");
  auto fin2 = File(file2, "r");
  
  while(true) {
    auto fields1 = fin1.readln.to!string.strip.split('\t');
    auto fields2 = fin2.readln.to!string.strip.split('\t');

    if(fields1.length != 4 || fields2.length !=4)
      break;

    freqMat[fields1[3].to!uint][fields2[3].to!uint]++;
    freqMat[fields2[3].to!uint][fields1[3].to!uint]++;
  }
  
  return freqMat;
}



