/***
 * sparse matrix and full vector version
 ***/
import std.stdio, std.conv, std.string, std.algorithm, std.array, std.math, std.parallelism;


version(unittest){}
 else{
   void main(string[] args)
   {
     auto fragFile = args[1];
     auto DS1File = args[2];
     auto DS2File = args[3];
     auto SSFile = args[4];
     auto outMatrixFile = args[5];
     auto outVectorFile = args[6];

     writeln(1);
     auto matSize = countLine(fragFile);
     writeln(2);
     auto sparseODS = DStoCfm(DS1File, DS2File);
     writeln(3);
     auto OSS = SStoCfv(SSFile, matSize);

     writeln(4);
     auto B = new double[](OSS.length);
     auto B0 = 1.0;
     auto SDS = new double[](OSS.length);
     auto SSS = new double[](OSS.length);
     auto dB = new double[](OSS.length);
     /*
     double[uint] B;
     auto B0 = 1.0;
     double[uint] SDS;
     double[uint] SSS;
     double[uint] dB;
     */
     auto sparseW = sparseODS.dup;
     writeln(5);
     auto initialMeanVisibility = sparseW.visibility(matSize).mean;

     // init
     B.fill(1.0);
     B0 = 1.0;
     SDS.fill(0.0);
     SSS.fill(0.0);
     dB.fill(0.0);
     
     //main iteration
     foreach(loopCount; 0..1000) {
       writeln(loopCount);
       writeln("a");
       SDS = sparseW.visibility(matSize);
       writeln("b");
       SSS[] = OSS[] / (B[] * B0); //normalize OSS
       writeln("c");
       dB[]= SDS[] + SSS[];
       writeln("d");
       dB[] /= dB.mean;
       writeln("e");
       dB = dB.map!(x => x<=0.0 ? 1.0 : x).array; //negative values are ignored.
       writeln("f");
       
       //condition of termination
       auto flag = true;
       foreach(elem; dB) {
         if(abs(elem - 1.0) < 0.00001)
           flag = flag & true;
         else
           flag = false;
       }
       if(flag) {
         writeln("Finished at # of loop: ", loopCount);
         break;
       }

       writeln("g");
       foreach(key; sparseW.byKey.parallel) {
         sparseW[key] /= dB[key[0]] * dB[key[1]];
       }

       writeln("h");
       B[] *= dB[];
       writeln("i");
       B0 = B.mean;
     }

     writeln(6);
          
     //TDS: sparseW;
     //TSS: SSS[] = OSS[] / (B[] * B0);
     SSS[] = OSS[] / (B[] * B0);
     writeln("T: ", SSS.mean / sparseW.mean);
     writeln("O: ", OSS.mean / sparseODS.mean);

     writeln(7);
          
     // scaling
     auto finalMeanVisibility = sparseW.visibility(matSize).mean;
     auto scaler = initialMeanVisibility / finalMeanVisibility;
     foreach(key; sparseW.byKey) {
       sparseW[key] *= scaler;
     }
     
     SSS[] *= initialMeanVisibility / finalMeanVisibility;

     writeln(8);
     
     auto fout = File(outMatrixFile, "w");
     foreach(key; sparseW.byKey) {
       fout.writeln(key[0], "\t", key[1], "\t", sparseW[key]);
     }

     fout = File(outVectorFile, "w");
     foreach(elem; SSS) {
       fout.writeln(elem);
     }
   }
 }

/*
double[uint] visibility(double[Tuple!(uint, uint)] sparseMat, uint matSize) {
  double[uint] v;
  foreach(i; 0..matSize) {
    foreach(key; sparseMat.byKey) {
      if(key[0] == i || key[1] == i) {
        v[i] += sparseMat[key];
      }
    }
  }
  return v;
}
*/
double[] visibility(double[Tuple!(uint, uint)] sparseMat, uint size) {
  auto v = new double[](size);
  /*
  auto tmpMat = new Tuple!(uint, uint, double)[](sparseMat.length);
  uint index = 0;
  foreach(key; sparseMat.byKey) {
    tmpMat[index++] = tuple(key[0],
                            key[1],
                            sparseMat[key]);
  }
  */
  foreach(key; sparseMat.byKey.parallel) {
    auto val = sparseMat[key];
    v[key[0]] += val;
    v[key[1]] += val;      
  }
  return v;
}


// mean of positive value
double mean(double[] arr) {
  auto farr = arr.filter!(x => x > 0.0);
  return farr.sum / farr.array.length;
}


// mean of positive value
double mean(double[Tuple!(uint, uint)] sparseMat) {
  return sparseMat.byValue.sum / sparseMat.length;
}


double[Tuple!(uint, uint)] DStoCfm(string file1, string file2)
{
  double[Tuple!(uint, uint)] sparseFreqMat;
  auto fin1 = File(file1, "r");
  auto fin2 = File(file2, "r");

  while(true) {
    auto fields1 = fin1.readln.to!string.strip.split('\t');
    auto fields2 = fin2.readln.to!string.strip.split('\t');

    if(fields1.length != 4 || fields2.length !=4)
      break;

    auto i = fields1[3].to!uint;
    auto j = fields2[3].to!uint;
        
    //double.init == 0, but first increment return 1.
    if(i < j) {
      sparseFreqMat[tuple(i, j)] += 1;
    }
    else {
      sparseFreqMat[tuple(j, i)] += 1;
    }
    
  }

  return sparseFreqMat;
}

/*
double[uint] SStoCfv(string file)
{
  double[uint] sparseFreqVec;
  auto fin = File(file, "r");
  
  while(true) {
    auto fields = fin.readln.to!string.strip.split('\t');

    if(fields.length != 4)
      break;

    sparseFreqVec[fields[3].to!uint]++;
  }

  return sparseFreqVec;
}
*/

double[] SStoCfv(string file, uint size)
{
  auto freqMat = new double[](size);
  freqMat.fill(0.0);
    
  auto fin = File(file, "r");
  
  while(true) {
    auto fields = fin.readln.to!string.strip.split('\t');

    if(fields.length != 4)
      break;

    freqMat[fields[3].to!uint]++;
  }
  
  return freqMat;
}


uint countLine(string filename)
{
  uint count = 0;
  foreach(line; File(filename, "r").byLine) {
    if(line != "") {
      count++;
    }
  }     
  return count;
}
