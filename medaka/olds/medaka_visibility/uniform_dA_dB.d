import std.stdio, std.conv, std.string, std.array, std.algorithm, std.math, std.parallelism, std.random;


version(unittest) {}
 else {
   void main(string[] args)
   {     
     auto fragFile = args[1];
     auto sampleSize = args[2].to!ulong;
     auto binSize = args[3].to!double;
     auto outFile = args[4];

     auto distFromFragEnd = readFrag(fragFile).uniformDistFromFragEnd;

     auto maxD = reduce!(max)(distFromFragEnd);

     auto count = new ulong[]((log10(maxD + maxD) / binSize).to!ulong + 1);

     auto randomID = new ulong[](sampleSize);
     foreach(i; 0..sampleSize) {
       randomID[i] = uniform(0, distFromFragEnd.length);
     }
     
     foreach(dA; distFromFragEnd) {
       foreach(dB; distFromFragEnd) {
          count[(log10(dA + dB) / binSize).to!ulong]++;
       }
     }

     auto fout = File(outFile, "w");
     foreach(c; count) {
       fout.writeln(c);
     }
   }
 }


uint[] uniformDistFromFragEnd(Fragment[] frags)
{
  auto distFromFragEnd = appender!(uint[]);
  foreach(frag; frags.parallel)
    distFromFragEnd.put(frag.uniformDistFromFragEnd);
  return distFromFragEnd.data;
}


class Fragment {
private:
  uint id_, chrID_, start_, end_;

public:
  this(uint id, uint chrID, uint start, uint end) {
    id_=id; chrID_=chrID; start_=start; end_=end;
  }

  @property {
    uint id() {return id_;}
    uint chrID() {return chrID_;}
    uint start() {return start_;}
    uint end() {return end_;}
  }

  uint[] uniformDistFromFragEnd() {
    auto maxLen = end - start + 1;
    auto arr = new uint[](maxLen);
    foreach(i; 0..maxLen) {
      arr[i] = i + 1;
    }
    return arr ~ arr.dup; //+ and - strand
  }
}


Fragment[] readFrag(string file)
{
  Fragment[] frags;  
  foreach(line; File(file, "r").byLine) {
    if(line != "") {
      auto fields = line.to!string.split("\t");
      frags ~= new Fragment(fields[0].to!uint, fields[1].to!uint,
                            fields[2].to!uint, fields[3].to!uint);
    }
  }
  return frags;
}

