import sys
import re


# This chromosome name parser is for oryLat2
def toID(chrName):
    if chrName == "chr1":
        return '0'
    elif chrName == "chr2":
        return '1'
    elif chrName == "chr3":
        return '2'
    elif chrName == "chr4":
        return '3'
    elif chrName == "chr5":
        return '4'
    elif chrName == "chr6":
        return '5'
    elif chrName == "chr7":
        return '6'
    elif chrName == "chr8":
        return '7'
    elif chrName == "chr9":
        return '8'
    elif chrName == "chr10":
        return '9'
    elif chrName == "chr11":
        return '10'
    elif chrName == "chr12":
        return '11'
    elif chrName == "chr13":
        return '12'
    elif chrName == "chr14":
        return '13'
    elif chrName == "chr15":
        return '14'
    elif chrName == "chr16":
        return '15'
    elif chrName == "chr17":
        return '16'
    elif chrName == "chr18":
        return '17'
    elif chrName == "chr19":
        return '18'
    elif chrName == "chr20":
        return '19'
    elif chrName == "chr21":
        return '20'
    elif chrName == "chr22":
        return '21'
    elif chrName == "chr23":
        return '22'
    elif chrName == "chr24":
        return '23'
    else:
        print "ERROR: invalid chromosome name, " + chrName
        quit()

        
# Assume end-to-end alignment
def CIGAR_parser(cigar):
    cigar_op = ['M', 'I', 'D', 'N', 'S', 'H', 'P', '=', 'X']
    cigar_dict = {'M':0, 'I':0, 'D':0, 'N':0, 'S':0, 'H':0, 'P':0, '=':0, 'X':0}
    for op in cigar_op:
        cigar_dict[op] = sum([int(x) for x in re.findall(r'(\d+)' + op, cigar)])
    return cigar_dict

    
args = sys.argv
if (len(args) != 7) :
    print "Usage: # python %s <DS1.sam> <DS2.sam> <SS.sam> <DS1 out> <DS2 out> <SS out>"
    quit()

    
DS1file = args[1];
DS2file = args[2];
SSfile = args[3];
outDS1File = args[4];
outDS2File = args[5];
outSSFile = args[6];
    
finDS1 = open(DS1file, "r")
finDS2 = open(DS2file, 'r')
foutDS1 = open(outDS1File, "w")
foutDS2 = open(outDS2File, "w")

lines1 = finDS1.readlines()

for i, line2 in enumerate(finDS2) :
    if(line2[0] != '@' and line2 != '\n'):
        fields1 = lines1[i].split('\t')
        fields2 = line2.split('\t')
        chrID1 = toID(fields1[2]);
        chrID2 = toID(fields2[2]);
        strand1 = ''
        strand2 = ''
        pos1 = int(fields1[3])
        pos2 = int(fields2[3])
        
        # assume bowtie2 end-to-end mode
        if (int(fields1[1]) & 16 == 16) :
            strand1 = '-'
            cigar1 = CIGAR_parser(fields1[5])
            pos1 = pos1 - 1 + cigar1['M'] + cigar1['D'] + cigar1['N'] + cigar1['P']
        else :
            strand1 = '+'
            
        if (int(fields2[1]) & 16 == 16) :
            strand2 = '-'
            cigar2 = CIGAR_parser(fields2[5])
            pos2 = pos2 - 1 + cigar2['M'] + cigar2['D'] + cigar2['N'] + cigar2['P']
        else :
            strand2 = '+'
            
        foutDS1.write(chrID1 + '\t' + str(pos1) + '\t' + strand1 + '\n')
        foutDS2.write(chrID2 + '\t' + str(pos2) + '\t' + strand2 + '\n')


finSS = open(SSfile, "r")
foutSS = open(outSSFile, "w")

for line in finSS :
    if(line[0] != '@' and line != '\n'):
        fields = line.split('\t')
        chrID = toID(fields[2]);
        strand = ''
        pos = int(fields[3])
    
        # assume bowtie2 end-to-end mode
        if (int(fields[1]) & 16 == 16) :
            strand = '-'
            cigar = CIGAR_parser(fields[5])
            pos = pos - 1 + cigar['M'] + cigar['D'] + cigar['N'] + cigar['P']
        else :
            strand = '+'
        
        foutSS.write(chrID + '\t' + str(pos) + '\t' + strand + '\n')
