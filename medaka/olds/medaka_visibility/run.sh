path=~/Workspace/Hi-C_workbench/refined_hic_preprocess/medaka_visibility/
genome_path=/work/yuichi/medaka/genome/UCSC/oryLat2_softMasked_including_scafold/

#---------------------
# fragment
#---------------------
python ${path}restrict_to_fragments.py ${genome_path}oryLat2.fa HindIII ${genome_path}oryLat2.HindIII_fragments

#---------------------
# read
#---------------------
zcat 20140415_st25_L001_R1.fastq.gz 20140415_st25_L002_R1.fastq.gz > 20140415_st25_R1.fastq
zcat 20140415_st25_L001_R2.fastq.gz 20140415_st25_L002_R2.fastq.gz > 20140415_st25_R2.fastq
zcat 20140522_st25_L001_R1.fastq.gz 20140522_st25_L002_R1.fastq.gz > 20140522_st25_R1.fastq
zcat 20140522_st25_L001_R2.fastq.gz 20140522_st25_L002_R2.fastq.gz > 20140522_st25_R2.fastq
gzip -d 20140627_2st25_R1.fastq.gz 20140627_2st25_R2.fastq.gz


${path}iterative_mapping -p 8 -m 25 -s 5 -l 50 ${genome_path}bt2/oryLat2 20140415_st25_R1.fastq 20140415_st25_R1.sam >20140415_st25_R1.iterative_mapping.log 2>&1
${path}iterative_mapping -p 8 -m 25 -s 5 -l 50 ${genome_path}bt2/oryLat2 20140415_st25_R2.fastq 20140415_st25_R2.sam >20140415_st25_R2.iterative_mapping.log 2>&1
${path}iterative_mapping -p 8 -m 25 -s 5 -l 50 ${genome_path}bt2/oryLat2 20140522_st25_R1.fastq 20140522_st25_R1.sam >20140522_st25_R1.iterative_mapping.log 2>&1
${path}iterative_mapping -p 8 -m 25 -s 5 -l 50 ${genome_path}bt2/oryLat2 20140522_st25_R2.fastq 20140522_st25_R2.sam >20140522_st25_R2.iterative_mapping.log 2>&1
${path}iterative_mapping -p 8 -m 25 -s 5 -l 50 ${genome_path}bt2/oryLat2 20140627_2st25_R1.fastq 20140627_2st25_R1.sam >20140627_2st25_R1.iterative_mapping.log 2>&1
${path}iterative_mapping -p 8 -m 25 -s 5 -l 50 ${genome_path}bt2/oryLat2 20140627_2st25_R2.fastq 20140627_2st25_R2.sam >20140627_2st25_R2.iterative_mapping.log 2>&1


(${path}divide_DS_SS 20140415_st25_R1.sort.sam 20140415_st25_R2.sort.sam 20140415_st25_R1.DS.sam 20140415_st25_R2.DS.sam 20140415_st25.SS.sam) \
    & (${path}divide_DS_SS 20140522_st25_R1.sort.sam 20140522_st25_R2.sort.sam 20140522_st25_R1.DS.sam 20140522_st25_R2.DS.sam 20140522_st25.SS.sam) \
    & (${path}divide_DS_SS 20140627_2st25_R1.sort.sam 20140627_2st25_R2.sort.sam 20140627_2st25_R1.DS.sam 20140627_2st25_R2.DS.sam 20140627_2st25.SS.sam)


python ${path}decide_read_pos.py 20140415_st25_R1.DS.sam 20140415_st25_R2.DS.sam 20140415_st25.SS.sam 20140415_st25_R1.DS 20140415_st25_R2.DS 20140415_st25.SS
python ${path}decide_read_pos.py 20140522_st25_R1.DS.sam 20140522_st25_R2.DS.sam 20140522_st25.SS.sam 20140522_st25_R1.DS  20140522_st25_R2.DS 20140522_st25.SS
python ${path}decide_read_pos.py 20140627_2st25_R1.DS.sam 20140627_2st25_R2.DS.sam 20140627_2st25.SS.sam 20140627_2st25_R1.DS 20140627_2st25_R2.DS 20140627_2st25.SS


${path}map_to_fragment 20140415_st25_R1.DS 20140415_st25_R2.DS 20140415_st25.SS ${genome_path}oryLat2.HindIII_fragments 20140415_st25_R1.DS.mapped 20140415_st25_R2.DS.mapped 20140415_st25.SS.mapped
${path}map_to_fragment 20140522_st25_R1.DS 20140522_st25_R2.DS 20140522_st25.SS ${genome_path}oryLat2.HindIII_fragments 20140522_st25_R1.DS.mapped 20140522_st25_R2.DS.mapped 20140522_st25.SS.mapped
${path}map_to_fragment 20140627_2st25_R1.DS 20140627_2st25_R2.DS 20140627_2st25.SS ${genome_path}oryLat2.HindIII_fragments 20140627_2st25_R1.DS.mapped 20140627_2st25_R2.DS.mapped 20140627_2st25.SS.mapped


${path}get_dA_dB 20140415_st25_R1.DS.mapped 20140415_st25_R2.DS.mapped 20140415_st25.SS.mapped ${genome_path}oryLat2.HindIII_fragments 20140415_st25.DS.dA_dB 20140415_st25.SS.dA_dB
${path}get_dA_dB 20140522_st25_R1.DS.mapped 20140522_st25_R2.DS.mapped 20140522_st25.SS.mapped ${genome_path}oryLat2.HindIII_fragments 20140522_st25.DS.dA_dB 20140522_st25.SS.dA_dB
${path}get_dA_dB 20140627_2st25_R1.DS.mapped 20140627_2st25_R2.DS.mapped 20140627_2st25.SS.mapped ${genome_path}oryLat2.HindIII_fragments 20140627_2st25.DS.dA_dB 20140627_2st25.SS.dA_dB


${path}uniform_dA_dB ${genome_path}oryLat2.HindIII_fragments oryLat2.HindIII_fragments.uniform_dA_dB


20140415_st25_DS_min=0
20140415_st25_DS_max=0
20140415_st25_SS_min=0
20140415_st25_SS_max=0
20140522_st25_DS_min=0
20140522_st25_DS_max=0
20140522_st25_SS_min=0
20140522_st25_SS_max=0
20140627_2st25_DS_min=0
20140627_2st25_DS_max=0
20140627_2st25_SS_min=0
20140627_2st25_SS_max=0


${path}dA_dB_filtering 20140415_st25_R1.DS.mapped 20140415_st25_R2.DS.mapped 20140415_st25.SS.mapped ${genome_path}oryLat2.HindIII_fragments $20140415_st25_DS_min $20140415_st25_DS_max $20140415_st25_SS_min $20140415_st25_SS_max 20140415_st25_R1.DS.valid 20140415_st25_R2.DS.valid 20140415_st25.SS.valid
${path}dA_dB_filtering 20140522_st25_R1.DS.mapped 20140522_st25_R2.DS.mapped 20140522_st25.SS.mapped ${genome_path}oryLat2.HindIII_fragments $20140522_st25_DS_min $20140522_st25_DS_max $20140522_st25_SS_min $20140522_st25_SS_max 20140522_st25_R1.DS.valid 20140522_st25_R2.DS.valid 20140522_st25.SS.valid
${path}dA_dB_filtering 20140627_2st25_R1.DS.mapped 20140627_2st25_R2.DS.mapped 20140627_2st25.SS.mapped ${genome_path}oryLat2.HindIII_fragments $20140627_2st25_DS_min $20140627_2st25_DS_max $20140627_2st25_SS_min $20140627_2st25_SS_max 20140627_2st25_R1.DS.valid 20140627_2st25_R2.DS.valid 20140627_2st25.SS.valid


# cfm := contact frequency matrix
${path}convert_to_cfm ${genome_path}oryLat2.HindIII_fragments 20140415_st25_R1.DS.valid 20140415_st25_R2.DS.valid 20140415_st25.cfm.no_norm
${path}convert_to_cfm ${genome_path}oryLat2.HindIII_fragments 20140522_st25_R1.DS.valid 20140522_st25_R2.DS.valid 20140522_st25.cfm.no_norm
${path}convert_to_cfm ${genome_path}oryLat2.HindIII_fragments 20140627_2st25_R1.DS.valid 20140627_2st25_R2.DS.valid 20140627_2st25.cfm.no_norm

# cfv := contact frequency vector
${path}convert_to_SS_cfv ${genome_path}oryLat2.HindIII_fragments 20140415_st25.SS.valid 20140415_st25.cfv.no_norm
${path}convert_to_SS_cfv ${genome_path}oryLat2.HindIII_fragments 20140522_st25.SS.valid 20140522_st25.cfv.no_norm
${path}convert_to_SS_cfv ${genome_path}oryLat2.HindIII_fragments 20140627_2st25.SS.valid 20140627_2st25.cfv.no_norm


${path}normalize_visibility ${genome_path}oryLat2.HindIII_fragments 20140415_st25_R1.DS.valid 20140415_st25_R2.DS.valid 20140415_st25.SS.valid 20140415_st25.cfm.norm 20140415_st25.cfv.norm
${path}normalize_visibility ${genome_path}oryLat2.HindIII_fragments 20140522_st25_R1.DS.valid 20140522_st25_R2.DS.valid 20140522_st25.SS.valid 20140522_st25.cfm.norm 20140522_st25.cfv.norm
${path}normalize_visibility ${genome_path}oryLat2.HindIII_fragments 20140627_2st25_R1.DS.valid 20140627_2st25_R2.DS.valid 20140627_2st25.SS.valid 20140627_2st25.cfm.norm 20140627_2st25.cfv.norm


${path}calc_visibility 20140415_st25.cfm.no_norm 20140415_st25.cfv.no_norm 20140415_st25.visibility.no_norm
${path}calc_visibility 20140522_st25.cfm.no_norm 20140522_st25.cfv.no_norm 20140522_st25.visibility.no_norm
${path}calc_visibility 20140627_2st25.cfm.no_norm 20140627_2st25.cfv.no_norm 20140627_2st25.visibility.no_norm
${path}calc_visibility 20140415_st25.cfm.norm 20140415_st25.cfv.norm 20140415_st25.visibility.norm
${path}calc_visibility 20140522_st25.cfm.norm 20140522_st25.cfv.norm 20140522_st25.visibility.norm
${path}calc_visibility 20140627_2st25.cfm.norm 20140627_2st25.cfv.norm 20140627_2st25.visibility.norm
