import std.stdio, std.conv, std.string, std.algorithm, std.array,
  std.math, std.file;

version(unittest){}
 else{
   void main(string[] args)
   {
     auto DS1File = args[1];
     auto DS2File = args[2];
     auto outFile = args[3];

     auto sparseCfm = DStoCfm(DS1File, DS2File);
     
     auto fout = File(outFile, "w");
     foreach(key; sparseCfm.byKey) {
       fout.writeln(key[0], "\t", key[1], "\t", sparseCfm[key]);
     }
   }
 }




uint[Tuple!(uint, uint)] DStoCfm(string file1, string file2)
{
  uint[Tuple!(uint, uint)] sparseFreqMat;
  auto fin1 = File(file1, "r");
  auto fin2 = File(file2, "r");

  while(true) {
    auto fields1 = fin1.readln.to!string.strip.split('\t');
    auto fields2 = fin2.readln.to!string.strip.split('\t');

    if(fields1.length != 4 || fields2.length !=4)
      break;

    auto i = fields1[3].to!uint;
    auto j = fields2[3].to!uint;
        
    //uint.init == 0, so first increment return 1.
    sparseFreqMat[tuple(min(i, j), max(i, j))]++;
  }

  return sparseFreqMat;
}


unittest
{
  auto deleteme1 = testFilename();
  auto deleteme2 = testFilename();
  scope(exit) { std.file.remove(deleteme1); }
  scope(exit) { std.file.remove(deleteme2); }

  auto ds1 = File(deleteme1, "w");
  auto ds2 = File(deleteme2, "w");

  ds1.write("0\t-\t20\t18\n3\t-\t82\t25\n3\t-\t82\t18\n");
  ds2.write("0\t-\t18\t30\n3\t-\t82\t18\n3\t-\t82\t25\n");

  ds1.close;
  ds2.close;
  
  auto cfm = DStoCfm(deleteme1, deleteme2);

  assert(cfm.length == 2);
  assert(cfm[tuple(uint(18), uint(30))] == 1);
  assert(cfm[tuple(uint(18), uint(25))] == 2);
}


version(unittest)
string testFilename(string file = __FILE__, size_t line = __LINE__) @safe pure
{
  import std.conv : text;
  import std.path : baseName;
  return text("deleteme-.", baseName(file), ".", line);
}
