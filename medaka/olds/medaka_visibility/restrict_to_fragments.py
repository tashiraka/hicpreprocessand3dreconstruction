#!/bio/bin/python
# -*- coding: utf-8 -*-
from Bio import SeqIO, Restriction, Seq, SeqUtils
from argparse import RawTextHelpFormatter
import numpy as np
import argparse
import re


parser = argparse.ArgumentParser(
    description='''
  This command is tool to cut out restriction fragments from a genome.
  A site position is followed by 1-origin coordinate of the genome.
  This command returns the positions where overhangs fullfilled.
  A restriction fragment is represented as a closed interval, [start, end].
  The output format is as follows.
  <fragment ID>\t<chromosome ID>\t<start position>\t<end position>
    
  For example:
  5\'-GAATTC-3\'
  5\'-CTTAAG-3\' is recognizad by a restriction enzyme EcoRI.
  EcoRI cuts this sequence to
  5\'-G  AATTC-3\'
  3\'-CTTAA  G-5\'.
  Then this command returns, (assuming the sequence is called chr1)
  chr1\t1\t5
  chr1\t6\t10
  ''',
    epilog="Author: Yuichi Motai",
    formatter_class=RawTextHelpFormatter)

parser.add_argument('-v', '--version', action='version', version='version 1.0')
parser.add_argument('genome', help='fasta file containing reference genome')
parser.add_argument('RE', help='restriction enzyme name')
parser.add_argument('out_file', help='output file')
args = parser.parse_args()


# This chromosome name parser is for oryLat2
def toID(chrName):
    if chrName == "chr1":
        return '0'
    elif chrName == "chr2":
        return '1'
    elif chrName == "chr3":
        return '2'
    elif chrName == "chr4":
        return '3'
    elif chrName == "chr5":
        return '4'
    elif chrName == "chr6":
        return '5'
    elif chrName == "chr7":
        return '6'
    elif chrName == "chr8":
        return '7'
    elif chrName == "chr9":
        return '8'
    elif chrName == "chr10":
        return '9'
    elif chrName == "chr11":
        return '10'
    elif chrName == "chr12":
        return '11'
    elif chrName == "chr13":
        return '12'
    elif chrName == "chr14":
        return '13'
    elif chrName == "chr15":
        return '14'
    elif chrName == "chr16":
        return '15'
    elif chrName == "chr17":
        return '16'
    elif chrName == "chr18":
        return '17'
    elif chrName == "chr19":
        return '18'
    elif chrName == "chr20":
        return '19'
    elif chrName == "chr21":
        return '20'
    elif chrName == "chr22":
        return '21'
    elif chrName == "chr23":
        return '22'
    elif chrName == "chr24":
        return '23'
    else:
        print "ERROR: invalid chromosome name, " + chrName
        quit()


restE = getattr(Restriction, args.RE)
fout = open(args.out_file, "w")
fragment_id = 0


if(restE.is_blunt()):
    for chrom in SeqIO.parse(open(args.genome, 'r'), 'fasta') :
        if chrom.name != 'chrM' \
           and re.match(r'scaffold', chrom.name) is None \
           and re.match(r'ultracontig', chrom.name) is None:
            prev_rsite = 1
            
            for rsite in restE.search(chrom.seq) :
                fout.write(str(fragment_id) + "\t" + toID(chrom.name) \
                           + '\t' + str(prev_rsite) + '\t' + str(rsite - 1) + '\n')
                prev_rsite = rsite
                fragment_id += 1
            else:
                fout.write(str(fragment_id) + "\t" + toID(chrom.name) \
                           + "\t" + str(prev_rsite) + "\t" + str(len(chrom.seq)) + '\n')
                fragment_id += 1

elif(restE.is_5overhang()):
    for chrom in SeqIO.parse(open(args.genome, 'r'), 'fasta') :
        if chrom.name != 'chrM' \
           and re.match(r'scaffold', chrom.name) is None \
           and re.match(r'ultracontig', chrom.name) is None:
            prev_rsite = 1
            
            for rsite in restE.search(chrom.seq) :
                fout.write(str(fragment_id) + "\t" + toID(chrom.name) \
                           + '\t' + str(prev_rsite) + '\t' + str(rsite - 1 - restE.ovhg) + '\n')
                prev_rsite = rsite
                fragment_id += 1
            else:
                fout.write(str(fragment_id) + "\t" + toID(chrom.name) \
                           + "\t" + str(prev_rsite) + "\t" + str(len(chrom.seq)) + '\n')
                fragment_id += 1
            
elif(restE.is_3overhag()):
    for chrom in SeqIO.parse(open(args.genome, 'r'), 'fasta') :
        if chrom.name != 'chrM' \
           and re.match(r'scaffold', chrom.name) is None \
           and re.match(r'ultracontig', chrom.name) is None:
            prev_rsite = 1
        
            for rsite in restE.search(chrom.seq) :
                fout.write(str(fragment_id) + "\t" + toID(chrom.name) \
                           + '\t' + str(prev_rsite) + '\t' + str(rsite - 1) + '\n')
                prev_rsite = rsite - restE.ovhg
                fragment_id += 1
            else:
                fout.write(str(fragment_id) + "\t" + toID(chrom.name) \
                           + "\t" + str(prev_rsite) + "\t" + str(len(chrom.seq)) + '\n')
                fragment_id += 1

else:
    print 'ERROR: The type of overhang is \'unkown\''    
    quit
