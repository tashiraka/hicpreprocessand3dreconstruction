/***
 * sparse matrix and full vector version
 ***/
import std.stdio, std.conv, std.string, std.algorithm, std.array,
  std.math, std.parallelism, std.typecons;


version(unittest){}
 else{
   void main(string[] args)
   {
     auto fragFile = args[1];
     auto DS1File = args[2];
     auto DS2File = args[3];
     auto SSFile = args[4];
     auto outMatrixFile = args[5];
     auto outVectorFile = args[6];

     writeln("a");
     auto matSize = countLine(fragFile);
     writeln("b");
     auto sparseODS = DStoCfm(DS1File, DS2File);
     writeln("c");
     auto OSS = SStoCfv(SSFile, matSize);
     writeln("d");
     auto B = new double[](matSize);
     auto B0 = 1.0;
     auto SDS = new double[](matSize);
     auto SSS = new double[](matSize);
     auto dB = new double[](matSize);
     writeln("e");
     auto sparseW = sparseODS.dup;
     writeln("f");
     auto initialMeanVisibility = sparseW.visibility(matSize).mean;
     writeln("g");
     foreach(x; sparseODS) {
       if(isNaN(x.elem)) {
         throw new Exception("sparseODS hae NaN.");
       }
     }
     writeln("h");
     foreach(x; OSS) {
       if(isNaN(x)) {
         throw new Exception("OSS hae NaN.");
       }
     }
     writeln("i");
     if(isNaN(sparseODS.mean))
       throw new Exception("mean of sparseODS is NaN.");
     writeln("j");
     // init
     B.fill(1.0);
     B0 = 1.0;
     SDS.fill(0.0);
     SSS.fill(0.0);
     dB.fill(0.0);

     auto flag = true;
          
     //main iteration
     foreach(loopCount; 0..100) {
       writeln(loopCount, " start");

       // SDS and SSS have different order, so different weight.it's wrong.
       auto Wmean = sparseW.mean; // add
       foreach(ref w; sparseW) { // add
         w.elem = w.elem * 1000.0 / Wmean;
       }

       SDS = sparseW.visibility(matSize);
       foreach(x; SDS) {
         if(isNaN(x)) {
           throw new Exception("SDS hae NaN.");
         }
       }
       SSS[] = OSS[] / B[]; //normalize OSS
       //       SSS[] = OSS[] / (B[] * B0); //normalize OSS

       auto SSSmean = SSS.mean; //add
       SSS[] = SSS[] * 1000.0 / SSSmean; //add
       
       foreach(x; SSS) {
         if(isNaN(x)) {
           throw new Exception("SSS hae NaN.");
         }
       }
       
       dB[]= SDS[] + SSS[];

       writeln(dB.mean);
       writeln(dB.map!(x => x^^2).array.mean - (dB.mean)^^2);
       writeln(sparseW.mean);
       writeln(SSS.mean);
       
       foreach(x; dB) {
         if(isNaN(x)) {
           throw new Exception("dB 1 hae NaN.");
         }
       }
       
       if(isNaN(dB.mean)) {
         throw new Exception("dB.mean is NaN.");
       }
       
       //dB[] /= dB.mean;
       foreach(x; dB) {
         if(isNaN(x)) {
           throw new Exception("dB 2 hae NaN.");
         }
       }

       dB = dB.map!(x => x<=0.0 ? 1.0 : x).array; //negative values are ignored.
       foreach(x; dB) {
         if(isNaN(x)) {
           throw new Exception("dB 3 hae NaN.");
         }
       }

       //condition of termination
       flag = true;       
       foreach(elem; dB) {
         if(abs(elem - 1.0) < 0.00001) {
           flag = true;
         }
         else {
           flag = false;
           break;
         }
       }
       
       if(flag) {
         writeln("Finished at # of loop: ", loopCount);
         break;
       }

       foreach(ref entry; sparseW.parallel) {
         entry.elem /= dB[entry.id1] * dB[entry.id2];
       }

       foreach(x; sparseW) {
         if(isNaN(x.elem)) {
           throw new Exception("sparseW hae NaN.");
         }
       }

       
       B[] *= dB[];
       foreach(x; B) {
         if(isNaN(x)) {
           throw new Exception("B hae NaN.");
         }
       }
       
       B0 = B.mean;
       if(isNaN(B0)) {
         throw new Exception("B0 hae NaN.");
       }
       //       writeln(loopCount, " end");
     }

     if(!flag) {
       writeln("Finished at # of loop: not converged");
     }

     //TDS: sparseW;
     //TSS: SSS[] = OSS[] / (B[] * B0);
     SSS[] = OSS[] / (B[] * B0);
     foreach(x; SSS) {
       if(isNaN(x)) {
         throw new Exception("last SSS hae NaN.");
       }
     }
     if(isNaN(SSS.mean)) {
         throw new Exception("last SSS.mean hae NaN.");
     }
     if(isNaN(sparseW.mean)) {
         throw new Exception("last sparseW.mean hae NaN.");
     }
     if(isNaN(OSS.mean)) {
         throw new Exception("last OSS.mean hae NaN.");
     }
     if(isNaN(sparseODS.mean)) {
       throw new Exception("last sparseODS.mean hae NaN.");
     }
     writeln("T: ", SSS.mean / sparseW.mean);
     writeln("O: ", OSS.mean / sparseODS.mean);

     // scaling
     auto finalMeanVisibility = sparseW.visibility(matSize).mean;
     auto scaler = initialMeanVisibility / finalMeanVisibility;
     foreach(ref entry; sparseW.parallel) {
       entry.elem *= scaler;
     }
     
     SSS[] *= initialMeanVisibility / finalMeanVisibility;

     auto fout = File(outMatrixFile, "w");
     foreach(entry; sparseW) {
       fout.writeln(entry.id1, "\t", entry.id2, "\t", entry.elem);
     }

     fout = File(outVectorFile, "w");
     foreach(elem; SSS) {
       fout.writeln(elem);
     }
   }
 }


double[] visibility(Tuple!(uint, "id1", uint, "id2", double, "elem")[] sparseMat, uint size) {
  auto v = new double[](size);
  v.fill(0);
  foreach(entry; sparseMat.parallel) {
    v[entry.id1] += entry.elem;
    v[entry.id2] += entry.elem;      
  }
  return v;
}


// mean of positive value
double mean(double[] arr) {
  auto farr = arr.filter!(x => x > 0.0);
  return farr.sum / farr.array.length;
}


// mean of positive value
double mean(Tuple!(uint, "id1", uint, "id2", double, "elem")[] sparseMat) {
  return reduce!((a, b) => a + b.elem)(0.0, sparseMat) / sparseMat.length;
}


Tuple!(uint, "id1", uint, "id2", double, "elem")[] DStoCfm(string file1,
                                                           string file2)
{
  uint[Tuple!(uint, uint)] sparseFreqMatAS;
  auto indices1 = File(file1, "r")
    .byLine.map!(x => x.to!string.strip.split('\t')[3]).array;
  auto indices2 = File(file2, "r")
    .byLine.map!(x => x.to!string.strip.split('\t')[3]).array;

  if(indices1.length != indices2.length) {
    throw new Exception("ERROR: # of entry in DS files are different.");
  }
  
  foreach(k; 0..indices1.length) {
    auto i = indices1[k].to!uint;
    auto j = indices2[k].to!uint;
    //double.init == 0, but first increment return 1.
    if(i < j) {
      sparseFreqMatAS[tuple(i, j)] += 1;
    }
    else {
      sparseFreqMatAS[tuple(j, i)] += 1;
    }
  }
  auto sparseFreqMat = new Tuple!(uint, "id1",
                                  uint, "id2",
                                  double, "elem")[](sparseFreqMatAS.length);

  uint index = 0;
  foreach(key; sparseFreqMatAS.byKey) {
    sparseFreqMat[index++] = tuple(key[0], key[1],
                                   sparseFreqMatAS[key].to!double);
  }
  
  return sparseFreqMat;
}


double[] SStoCfv(string file, uint size)
{
  auto freqMat = new double[](size);
  freqMat.fill(0);

  auto indices = File(file, "r")
    .byLine.map!(x => x.to!string.strip.split('\t')[3]).array;

  foreach(k; 0..indices.length) {
    freqMat[indices[k].to!uint] += 1.0;
  }
  
  return freqMat;
}


uint countLine(string filename)
{
  uint count = 0;
  foreach(line; File(filename, "r").byLine) {
    if(line != "") {
      count++;
    }
  }     
  return count;
}
