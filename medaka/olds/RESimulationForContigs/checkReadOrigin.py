#!/bio/bin/python
# -*- coding: utf-8 -*-
from Bio import SeqIO, Restriction, Seq, SeqUtils
from argparse import RawTextHelpFormatter
import numpy as np
import sys
import re


args = sys.argv
bamFile = args[1]
strainName = args[2] #Hd-rR of HNI
fragFile = args[3] #Corresponding to strainName
outFile = args[4]
