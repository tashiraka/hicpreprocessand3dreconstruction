#!/bio/bin/python
# -*- coding: utf-8 -*-
from Bio import SeqIO, Restriction, Seq, SeqUtils
from argparse import RawTextHelpFormatter
import numpy as np
import argparse
import re


parser = argparse.ArgumentParser(
    description='''
  This command is tool to create Hi-C reads for RE simulation.
  A site position is followed by 1-origin coordinate of the genome.
  This command returns the positions where overhangs fullfilled.
  A restriction fragment is represented as a closed interval, [start, end].
  The input format is as follows.
  <fragment ID>\t<sequence name>\t<start position>\t<end position>\t<gap-containing flag>
    
  For example:
  5\'-GAATTC-3\'
  5\'-CTTAAG-3\' is recognizad by a restriction enzyme EcoRI.
  EcoRI cuts this sequence to
  5\'-G  AATTC-3\'
  3\'-CTTAA  G-5\'.
  Then this command returns, (assuming the sequence is called chr1)
  chr1\t1\t5
  chr1\t6\t10
  ''',
    epilog="Author: Yuichi Motai",
    formatter_class=RawTextHelpFormatter)

parser.add_argument('-v', '--version', action='version', version='version 1.0')
parser.add_argument('genome', help='fasta file containing reference genome')
parser.add_argument('RE', help='restriction enzyme name')
parser.add_argument('gap', help='gap.txt provided by UCSC Genome Browser')
parser.add_argument('maxSelectLen', help='The max length of selected DNA molecules')
parser.add_argument('minSelectLen', help='The min length of selected DNA molecules')
parser.add_argument('readLen', help='The read length')
parser.add_argument('stepLen', help='The step size of read creation')
parser.add_argument('outFile', help='output file')
parser.add_argument('totalReadFile', help='file for the number of total read')
args = parser.parse_args()

maxSelectLen = int(args.maxSelectLen)
minSelectLen = int(args.minSelectLen)
readLen = int(args.readLen)
stepLen = int(args.stepLen)
outFile = args.outFile
totalReadFile = args.totalReadFile

qality = 'J' * readLen

# bin\tchrom\tchromStart\tchromEnd\tix\tn\tsize\ttype\tbridge
# Note: all start coordinates in our database are 0-based, not 1-based.
# Note: [chromStart, chromEnd)
gap = {}
for line in open(args.gap):
    record = line.rstrip().split('\t')

    if record[1] in gap:
        gap[record[1]].append((int(record[2]) + 1, int(record[3])))
    else:
        gap.setdefault(record[1], [(int(record[2]) + 1, int(record[3]))])

        
restE = getattr(Restriction, args.RE)    

def fragStartBlunt(rsite, ovhg):
    return rsite

def fragEndBlunt(rsite, ovhg):
    return rsite - 1

def fragStart5overhang(rsite, ovhg):
    return rsite

def fragEnd5overhang(rsite, ovhg):
    return rsite - 1 - ovhg

def fragStart3overhang(rsite, ovhg):
    return rsite - restE.ovhg

def fragEnd3overhang(rsite, ovhg):
    return rsite - 1


def fragmentation(fragStart, fragEnd):
    fragment_id = 0

    for chrom in SeqIO.parse(open(args.genome, 'r'), 'fasta'):
        prev_rsite = 1
       
        for rsite in restE.search(chrom.seq):
            start = prev_rsite
            end = fragEnd(rsite, restE.ovhg)
            has_gap = False
            if chrom.name in gap:
                for g in gap[chrom.name]:
                    if(g[0] <= end and start <= g[1]):
                        has_gap = True
                        break

            gap_flag = '1' if has_gap else '0'

            # Create Hi-C read from the fragment start
            # Not include the start of contig.
            startTotalRead = 0
            endTotalRead = 0

            if start != 1:
                maxPos = start + maxSelectLen - 1
                if end < maxPos:
                    maxPos = end
                
                for readPos in range(start, maxPos - readLen + 1, stepLen):
                    fout.write('@' + str(fragment_id) + '_start_' + str(readPos) + '\n')
                    fout.write(str(chrom.seq[readPos - 1 : readPos - 1 + readLen].reverse_complement()) + '\n')
                    fout.write('+\n')	
                    fout.write(qality + '\n')
                    startTotalRead += 1

            # Create Hi-C read from the fragment end
            minPos = end - maxSelectLen + 1
            if minPos < start:
                minPos = start
            
            for readPos in range(minPos, end - readLen, stepLen):
                fout.write('@' + str(fragment_id) + '_end_' + str(readPos) + '\n')
                fout.write(str(chrom.seq[readPos - 1 : readPos - 1 + readLen]) + '\n')
                fout.write('+\n')	
                fout.write(qality + '\n')
                endTotalRead += 1


            prev_rsite = fragStart(rsite, restE.ovhg)
            foutTotal.write(str(fragment_id) + '\t' + str(startTotalRead) + '\t' + str(endTotalRead) + '\n')
            fragment_id += 1

        else:
            start = prev_rsite
            end = len(chrom.seq)
            has_gap = False
            if chrom.name in gap:
                for g in gap[chrom.name]:
                    if(g[0] <= end and start <= g[1]):
                        has_gap = True
                        break

            gap_flag = '1' if has_gap else '0'

            
            # Create Hi-C read from the fragment start
            # Not include the start of contig.
            startTotalRead = 0
            endTotalRead = 0

            if prev_rsite != 1:
                mxPos = start + maxSelectLen - 1
                if end < maxPos:
                    maxPos = end
                
                for readPos in range(start, maxPos - readLen, stepLen):
                    fout.write('@' + str(fragment_id) + '_start_' + str(readPos) + '\n')
                    fout.write(str(chrom.seq[readPos - 1 : readPos - 1 + readLen].reverse_complement()) + '\n')
                    fout.write('+\n')	
                    fout.write(qality + '\n')

                    startTotalRead += 1

            # Not include the end of contig.

            foutTotal.write(str(fragment_id) + '\t' + str(startTotalRead) + '\t' + str(endTotalRead) + '\n')
            fragment_id += 1
            
fout = open(outFile, 'w')
foutTotal = open(totalReadFile, 'w')

if(restE.is_blunt()):
    fragmentation(fragStartBlunt, fragEndBlunt)
elif(restE.is_5overhang()):
    fragmentation(fragStart5overhang, fragEnd5overhang)           
elif(restE.is_3overhag()):
    fragmentation(fragStart3overhag, fragEnd3overhag)
else:
    print 'ERROR: The type of overhang is \'unkown\''    
    quit
            
