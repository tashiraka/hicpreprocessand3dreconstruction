import sys, os, logging, subprocess, pysam, tempfile, re, glob


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def removeDup(inStream, outStream):
    prevLine = ''

    for line in inStream:
        if prevLine == '':
            prevLine = line
        elif line.split('\t')[0] == prevLine.split('\t')[0]:
            mapq = int(line.split('\t')[4])
            prevMapq = int(prevLine.split('\t')[4])
            readLen = len(line.split('\t')[9])
            prevReadLen = len(line.split('\t')[9])
            
            if (mapq > prevMapq) or (mapq == prevMapq and readLen > prevReadLen):
                prevLine = line
            else:
                prevLine = prevLine
        else:
            outStream.write(prevLine)
            prevLine = line

    outStream.write(prevLine)

    
def mergeBam(bamPrefix):
    # The input files of iterative mapping
    bamPath = '/'.join(bamPrefix.split('/')[:-1])

    bams = filter(lambda x: re.match(bamPrefix + '.[0-9]+$', x), glob.glob(bamPath + '/*'))
    #bams = filter(lambda x: re.match(bamPrefix + '.[0-9]+$', x), subprocess.Popen(['ls', '-d', bamPath + '/*'], stdout=subprocess.PIPE).communicate()[0].split())

    logging.info('Input BAM files: %s', ' '.join(bams))

    # Sort the input files
    sortPipes = []
    for bam in bams:
        cmd = ['samtools', 'sort', '-n', bam, bam + '.sorted']
        logging.info('Sort BAM files command: {0}'.format(' '.join(cmd)))
        sortPipes.append(subprocess.Popen(cmd))
    sortedBams = [bam + '.sorted.bam' for bam in bams]
    [p.communicate() for p in sortPipes]

    # Merge the mapped reads
    mergedBam = bamPrefix + '.merged'
    cmd = ['samtools', 'merge', '-fn', mergedBam] + sortedBams
    logging.info('Merge BAM files command: {0}'.format(' '.join(cmd)))        
    p = subprocess.Popen(cmd)
    p.communicate()

    for sortedBam in sortedBams:
        os.remove(sortedBam)
    
    # Filter out the secondary reads
    filteredBam = mergedBam + '.filtered'
    cmd = ['samtools', 'view', '-b', '-F', '0x100', mergedBam]
    logging.info('Filter command: {0} > {1}'.format(' '.join(cmd), filteredBam))
    p = subprocess.Popen(cmd, stdout=open(filteredBam, 'w'))
    p.communicate()
 
    os.remove(mergedBam)
    
    # Remove the duplicated reads
    logging.info('Remove duplicated records')

    noDupSam = filteredBam + '.noDup.sam'
    p = subprocess.Popen(['samtools', 'view', '-H', filteredBam], stdout=open(noDupSam, 'w'))
    p.communicate()
    
    p = subprocess.Popen(['samtools', 'view', filteredBam], stdout=subprocess.PIPE)
    removeDup(p.stdout, open(noDupSam, 'a'))
    p.communicate()

    os.remove(filteredBam)

    # Create bam file
    outBam = bamPrefix
    p = subprocess.Popen(['samtools', 'view', '-Sb', noDupSam], stdout=open(outBam, 'w'))
    p.communicate()
    os.remove(noDupSam)
    
args = sys.argv
bamPrefix = args[1] # if SRR123.bam.X, then SRR123.bam
logging.info('Start merging')
logging.info('merge({0})'.format(bamPrefix))
mergeBam(bamPrefix)
logging.info('End merging')
