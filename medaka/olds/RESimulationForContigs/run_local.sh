cmdPath=~/Workspace/Hi-C_workbench/master/medaka/src/RESimulationForContigs
HdrRPath=data/Hd-rR
HNIPath=data/HNI
HdrRGenome=${HdrRPath}/Hd-rR-P6wP5.falcon_contig-20150507.quiver.pilon.fasta
HNIGenome=${HNIPath}/HNI-168cells-falcon_contigs-20150508.p_ctg.quiver.pilon.fasta
HdrRGap=${HdrRPath}/gap.txt
HNIGap=${HNIPath}/gap.txt
#Contigs do not have gaps.
touch $HdrRGap
touch $HNIGap
REs=`python ${cmdPath}/listUpRE.py | sed -e "s/\[\|\]\|'\|\,//g"`

echo $REs

maxSelectLen=500
minSelectLen=100
readLen=50
stepLen=10

for re in $REs
do
    echo $re
    fragFile=${HdrRGenome}.fragments.${re}
    #python ${cmdPath}/createREFragments.py $HdrRGenome $re $HdrRGap $fragFile

    binSize=1000
    maxFragLen=10000
    outFile=${fragFile}.freqTable
    ${cmdPath}/createFreqTableOfFragLenForContigs $fragFile $binSize $maxFragLen $outFile

    #outFile=${HdrRGenome}.${re}.fasta
    #python ${cmdPath}/createReadsForContigs.py $HdrRGenome $re $HdrRGap \
    #    $maxSelectLen $minSelectLen $readLen $stepLen $outFile
    
    fragFile=${HNIGenome}.fragments.${re}
    #python ${cmdPath}/createREFragments.py $HNIGenome $re $HNIGap $fragFile

    binSize=1000
    maxFragLen=10000
    outFile=${fragFile}.freqTable
    ${cmdPath}/createFreqTableOfFragLenForContigs $fragFile $binSize $maxFragLen $outFile

    #outFile=${fragFile}.fasta
    #python ${cmdPath}/createReadsForContigs.py $HNIGenome $re $HNIGap \
    #    $maxSelectLen $minSelectLen $readLen $stepLen $outFile

done
