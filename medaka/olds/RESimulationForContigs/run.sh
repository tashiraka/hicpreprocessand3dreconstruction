cmdPath=~/Workspace/Hi-C_workbench/master/medaka/src/RESimulationForContigs
HdrRPath=/work/yuichi/medaka/genome/Hd-rR/20150507
HNIPath=/work/yuichi/medaka/genome/HNI/20150508
F1Path=/work/yuichi/medaka/genome/F1_Hd-rR_HNI
HdrRGenome=${HdrRPath}/Hd-rR-P6wP5.falcon_contig-20150507.quiver.pilon.fasta
HNIGenome=${HNIPath}/HNI-168cells-falcon_contigs-20150508.p_ctg.quiver.pilon.fasta
F1Genome=${F1Path}/Hd-rR20150507_HNI20150508.fasta
HdrRGap=${HdrRPath}/gap.txt
HNIGap=${HNIPath}/gap.txt
F1Gap=${F1Path}/gap.txt
#Contigs do not have gaps.
touch $HdrRGap
touch $HNIGap
touch $F1Gap
REs=`python ${cmdPath}/listUpRE.py`
REs=${REs//\[/}
REs=${REs//\]/}
REs=${REs//\'/}
REs=${REs//\,/}
maxSelectLen=500
minSelectLen=100
readLen=50
stepLen=10

for re in $REs
do
    echo $re

    strain="Hd-rR"
    fragFile=${HdrRGenome}.fragments.${re}
    HdrRFragFile=$fragFile
    binSize=1000
    maxFragLen=10000
    outFile=${fragFile}.freqTable
    readFile=${fragFile}.fastq
    totalReadFile=${fragFile}.totalRead
    HdrRTotalReadFile=$totalReadFile
    mappedFile=${fragFile}.bam
    mappingMinLen=25
    mappingStepLen=5
    mapq=30
    nThread=24
    readGroupInfo="@RG\tID:Hd-rR_${re}\tSM:Hd-rR_${re}\tPL:dummy\tLB:dummy\tPU:dummy"
    HdrRFilteredMappedFile=${mappedFile}.filtered.${mapq}
    readOriginTable=${filteredMappedFile}.originTable
    
    #python ${cmdPath}/createREFragments.py $strain $HdrRGenome $re $HdrRGap $fragFile
    #python ${cmdPath}/createFreqTableOfFragLenForContigs.py $fragFile $binSize $maxFragLen $outFile
    #python ${cmdPath}/createReadsForContigs.py $HdrRGenome $re $HdrRGap \
    #    $maxSelectLen $minSelectLen $readLen $stepLen $readFile $totalReadFile
    #pigz $readFile
    #python ${cmdPath}/iterative_mapping.py 'bwa' $readGroupInfo \
    #       ${F1Path}/bwaIndex/Hd-rR20150507_HNI20150508.fasta \
    #       $readFile $mappedFile $mappingMinLen $mappingStepLen $mapq $nThread
    #python ${cmdPath}/mergeIterativeMappedBam.py $mappedFile
    #samtools view -b -q $mapq $mappedFile > $filteredMappedFile
    
    strain="HNI"
    fragFile=${HNIGenome}.fragments.${re}
    HNIFragFile=$fragFile
    binSize=1000
    maxFragLen=10000
    outFile=${fragFile}.freqTable
    readFile=${fragFile}.fastq
    totalReadFile=${fragFile}.totalRead
    HNITotalReadFile=$totalReadFile
    mappedFile=${fragFile}.bam
    mappingMinLen=25
    mappingStepLen=5
    mapq=30
    nThread=24
    readGroupInfo="@RG\tID:HNI_${re}\tSM:HNI_${re}\tPL:dummy\tLB:dummy\tPU:dummy"     
    HNIFilteredMappedFile=${mappedFile}.filtered.${mapq}
    readOriginTable=${filteredMappedFile}.originTable

    #python ${cmdPath}/createREFragments.py $strain $HNIGenome $re $HNIGap $fragFile
    #python ${cmdPath}/createFreqTableOfFragLenForContigs.py $fragFile $binSize $maxFragLen $outFile
    #python ${cmdPath}/createReadsForContigs.py $HNIGenome $re $HNIGap \
    #    $maxSelectLen $minSelectLen $readLen $stepLen $readFile $totalReadFile
    #pigz $readFile
    #python ${cmdPath}/iterative_mapping.py 'bwa' $readGroupInfo \
    #       ${F1Path}/bwaIndex/Hd-rR20150507_HNI20150508.fasta \
    #       $readFile $mappedFile $mappingMinLen $mappingStepLen $mapq $nThread
    #python ${cmdPath}/mergeIterativeMappedBam.py $mappedFile
    #samtools view -b -q $mapq $mappedFile > $filteredMappedFile


    statsFile=${F1Genome}.RESimulationStats.${re}

    ./originStatistics $HdrRFilteredMappedFile $HNIFilteredMappedFile $HdrRFragFile $HNIFragFile $HdrRTotalReadFile $HNITotalReadFile $statsFile

done
