/// Written in the Dprogramming language.

/**
 * Create the data table containing the origin and posions 
 * of reads mapped to the reference genome of F1 medaka 
 * derived from Hd-rR and HNI.
 *
 * Author: Yuichi Motai
 */


import std.stdio, std.string, std.conv, std.algorithm, std.array, std.process,
  std.regex;


version(unittest){}
 else{
   void main(string[] args)
   {
     auto bamFile = args[1];
     auto strainName = args[2]; //Hd-rR of HNI
     auto fragFile = args[3]; //Corresponding to strainName
     auto outFile = args[4];

     //fragment_id\tchrom.name\tstart\tend\tgap_flag
     string[][] fragTable;
     foreach(line; File(fragFile).byLine) {
       fragTable ~= line.to!string.strip.split('\t');
     }
     
     auto fout = File(outFile, "w");
     fout.writeln("OriginalStrain\tOriginalContig\tOriginalPos\tOriginalStart_or_End\tMappedStrain\tMappedContig\tMappedPos\tMappedStart_or_End");

     auto pipe = pipeProcess(["samtools", "view", bamFile], Redirect.stdout);
     foreach(line; pipe.stdout.byLine) {
       auto samRecord = new SamRecord(line.to!string);
       auto qnameFields = samRecord.qname.split("_");
       auto rnameFields = samRecord.rname.split("|");

       auto fragId = qnameFields[0];
       auto fragInfo = fragTable[fragId.to!int];
       if(fragId != fragInfo[0]) {
         throw new Exception("Error: Fragment IDs are not sorted.");
       }
       
       fout.writeln(strainName, "\t", fragInfo[1], "\t", qnameFields[2], "\t", qnameFields[1], "\t",
                    rnameFields[0], "\t", rnameFields[1], "\t", samRecord.pos, "\t", samRecord.whichEnd);
     }

   }
 }


class SamRecord
{
  string qname, rname;
  uint pos, fragID, DNALen;
  string whichEnd;
  
  this(string record) {
    auto fields = record.split('\t');
    qname = fields[0];
    rname = fields[2];
    pos = fields[3].to!uint;

    auto flag = fields[1].to!uint;
    whichEnd = (flag & 0x10) == 0 ? "end" : "start"; // + strand means end, while - start.
  }
}

