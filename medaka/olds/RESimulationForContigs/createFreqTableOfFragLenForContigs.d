import std.stdio, std.string, std.conv, std.algorithm, std.array;

void main(string[] args)
{
  auto fragFile = args[1];
  auto binSize = args[2].to!int;
  auto maxFragLen = args[3].to!int;
  auto outFile = args[4];
 
  auto frags = readFrags(fragFile);
  
  // Calculate the fragment lengthes.
  int[] fragLens;
  foreach(i, frag; frags) {
    if(0 < i && i < frags.length - 1) {
        if(frag.contigName == frags[i-1].contigName &&
           frag.contigName == frags[i+1].contigName) {
          fragLens ~= frag.endPos - frag.startPos + 1;
        }
    }
  }
  
  auto freqTable = new int[](maxFragLen / binSize + 1);
  foreach(fragLen; fragLens) {
    auto binNum = fragLen / binSize;
    if(binNum < freqTable.length) {
      freqTable[binNum]++;
    }
    else {
      freqTable[$-1]++;
    }
  }
  
  auto fout = File(outFile, "w");
  foreach(i, freq; freqTable) {
    if(i < freqTable.length - 1) {
      fout.writeln(i * binSize, '\t', (i+1) * binSize, '\t', freq);
    }
    else {
      fout.writeln(i * binSize, '\t', int.max, '\t', freq);
    }
  }
}


Fragment[] readFrags(string filename)
{
  Fragment[] frags;
  foreach(line; File(filename).byLine) {
    auto fields = line.to!string.strip.split('\t');
    frags ~= new Fragment(fields[0].to!int, fields[1], fields[2].to!int, 
                          fields[3].to!int, fields[4].to!int);
    
  }
  
  return frags;
}


class Fragment
{
  int fragID, startPos, endPos, gapFlag;
  string contigName;

  this(int a, string b, int c, int d, int f) {
    fragID = a; contigName = b; startPos = c, endPos = d; gapFlag = f;
  }
}
