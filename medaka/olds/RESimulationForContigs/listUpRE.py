from Bio import Restriction

reNames = Restriction.Restriction.AllEnzymes.elements()

reList = [re for re in reNames if \
          getattr(Restriction.Restriction, re).is_palindromic() and \
          getattr(Restriction.Restriction, re).cut_once() and \
          getattr(Restriction.Restriction, re).is_5overhang() and \
          getattr(Restriction.Restriction, re).is_defined() and \
          getattr(Restriction.Restriction, re).is_methylable() and \
          getattr(Restriction.Restriction, re).is_comm() and \
          getattr(Restriction.Restriction, re).size == 4]

print reList
