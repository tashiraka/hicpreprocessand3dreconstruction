/// Written in the Dprogramming language.
/**
 * Create the data table containing the origin and posions 
 * of reads mapped to the reference genome of F1 medaka 
 * derived from Hd-rR and HNI.
 *
 * Author: Yuichi Motai
 */


import std.stdio, std.string, std.conv, std.algorithm, std.array, std.process,
  std.regex;


version(unittest){}
 else{
   void main(string[] args)
   {
     auto HdrRBamFile = args[1];
     auto HNIBamFile = args[2];
     auto HdrRFragFile = args[3];
     auto HNIFragFile = args[4];
     auto HdrRTotalReadFile = args[5];
     auto HNITotalReadFile = args[6];
     auto outFile = args[7];

     auto HdrRFrags = readFrag(HdrRFragFile);
     auto HNIFrags = readFrag(HNIFragFile);

     uint HdrRFragNum = 0;
     foreach(frags; HdrRFrags) {
       HdrRFragNum += frags.length;
     }

     uint HNIFragNum = 0;
     foreach(frags; HNIFrags) {
       HNIFragNum += frags.length;
     }

     auto HdrRStat = new uint[][](HdrRFragNum, 2);
     auto HNIStat = new uint[][](HNIFragNum, 2);

     auto fout = File(outFile, "w"); 
     
     fout.writeln("Strain\tfragmentID\tTotalRead\tmappedRead\tmissMappedRead");

     auto HdrRPipe = pipeProcess(["samtools", "view", HdrRBamFile], Redirect.stdout); 

     foreach(line; HdrRPipe.stdout.byLine) {
       auto record = new SamRecord(line.to!string);
       auto qnameFields = record.qname.split("_");
       auto originalFragID = qnameFields[0].to!uint;
       auto originalStartOrEnd = qnameFields[1];
       auto originalPos = qnameFields[2];

       auto rnameFields = record.rname.split("|");
       auto mappedStrain = rnameFields[0];
       auto mappedContig = rnameFields[1];

       auto wrongStrainFlag = false;
       
       if(mappedStrain == "Hd-rR") {
         record.searchFrag(HdrRFrags);
       }
       else if(mappedStrain == "HNI") {
         record.searchFrag(HNIFrags);
         wrongStrainFlag = true;         
       }
       else {
         throw new Exception("ERROR: Strain name is not Hd-rR or HNI either.");
       }

       auto mappedFlag = true;

       if(wrongStrainFlag) {
         mappedFlag = false;
       }
       else if(originalFragID != record.fragID) {
         mappedFlag = false;
       }
       else if(record.DNALen < 50 || 500 < record.DNALen) {
         mappedFlag = false;
       }

       if(mappedStrain == "Hd-rR") {
         if(mappedFlag) {
           HdrRStat[record.fragID][0]++;
         }
         else {
           HdrRStat[record.fragID][1]++;
         }
       }
       else if(mappedStrain == "HNI") {
         if(mappedFlag) {
           HNIStat[record.fragID][0]++;
         }
         else {
           HNIStat[record.fragID][1]++;
         }
       }
     }
     

     foreach(line; File(HdrRTotalReadFile).byLine) {
       auto fields = line.to!string.strip.split("\t");
       fout.writeln("Hd-rR\t", fields[0], "\t",
                    fields[1].to!uint + fields[2].to!uint, "\t",
                    HdrRStat[fields[0].to!uint][0], "\t",
                    HdrRStat[fields[0].to!uint][1]);       
     }


     auto HNIPipe = pipeProcess(["samtools", "view", HNIBamFile], Redirect.stdout); 

     foreach(line; HNIPipe.stdout.byLine) {
       auto record = new SamRecord(line.to!string);
       
       auto qnameFields = record.qname.split("_");
       auto originalFragID = qnameFields[0].to!uint;
       auto originalStartOrEnd = qnameFields[1];
       auto originalPos = qnameFields[2];

       auto rnameFields = record.rname.split("|");
       auto mappedStrain = rnameFields[0];
       auto mappedContig = rnameFields[1];

       auto wrongStrainFlag = false;
       
       if(mappedStrain == "Hd-rR") {
         record.searchFrag(HdrRFrags);
         wrongStrainFlag = true;
       }
       else if(mappedStrain == "HNI") {
         record.searchFrag(HNIFrags);
       }
       else {
         throw new Exception("ERROR: Strain name is not Hd-rR or HNI either.");
       }
       
       auto mappedFlag = true;

       if(wrongStrainFlag) {
         mappedFlag = false;
       }
       else if(originalFragID != record.fragID) {
         mappedFlag = false;
       }
       else if(record.DNALen < 50 || 500 < record.DNALen) {
         mappedFlag = false;
       }

       if(mappedStrain == "Hd-rR") {
         if(mappedFlag) {
           HdrRStat[record.fragID][0]++;
         }
         else {
           HdrRStat[record.fragID][1]++;
         }
       }
       else if(mappedStrain == "HNI") {
         if(mappedFlag) {
           HNIStat[record.fragID][0]++;
         }
         else {
           HNIStat[record.fragID][1]++;
         }
       }
     }

     foreach(line; File(HNITotalReadFile).byLine) {
       auto fields = line.to!string.strip.split("\t");
       fout.writeln("HNI\t", fields[0], "\t",
                    fields[1].to!uint + fields[2].to!uint, "\t",
                    HNIStat[fields[0].to!uint][0], "\t",
                    HNIStat[fields[0].to!uint][1]);       
     }
   }
 }


 enum Strand : char {plus='+', minus='-'}
     
class SamRecord
{
  string qname, rname, mappedStrain;
  uint pos, fragID, DNALen;
  Strand strand;
  bool gapFlag;
  
  this(string record) {
    auto fields = record.split('\t');
    qname = fields[0];
    rname = fields[2];

    pos = fields[3].to!uint;

    auto flag = fields[1].to!uint;
    strand = (flag & 0x10) == 0 ? Strand.plus : Strand.minus;

    auto cigar = fields[5];
    if(strand == Strand.minus) {
      pos = calcReverse5primePos(cigar);
    }
  }

  void searchFrag(Fragment[][string] frags) {    
    auto index = binarySearchFrag(frags[rname]);
    fragID = frags[rname][index].id;
    gapFlag = frags[rname][index].gapFlag;
    DNALen = estimateDNALength(frags[rname][index]);
  }
  
private:
    uint estimateDNALength(Fragment frag) {
      return strand == Strand.plus ? frag.end - pos + 1 : pos - frag.start  + 1;
    }

  
  uint binarySearchFrag(Fragment[] frags) {
    uint startID = 0;
    uint endID = (frags.length - 1).to!uint;
    
    if(strand == Strand.plus) { // search the template 5' ends of the fragments
      while(startID < endID) {
        auto middleID = (startID + endID) / 2;
        if(frags[middleID].end <= pos) {
          startID = middleID + 1;
        } else {
          endID = middleID;
        }
      }
    }
    else {
      while(startID < endID) { // search the reverse 5' ends of the fragments
        auto middleID = (startID + endID + 1) / 2;
        if(pos <= frags[middleID].start) {
          endID = middleID - 1;
        } else {
          startID = middleID;          
        }
      }
    }
    
    if(startID > endID)
      throw new Exception("ERROR: binary search startID > endID");
    
    return startID;
  }
  
  uint calcReverse5primePos(string cigar) {
    auto cigarDict = parseCigar(cigar);

    if(cigarDict['='] != 0 || cigarDict['X'] != 0) {
      throw new Exception("ERROR: CIGAR strings has =/X instead of M.");
    }

    return pos - 1 + cigarDict['M'] + cigarDict['D']
      + cigarDict['N'] + cigarDict['S'] + cigarDict['H'];
  }

  int[char] parseCigar(string cigar) {
    auto cigarDict = ['M':0, 'I':0, 'D':0, 'N':0,
                      'S':0, 'H':0, 'P':0, '=':0, 'X':0];

    foreach(op; cigarDict.byKey) {
      auto capture = matchFirst(cigar, r"(\d+)" ~ op);
      cigarDict[op] = capture.empty ? 0 : capture.hit.split(op)[0].to!int;
    }

    return cigarDict;
  }
}


class Fragment
{
  uint id, start, end;
  string rname;
  bool gapFlag;
  
  this(uint id_, string rname_, uint start_, uint end_, bool gapFlag_) {
    id=id_; rname=rname_; start=start_; end=end_; gapFlag = gapFlag_;
  }
}


Fragment[][string] readFrag(string file)
{
  Fragment[][string] frags;
  foreach(line; File(file, "r").byLine) {
    if(line != null) {
      auto fields = line.to!string.split("\t");
      frags[fields[1]] ~= new Fragment(fields[0].to!uint, fields[1],
                                       fields[2].to!uint, fields[3].to!uint,
                                       fields[4] == "1" ? true : false);
    }    
  }
  
  return frags;
}
