function [X, info] = MBO(D,H,options)
% Find optimal coordinates corresponding to the distance matrix D using
% manifold based optimization.
%
% function [X, info] = MBO(D)
% function [X, info] = MBO(D,H)
% function [X, info] = MBO(D,H,options)
% function [X, info] = MBO(D,[],options)
%
% This program attempts to find coordinates X(1:n,1:3) that best correspond
% to the provided distance matrix D. The function will minimize the cost
% function
%
% f(X) = sum(sum((H.*(dist(X) - D)).^2))
%
% where H is a weight matrix and dist(X) are the pair-wise eucledian
% distances between the coordinates X.
%
%
% Input:
%    D (1 x n*(n-1)/2 double array)
%       Array with the lower triangual elements of the (n x n) symmetric
%       matrix containg the Eucledian distances between n points. 
%       Hence, D = squareform(D_full), where D_full is the distance matrix
%       in full n x n storage.
%
%    H (1 x n*(n-1)/2 double array)
%       Array with the weight corresponding to each element in D. In the
%       case that H is not provided or that H = [], H is a vector of all
%       ones.
%
%    options (struct array)
%       Struct array with options that are sent to the manopt-solver, see
%       manopt documentation for details. In adddition options can contain
%       the following values.
%          solver: Selecting which manopt-solver to use: 'TR'
%          (trustregion) or 'GD' (steepest descent).
%
%          X0: Initial guess for the coordinates (n x 3 array). If X0 = 0,
%          the initial guess is found from singular value decomposition
%          (classical multidimensional scaling solution). If X0 is not
%          provided, a random initial guess is used.
% 
% Output:
%    X (n x 3 double array)
%       The optimal coordinates found by the solver.
%
%    info (struct array)
%       Struct array with information about the solver iterations (see
%       manopt documentation for details). In addition, info.cpuTime
%       contains the time (in seconds) the optimization algorithm used to
%       converge to the solution.
%

% add path to the manopt toolbox
[path,~,~] = fileparts(mfilename('fullpath'));
addpath(genpath(sprintf('%s/manopt/',path)));

% check input matrix
if size(D,1) > size(D,2)
  D = D';
end
m = size(D,2);
n = ceil(sqrt(2*m)); % (1 + sqrt(1+8*n))/2, but works for large n
if n*(n-1)/2 ~= m
  error('Input vector D has wrong length.');
end

% check number of input arguments
if nargin == 1
  H = [];
  options.solver = 'TR';
elseif nargin == 2
  options.solver = 'TR';
end
if ~isfield(options,'solver')
  options.solver = 'TR';
end

% use only the nonzero entries in D
if isempty(H)
  mask = (D > 0);
else
  mask = (D > 0 & H > 0);
  H = transpose(H(mask));
  H2 = H.*H;
end

% find trur indices of nonzero elements
[is,js]= find(tril(true(n),-1));
Dt = D(mask)'; % the nonzero distances
is = is(mask); % row index in full storage
js = js(mask); % column index in full storage
nnzd = length(Dt); % number of nonzero entries

% EIJ matrix needed for efficient calculation of gradient and hessian
EIJ = speye(n);
EIJ = EIJ(:,is) - EIJ(:,js);

% find starting guess from SVD
if isfield(options,'X0')
  if options.X0 == 0
    DD = sparse(squareform(D.^2));
    DDE = repmat(sum(DD,2),[1,n]);
    EDD = repmat(sum(DD,1),[n,1]);
    P = -0.5*(DD - DDE/n - EDD/n + sum(sum(DD))/n^2); % gram matrix
    [X0, S0, ~] = svds(P,3);
    options.X0 = X0*sqrt(S0);
    clear X0 DDE EDD P S0
  end
else
  options.X0 = [];
end

% Set up solver -----------------------------------------------------------

% Create the problem structure
manifold = symfixedrankYYfactory(n,3);
problem.M = manifold;

% Define the problem cost function, gradient and hessian
if isempty(H) || all(H(:) == 1)
  problem.cost = @cost;
  problem.grad = @grad;
  problem.hess = @hess;
else
  problem.cost = @costH;
  problem.grad = @gradH;
  problem.hess = @hessH;
end

% Execute the optimization
tmp = tic;
if strcmp(options.solver,'TR')
  [X, ~, info] = trustregions(problem,options.X0,options);
elseif strcmp(options.solver,'GD')
  [X, ~, info] = steepestdescent(problem,options.X0,options);
end
info(1).cpuTime = toc(tmp);

  function [f, store] = costH(Y, store)
    if ~isfield(store, 'xij')
      store.xij = EIJ'*Y;
    end
    if ~isfield(store, 'estDists')
      store.estDists = sqrt(sum(store.xij.*store.xij,2));
    end
    if ~isfield(store, 'f')
      errDists = H.*(store.estDists - Dt);
      store.f = (errDists'*errDists)/nnzd;
    end
    f = store.f;
  end
  function [g, store] = gradH(Y, store)
    
    if ~isfield(store, 'xij')
      store.xij = EIJ'*Y;
    end
    if ~isfield(store, 'estDists')
      store.estDists = sqrt(sum(store.xij.*store.xij,2));
    end
    if ~isfield(store, 'g')
      store.g = (EIJ*bsxfun(@times,2*H2.*(1 - Dt./store.estDists),store.xij))/nnzd;
    end
    g = store.g;
  end
  function [h, store] = hessH(Y, U, store)
    
    if ~isfield(store, 'xij')
      store.xij = EIJ'*Y;
    end
    if ~isfield(store, 'estDists')
      store.estDists = sqrt(sum(store.xij.*store.xij,2));
    end
    
    zij = EIJ'*U;
    crossYZ = 2*sum(store.xij.*zij,2);
    h = manifold.ehess2rhess(Y, grad(Y,store), EIJ*bsxfun(@times,2.*H2.*(1 - Dt./store.estDists),zij) + EIJ*bsxfun(@times,H2.*Dt.*crossYZ./store.estDists.^3,store.xij), U)/nnzd;
  end

  function [f, store] = cost(Y, store)
    
    if ~isfield(store, 'xij')
      store.xij = EIJ'*Y;
    end
    if ~isfield(store, 'estDists')
      store.estDists = sqrt(sum(store.xij.*store.xij,2));
    end
    if ~isfield(store, 'f')
      errDists = store.estDists - Dt;
      store.f = (errDists'*errDists)/nnzd;
    end
    f = store.f;
  end
  function [g, store] = grad(Y, store)
    
    if ~isfield(store, 'xij')
      store.xij = EIJ'*Y;
    end
    if ~isfield(store, 'estDists')
      store.estDists = sqrt(sum(store.xij.*store.xij,2));
    end
    if ~isfield(store, 'g')
      store.g = (EIJ*bsxfun(@times,2*(1 - Dt./store.estDists),store.xij))/nnzd;
    end
    g = store.g;
  end
  function [h, store] = hess(Y, U, store)
    
    if ~isfield(store, 'xij')
      store.xij = EIJ'*Y;
    end
    if ~isfield(store, 'estDists')
      store.estDists = sqrt(sum(store.xij.*store.xij,2));
    end
    
    zij = EIJ'*U;
    crossYZ = 2*sum(store.xij.*zij,2);
    
    h = manifold.ehess2rhess(Y, grad(Y,store), EIJ*bsxfun(@times,2*(1 - Dt./store.estDists),zij) + EIJ*bsxfun(@times,Dt.*crossYZ./store.estDists.^3,store.xij), U)/nnzd;
  end

end