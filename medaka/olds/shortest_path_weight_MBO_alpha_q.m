function [X, s] = shortest_path_weight_MBO_alpha_q(CFMFile, outXFile, outIDFile, outParamFile)
    fprintf('Initializing contact matrix\n');
    CFM = load(CFMFile);
    CFM(:,1:2) = CFM(:,1:2) + 1;
    CFM = spconvert(CFM);
    nbins = size(CFM, 1);
    bins_with_reads = find(sum(CFM, 1) + sum(CFM, 2)' ~= 0)';
    CFM = CFM(bins_with_reads, bins_with_reads);
    
    fprintf('Finding all-pairs shortest path\n');
    D = CFM;
    D(find(D)) = D(find(D)).^(-1);
    D = D.'; % This script assumes CFM is upper triangle.
    D = squareform(graphallshortestpaths(D, 'Directed', false));

    H = CFM.';
    H(1:nbins+1:nbins*nbins) = 0;
    H = squareform(H);
    H = (1 + D.^(-1) - H).^(-1);
    
    
    fprintf('3D riconstruction with optimization of alpha and q\n');
    alpha_min = 0.1;
    alpha_max = 3;
    q_min = 1;
    q_max = 3;

    fprintf('Optimizing alpha\n');
    log_alpha = fminbnd(@(x) MBO_known_alpha(D, H, exp(x), q_min, q_max), ...
                        log(alpha_min), log(alpha_max), ...
                        optimset('TolX', 0.001, 'Display', 'notify', 'MaxIter', 100));

    alpha = exp(log_alpha);
    fprintf('alpha optimized: alpha =  %f\n', alpha);

    [error, X, q] = MBO_known_alpha(D, H, alpha, q_min, q_max);
    
    fprintf('interpolate the bins without contacts\n');
    bins = (1:nbins)';
    X = interp1(bins_with_reads, X, bins, 'linear');
    
    fprintf('Writing X\n');
    fid = fopen(outXFile, 'w');
    fprintf(fid, '%f\t%f\t%f\n', X.');
    fclose(fid);
    
    fprintf('Writing reconstructed IDs\n');
    fid = fopen(outIDFile, 'w');
    bins_with_reads = bins_with_reads - 1;
    fprintf(fid, '%d\n', bins_with_reads);
    fclose(fid);
    
    fprintf('Writing parameter info\n');
    fid = fopen(outParamFile, 'w');
    fprintf(fid, 'alpha %f\n', alpha);
    fprintf(fid, 'q %f\n', q);
    fprintf(fid, 'error %f\n', error);
    fclose(fid);
end


function [error, X, q] = MBO_known_alpha(D, H, alpha, q_min, q_max)
    Da = D.^alpha;
    
    log_q = fminbnd(@(x) MBO_known_q(Da, H, exp(x)), ...
                    log(q_min), log(q_max), ...
                    optimset('TolX', 0.001, 'Display', 'notify', ...
                             'MaxIter', 100));

    q = exp(log_q);
    fprintf('q optimized: q = %f\n', q);
    
    [error, X] = MBO_known_q(Da, H, q);        
end

function [error, X] = MBO_known_q(D, H, q)    
    fprintf('Finding coordinates using MBO\n');
    Hq = H.^(-q);
    options.solver = 'TR';
    options.tolcost = 1e-12;
    options.tolgradnorm = 1e-7;
    options.Delta0 = 10;
    options.Delta_bar = 10*options.Delta0;
    [X, info] = MBO(D, Hq, options);
    
    fprintf('Scaling to best fit original original distance matrix for 1-norm\n');
    DX = pdist(X);
    [scale, error] = fminbnd(@(x) norm(D - exp(x) * DX, 1), log(0.001), log(1000), ...
                             optimset('TolX', 0.001, 'Display', 'notify', 'MaxIter', 100));
    X = scale * X;
end