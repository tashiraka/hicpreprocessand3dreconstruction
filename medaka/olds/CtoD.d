import std.stdio, std.conv, std.string, std.array, std.algorithm, std.parallelism, std.typecons;


version(unittest){}
 else {
   void main(string[] args)
   {
     auto cfmFile = args[1];
     auto outFile = args[2];

     auto cfm = readSpMat(cfmFile);
     auto distMat = cfmToDistMat(cfm);
     writeMat(distMat, outFile);
   }
 }


// Warshall-Floyd Algorithm
double[][] cfmToDistMat(double[][] cfm)
{
  auto distMat = new double[][](cfm.length, cfm.length);

  // test
  foreach(i, cf; cfm) {
    if(reduce!((a,b) => a+b)(0.0, cf) == 0) {
      throw new Exception("ERROR: Not connected, " ~ i.to!string ~ "-th.");
    }
  }

  // initialization
  foreach(i, ref row; distMat)
    foreach(j, ref elem; row)
      if(i == j)
        elem = 0.0;
      else
        elem = cfm[i][j] == 0.0 ? double.infinity : 1.0 / cfm[i][j];

  // iteration
  foreach(k; 0..cfm.length) {
    writeln(k+1, " ", cfm.length);
    foreach(i; 0..cfm.length) 
      foreach(j; (i+1)..cfm.length) 
        if(distMat[i][j] > distMat[i][k] + distMat[k][j]) { 
          distMat[i][j] = distMat[i][k] + distMat[k][j];
          distMat[j][i] = distMat[i][j];
        }
  }
    
  foreach(i, row; distMat) 
    foreach(j, elem; row) 
      if(distMat[i][j] == double.infinity) 
        throw new Exception("ERROR: multiple connected components");
  
  return distMat;
}


double[][] readMat(string filename)
{
  auto matApp = appender!(double[][]);
  auto fin = File(filename, "r");
  foreach(line; fin.byLine) {
    matApp.put(line.to!string.strip.split("\t").map!(x => x.to!double).array);
  }
  return matApp.data;
}


double[][] readSpMat(string filename)
{
  double[Tuple!(uint, uint)] spmat;
  auto fin = File(filename, "r");
  uint size = 0;
  foreach (line; fin.byLine) {
    auto fields = line.to!string.strip.split("\t"); 
    spmat[tuple(fields[0].to!uint, fields[1].to!uint)] = fields[2].to!double;
    size = fields[0].to!uint;
  }

  auto mat = new double[][](size, size);
  foreach (key; spmat.byKey) {
    if (key[0] == key[1]) {
      mat[key[0]][key[1]] = spmat[key];	    
    }
    else {
      mat[key[0]][key[1]] = spmat[key];
      mat[key[1]][key[0]] = spmat[key];
    }
  }
  
  return mat;
}


void writeMat(double[][] mat, string filename)
{
  auto fout = File(filename, "w");
  foreach(row; mat) {
    foreach(i, elem; row) {
      fout.write(format("%f", elem));
      if(i < row.length - 1) {
        fout.write("\t");
      }
    }
    fout.writeln;
  }
}
