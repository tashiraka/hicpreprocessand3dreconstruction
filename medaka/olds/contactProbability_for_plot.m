function contactProbability_for_plot(contactProbabilityFile, resolution)
cp = load(contactProbabilityFile);
minSize = size(cp, 1);

for chr = 1:24
    cpChr = cp(cp(:, 1) == chr, :);
    nbin = size(cpChr, 1);
    minSize = min(minSize, nbin);

    cpChr(:, 2) = cpChr(:, 2) * resolution;
    fid = fopen(strcat(contactProbabilityFile, '.chr', num2str(chr)), ...
                'w');
    fprintf(fid, '%d\t%d\t%f\n', cpChr.');
    fclose(fid);
end

averageCp = zeros(minSize);
chrCp = cp(ismember(cp(:, 1), [1:24]), :);
fid = fopen(strcat(contactProbabilityFile, '.mean'), 'w');

for s = 1:minSize
    elems = chrCp(chrCp(:, 2) == s, 3);
    if size(elems, 1) ~= 24
        fprintf('Error: the lack of some chromosome.%d,%d\n', ...
                size(elems, 1), size(elems, 2));
        exit();
    end
    fprintf(fid, 'mean\t%d\t%f\n', s * resolution, mean(elems));
end

fclose(fid);
exit();