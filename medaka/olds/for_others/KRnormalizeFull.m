function KRnormalizeFull(CFMFile, outFile, biasFile)
% This program does not remove diagonal element form Hi-C
% contact map. The rows and columns of the Hi-C map
% without coverage are removed. Additionally those with
% totally one or two counts are removed for convergence
% of bnewt. In the output, these rows and columns are
% included as those whose elements are all zeros.
%
% I call a contact frequency matrix as "cfm".
% inFile: A file name of raw cfm.
% outFile: A file name for output of the normalized cfm.
% biasFile: A file name for output of the 1D bias vector.
%
% I want to write the outFile as TAB-separate values (TSV), but I cannot.
% So I usually change the format of the outFile to the TSV-like format
% by the following command.
%
% sed -E 's/[\t ]+/\t/g' outFile > outFile
%
%
    CFM = load(CFMFile);    
    nbin = size(CFM, 1);
    
    sumCFM = sum(CFM, 1);
    nonZeroSumIDs = find(sumCFM ~= 0 & sumCFM ~= 1 & sumCFM ~=2);
    % Sum=2 prevent the convergence of bnewt.
    clear sumCFM;
    reducedCFM = CFM(nonZeroSumIDs, nonZeroSumIDs);
    clear CFM;

    matrixSize = size(reducedCFM, 1);
    [x, res, notConvergeFlag] = bnewtFull(reducedCFM);
    clear res;

    if notConvergeFlag
        fid = fopen(outFile,'w');
        fprintf(fid, 'Not converged\n');
        fclose(fid);
    else
        normedReducedCFM = diag(x)*reducedCFM*diag(x);
        clear reducedCFM;
    
        normedCFM = zeros(nbin, nbin);
        normedCFM(nonZeroSumIDs, nonZeroSumIDs) = normedReducedCFM;
        clear normedReducedCFM;
        
        save(outFile, '-ascii', '-tabs', 'normedCFM');
        %dlmwrite(outFile, normedCFM, 'delimiter', '\t');
        clear normedCFM;
    end
    
    clear normedReducedCFM;
    clear nonZeroSumIDs;
    
    fid = fopen(biasFile,'w');
    fprintf(fid, '%f\n', x);
    fclose(fid);
    clear x;
end