import std.stdio, std.string, std.conv, std.process;


void main(string[] args)
{
  auto bam = args[1];
  auto optionalFormat = args[2];
  auto nthreads = args[3].to!int;
  auto outFile = args[4];

  auto flag = 0x400;
  
  flagToOptionalFields(bam, outFile, flag, optionalFormat, nthreads);
}


// produce the paired flag and fields
void flagToOptionalFields(string inBam, string outBam, int flag, string optionalFormat, int nthreads) {
  auto oStream = pipeProcess(["sambamba", "view", "-f", "bam", "-S",
                              "-t", nthreads.to!string, "-o", outBam, "/dev/stdin"]);
  
  // Output header
  auto headerStream = pipeProcess(["sambamba", "view", "-H",
                                  "-t", nthreads.to!string, inBam]);
  foreach (line; headerStream.stdout.byLine) 
    oStream.stdin.writeln(line);

  // Output alignments
  auto iStream = pipeProcess(["sambamba", "view",
                              "-t", nthreads.to!string, inBam]);

  foreach (line; iStream.stdout.byLine) {
    auto fields = line.to!string.strip.split('\t');
    auto flags = fields[1].to!int;

    // 0x100 is secondary flags
    if ((flags & 0x100) != 0)
      throw new Exception("ERROR: Not primary alignment!!");

    if ((flags & flag) != 0) {
      fields[1] = (flags ^ flag).to!string;
      fields ~= optionalFormat;
      oStream.stdin.writeln(fields.join("\t"));
    }
    else oStream.stdin.writeln(line);
  }
}
