function [X, s] = shortest_path(CFMFile, outFile)
% upper trianged sparse matrix

fprintf('initialize contact matrix\n');
C = load(CFMFile);
C = spconvert(C);
C = C.';
nbins = size(C, 1);
s = find(sum(C, 1) + sum(C, 2)' ~= 0)'; % bins with interactions
sn = find(sum(C, 1) + sum(C, 2)' == 0)';  % find bins with no interactions
C(find(C)) = C(find(C)).^-1; % distance = 1 / frequency

fprintf('shortest path\n');
CM = squareform(graphallshortestpaths(C, 'Directed', false));

fprintf('Writing\n')
save(outFile, 'CM', '-v7.3');

exit();
end
