function [X, s] = shortest_path_weight_MBO(CFMFile, outXFile, ...
                                           outIDFile, outParamFile, ...
                                           CFMImageFile, recoveredCFMImageFile)
    fprintf('Initializing contact matrix\n');
    CFM = load(CFMFile);
    CFM(:,1:2) = CFM(:,1:2) + 1;
    CFM = spconvert(CFM);

    fprintf('Writing CFM\n');
    saveas(plot(HeatMap(log10(CFM + CFM.' - diag(diag(CFM))), 'Colormap', 'redbluecmap')), CFMImageFile, 'png');
    
    nbins = size(CFM, 1);
    bins_with_reads = find(sum(CFM, 1) + sum(CFM, 2)' ~= 0)';
    CFM = CFM(bins_with_reads, bins_with_reads);
        
    fprintf('Finding all-pairs shortest path\n');
    D = CFM;
    D(find(D)) = D(find(D)).^(-1);
    D = D.'; % This script assumes CFM is upper triangle.
    D = squareform(graphallshortestpaths(D, 'Directed', false));
    save(strcat(CFMFile, '.D.mat'), 'D', '-7.3');

    alpha = 1;
    q = 1;

    %load(strcat(CFMFile, '.D.mat'));
    D(find(D)) = D(find(D)).^(alpha);

    H = CFM.';
    H(1:nbins+1:nbins*nbins) = 0;
    H = squareform(H);
    H = (1 + D.^(-1) - H).^(-q);    
    
    fprintf('Finding coordinates using MBO\n');
    options.solver = 'TR';
    options.tolcost = 1e-12;
    options.tolgradnorm = 1e-7;
    options.Delta0 = 10;
    options.Delta_bar = 10*options.Delta0;
    [X, info] = MBO(D, H, options);
    
    clear H;
    
    fprintf('interpolate the bins without contacts\n');
    bins = (1:nbins)';
    X = interp1(bins_with_reads, X, bins, 'linear');
    
    fprintf('Writing X\n');
    fid = fopen(outXFile, 'w');
    fprintf(fid, '%f\t%f\t%f\n', X.');
    fclose(fid);
    
    fprintf('Writing reconstructed IDs\n');
    fid = fopen(outIDFile, 'w');
    bins_with_reads = bins_with_reads - 1;
    fprintf(fid, '%d\n', bins_with_reads);
    fclose(fid);
    
    fprintf('Writing parameter info\n');
    fid = fopen(outParamFile, 'w');
    fprintf(fid, 'alpha %f\n', alpha);
    fprintf(fid, 'q %f\n', q);
    fprintf(fid, 'error %f\n', error);
    fclose(fid);
    
    fprintf('Writing recovered CFM\n');
    DX = pdist(X).^(-alpha);
    saveas(plot(HeatMap(squareform(log10(DX)), 'Colormap', 'redbluecmap')), recoveredCFMImageFile, ...
           'png');
end
