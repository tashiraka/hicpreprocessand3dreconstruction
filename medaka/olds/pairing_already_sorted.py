import sys, os, logging, subprocess, pysam, tempfile, re


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def pairing(bam, outFile, minMapq, nthreads):
    # produce the paired flag and fields
    oStream = subprocess.Popen(['sambamba', 'view', '-f', 'bam', '-S', '/dev/stdin', '-t', str(nthreads)],
                               stdin=subprocess.PIPE, stdout=open(outFile, 'w'))

    # Out header
    headerStream = subprocess.Popen(['sambamba', 'view', '-H', '-t', str(nthreads), bam],
                                    stdout=subprocess.PIPE)
    for line in headerStream.stdout:
        oStream.stdin.write(line)
        
    iStream = subprocess.Popen(['sambamba', 'view', '-t', str(nthreads), bam],
                               stdout=subprocess.PIPE)
    
    while True:
        line1 = iStream.stdout.readline()
        line2 = iStream.stdout.readline()

        if line1 == '': break

        if line1 != '' and line2 == '':
            exit("The number of the alignments is odd.")
            
        fields1 = line1.split('\t')
        fields2 = line2.split('\t')
            
        if fields1[0] != fields2[0]:
            exit("Not paired entry.")            
        else:
            # Produce flags
            flag1 = int(fields1[1])
            flag2 = int(fields2[1])

            # 0x100 is secondary flags
            if flag1 & 0x100 != 0 or flag2 & 0x100 != 0:
                logging.info("ERROR: Not primary alignment!!")
                exit()

            mapq1 = int(fields1[4])
            mapq2 = int(fields2[4])
            newFlag1 = 0x1 + 0x2 + (0x4 if mapq1 < minMapq else 0) + (0x8 if mapq2 < minMapq else 0) + (flag1 & 0x10) + ((flag2 & 0x10) << 1) + 0x40
            newFlag2 = 0x1 + 0x2 + (0x4 if mapq2 < minMapq else 0) + (0x8 if mapq1 < minMapq else 0) + (flag2 & 0x10) + ((flag1 & 0x10) << 1) + 0x80
            fields1[1] = str(newFlag1)
            fields2[1] = str(newFlag2)
            
            # Produce fields
            # RNEXT. samtools automaticaly convert the same RNAME to '='.
            # sambamba does too ?
            fields1[6] = fields2[2];
            fields2[6] = fields1[2];
            
            # PNEXT
            fields1[7] = fields2[3];
            fields2[7] = fields1[3];            

            oStream.stdin.write('\t'.join(fields1))
            oStream.stdin.write('\t'.join(fields2))
        
    iStream.communicate()
    oStrean.stdin.close()
    oStream.communicate()
    

args = sys.argv
bam = args[1]
minMapq = int(args[2])
nthreads = int(args[3])
outFile = args[4]

logging.info('Start of pairing')
logging.info('pairing({0}, {1}, {2}, {3})'.format(bam, outFile, minMapq, nthreads))

pairing(bam, outFile, minMapq, nthreads)

logging.info('End of pairing')
