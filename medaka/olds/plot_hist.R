options(warn=1)
args <- commandArgs(trailingOnly = TRUE)
sizeFile <- args[1]
geneFile <- args[2]
resolution <- as.integer(args[3])
outFile <- args[4]

size <- read.table(sizeFile)
genes <- read.table(geneFile)
chr <- gsub(' ', '', paste('chr', c(1:24)))
geneDensity <- data.frame(chr, rep(0, length=length(chr)))
rownames(geneDensity) = chr

for (c in chr) {
    s <- ceiling((size[size$V1==c, 2] - 1) / resolution) # size is 1-based, UCSC table tracks are 0-based
    geneNum <- rep(0, length=s)
    bins <- ceiling(genes[genes$V3==c, 5] / resolution)
    for (b in bins) {
        geneNum[b] <- geneNum[b] + 1
    }
    geneDensity[c, 2] <- mean(geneNum)
}

write.table(geneDensity, outFile, row.names=F, col.names=F, sep='\t', quote=F)
