function [X, s] = reconstruction(CFMFile, outXFile, outIDFile, outParamFile)
    fprintf('Initializing contact matrix\n');
    CFM = load(CFMFile);
    CFM(:,1:2) = CFM(:,1:2) + 1;
    CFM = spconvert(CFM);
    nbins = size(CFM, 1);
    bins_with_reads = find(sum(CFM, 1) + sum(CFM, 2)' ~= 0)';
    CFM = CFM(bins_with_reads, bins_with_reads);
    
    fprintf('3D riconstruction with optimization of alpha and q\n');
    alpha_min = 0.1;
    alpha_max = 3;
    q_min = 1;
    q_max = 3;

    [X, alpha, q, error] = floyd_warshall_MBO_search_alpha_and_q(CFM, ...
                                                      alpha_min , alpha_max, q_min, q_max);
    
    fprintf('interpolate the bins without contacts\n');
    bins = (1:nbins)';
    X = interp1(bins_with_reads, X, bins, 'linear');
    
    fprintf('Writing X\n');
    fid = fopen(outXFile, 'w');
    fprintf(fid, '%f\t%f\t%f\n', X.');
    fclose(fid);
    
    fprintf('Writing reconstructed IDs\n');
    fid = fopen(outIDFile, 'w');
    bins_with_reads = bins_with_reads - 1;
    fprintf(fid, '%d\n', bins_with_reads);
    fclose(fid);
    
    fprintf('Writing parameter info\n');
    fid = fopen(outParamFile, 'w');
    fprintf(fid, 'alpha %f\n', alpha);
    fprintf(fid, 'q %f\n', q);
    fprintf(fid, 'error %f\n', error);
    fclose(fid);
end

function [X, alpha, q, error] = ...
    floyd_warshall_MBO_search_alpha_and_q(CFM, alpha_min, alpha_max, ...
                                          q_min, q_max)
    %fprintf(['Finding all-pairs shortest path using Floyd-Warshall ' ...
    'algorithm\n']);
    fprintf('Finding all-pairs shortest path\n');
    CFM(find(CFM)) = CFM(find(CFM)).^(-1);
    CFM = CFM.';
    D = squareform(graphallshortestpaths(CFM, 'Directed', false));
    H(D == Cc) = CFM;
    n = size(CFM, 1);
    H = ones(n, n);
    H(CM == CC) = 2^(-1.5);

    %D = CFM;
    %D(find(D)) = D(find(D)).^(-1);
    %D(1:n+1:n*n) = 0;
    
    % Floyd-Warshall algorithm with path reconstruction
    %next = zeros(n, n); % next is right before j in a shortest
                        % path.
                        %for col = 1:n
                        %        next(:, col) = col;
                        %end

    %fprintf('In O(|V|^3) loop\n')
    %tic
    %for k = 1:n
    %    for i = 1:(n-1)
    %        for j = (i+1):n
    %            if D(i, k) + D(k, j) < D(i, j)
    %                D(i, j) = D(i, k) + D(k, j);
    %                D(j, i) = D(i, j);
    %                next(i, j) = next(i, k);
    %            end
    %        end
    %    end
    %end
  
    %toc
    %full(D)
    %next
    %fprintf('Reconstructing path\n');
    %H = zeros(n, n);
    %tic
    %for i = 1:(n-1)
    %    for j = (i+1):n
    %        v = i;
    %        len = 0;
    %        while v ~= j
    %            v = next(v, j);
    %            len = len + 1;
    %        end
    %        H(i, j) = len;
    %        H(j, i) = len;
    %    end
    %end
    %toc

    %D = squareform(D);
    %H = ones(n,n);
    %A = CFM;
    %A(find(A)) = A(find(A)).^(-0.8);
    %D = D.^(0.8);
    %H(A == D) = 2^(-1.5);
    %H = H.^(-1.5);
    %H(1:n+1:n*n) = 0;
    %D = squareform(D);
    %H = squareform(H);
    %save('D.mat', 'D');
    %save('H.mat', 'H')
    fprintf('Optimizing alpha and q\n\n');
    fprintf('-----------------------\n\n');
    %log_alpha = fminbnd(@(x) floyd_warshall_MBO_search_q(D, H, exp(x), q_min, q_max), ...
    %                    log(alpha_min), log(alpha_max), ...
    %                    optimset('TolX', 0.001, 'Display', 'notify', 'MaxIter', 100));
    fprintf('-----------------------\n\n');

    %alpha = exp(log_alpha);
    %fprintf('alpha optimized: alpha =  %f\n', alpha);

    %[error, X, q] = floyd_warshall_MBO_search_q(D, H, alpha,
    %q_min, q_max);
    alpha = 0.8;
    q = 1.5;
    %[error, X] = floyd_warshall_MBO(D, H, q);
end

function [error, X, q] = floyd_warshall_MBO_search_q(D, H, alpha, ...
                                                     q_min, q_max)
    fprintf('Optimizing q\n\n');
    fprintf('>>>>>>>>>>>>>>>>>>>>');
    Da = D.^alpha;
    tic
    log_q = fminbnd(@(x) floyd_warshall_MBO(Da, H, exp(x)), ...
                    log(q_min), log(q_max), ...
                    optimset('TolX', 0.001, 'Display', 'notify', ...
                             'MaxIter', 100));
    toc
    fprintf('>>>>>>>>>>>>>>>>>>>>');

    q = exp(log_q);
    fprintf('q optimized: q = %f\n', q);
    
    [error, X] = floyd_warshall_MBO(Da, H, q);        
end

function [error, X] = floyd_warshall_MBO(D, H, q)    
    fprintf('Finding coordinates using MBO\n');
    %Hq = H.^(-q);
    options.solver = 'TR';
    options.tolcost = 1e-12;
    options.tolgradnorm = 1e-7;
    options.Delta0 = 10;
    options.Delta_bar = 10*options.Delta0;
    %[X, info] = MBO(D, Hq, options);
    [X, info] = MBO(D, H, options);

    fprintf('Scaling to best fit original original distance matrix for 1-norm\n');
    DX = pdist(X);
    [scale, error] = fminbnd(@(x) norm(D - exp(x) * DX, 1), log(0.001), log(1000), ...
                             optimset('TolX', 0.001, 'Display', 'notify', 'MaxIter', 100));
    X = scale * X;
end