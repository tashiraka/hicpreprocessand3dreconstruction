import std.stdio, std.string, std.conv, std.process, std.algorithm;


void main(string[] args)
{
  auto bam = args[1];
  auto optional = args[2];
  auto nthreads = args[3].to!int;
  
  calc_dup_ratio(bam, optional, nthreads);
}


void calc_dup_ratio(string inBam, string optional, int nthreads) {
  auto iStream = pipeProcess(["sambamba", "view",
                              "-t", nthreads.to!string, inBam]);
  auto pe = 0;
  auto se = 0;
  auto both = 0;
  auto onlySe = 0;
  
  foreach (line; iStream.stdout.byLine) {
    auto fields = line.to!string.strip.split('\t');
    auto flag = fields[1].to!int;
    auto optionals = fields[11..$];

    auto peDup = (flag & 0x400) != 0;
    auto seDup = optionals.canFind(optional);

    if (peDup) {
      pe++;
      if (seDup) {
        se++;
        both++;
      }
    }
    else if (seDup) {
      se++;
      onlySe++;
    }
  }

  writeln("Total SE duplicates: ", se);
  writeln("Total PE duplicates: ", pe);
  writeln("Both SE and PE duplicates: ", both);
  writeln("Only SE duplicates: ", onlySe);
  writeln("Ratio of both / SE duplicates: ", both.to!double / se.to!double);
}
