function KRnormalizeFull(inFile, outFile, biasFile)
% I call a contact frequency matrix as "cfm".
% inFile: A file name of raw cfm.
% outFile: A file name for output of the normalized cfm.
% biasFile: A file name for output of the 1D bias vector.
%
% I want to write the outFile as TAB-separate values (TSV), but I cannot.
% So I usually change the format of the outFile to the TSV-like format
% by the following command.
%
% sed -E 's/[\t ]+/\t/g' outFile > outFile
%
cfm = load(inFile);

% reconstruct the full of symmetric sparse matrix
% diagonal element to 0.
cfm = cfm - diag(diag(cfm));

% delete the no-read loci from contact frequency matrix
columnSum = sum(cfm);
noCoverageIndeces = find(columnSum == 0);
cfm(:, noCoverageIndeces) = [];
cfm(noCoverageIndeces, :) = [];

% normalization
[x, res] = bnewtFull(cfm);
normedCfm = diag(x)*cfm*diag(x);

% re-insert the no-read loci
for i = noCoverageIndeces
    normedCfm = cat(1, ...
                      normedCfm(1:i-1, :), ...
                      zeros(1, size(normedCfm, 2)), ...
                      normedCfm(i:end, :));
    normedCfm = cat(2, ...
                      normedCfm(:, 1:i-1), ...
                      zeros(size(normedCfm, 1), 1), ...
                      normedCfm(:, i:end));
    x = cat(1, x(1:i-1, :), inf, x(i:end, :));
end


save(outFile, '-ascii', '-tabs', 'normedCfm');
save(biasFile, '-ascii', '-tabs', 'x');

exit();