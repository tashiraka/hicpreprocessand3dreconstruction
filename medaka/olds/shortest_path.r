library("igraph")
library("foreach")
library("doMC")

args <- commandArgs(trailingOnly = TRUE)
cfmFile <- args[1]
distanceFile <- args[2]
edgeNumFile <- args[3]

alpha <- 1

cfm <- read.table(cfmFile)
maxVertex <- tail(cfm[[1]], 1)

if (maxVertex < 2) {
  stop("ERROR: the number of vertices < 2.")
}

g <- graph.data.frame(cfm[1:2], directed=F)
E(g)$weight <- cfm[[3]] ^ (-alpha)
g <- simplify(g, remove.loops=T)

registerDoMC(8)

"Size"
maxVertex


"Calculating the number of edges"
nedges <- foreach (from = 0:(maxVertex - 1), .combine=rbind) %do% {
    foreach (to = (from + 1):maxVertex, .combine=rbind) %dopar% {
        c(from, to,
          length(get.shortest.paths(g, from=toString(from), to=toString(to),
                                    weights=E(g)$weight, output="epath")$epath[[1]]))
    }
}

"Writing"
write.table(nedges, edgeNumFile, quote=F, sep="\t", row.names=F, col.names=F)

"Calculating the shortest path"
d <- shortest.paths(g, weights=E(g)$weight)
"Writing"
write.table(d, distanceFile, quote=F, sep="\t", row.names=F, col.names=F)
