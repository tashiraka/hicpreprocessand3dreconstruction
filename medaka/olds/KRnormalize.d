import std.stdio, std.string, std.conv, std.algorithm, std.typecons, std.file,
  std.process, std.path, std.random;


version (unittest) {} else {
  void main(string[] args)
  {
    /* CFM: "contact frequency matrix"
     * sparseCFMFile contains the upper (or lower) triangular sub-matrix
     * in sparse format
     */
    auto sparseCFMFile = args[1];    
    auto outFile = args[2];
    auto biasVectorFile = args[3];

    // Load the CFM
    double[Tuple!(ulong, ulong)] sparseCFM;
    ulong matrixSize = 0;
    foreach (line; File(sparseCFMFile).byLine) {
      auto fields = line.to!string.strip.split;
      auto id1 = fields[0].to!ulong;
      auto id2 = fields[1].to!ulong;
      auto value = fields[2].to!double;
      sparseCFM[tuple(id1, id2)] = value;
      matrixSize = max(matrixSize, id1);
    }

    // Caluculate sum of rows of CFM
    auto sumOfRows = new double[](matrixSize);
    sumOfRows.fill(0.0);
    foreach (pair; sparseCFM.byKeyValue) sumOfRows[pair.key[0]] += pair.value;
    
    // Remove rows and columns whose sums are zero from CFM
    auto zeroRemovedIDTransform = new long[](matrixSize);
    ulong[] reverseTransform;
    long zeroRemovedID = 0;
    foreach (originalID, sumOfRow; sumOfRows) {
      if (sumOfRow == 0) {
        zeroRemovedIDTransform[originalID] = -1;
      }
      else {
        zeroRemovedIDTransform[originalID] = zeroRemovedID;
        reverseTransform ~= originalID; // reverseTransform[zeroRemovedID] == i;
        ++zeroRemovedID;
      }
    }

    // Write zero-removed CFM
    auto deleteme = tmpFilename();
    auto tmpFout = File(deleteme, "w");
    foreach (pair; sparseCFM.byKeyValue)
      tmpFout.writeln(zeroRemovedIDTransform[pair.key[0]], "\t",
                      zeroRemovedIDTransform[pair.key[1]], "\t",
                      pair.value);
    tmpFout.close;

    // KR normalization
    auto deleteme2 = tmpFilename();
    auto command = ["matlab", "-r", "KRnormalize.m",
                    deleteme, deleteme2, biasVectorFile];
    auto matlabOut = execute(command);

    // Reconstruct original sized matrix
    auto fout = File(outFile, "w");
    foreach (line; File(deleteme2).byLine) {
      auto fields = line.to!string.strip.split;
      auto id1 = fields[0].to!ulong;
      auto id2 = fields[1].to!ulong;
      auto value = fields[2].to!double;
      fout.writeln(reverseTransform[id1], "\t",
                   reverseTransform[id2], "\t",
                   value);
    }
    
    // Remove tmporary files
    remove(deleteme);
    remove(deleteme2);
    
  }
 }


string tmpFilename(string file = __FILE__, size_t line = __LINE__) @safe
{
  return text("deleteme.", baseName(file), ".", randomString(10));
}


string randomString(int n) @safe
{
  string a2z = "abcdefghijklmnopqrstuvwxyz";  
  Random gen;
  string res;
  foreach (i; 0..n) res ~= a2z[uniform(0, 26, gen)];
  return res;
}
