import std.stdio, std.conv, std.string, std.algorithm;


void main(string[] args)
{
  auto dsFile = args[1];
  auto fragFile = args[2];
  auto outFile = args[3];

  auto frags = readFrag(fragFile);

  // qname1 rname1 strand1 pos1 fragID1 mapq1 mappedLen1 DNALen1 \
  // qname2 rname2 strand2 pos2 fragID2 mapq2 mappedLen2 DNALen2
  foreach (line; File(dsFile).byLine) {
    auto fields = line.to!string.strip.split;
    auto fragID1 = fields[4].to!ulong;
    auto fragID2 = fields[12].to!ulong;

    auto str1 = fields[2];
    auto str2 = fields[10];

    auto frag1 = frags[fragID1];
    auto frag2 = frags[fragID2];


    if (frag1.gap || frag1.tel || frag2.gap || frag2.tel)
      throw new Exception("Gap or telomere fragment");

    if (str1 == "+") frag1.depthFwd++;
    else frag1.depthRev++;
    if (str2 == "+") frag2.depthFwd++;
    else frag2.depthRev++;
  }
  
  auto fout = File(outFile, "w");
  
  foreach (frag; frags) {
    if (!frag.gap && !frag.tel) {
      fout.writeln(frag.fragLen, "\t", frag.depthFwd);
      fout.writeln(frag.fragLen, "\t", frag.depthRev);
    }
  }
}


class Fragment
{
  uint id;
  string rname;
  uint fragLen, depthFwd=0, depthRev=0;
  bool gap=false, tel=false;

  this(uint id_, string rname_, uint len, bool gap_)
  {
    id = id_;
    rname = rname_;
    fragLen = len;
    gap = gap_;
  }
}


//<fragID> <name> <start> <end> <fragStart> <fragEnd> <startNoOvhg> endNoOvhg> <gapStart> <gapEnd>

Fragment[] readFrag(string file)
{
  Fragment[] frags;
  auto count = 0;
  
  foreach (line; File(file).byLine) {
      auto fields = line.to!string.strip.split("\t");

      if (count != fields[0].to!uint)
        throw new Exception("fragID is not ordered or continuous");

      frags ~= new Fragment(fields[0].to!uint, fields[1],
                            fields[5].to!uint - fields[4].to!uint,
                            fields[8] != "*");

      count++;
  }

  auto prevRname = "";
  frags[0].tel = true;
  foreach (i; 1..frags.length) {
    if (prevRname != frags[i].rname) {
      frags[i-1].tel = true;
      frags[i].tel = true;
    }
    prevRname = frags[i].rname;
  }
  frags[$-1].tel = true;
  
  return frags;
}
