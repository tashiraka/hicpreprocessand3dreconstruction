function [X, s, info] = shortest_path_MBO_alpha(CFMFile, outXFile, outIDFile)
fprintf('initialize contact matrix\n');
C = load(CFMFile);
C(:,1:2) = C(:,1:2) + 1;
C = spconvert(C);
C = C.';
nbins = size(C, 1);
s = find(sum(C, 1) + sum(C, 2)' ~= 0)'; % bins with interactions
sn = find(sum(C, 1) + sum(C, 2)' == 0)';  % find bins with no interactions


% matrix for selecting new collection of bins
I = true(nbins, nbins);
I(1:nbins+1:nbins*nbins) = false; % diagonals to zero
I(sn,:) = false;
I(:,sn) = false;
I = squareform(I);

q = 1.5;

alphas = [0.8];
for alpha = alphas
    CC = C;
    CC(find(CC)) = CC(find(CC)).^(-alpha); % distance = 1 / frequency
    
    fprintf('shortest path\n');
    CM = graphallshortestpaths(CC, 'Directed', false);
    H = ones(nbins, nbins);
    H(CM == CC) = 2^(-1.5);
    CM = squareform(CM);
    H(1:nbins+1:nbins*nbins) = 0;
    H = squareform(H);

    fprintf('remove bins with no interactions\n');
    CM = CM(I);
    H = H(I);
    
    fprintf('find coordinates using MBO\n');
    options.solver = 'TR';
    options.tolcost = 1e-12;
    options.tolgradnorm = 1e-7;
    options.Delta0 = 10;
    options.Delta_bar = 10*options.Delta0;
    [X, info] = MBO(CM, H, options);

    fprintf('interpolate the bins without contacts\n');
    su = 1:nbins;
    X = interp1(s, X, su', 'linear');

    fid = fopen(strcat(outXFile, '.alpha_', num2str(alpha)), 'w');
    fprintf(fid, '%f\t%f\t%f\n', X.');
    fclose(fid);

    fid = fopen(strcat(outIDFile, '.', num2str(alpha)), 'w');
    s = s - 1;
    fprintf(fid, '%d\n', s);
    fclose(fid);
    s = s + 1;
end

exit();
end    
