#!/bio/bin/python
# -*- coding: utf-8 -*-
from Bio import SeqIO, Restriction, Seq, SeqUtils
from argparse import RawTextHelpFormatter
import numpy as np
import argparse
import re


parser = argparse.ArgumentParser(
    description='''
  This command is tool to cut out restriction fragments from a genome.
  A site position is followed by 1-origin coordinate of the genome.
  This command returns the positions where overhangs fullfilled.
  A restriction fragment is represented as a closed interval, [start, end].
  The output format is as follows.
  <fragment ID>\t<sequence name>\t<start position>\t<end position>\t<gap-containing flag>
    
  For example:
  5\'-GAATTC-3\'
  5\'-CTTAAG-3\' is recognizad by a restriction enzyme EcoRI.
  EcoRI cuts this sequence to
  5\'-G  AATTC-3\'
  3\'-CTTAA  G-5\'.
  Then this command returns, (assuming the sequence is called chr1)
  chr1\t1\t5
  chr1\t6\t10
  ''',
    epilog="Author: Yuichi Motai",
    formatter_class=RawTextHelpFormatter)

parser.add_argument('-v', '--version', action='version', version='version 1.0')
parser.add_argument('genome', help='fasta file containing reference genome')
parser.add_argument('RE', help='restriction enzyme name')
parser.add_argument('gap', help='gap.txt provided by UCSC Genome Browser')
parser.add_argument('out_file', help='output file')
args = parser.parse_args()


# bin\tchrom\tchromStart\tchromEnd\tix\tn\tsize\ttype\tbridge
# Note: all start coordinates in our database are 0-based, not 1-based.
# Note: [chromStart, chromEnd)
gap = {}
for line in open(args.gap):
    record = line.rstrip().split('\t')

    if record[1] in gap:
        gap[record[1]].append((int(record[2]) + 1, int(record[3])))
    else:
        gap.setdefault(record[1], [(int(record[2]) + 1, int(record[3]))])

        
restE = getattr(Restriction, args.RE)
fout = open(args.out_file, 'w')
    

def fragStartBlunt(rsite, ovhg):
    return rsite

def fragEndBlunt(rsite, ovhg):
    return rsite - 1

def fragStart5overhang(rsite, ovhg):
    return rsite

def fragEnd5overhang(rsite, ovhg):
    return rsite - 1 - ovhg

def fragStart3overhang(rsite, ovhg):
    return rsite - restE.ovhg

def fragEnd3overhang(rsite, ovhg):
    return rsite - 1


def fragmentation(fragStart, fragEnd):
    fragment_id = 0
    for chrom in SeqIO.parse(open(args.genome, 'r'), 'fasta'):
        prev_rsite = 1
       
        for rsite in restE.search(chrom.seq):
            start = prev_rsite
            end = fragEnd(rsite, restE.ovhg)
            has_gap = False
            if chrom.name in gap:
                for g in gap[chrom.name]:
                    if(g[0] <= end and start <= g[1]):
                        has_gap = True
                        break

            gap_flag = '1' if has_gap else '0'
            fout.write(str(fragment_id) + '\t' + chrom.name + '\t' + \
                       str(start) + '\t' + str(end) + '\t' + gap_flag + '\n')
            prev_rsite = fragStart(rsite, restE.ovhg)
            fragment_id += 1
            
        else:
            start = prev_rsite
            end = len(chrom.seq)
            has_gap = False
            if chrom.name in gap:
                for g in gap[chrom.name]:
                    if(g[0] <= end and start <= g[1]):
                        has_gap = True
                        break

            gap_flag = '1' if has_gap else '0'
            fout.write(str(fragment_id) + '\t' + chrom.name + '\t' + \
                       str(start) + '\t' + str(end) + '\t' + gap_flag + '\n')
            fragment_id += 1
            
    
if(restE.is_blunt()):
    fragmentation(fragStartBlunt, fragEndBlunt)
elif(restE.is_5overhang()):
    fragmentation(fragStart5overhang, fragEnd5overhang)           
elif(restE.is_3overhag()):
    fragmentation(fragStart3overhag, fragEnd3overhag)
else:
    print 'ERROR: The type of overhang is \'unkown\''    
    quit
            
