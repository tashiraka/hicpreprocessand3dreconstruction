function [X, s] = shortest_path_classicalMDS(CFMFile, outXFile, outIDFile)
% upper trianged sparse matrix
%
%    X (array nbins x 3) 
%       Coordinates found by classical multidimensional scaling.
%
%    s (integer array)
%      Array with the indices to the coordinates in X that
%      correspond to bins with a contact (coordinates to the bins without
%      any contacts are found by linear interpolation).
%

fprintf('initialize contact matrix\n');
C = load(CFMFile);
C(:,1:2) = C(:,1:2) + 1;
C = spconvert(C);
C = C.';
nbins = size(C, 1);
s = find(sum(C, 1) + sum(C, 2)' ~= 0)'; % bins with interactions
sn = find(sum(C, 1) + sum(C, 2)' == 0)';  % find bins with no interactions

C(find(C)) = C(find(C)).^-1; % distance = 1 / frequency

fprintf('shortest path\n');
CM = squareform(graphallshortestpaths(C, 'Directed', false));

fprintf('remove bins with no interactions\n');
% matrix for selecting new collection of bins
I = true(nbins, nbins);
I(1:nbins+1:nbins*nbins) = false; % diagonals to zero
I(sn,:) = false;
I(:,sn) = false;
I = squareform(I);

CM = CM(I);

fprintf('find coordinates using CMDS\n');
X = cmdscale(CM, 3);

fprintf('interpolate the bins without contacts\n');
su = 1:nbins;
X = interp1(s, X, su,'linear');

fid = fopen(outXFile, 'w');
fprintf(fid, '%f\t%f\t%f\n', X.');
fclose(fid);

fid = fopen(outIDFile, 'w');
s = s - 1;
fprintf(fid, '%d\n', s);
fclose(fid);

exit();
end

