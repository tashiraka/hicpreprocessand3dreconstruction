import std.stdio, std.conv, std.string, std.typecons, std.algorithm,
  std.math, std.array;

enum Strand: char {fwd=0, rev=1}


void main(string[] args)
{
  auto DSFile = args[1];
  auto SSFile = args[2];
  auto outFile = args[3];
  
  uint[][Strand][string] ds;

  // Read DS    
  // qname1 rname1 strand1 pos1 fragID1 mapq1 mappedLen1 DNALen1        \
  // qname2 rname2 strand2 pos2 fragID2 mapq2 mappedLen2 DNALen2

  writeln("Reading DS");
  foreach (line; File(DSFile).byLineCopy) {
    auto fields = line.strip.split;

    auto rname1 = fields[1];
    auto rname2 = fields[9];
    auto str1 = fields[2] == "+" ? Strand.fwd : Strand.rev;
    auto str2 = fields[10] == "+" ? Strand.fwd : Strand.rev;
    auto pos1 = fields[3].to!uint;
    auto pos2 = fields[11].to!uint;

    ds[rname1][str1] ~= pos1;
    ds[rname2][str2] ~= pos2;
  }

  
  uint[][Strand][string] ss;
  
  // Read SS
  writeln("Reading SS");
  foreach (line; File(SSFile).byLineCopy) {
    auto fields = line.strip.split;

    auto rname1 = fields[1];
    auto str1 = fields[2] == "+" ? Strand.fwd : Strand.rev;
    auto pos1 = fields[3].to!uint;

    ss[rname1][str1] ~= pos1;
  }

  writeln("Sorting");
  foreach (rname; ds.byKey) { auto ds1 = ds[rname];
    foreach (str; ds1.byKey) { auto ds2 = ds1[str];
      ds2 = ds2.sort.uniq.array; }}

  foreach (rname; ss.byKey) { auto ss1 = ss[rname];
    foreach (str; ss1.byKey) { auto ss2 = ss1[str];
      ss2 = ss2.sort.uniq.array; }}


  writeln("Searching");
  auto fout = File(outFile, "w");

  foreach (rname; ss.byKey) {
    writeln(rname);
    
    foreach (str; ss[rname].byKey) {
      ulong id = 0;
      
      foreach (pos; ss[rname][str]) {
        if (rname in ds && str in ds[rname]) {
          //auto dist = ds[rname][str].map!(x => abs(x - pos)).minPos[0];
          //fout.writeln(dist);
          auto tmp = ds[rname][str];
          if (id == tmp.length + 1)
            fout.writeln(pos - tmp[$-1]);
          else {
            auto arr = tmp[id..$];
            
            foreach (i, p; arr) {
              if (i == arr.length - 1) {
                fout.writeln(pos - p);
                id = i;
              }
              if (pos <= p) {
                fout.writeln(min(pos - p, arr[i+1] - pos));
                id = i;
                break;
              }
            }
          } 
        }
        else fout.writeln("NaN");
      }
    }
  }  
}
