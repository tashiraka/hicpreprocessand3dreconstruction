import std.stdio, std.conv, std.string;


void main(string[] args)
{
  auto dsFile = args[1];
  auto fragFile = args[2];
  auto outDsFile = args[3];
  auto outSsFile = args[4];

  auto frags = readFrag(fragFile);
  auto foutDs = File(outDsFile, "w");
  auto foutSs = File(outSsFile, "w");

  // qname1 rname1 strand1 pos1 fragID1 mapq1 mappedLen1 DNALen1 \
  // qname2 rname2 strand2 pos2 fragID2 mapq2 mappedLen2 DNALen2
  foreach (line; File(dsFile).byLine) {
    auto fields = line.to!string.strip.split;
    auto rname1 = fields[1];
    auto rname2 = fields[9];
    auto fragID1 = fields[4].to!ulong;
    auto fragID2 = fields[12].to!ulong;

    auto frag1 = frags[fragID1];
    auto frag2 = frags[fragID2];
    
    if (!frag1.gap && !frag2.gap && !frag1.tel && !frag2.tel)
      foutDs.writeln(line);
    else if ((frag1.gap || frag1.tel) && !(frag2.gap || frag2.tel))
      foutSs.writeln(fields[8..16].join("\t"));
    else if (!(frag1.gap || frag1.tel) && (frag2.gap || frag2.tel))
      foutSs.writeln(fields[0..8].join("\t"));
  }
}


class Fragment
{
  uint id;
  string rname;
  bool gap = false;
  bool tel = false;
  
  this(uint id_, string rname_, bool gap_)
  {
    id = id_;
    rname = rname_;
    gap = gap_;
  }
}


//<fragID> <name> <start> <end> <fragStart> <fragEnd> <startNoOvhg> <endNoOvhg> <gapStart> <gapEnd>

Fragment[] readFrag(string file)
{
  Fragment[] frags;
  auto count = 0;
  
  foreach (line; File(file).byLine) {
      auto fields = line.to!string.strip.split("\t");

      if (count != fields[0].to!uint)
        throw new Exception("fragID is not ordered or continuous");

      frags ~= new Fragment(fields[0].to!uint, fields[1],
                            fields[8] != "*");
      count++;
  }


  auto prevRname = "";
  frags[0].tel = true;
  foreach (i; 1..frags.length) {
    if (prevRname != frags[i].rname) {
      frags[i-1].tel = true;
      frags[i].tel = true;
    }
    prevRname = frags[i].rname;
  }
  frags[$-1].tel = true;

  return frags;
}
