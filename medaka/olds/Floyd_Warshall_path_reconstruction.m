function [X, s, info] = Floyd_Warshall_path_reconstruction(CFMFile, outDistanceFile, outPathFile)
fprintf('initialize contact matrix\n');
C = load(CFMFile);
C(:,1:2) = C(:,1:2) + 1;
C = spconvert(C);
C = C.';
nbins = size(C, 1);
s = find(sum(C, 1) + sum(C, 2)' ~= 0)'; % bins with interactions
sn = find(sum(C, 1) + sum(C, 2)' == 0)';  % find bins with no
                                          % interactions

% Seaching optimized alpha and q
log_alpha = fminbnd(@(x) floyd_warshall_MBO(C, exp(x)), log(0.1), log(3), ...
                    optimset('TolX', 0.001, 'Display', 'off', ...
                             'MaxIter', 10));
alpah = exp(log_alpha);
[error, X, s] = floyd_warshall_MBO(C, alpha);
end

function [error] = floyd_warchall_MBO(C)
end
function [dist, edgeNum] = shortest_path(C)
end