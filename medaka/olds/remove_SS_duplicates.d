import std.stdio, std.string, std.conv, std.process, std.algorithm, std.array;

/*
 * Output format is same as .hic format used in Juicebox of Aiden Lab.
 * <readname> <str> <chr> <pos> <frag> <mapq>
 *
 * str = strand (0 for forward, anything else for reverse)
 */
void main(string[] args)
{
  auto bam = args[1];
  auto SEDupInDS = args[2].to!int;
  auto PEDupInDS = args[3].to!int;
  auto nthreads = args[4].to!int;
  auto outFile = args[5];

  auto removingRatio = PEDupInDS.to!double / SEDupInDS.to!double;

  removeDup(bam, outFile, removingRatio,  nthreads);
}


enum Strand : char {fwd=0, rev=1}


void removeDup(string bam, string outFile, double removingRatio, int nthreads)
{
  auto pipe = pipeProcess(["sambamba", "view",
                           "-t", nthreads.to!string,
                           bam]);
  SamRecord[] records;
  
  foreach (line; pipe.stdout.byLine)
    records ~= new SamRecord(line.to!string);

  SamRecord[][uint] fwdRec;  
  foreach (rec; records.filter!(x => x.strand == Strand.fwd))
    fwdRec[rec.pos] ~= rec;

  foreach (key; fwdRec.byKey) {
    auto sorted = fwdRec[key].sort!((x, y) => x.mapq > y.mapq);
  }
  
  SamRecord[][uint] revRec;  
  foreach (rec; records.filter!(x => x.strand == Strand.rev))
    revRec[rec.pos] ~= rec;

  
  auto fout = File(outFile, "w");
  
  //fout.writeln(record);
}


class SamRecord
{
  string qname, rname, mapq;
  uint pos;
  Strand strand;
  string frag = "*";
  
  this(string record) {
    auto fields = record.split('\t');
    qname = fields[0];
    rname = fields[2];
    pos = fields[3].to!uint;
    mapq = fields[4];
    
    auto flag = fields[1].to!uint;
    strand = (flag & 0x10) == 0 ? Strand.fwd : Strand.rev;

    auto cigar = fields[5];
    if(strand == Strand.rev) {
      pos = calcReverse5primePos(cigar);
    }
  }

  override string toString()
  {
    return qname ~ " " ~
           strand.to!string ~ " " ~
           rname ~ " " ~
           pos.to!string ~ " " ~
           frag ~ " " ~
           mapq;
  }
  
private:  
  uint calcReverse5primePos(string cigar) {
    auto cigarDict = parseCigar(cigar);

    if(cigarDict['='] != 0 || cigarDict['X'] != 0) {
      throw new Exception("ERROR: CIGAR strings has =/X instead of M.");
    }

    return pos - 1 + cigarDict['M'] + cigarDict['D']
      + cigarDict['N'] + cigarDict['S'] + cigarDict['H'];
  }

  int[char] parseCigar(string cigar) {
    auto cigarDict = ['M':0, 'I':0, 'D':0, 'N':0,
                      'S':0, 'H':0, 'P':0, '=':0, 'X':0];

    foreach(op; cigarDict.byKey) {
      auto capture = matchFirst(cigar, r"(\d+)" ~ op);
      cigarDict[op] = capture.empty ? 0 : capture.hit.split(op)[0].to!int;
    }

    return cigarDict;
  }
}
