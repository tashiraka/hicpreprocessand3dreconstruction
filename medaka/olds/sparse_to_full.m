function sparse_to_full(inFile, outFile)

% input a half of symmetric sparse matrix
spCFM = load(inFile);
spCFM(:, 1:2) = spCFM(:, 1:2) + sparse(1);
spCFM = spconvert(spCFM);

% reconstruct the full of symmetric sparse matrix
% diagonal element to 0.
spCFM = spCFM - diag(diag(spCFM));
spCFM = spCFM + spCFM.';

fullcfm = full(spCFM);
size(fullcfm)
save(outFile, '-ascii', '-tabs', 'fullcfm');
