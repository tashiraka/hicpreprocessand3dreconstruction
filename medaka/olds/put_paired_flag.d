import std.stdio, std.string, std.conv, std.process;


void main(string[] args)
{
  auto bam = args[1];
  auto nthreads = args[2].to!int;
  auto outFile = args[3];
  
  set_paired_flag(bam, outFile, nthreads);
}


// produce the paired flag and fields
void set_paired_flag(string inBam, string outBam, int nthreads) {
  auto oStream = pipeProcess(["sambamba", "view", "-f", "bam", "-S",
                              "-t", nthreads.to!string, "-o", outBam, "/dev/stdin"]);

  // Output header
  auto headerStream = pipeProcess(["sambamba", "view", "-H",
                                  "-t", nthreads.to!string, inBam]);
  foreach (line; headerStream.stdout.byLine)
    oStream.stdin.writeln(line);

  // Output alignments
  auto iStream = pipeProcess(["sambamba", "view",
                              "-t", nthreads.to!string, inBam]);

  while (true) {
    auto line1 = iStream.stdout.readln;
    auto line2 = iStream.stdout.readln;

    if (line1 !is null && line2 is null)
      throw new Exception("The number of the alignments is odd.");
    else if (line1 is null) break;
            
    auto fields1 = line1.split('\t'); // Do not strip, so containg '\n'
    auto fields2 = line2.split('\t');
            
    if (fields1[0] != fields2[0])
      throw new Exception("Not paired entry.\n" ~ line1 ~  line2);

    // Produce flags
    auto flag1 = fields1[1].to!int;
    auto flag2 = fields2[1].to!int;

    /*
     * 0x1    read paired
     * 0x2    read mapped in proper pair
     * 0x4    read unmapped
     * 0x8    mate unmapped
     * 0x10   read reverse strand
     * 0x20   mate reverse strand
     * 0x40   first in pair
     * 0x80   second in pair
     * 0x100  not primary alignment
     * 0x200  read fails platform/vendor quality checks
     * 0x400  read is PCR or optical duplicate
     * 0x800  supplementary alignment
     */
    // 0x100 is secondary flags
    if ((flag1 & 0x101) != 0 || (flag2 & 0x101) != 0)
      throw new Exception("ERROR: Not primary alignment OR already paired!!");

    flag1 += 0x1 + ((flag2 & 0x10) << 1) + 0x40;
    flag2 += 0x1 + ((flag1 & 0x10) << 1) + 0x80;
    fields1[1] = flag1.to!string;
    fields2[1] = flag2.to!string;
            
    // Produce fields
    // RNEXT. samtools automaticaly convert the same RNAME to '='.
    // sambamba does too ?
    fields1[6] = fields2[2];
    fields2[6] = fields1[2];
            
    // PNEXT
    fields1[7] = fields2[3];
    fields2[7] = fields1[3];            

    oStream.stdin.write(fields1.join("\t"));
    oStream.stdin.write(fields2.join("\t"));
  }
}
