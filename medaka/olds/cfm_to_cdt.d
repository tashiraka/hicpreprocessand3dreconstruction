import std.stdio, std.conv, std.string, std.algorithm, std.array, std.typecons;

// The format of binInfo: [binID, contigName, startCoord, endCoord]
// If you want to get sub contact frequency matrix, you extract bin from binInfo file.
void main(string[] args)
{
  auto sparseCfmFile = args[1];
  auto binInfoFile = args[2];
  auto outFile = args[3];

  uint[Tuple!(uint, uint)] sparseCFM;
  foreach(line; File(sparseCfmFile).byLine) {
    auto fields = line.to!string.strip.split("\t");
    sparseCFM[tuple(fields[0].to!uint, fields[1].to!uint)] = fields[2].to!uint;
  }

  string[] binInfo;
  foreach(line; File(binInfoFile).byLine) {
    binInfo ~= line.to!string.strip.replace("\t", "|");
  }

  auto fout = File(outFile, "w");
  fout.write("HiCMatrix\tRegions");
  foreach(info; binInfo) {
    fout.write("\t" ~ info);
  }
  fout.writeln;

  auto firstBinID = binInfo[0].split("|")[0].to!uint;
  auto lastBinID = binInfo[$ - 1].split("|")[0].to!uint;
  foreach(uint i, info; binInfo) {
    fout.write(info ~ "\t" ~ info);
    foreach(binID; firstBinID..(lastBinID+1)) {
      if(i + firstBinID > binID) {
        if((tuple(binID, i + firstBinID) in sparseCFM) is null) {
          fout.write("\t0");
        }
        else {
          fout.write("\t" ~ sparseCFM[tuple(binID, i + firstBinID)].to!string);
        }
      }
      else {
        if((tuple(i + firstBinID, binID) in sparseCFM) is null) {
          fout.write("\t0");
        }
        else {
          fout.write("\t" ~ sparseCFM[tuple(i + firstBinID, binID)].to!string);
        }
      }
    }
    fout.writeln;
  }
}
