function save_heatmap(CFMFile, outFile)
CFM = load(CFMFile);
CFM(:,1:2) = CFM(:,1:2) + 1;
CFM = spconvert(CFM);
CFM = CFM + CFM.' - diag(diag(CFM));
CFM = log10(CFM);

saveas(plot(HeatMap(CFM, 'Colormap', 'redbluecmap')), outFile, 'png');
end