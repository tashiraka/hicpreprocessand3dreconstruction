import std.stdio, std.string, std.conv, std.process;


void main(string[] args)
{
  auto bam = args[1];
  auto minMapq = args[2].to!int;
  auto nthreads = args[3].to!int;
  auto DSFile = args[4];
  auto SSFile = args[5];
  
  pairing(bam, DSFile, SSFile, minMapq, nthreads);
}


// produce the paired flag and fields
void pairing(string inBam, string DSBam, string SSBam, int minMapq, int nthreads) {
  auto dsStream = pipeProcess(["sambamba", "view", "-f", "bam", "-S",
                              "-t", nthreads.to!string, "-o", DSBam, "/dev/stdin"]);
  auto ssStream = pipeProcess(["sambamba", "view", "-f", "bam", "-S",
                              "-t", nthreads.to!string, "-o", SSBam, "/dev/stdin"]);
  
  // Output header
  auto headerStream = pipeProcess(["sambamba", "view", "-H",
                                  "-t", nthreads.to!string, inBam]);
  foreach (line; headerStream.stdout.byLine) {
    dsStream.stdin.writeln(line);
    ssStream.stdin.writeln(line);
  }

  // Output alignments
  auto iStream = pipeProcess(["sambamba", "view",
                              "-t", nthreads.to!string, inBam]);

  while (true) {
    auto line1 = iStream.stdout.readln;
    auto line2 = iStream.stdout.readln;

    if (line1 !is null && line2 is null)
      throw new Exception("The number of the alignments is odd.");
    else if (line1 is null) break;
            
    auto fields1 = line1.split('\t'); // Do not strip, so containg '\n'
    auto fields2 = line2.split('\t');
            
    if (fields1[0] != fields2[0])
      throw new Exception("Not paired entry.\n" ~ line1.to!string ~  line2.to!string);

    // Produce flags
    auto flag1 = fields1[1].to!int;
    auto flag2 = fields2[1].to!int;

    // 0x100 is secondary flags
    if ((flag1 & 0x100) != 0 || (flag2 & 0x100) != 0)
      throw new Exception("ERROR: Not primary alignment!!");

    auto mapq1 = fields1[4].to!int;
    auto mapq2 = fields2[4].to!int;
    auto valid1 = mapq1 >= minMapq;
    auto valid2 = mapq2 >= minMapq;
    if (valid1 && valid2) dsStream.stdin.write(line1, line2);
    else if (valid1 && !valid2) ssStream.stdin.write(line1);
    else if (!valid1 && valid2) ssStream.stdin.write(line2);
  }
}
