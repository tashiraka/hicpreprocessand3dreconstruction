import std.stdio, std.conv, std.string, std.algorithm, std.array;


/*
 * Return:
 *     <fragLen> <read depth at an end of fragment> <# paired fragment> 
 */


void main(string[] args)
{
  auto dsFile = args[1];
  auto fragFile = args[2];

  auto frags = readFrag(fragFile);

  // qname1 rname1 strand1 pos1 fragID1 mapq1 mappedLen1 DNALen1 \
  // qname2 rname2 strand2 pos2 fragID2 mapq2 mappedLen2 DNALen2
  foreach (line; File(dsFile).byLine) {
    auto fields = line.to!string.strip.split;
    auto fragID1 = fields[4].to!ulong;
    auto fragID2 = fields[12].to!ulong;

    auto str1 = fields[2];
    auto str2 = fields[10];

    auto frag1 = frags[fragID1];
    auto frag2 = frags[fragID2];


    //if (frag1.gap || frag1.tel || frag2.gap || frag2.tel)
    //  throw new Exception("Gap or telomere fragment");

    if (str1 == "+") {
      frag1.depthFwd++;
      frag1.pairedIdsFwd ~= frag2.id;
    }
    else {
      frag1.depthRev++;
      frag1.pairedIdsRev ~= frag2.id;
    }
    if (str2 == "+") {
      frag2.depthFwd++;
      frag2.pairedIdsFwd ~= frag1.id;
    }
    else {
      frag2.depthRev++;
      frag2.pairedIdsRev ~= frag1.id;      
    }
  }
  
  foreach (frag; frags) {
    writeln(frag.fragLen, "\t", frag.depthFwd, "\t",
            frag.pairedIdsFwd.sort.uniq.array.length);
    writeln(frag.fragLen, "\t", frag.depthRev, "\t",
            frag.pairedIdsRev.sort.uniq.array.length);
  }
}


class Fragment
{
  uint id;
  string rname;
  uint fragLen, depthFwd=0, depthRev=0;
  uint[] pairedIdsFwd, pairedIdsRev;

  this(uint id_, string rname_, uint len)
  {
    id = id_;
    rname = rname_;
    fragLen = len;
  }
}


//<fragID> <name> <start> <end> <fragStart> <fragEnd> <startNoOvhg> endNoOvhg> <gapStart> <gapEnd>

Fragment[] readFrag(string file)
{
  Fragment[] frags;
  auto count = 0;
  
  foreach (line; File(file).byLine) {
      auto fields = line.to!string.strip.split("\t");

      if (count != fields[0].to!uint)
        throw new Exception("fragID is not ordered or continuous");

      frags ~= new Fragment(fields[0].to!uint, fields[1],
                            fields[5].to!uint - fields[4].to!uint);

      count++;
  }

  return frags;
}
