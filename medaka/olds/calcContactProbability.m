function calcContactProbability(CFMFilename, binInfoFile, outFile)
% CFM is upper or lower triangle.
% CFM contains the entry of (matrix size, matrix size, value). So
% OK only spconvert.
% binInfoFile fields: binID contigName startCoord endCoord

CFM = load(CFMFilename);
CFM = spconvert(CFM);
CFM = CFM + CFM.' - diag(diag(CFM));

zeroSumIDs = find(sum(CFM, 1) == 0);
zeroSumFlags = false(size(CFM, 1), size(CFM, 1));
zeroSumFlags(zeroSumIDs, zeroSumIDs) = true;

binInfo = load(binInfoFile);
binInfo(:, 1) = binInfo(:, 1) + 1;
contigs = unique(binInfo(:, 2))';

fid = fopen(outFile, 'w');

for contig = contigs
    intraChrIDs = binInfo(find(binInfo(:, 2) == contig), 1);
    intraChrCFM = logical(CFM(intraChrIDs, intraChrIDs));
    intraZeroSumFlags = zeroSumFlags(intraChrIDs, intraChrIDs);
    N = size(intraChrCFM, 1);
    
    if N < 2
        continue
    end
    
    for s = 1:(N - 1)
        % s-th off diagonal
        count = full(sum(diag(intraChrCFM, s)));
        % Ignore bins with all zeros
        zeroSumCount = full(sum(diag(intraZeroSumFlags, s)));;
        
        %n = N - s;
        n = N - s - zeroSumCount;
        if n > 0
            fprintf(fid, '%d\t%d\t%f\n', contig, s, count / n);
        else
            fprintf(fid, '%d\t%d\t%f\n', contig, s, 0);
        end
    end
end

fclose(fid);
exit();