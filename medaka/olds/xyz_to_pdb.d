import std.stdio, std.conv, std.string, std.array, std.algorithm, std.typecons, std.math;


version(unittest){}
 else{
   void main(string[] args)
   {
     auto coordFile = args[1];
     auto binInfoFile = args[2];
     auto outFile = args[3];

     auto coords = readCoord(coordFile);
     auto binInfo = readBinInfo(binInfoFile);
     auto pdb = coordsToPdb(coords, binInfo);

     auto fout = File(outFile, "w");
     foreach (entry; pdb) fout.writeln(entry);
   }
 }


double[][] readCoord(string filename)
{
  double[][] coords;
  foreach (line; File(filename).byLine)
    coords ~= line.to!string.strip.split("\t").map!(x => x.to!double).array;
  return coords;
}

string[][] readBinInfo(string filename)
{
  string[][] binInfo;
  foreach (line; File(filename, "r").byLine)
    binInfo ~= line.to!string.strip.split("\t");
  return binInfo;
}


uint[] getChrTerminal(string[][] binInfo)
{
  uint[] chrTerminal;
  if (binInfo.length < 1) throw new Exception("Empty binInfo");
  
  foreach(i; 1..binInfo.length) {
    if(binInfo[i][1] != binInfo[i-1][1]) {
      chrTerminal ~= binInfo[i-1][0].to!uint;
    }
  }
  
  return chrTerminal ~ binInfo[$-1][0].to!uint;
}

double[][] adjustDigit(double[][] coords, int digit)
{
  auto upperLimit = 10 ^^ digit;
  auto lowerLimit = 10 ^^ (digit - 1);
  double absMax = 0;
  foreach (coord; coords) absMax = max(absMax, coord.map!(x => abs(x)).reduce!max);

  auto shifter = 1.0;
  if (absMax < lowerLimit) {
    shifter = 10.0;
    while (absMax * shifter < lowerLimit) shifter *= 10;
  }
  else if (upperLimit < absMax) {
    shifter = 0.1;
    while (upperLimit < absMax * shifter) shifter *= 0.1;
  }

  double[][] scaledCoords;
  foreach (i; 0..coords.length) scaledCoords ~= coords[i].map!(x => x * shifter).array;
  return scaledCoords;
}

// The format of binInfo: [binID, contigName, startCoord, endCoord]
string[] coordsToPdb(double[][] coords, string[][] binInfo)
{
  if (coords.length != binInfo.length)
    throw new Exception("The length of coordinates and binInfo are different.");

  coords = coords.adjustDigit(3);
  
  string[] entries;
  string[] connectEntries;
  auto chrIDs = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
                 "A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
                 "K", "L", "M", "N", "O", "p", "Q", "R", "S", "T",
                 "U", "v", "W", "X", "Y", "Z",];
  auto chrID = 0;
  auto chrTerminal = getChrTerminal(binInfo);
  auto coordID = 0;
  Tuple!(ulong, bool)[] connection;
  
  foreach(i, coord; coords) {
    // 1..6 (6)
    auto atom = "HETATM";
    // 7..11 (5)
    auto serial = format("%5d", i);
    if(serial.length != 5) throw new Exception("ERROR: serial.length != 5");
    // 13..16 (1 + 4)
    auto name = "     ";
    // 17 (1)
    auto altLoc = " ";
    // 18..20 (3)
    auto resName = "   ";
    // 22 (1 + 1)
    auto chainID = " " ~ chrIDs[chrID];
    if(chainID.length != 2) throw new Exception("ERROR: chainID.length != 2");
    // 23..26 (4)
    auto resSeq = "    ";
    // 27 (1)
    auto iCode = " ";
    // 31..38 (3 + 8)
    auto x = format("   %8.3f", coords[coordID][0]);
    if(x.length != 11) throw new Exception("ERROR: x.length != 11");
    // 39..46 (8)
    auto y = format("%8.3f", coords[coordID][1]);
    if(y.length != 8) throw new Exception("ERROR: y.length != 8");
    // 47..54 (8)
    auto z = format("%8.3f", coords[coordID][2]);
    if(z.length != 8) throw new Exception("ERROR: z.length != 8");
    // 55..60 (6)
    auto occupancy = "      ";
    // 61..66 (6)
    auto tempFactor = "      ";
    // 77..78 (10 + 2)
    auto element = "            ";
    // 79..80 (2)
    auto charge = "  ";
    
    entries ~= atom ~ serial ~ name ~ altLoc ~ resName ~ chainID ~ resSeq
      ~ iCode ~ x ~ y ~ z ~ occupancy ~ tempFactor ~ element ~ charge;
    
    coordID++;
    connection ~= tuple(i, true);
  

    if(i == chrTerminal[chrID]) {      
      auto ter = "TER   ";
      serial = "     ";
      resName = "   ";
      chainID = " " ~ chrIDs[chrID];
      if(chainID.length != 2) throw new Exception("ERROR: chainID.length != 2");
      
      resSeq = "    ";
      iCode = " ";
      
      entries ~= ter ~ serial ~ resName ~ chainID ~ resSeq ~ iCode;
      chrID++;
      connection ~= tuple(i, false);
    }
  }
  
  foreach(i, noUse; connection) {
    if(i < connection.length - 1
       && connection[i][1] && connection[i+1][1])
      {
	if(connection[i][0].to!string.length > 5
           || connection[i+1][0].to!string.length > 5)
	  throw new Exception("Atom's ID overflows in CONNECT.");
        
	auto id1 = format("%5d", connection[i][0]);
        auto id2 = format("%5d", connection[i+1][0]);
        if(id1.length != 5 || id2.length != 5)
          throw new Exception("ERROR: CONECT length != 5");
        
	entries ~= "CONECT" ~ id1 ~ id2 ~ "               ";
      }
  }
  
  
  entries ~= "END   ";
  
  return entries;
}
