function KRnormalize(inFile, outFile, biasFile)

fprintf('Loading the upper triangled contact frequency matrix from the file.\n');
spCFM = load(inFile);
spCFM(:, 1:2) = spCFM(:, 1:2) + sparse(1);
spCFM = spconvert(spCFM);

fprintf('Creating the full matrix from loaded upper (or lower) triangle matrix.\n');
spCFM = spCFM + spCFM.' - diag(diag(spCFM));
nbin = size(spCFM, 1);

fprintf(['Removing the rows and columns with all zeros or sum one ' ...
         'or sum two.\n']);
sumSpCFM = sum(spCFM);
nonZeroSumIDs = find(sumSpCFM ~= 0 & sumSpCFM ~= 1 & sumSpCFM ~= 2);
reducedSpCFM = spCFM(nonZeroSumIDs, nonZeroSumIDs);

fprintf(['Normalizing (balancing) the contact frequency matrix (%d ' ...
         '* %d).\n'], size(nonZeroSumIDs, 2), size(nonZeroSumIDs,2));
[x, res, notConvergeFlag] = bnewt(reducedSpCFM);

if notConvergeFlag
    fid = fopen(outFile,'w');
    fprintf(fid, 'Not converged\n');
    fclose(fid);
else
    normedReducedSpCFM = diag(x)*reducedSpCFM*diag(x);
    fprintf('Inserting the rows and columns with all zeros.\n');
    normedSpCFM = sparse(nbin, nbin);
    normedSpCFM(nonZeroSumIDs, nonZeroSumIDs) = normedReducedSpCFM;

    bias = NaN(nbin, 1);
    bias(nonZeroSumIDs) = full(x);
    
    fprintf('Writing the normalized constact frequency matrix to the file.\n');
    [i, j, val] = find(triu(normedSpCFM));
    i = i - 1;
    j = j - 1;
    fid = fopen(outFile,'w');
    fprintf(fid, '%d\t%d\t%f\n', vertcat(i', j', val'));
    fclose(fid);

    fid = fopen(biasFile,'w');
    fprintf(fid, '%f\n', bias);
    fclose(fid);
    
end

exit();