import std.stdio, std.conv, std.string, std.array,
  std.algorithm, std.math, std.range;


version(unittest){}
 else {
   void main(string[] args)
   {
     auto EigValFile = args[1];
     auto EigVecFile = args[2];
     auto scalingFactor = args[3].to!double;
     auto outFile = args[4];

     auto eigVals = readVec(EigValFile);
     auto eigVecs = readMat(EigVecFile);
     auto coords = eigToX(eigVals, eigVecs, scalingFactor);
     writeMat(coords, outFile);
   }
 }


// assuming eigVals is sorted by ascending order.
double[][] eigToX(double[] eigVals, double[][] eigVecs, double scalingFactor)
{
  if(eigVals.length < 3)
    throw new Exception("ERROR: the number of eigenvalues < 3");
  
  auto eigVal1 = eigVals[$-1];
  auto eigVal2 = eigVals[$-2];
  auto eigVal3 = eigVals[$-3];

  if(eigVal1 < 0 || eigVal1 < 0 || eigVal3 < 0)
    throw new Exception("ERROR: one of top 3 eigenvalues are negative.");
  
  auto eigVec1 = eigVecs.map!(x => x[$-1]).array.standardize(scalingFactor);
  auto eigVec2 = eigVecs.map!(x => x[$-2]).array.standardize(scalingFactor);
  auto eigVec3 = eigVecs.map!(x => x[$-3]).array.standardize(scalingFactor);
  
  double[][] coords;
  
  foreach(i; 0..eigVec1.length) {
    coords ~= [sqrt(eigVal1) * eigVec1[i],
               sqrt(eigVal2) * eigVec2[i],
               sqrt(eigVal3) * eigVec3[i]];
  }
  
  return coords;
}


double[] standardize(double[] v, double scaleFactor)
{
  auto norm = v.euclideanNorm;
  return v[].map!(x => scaleFactor * x / norm).array;
}


double calcGofEigVec(double[][] coords, double[][] distMat)
{
  double sqrSum = 0.0;
  
  foreach(i, row; distMat) 
    foreach(j, elem; row) 
      sqrSum += (euclideanDist(coords[i], coords[j]) - distMat[i][j])^^2;

  return sqrSum.sqrt / distMat.length^^2;
}


double euclideanDist(double[] u, double[] v)
{
  return zip(u, v).map!(x => (x[0] - x[1])^^2).sum.sqrt;
}


double euclideanNorm(double[] v)
{
  return v.map!(x => x^^2).sum;
}


double[] readVec(string filename)
{
  auto vecApp = appender!(double[]);
  auto fin = File(filename, "r");
  foreach(line; fin.byLine) {
    vecApp.put(line.to!string.strip.to!double);
  }
  return vecApp.data;
}


double[][] readMat(string filename)
{
  auto matApp = appender!(double[][]);
  auto fin = File(filename, "r");
  foreach(line; fin.byLine) {
    matApp.put(line.to!string.strip.split("\t").map!(x => x.to!double).array);
  }
  return matApp.data;
}


void writeMat(double[][] mat, string filename)
{
  auto fout = File(filename, "w");
  foreach(row; mat) {
    foreach(i, elem; row) {
      fout.write(elem);
      if(i < row.length - 1) {
        fout.write("\t");
      }
    }
    fout.writeln;
  }
}
