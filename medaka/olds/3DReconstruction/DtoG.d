import std.stdio, std.conv, std.string, std.array, std.algorithm, std.math, std.parallelism;


version(unittest){}
 else {
   void main(string[] args)
   {
     auto distMatFile = args[1];
     auto outFile = args[2];

     auto distMat = readMat(distMatFile);
     auto gramMat = DtoG(distMat);
     writeMat(gramMat, outFile);
   }
 }


double[][] DtoG(double[][] distMat)
{
  immutable auto N = distMat.length;
  auto sqrDistMat = distMat.map!(x => x.map!(y => y^^2 / 2.0));

  auto sqrD = new double[](N);
  foreach(i; 0..N)
    sqrD[i] = sqrDistMat[i].sum / N;

  auto sqrAvrg = reduce!("a + b.sum")(0.0, sqrDistMat) / N / N;

  auto gramMat = new double[][](N, N);
  foreach(i; 0..N)
    foreach(j; 0..N)
      gramMat[i][j] = sqrD[i] + sqrD[j] - sqrAvrg - sqrDistMat[i][j];

  return gramMat;
}


double[][] readMat(string filename)
{
  auto matApp = appender!(double[][]);
  auto fin = File(filename, "r");
  foreach(line; fin.byLine) {
    matApp.put(line.to!string.strip.split("\t").map!(x => x.to!double).array);
  }
  return matApp.data;
}


void writeMat(double[][] mat, string filename)
{
  auto fout = File(filename, "w");
  foreach(row; mat) {
    foreach(i, elem; row) {
      fout.write(format("%f", elem));
      if(i < row.length - 1) {
        fout.write("\t");
      }
    }
    fout.writeln;
  }
}
