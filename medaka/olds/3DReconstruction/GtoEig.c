#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <f2c.h>
#include <clapack.h>


void readMat(char* filename, integer bufSize,
             doublereal** mat, integer* matSize)
{
  char* buf = (char*)malloc(sizeof(char*) * bufSize);
  *matSize = 0;
  FILE* fp = fopen(filename, "r");

  if(fp == NULL) {
    printf("ERROR: %s is not opend.", filename);
    exit(1);
  }

  while(fgets(buf, 1000000, fp) != NULL) {
    (*matSize)++;
  }

  rewind(fp);
  
  *mat = (doublereal*)malloc(sizeof(doublereal*) * (*matSize) * (*matSize));
  
  integer i = 0;
  integer j = 0;
  char* sepLineP;
  
  while(fgets(buf, 1000000, fp) != NULL) {
    sepLineP = strtok(buf, "\t\n");
    
    if(sepLineP != NULL) {
      (*mat)[i + j * (*matSize)] = atof(sepLineP);
      j++;
    }
    
    while(sepLineP != NULL) {
      sepLineP = strtok(NULL, "\t\n");

      if(sepLineP != NULL) {
        (*mat)[i + j * (*matSize)] = atof(sepLineP);
        j++;
      }
    }
    i++;
    j = 0;
  }
  
  fclose(fp);
}


void printMat(doublereal *A, integer rowSize, integer colSize, integer lda,
              char* filename)
{
  FILE* fp = fopen(filename, "w");

  if(fp == NULL) {
    printf("ERROR: %s is not opend.", filename);
    exit(1);
  }

  integer i = 0;
  integer j = 0;
  for (i = 0; i < rowSize; i++) {
    for (j = 0; j < colSize; j++) {
      fprintf(fp, "%f", A[i + j * lda]);

      if (j < colSize - 1)
        fprintf(fp, "\t");
    }
    
    fprintf(fp, "\n");
  }
}


int main(int argc, char* argv[])
{
  char* symMatFile = argv[1];
  char* outEigValFile = argv[2];
  char* outEigVecFile = argv[3];
  integer bufSize = 1000000;
  
  char jobz = 'V';
  char uplo = 'U';
  integer n = 0;
  doublereal* A;
  readMat(symMatFile, bufSize, &A, &n);
  integer lda = n;
  doublereal* w = (double*)malloc(sizeof(double) * n);
  doublereal* work = (double*)malloc(sizeof(double));
  integer lwork = -1;
  integer info = 0;
  
  dsyev_(&jobz, &uplo, &n, A, &lda, w, work, &lwork, &info);

  if(info) {
    printf("ERROR: dsyev for calculate the optimal size is failed.\n");
    return 1;
  }
  
  lwork = (integer)work[0];
  free(work);
  work = (double*)malloc(sizeof(double) * lwork);

  dsyev_(&jobz, &uplo, &n, A, &lda, w, work, &lwork, &info);

  if(info) {
    printf("ERROR: dsyev for calculate the eigs is failed.\n");
    return -1;
  }

  printMat(w, n, 1, 1, outEigValFile);
  printMat(A, n, n, lda, outEigVecFile);
  
  free(A);
  free(w);
  free(work);
  
  return 0;
}


/* 
int dsyev_(char *jobz, char *uplo, integer *n, doublereal *a,
	 integer *lda, doublereal *w, doublereal *work, integer *lwork, 
	integer *info)
{
    Purpose   
    =======   

    DSYEV computes all eigenvalues and, optionally, eigenvectors of a   
    real symmetric matrix A.   

    Arguments   
    =========   

    JOBZ    (input) CHARACTER*1   
            = 'N':  Compute eigenvalues only;   
            = 'V':  Compute eigenvalues and eigenvectors.   

    UPLO    (input) CHARACTER*1   
            = 'U':  Upper triangle of A is stored;   
            = 'L':  Lower triangle of A is stored.   

    N       (input) INTEGER   
            The order of the matrix A.  N >= 0.   

    A       (input/output) DOUBLE PRECISION array, dimension (LDA, N)   
            On entry, the symmetric matrix A.  If UPLO = 'U', the   
            leading N-by-N upper triangular part of A contains the   
            upper triangular part of the matrix A.  If UPLO = 'L',   
            the leading N-by-N lower triangular part of A contains   
            the lower triangular part of the matrix A.   
            On exit, if JOBZ = 'V', then if INFO = 0, A contains the   
            orthonormal eigenvectors of the matrix A.   
            If JOBZ = 'N', then on exit the lower triangle (if UPLO='L')   
            or the upper triangle (if UPLO='U') of A, including the   
            diagonal, is destroyed.   

    LDA     (input) INTEGER   
            The leading dimension of the array A.  LDA >= max(1,N).   

    W       (output) DOUBLE PRECISION array, dimension (N)   
            If INFO = 0, the eigenvalues in ascending order.   

    WORK    (workspace/output) DOUBLE PRECISION array, dimension (LWORK)   
            On exit, if INFO = 0, WORK(1) returns the optimal LWORK.   

    LWORK   (input) INTEGER   
            The length of the array WORK.  LWORK >= max(1,3*N-1).   
            For optimal efficiency, LWORK >= (NB+2)*N,   
            where NB is the blocksize for DSYTRD returned by ILAENV.   

            If LWORK = -1, then a workspace query is assumed; the routine   
            only calculates the optimal size of the WORK array, returns   
            this value as the first entry of the WORK array, and no error   
            message related to LWORK is issued by XERBLA.   

    INFO    (output) INTEGER   
            = 0:  successful exit   
            < 0:  if INFO = -i, the i-th argument had an illegal value   
            > 0:  if INFO = i, the algorithm failed to converge; i   
                  off-diagonal elements of an intermediate tridiagonal   
                  form did not converge to zero.   

    =====================================================================   
*/
