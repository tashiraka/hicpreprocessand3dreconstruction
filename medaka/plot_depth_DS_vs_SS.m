function plot_depth_DS_vs_SS(cfmFile, ssDepthFile, plotFile)
    cfm = load(cfmFile);
    cfm(:, 1:2) = cfm(:, 1:2) + sparse(1);
    cfm = spconvert(cfm);
    cfm = cfm + cfm.' - diag(diag(cfm));
    depthDS = full(sum(cfm));

    depthSS = load(ssDepthFile);

    saveas(scatter(log10(depthDS), log10(depthSS)), plotFile, 'eps');
    