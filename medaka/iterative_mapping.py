import os, sys, shutil, subprocess, tempfile, pysam, logging


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def isReadUnmapped(read, minMapq):
    if (read.mapq < minMapq):
        return True
#    for tag in read.tags:
#        if tag[0] == 'XT' and tag[1] == 'N':
#            return True
    return False


def filterUnmappedFastq(mappingFastqStream, inSam, unmappedFastqPath, minMapq, lenStep):
    # Collect the unmapped-primary read IDs.
    unmappedIDs = set()
    for read in pysam.Samfile(inSam, 'r'):
        if (not read.is_secondary) and isReadUnmapped(read, minMapq):
            unmappedIDs.add(read.qname)

    # Write the unmapped reads to FASTQ file.
    fout = open(unmappedFastqPath, 'w')
    numFiltered = 0
    numTotal = 0

    while True:
        line = mappingFastqStream.readline()

        if not line: break

        readID = line.split()[0][1:]
        
        if readID in unmappedIDs:
            fout.write(line)
            fout.write(mappingFastqStream.readline())
            fout.write(mappingFastqStream.readline())
            fout.write(mappingFastqStream.readline())
            numFiltered += 1
        numTotal += 1
        
    return numTotal, numFiltered


def iterativeMapping(readGroupInfo, indexPath, fastqPath, outPath,
                     minSeqLen, lenStep, minMapq, nThreads):
    # Get the read length.
    readCmd = ['pigz', '-dc', fastqPath]
    readProcess = subprocess.Popen(readCmd, stdout=subprocess.PIPE)
    readProcess.stdout.readline()
    rawSeqLen = len(readProcess.stdout.readline().strip())
    logger.info('The length of whole sequences in the file: %d', rawSeqLen)
    readProcess.terminate()

    # Current mapping-seaqence length in the iteration.
    currentSeqLen = minSeqLen

    # Current FASTQ file to map
    currentFastqPath = fastqPath
    readCmd = ['pigz', '-dc', currentFastqPath]
    
    # Main iteration
    firstFlag = True

    while True:
        logger.info('Mapping length: %d', currentSeqLen)
        trim_3 = rawSeqLen - currentSeqLen
        logger.info('Trimming length form 3\': %d', trim_3)

        if not firstFlag:
            readCmd = ['cat', currentFastqPath]

        currentOut = outPath + '.' + str(currentSeqLen)
        mapCmd = ['bowtie2', 
                  '--very-sensitive', 
                  '--rg-id', readGroupInfo, 
                  '-p', str(nThreads), 
                  '-3', str(trim_3), 
                  '-x', indexPath, 
                  '-q', '-']
        pipeline = []

        try:
            # Read out FASTQ to BWA
            logger.info('Read command: %s', ' '.join(readCmd))
            pipeline.append(subprocess.Popen(readCmd, stdout=subprocess.PIPE, bufsize=-1))

            # bowtie2
            logger.info('Map command: %s', ' '.join(mapCmd))
            pipeline.append(subprocess.Popen(mapCmd, stdin=pipeline[-1].stdout, 
                stdout=open(currentOut, 'w'), bufsize=-1))

            pipeline[-1].wait()
        finally:
            for process in pipeline:
                if process.poll() is None:
                    process.terminate()

        # Next map length
        nextSeqLen = currentSeqLen + lenStep
        if nextSeqLen > rawSeqLen:
            if currentSeqLen < rawSeqLen:
                nextSeqLen = rawSeqLen # Must mapp rawSeqLen
            else:
                break
                    
        # Go to the next iteration.
        logger.info('Save the unique aligments and send the non-unique ones to the next iteration')
        unmappedFastqPath = fastqPath + '.%d' % nextSeqLen + ".fastq"
        readProcess = subprocess.Popen(readCmd, stdout=subprocess.PIPE, bufsize=-1)
        
        # Filtering the unmapped reads.
        numTotal, numFiltered = filterUnmappedFastq(readProcess.stdout, currentOut, unmappedFastqPath, minMapq, lenStep)
        readProcess.communicate()

        logger.info(('{0} non-unique reads out of {1} are sent the next iteration.').format(numFiltered, numTotal))

        if numFiltered == 0:
            break

        # Prepare for next iteretion.
        if not firstFlag:
            os.remove(currentFastqPath)
        else:
            firstFlag = False
            
        currentSeqLen = nextSeqLen
        currentFastqPath = unmappedFastqPath

    # remove temp fastq
    if not firstFlag:
        os.remove(currentFastqPath)


args = sys.argv
readGroupInfo = args[1]
indexPath = args[2]
fastqPath = args[3]   
outPath = args[4]
minSeqLen = args[5]
lenStep = args[6]
minMapq = args[7]
nThreads = args[8]

logging.info('Start of iterative mapping')
logging.info('iterativeMapping(readGroupInfo={0}, indexPath={1}, fastqPath={2}, outPath={3}, minSeqLen={4}, lenStep={5}, minMapq={6}, nThreads={7}'.format(readGroupInfo, indexPath, fastqPath, outPath, minSeqLen, lenStep, minMapq, nThreads))

iterativeMapping(
    readGroupInfo=readGroupInfo,
    indexPath=indexPath,
    fastqPath=fastqPath,
    outPath=outPath,
    minSeqLen=int(minSeqLen),
    lenStep=int(lenStep),
    minMapq=int(minMapq),
    nThreads=nThreads
    )

logging.info('End of iterative mapping')
