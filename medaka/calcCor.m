function calcCor(subCfmFile, wholeCfmFile, binInfoFile, contigName, ...
                 s, e)
% CFM is upper or lower triangle.
% CFM contains the entry of (matrix size, matrix size, value). So
% OK only spconvert.
% binInfoFile fields: binID contigName startCoord endCoord gap
% extract chr1-chr2 matrix

CFM = load(wholeCfmFile);
CFM(:,1:2) = CFM(:,1:2) + 1;
CFM = spconvert(CFM);

fid = fopen(binInfoFile);
binInfo = textscan(fid, '%d %s %d %d %s');
fclose(fid);
binInfo{1} = binInfo{1} + 1;

% Bin IDs corresponding to contigs
IDs = binInfo{1}(strcmp(binInfo{2}, contigName));
CFM1 = CFM(IDs, IDs);


CFM2 = load(subCfmFile);
CFM2(:,1:2) = CFM2(:,1:2) + 1;
CFM2 = spconvert(CFM2);


depth1 = sum(CFM + CFM' - diag(diag(CFM)), 1);
depth1 = depth1(IDs);
depth2 = sum(CFM2 + CFM2' - diag(diag(CFM2)), 1);

if length(depth1) ~= length(depth2)
    fprintf('Error: two matrix size is different.');
    exit(1);
end

valid = (depth1 > 0 & depth2 > 0);

A = CFM1(s:e, s:e);
B = CFM2(s:e, s:e);
valid = valid(s:e);

A = A(valid, valid);
B = B(valid, valid);

A = A(triu(true(size(A))));
B = B(triu(true(size(B))));

A = full(A);
B = full(B);
size(A)

[rho, p] = corr(A, B, 'type', 'Spearman');
fprintf('Spearman correlation: %f\nP value: %f\n', rho, p);


[rho, p] = corr(A, B);
fprintf('Peason correlation: %f\nP value: %f\n', rho, p);