import std.stdio, std.string, std.conv, std.math;


void main(string[] args)
{  
  foreach (line; stdin.byLineCopy) {
    auto fields = line.strip.split;
    
    //auto rname1 = fields[1];
    //auto rname2 = fields[9];
    auto str1 = fields[2] == "+" ? Strand.fwd : Strand.rev;
    auto str2 = fields[10] == "+" ? Strand.fwd : Strand.rev;
    auto pos1 = fields[3].to!uint;
    auto pos2 = fields[11].to!uint;
    auto frag1 = fields[4].to!uint;
    auto frag2 = fields[12].to!uint;
    //auto libLen = fields[7].to!ulong + fields[15].to!ulong;

    // Remove not ligated
    if (frag1 == frag2) {
      if (str1 == Strand.fwd && str2 == Strand.rev && pos1 <= pos2)
        continue;
      else if (str1 == Strand.rev && str2 == Strand.fwd && pos2 <= pos1)
        continue;
    }

    writeln(line);
  }
}


enum Strand: char {fwd=0, rev=1}

