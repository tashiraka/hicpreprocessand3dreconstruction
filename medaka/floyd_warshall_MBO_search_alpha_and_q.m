function [X, alpha, q, error] = ...
    floyd_warshall_MBO_search_alpha_and_q(CFM, alpha_min, alpha_max, ...
                                          q_min, q_max)
    
    fprintf('Finding all-pairs shortest path using Floyd-Warshall algorithm\n');
    D = CFM;
    n = size(D, 1);
    D = D + D.';
    D(1:n+1:n*n) = 0;
    D(find(D)) = D(find(D)).^(-1);

    % Floyd-Warshall algorithm with path reconstruction
    next = zeros(n, n); % next is right before j in a shortest path.
    for col = 1:n
        next(:, col) = col;
    end

    for k = 1:n
        for i = 1:(n-1)
            for j = (i+1):n
                if D(i, k) + D(k, j) < D(i, j)
                    D(i, j) = D(i, k) + D(k, j);
                    D(j, i) = D(i, j);
                    next(i, j) = next(i, k);
                end
            end
        end
    end
    
    fprintf('Reconstructing path\n');
    H = zeros(n, n);
    for i = 1:(n-1)
        for j = (i+1):n
            v = i;
            len = 0;
            while v ~= j
                v = next(v, j);
                len = len + 1;
            end
            H(i, j) = len;
            H(j, i) = len;
        end
    end

    D = squareform(D);
    H(1:n+1:n*n) = 0;
    H = squareform(H);
    
    fprintf('Optimizing alpha and q\n');
    log_alpha = fminbnd(@(x) floyd_warshall_MBO_search_q(D, H, exp(x), q_min, q_max), ...
                        log(alpha_min), log(alpha_max), ...
                        optimset('TolX', 0.001, 'Display', 'notify', 'MaxIter', 100));
    
    alpha = exp(log_alpha);
    fprintf('alpha optimized: alpha =  %f\n', alpha);

    [error, X, q] = floyd_warshall_MBO_search_q(D, H, alpha, q_min, q_max);
end