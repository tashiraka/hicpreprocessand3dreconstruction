function hist_frequency(cfmFile, plotFile, logPlotFile)
    cfm = load(cfmFile);
    freq = cfm(:,3);
    saveas(histogram(freq), plotFile, 'eps');

    freq = log10(freq);
    saveas(histogram(freq), logPlotFile, 'eps');
