import std.stdio, std.string, std.conv;

// bin\tchrom\tchromStart\tchromEnd\tix\tn\tsize\ttype\tbridge
// Note: all start coordinates in our database are 0-based, not 1-based.
// Note: [chromStart, chromEnd)

void main(string[] args)
{
  auto fastaFile = args[1];
  auto outFile = args[2];

  string chrom;
  ulong chromStart = 0;
  ulong chromEnd = 0;
  string[] entries;
  auto prevNFlag = false;
  ulong pos = 0;
  
  foreach (line; File(fastaFile).byLine) {
    if (line[0] == '>') {
      if (prevNFlag) {
        ++pos;
        chromEnd = pos;
        prevNFlag = false;
        entries ~= "0\t" ~ chrom ~ "\t" ~ chromStart.to!string ~
          "\t" ~ chromEnd.to!string ~ "\t0\t0\t0\t0\t0";
      }
      
      chrom = line.to!string.strip.split[0][1..$];
      pos = 0;
    }
    else {
      foreach (base; line.to!string.strip) {
        ++pos;
        if (base == 'N' && !prevNFlag) {
          chromStart = pos;
          prevNFlag = true;
        }
        else if (base != 'N' && prevNFlag) {
          chromEnd = pos;
          prevNFlag = false;
          entries ~= "0\t" ~ chrom ~ "\t" ~ chromStart.to!string ~
            "\t" ~ chromEnd.to!string ~ "\t0\t0\t0\t0\t0";
        }
      }
    }
  }

  if (prevNFlag) {
    ++pos;
    chromEnd = pos;
    prevNFlag = false;
    entries ~= "0\t" ~ chrom ~ "\t" ~ chromStart.to!string ~
      "\t" ~ chromEnd.to!string ~ "\t0\t0\t0\t0\t0";
  }

  auto fout = File(outFile, "w");
  foreach (entry; entries) {
    fout.writeln(entry);
  }
}
