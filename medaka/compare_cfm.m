function compare_cfm(mFile1, mFile2, outFile)
    fprintf('Loading cfm 1\n');
    m1 = load(mFile1);
    m1(:, 1:2) = m1(:, 1:2) + sparse(1);
    m1 = spconvert(m1);

    fprintf('Loading cfm 2\n');
    m2 = load(mFile2);
    m2(:, 1:2) = m2(:, 1:2) + sparse(1);
    m2 = spconvert(m2);
  
    fprintf('Calc elementwise ratio, removing zero and diagonals\n');
    m1 = m1 - diag(diag(m1));
    m2 = m2 - diag(diag(m2));
    nonZero = find(m1 ~= 0 & m2 ~= 0);
    r = m1(nonZero) ./ m2(nonZero);
    r = full(r);
    
    fprintf('statistics\n');
    fprintf('Mean: %f\n', mean(r));
    fprintf('Variance: %f\n', var(r));
    fprintf('Standard deviation: %f\n', std(r));
    fprintf('Max: %f\n', max(r));
    fprintf('Min: %f\n', min(r));

    dlmwrite(outFile, r, 'delimiter', '\t');