#!/bio/bin/python
# -*- coding: utf-8 -*-
from Bio import SeqIO, Restriction, Seq, SeqUtils
from argparse import RawTextHelpFormatter
import numpy as np
import argparse
import re

parser = argparse.ArgumentParser(
    description='''
  This command is tool to cut out restriction fragments from a genome.
  A site position is followed by 1-origin coordinate of the genome.
  This command returns the positions where overhangs fullfilled.
  A restriction fragment is represented as a closed interval, [start, end].
  The output format is as follows.
  
  <fragID>\t<name>\t<start>\t<end>\t<fragStart>\t<fragEnd>\t<startNoOvhg>\t<endNoOvhg>\t<gapStart>\t<gapEnd>
  
  fragID     : ID in [0, #fragments - 1]
  name       : Contig name including this fragment
  start      : Start position
  end        : End position
  fragStart  : Start position as a blunted RE fragment
  fragEnd    : End position as a blunted RE fragment
  startNoOvhg: Start position of a fragment not including overhang
  endNoOvhg  : End position of a fragment not including overhang
  gapStart   : Start of a gap included in the fragment
  gapEnd     : End of a gap included in the fragment

  An interval [start, end] is not overlapped and 
  can be used for binning a reference genome.
  A fragStart and fragEnd can be used for the estimation
  of the distribution of the Hi-C liblary length.
  The start and end are the mid position of a RE cut overhang and 
  rounded to the nearest lower integer (e.g. mid position 1.5, then returning 1). 

  For example: EcoRI cuts as 5' overhang.
  5\'-GAATTC-3\'
  5\'-CTTAAG-3\' is recognizad by a restriction enzyme EcoRI.
  EcoRI cuts this sequence to
  5\'-G  AATTC-3\'
  3\'-CTTAA  G-5\'.
  Then this command returns
  start=1 end=2 fragStart=1 fragEnd=5
  start=3 end=6 fragStart=2 fragEnd=6
  ''',
    epilog="Author: Yuichi Motai",
    formatter_class=RawTextHelpFormatter)

parser.add_argument('-v', '--version', action='version', version='version 1.0')
parser.add_argument('genome', help='fasta file containing reference genome')
parser.add_argument('RE', help='restriction enzyme name')
parser.add_argument('gap', help='gap.txt provided by UCSC Genome Browser')
parser.add_argument('out_file', help='output file')
args = parser.parse_args()


# bin\tchrom\tchromStart\tchromEnd\tix\tn\tsize\ttype\tbridge
# Note: all start coordinates in our database are 0-based, not 1-based.
# Note: [chromStart, chromEnd)
gap = {}
for line in open(args.gap):
    record = line.rstrip().split('\t')

    if record[1] in gap:
        gap[record[1]].append((int(record[2]) + 1, int(record[3])))
    else:
        gap.setdefault(record[1], [(int(record[2]) + 1, int(record[3]))])

        
restE = getattr(Restriction, args.RE)
fout = open(args.out_file, 'w')
    

# Biopython "search" method returns cut sites as 2 in the following case.
# (the just right base position at a RE cutting gap in forward strand)
#  5\'-GAATTC-3\'
#  5\'-CTTAAG-3\' is recognizad by a restriction enzyme EcoRI.
#  EcoRI cuts this sequence to
#  5\'-G  AATTC-3\'
#  3\'-CTTAA  G-5\'.
#  

def fragStartBlunt(rsite, ovhg):
    return rsite

def fragEndBlunt(rsite, ovhg):
    return rsite - 1

def fragStart5overhang(rsite, ovhg):
    return rsite

def fragEnd5overhang(rsite, ovhg):
    return rsite - 1 - ovhg

def fragStart3overhang(rsite, ovhg):
    return rsite - ovhg

def fragEnd3overhang(rsite, ovhg):
    return rsite - 1

def startBlunt(rsite, ovhg):
    return rsite

def endBlunt(rsite, ovhg):
    return rsite - 1

def start5overhang(rsite, ovhg):
    return rsite - ovhg / 2

def end5overhang(rsite, ovhg):
    return rsite - ovhg / 2 - 1

def start3overhang(rsite, ovhg):
    return rsite - ovhg / 2

def end3overhang(rsite, ovhg):
    return rsite - ovhg / 2 - 1

def startNo5overhang(rsite, ovhg):
    return rsite - ovhg

def endNo5overhang(rsite, ovhg):
    return rsite - 1

def startNo3overhang(rsite, ovhg):
    return rsite

def endNo3overhang(rsite, ovhg):
    return rsite - ovhg - 1


def fragmentation(getStart, getEnd, getFragStart, getFragEnd, getStartNoOvhg, getEndNoOvhg):
    fragment_id = 0
    for chrom in SeqIO.parse(open(args.genome, 'r'), 'fasta'):
        prev_start = 1
        prev_fragStart = 1
        prev_startNoOvhg = 1
       
        for rsite in restE.search(chrom.seq):
            start = prev_start
            end = getEnd(rsite, restE.ovhg)
            fragStart = prev_fragStart
            fragEnd = getFragEnd(rsite, restE.ovhg)
            startNoOvhg = prev_startNoOvhg
            endNoOvhg = getEndNoOvhg(rsite, restE.ovhg)

            gapStart = '*'
            gapEnd = '*'
            if chrom.name in gap:
                for g in gap[chrom.name]:
                    if(start <= g[0] and g[1] <= end):
                        gapStart = g[0]
                        gapEnd = g[1]
                        break
            
            fout.write(str(fragment_id) + '\t' + chrom.name + '\t' + \
                       str(start) + '\t' + str(end) + '\t' + \
                       str(fragStart) + '\t' + str(fragEnd) + '\t' +\
                       str(startNoOvhg) + '\t' + str(endNoOvhg) + '\t' +\
                       str(gapStart) + '\t' + str(gapEnd) + '\n')

            prev_start = getStart(rsite, restE.ovhg)
            prev_fragStart = getFragStart(rsite, restE.ovhg)
            prev_startNoOvhg = getStartNoOvhg(rsite, restE.ovhg)
            fragment_id += 1
            
        else:
            start = prev_start
            end = len(chrom.seq)
            fragStart = prev_fragStart
            fragEnd = len(chrom.seq)
            startNoOvhg = prev_startNoOvhg
            endNoOvhg = len(chrom.seq)

            gapStart = '*'
            gapEnd = '*'
            if chrom.name in gap:
                for g in gap[chrom.name]:
                    if(start <= g[0] and g[1] <= end):
                        gapStart = g[0]
                        gapEnd = g[1]
                        break

            fout.write(str(fragment_id) + '\t' + chrom.name + '\t' + \
                       str(start) + '\t' + str(end) + '\t' + \
                       str(fragStart) + '\t' + str(fragEnd) + '\t' +\
                       str(startNoOvhg) + '\t' + str(endNoOvhg) + '\t' +\
                       str(gapStart) + '\t' + str(gapEnd) + '\n')
            

            fragment_id += 1
            
    
if(restE.is_blunt()):
    fragmentation(startBlunt, endBlunt, fragStartBlunt, fragEndBlunt, startBlunt, endBlunt)
elif(restE.is_5overhang()):
    fragmentation(start5overhang, end5overhang, fragStart5overhang, fragEnd5overhang, startNo5overhang, endNo5overhang) 
elif(restE.is_3overhag()):
    fragmentation(start3overhang, end3overhang, fragStart3overhang, fragEnd3overhang, startNo3overhang, endNo3overhang)
else:
    print 'ERROR: The type of overhang is \'unkown\''    
    quit
            
