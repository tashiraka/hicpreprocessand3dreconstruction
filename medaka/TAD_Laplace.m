function TAD_Laplace(H0File, H0nFile, binInfoFile, outFile, sig0, ms0)
% TADs extraction via Laplacian segmentation
%
%
% Original script probably is mistaken because of
% Toeplits normalization after log transformation.
%    log(H) ./ toeplits(log(H))
% Correctly as the original paper, 
%    H ./ toepolits(H)
% I used log transformation of above.
%    log(H ./ toepolits(H))
%
%
%
% --- input ---
% H0File:  Input sparse H-iC matrix, intra-chromosome
% H0nFile:  Input sparse Toeplitz normalizatied Hi-C matrix
% sig0:    Algebraic connectivity threshold
% ms0:     Threshold of region size: we donot split a region smaller
%          than ms0
% --- output ---
% chr_name<TAB>TAD_boundary_intra-chromosomal_index

% Reference:
% Hierachicial identification of topological domain via graph
% Laplacian
% J. Chen, A. O. Hero, I. Rajapakse
%
% Implemented by
% Jie Chen
% And modified
% Yuichi Moati
% http://www.jie-chen.com
% dr.jie.chen@ieee.org

    %% Default parameters
    if nargin < 6, ms0 = 3; end
    if nargin < 5, sig0 = 0.8; end

    
    %% Load Hi-C matrix
    fprintf('Load Hi-C matrix\n');
    H0 = load(H0File);
    H0(:, 1:2) = H0(:, 1:2) + sparse(1);
    H0 = spconvert(H0);
    H0 = H0 + H0.' - diag(diag(H0));

    
    %% Load Toeplitz normalized Hi-C matrix
    fprintf('Load Toeplitz normalized Hi-C matrix\n');
    H0n = load(H0nFile);
    H0n(:, 1:2) = H0n(:, 1:2) + sparse(1);
    H0n = spconvert(H0n);
    H0n = H0n + H0n.' - diag(diag(H0n));

    
    %% Load bin info
    fprintf('Load bin info\n');
    fid = fopen(binInfoFile);
    binInfo = textscan(fid, '%d %s %d %d %s');
    fclose(fid);
    binInfo{1} = binInfo{1} + 1;
    contigs = unique(binInfo{2}).';


    fprintf('Call TAD for each contigs\n');
    fid = fopen(outFile, 'w');

    for contig = contigs
        fprintf('%s\n', char(contig));
        %% extract intra region
        fprintf('extract intra region\n');
        intraIDs = binInfo{1}(strcmp(binInfo{2}, contig));
        H = H0(intraIDs, intraIDs);
        Hn = H0n(intraIDs, intraIDs);
        
        %% Remove unmappable region
        fprintf('Remove unmappable region\n');
        idx0 = sum(H)~=0;
        idx0n = sum(Hn)~=0;
        if ~isequal(idx0, idx0n)
            error('H and Hn has different zero rows/cols.');
        end
        
        H = H(idx0, idx0);
        Hn = Hn(idx0n, idx0n);
        H = full(H);
        Hn = full(Hn);

        if size(H, 1) < 2
            fprintf('#Mappable rows < 2 and skip\n');
            continue;
        end

        
        %% Log transformation
        fprintf('Log transformation\n');
        % Original normalized sample data (H0 >0)
        % max = 3957, min = 0.304
        % Original log(H0) >0
        % max = 8.28, min = 0.693 
        % Original (wrong) Toeplitz normalized data
        % max = 5.9873, min = 0.000143
        %
        % Medaka 5k KR-normalized st10 ~ st12 whole genome
        % max = 0.775, min = 0.00234
        % logmax = -0.2549, logmin = -6.0576
        % Medaka 5k Toeplitz-normalized st10 ~ st12 whole genome
        % max = 28761.2, min = 0.004838
        % logax = 10.2668, logmin = -5.3313

        % Originally, min(log(H), 6), this is about 6 / 8.28 = 0.725
        % So I can use H = min(log(H), max(max(log(H)) * 0.725).
        H = log(H);
        % Shift to be > 0
        H = H - min(min(H(H ~= -inf)));
        % Process -inf, because log(0) = -inf
        H(H == -inf) = -1;
        % Shift to be positive
        H = H + 1.001;

        %Hn = log(Hn);
        %Hn = Hn - min(min(Hn(Hn ~= -inf)));
        %Hn(Hn == -inf) = -1;
        %Hn = Hn + 1.001;
        
        %% Remove the diagonal
        fprintf('Remove the diagonal\n');
        H = H - diag(diag(H));
        Hn = Hn - diag(diag(Hn));
        L = size(H, 1);
        
        %% Splitting on Toeplitz normalized matrix
        fprintf('Splitting on Toeplitz normalized matrix\n');
        [Fdv, Fdvl] = Fdvectors(Hn, 0);   % Fiedler vector

        
        % Positions of starting domain
        fprintf('Positions of starting domain\n');
        Pos = [1; find(sign(Fdv(2:end)) - sign(Fdv(1:end-1))~=0) + 1; L]; 

        
        %% Recursive splitting
        fprintf('Recursive splitting\n');
        spa = zeros(L,1);
        spa(Pos)=1;

        
        for i = 1 : length(Pos)-1     
            % Block range
            idx = Pos(i):Pos(i+1)-1;
            
            % If block size <= ms0, we will not split again
            if length(idx) > ms0
                % Sub-matrix
                SH = H(idx, idx);
                % Fiedler number and vector
                [Fdv,Fdvl] = Fdvectors(SH);
                
                % If the Fiedler number of the block is small
                if Fdvl <= sig0
                    % Continue to split
                    sp = SubSplit(SH, sig0, ms0);
                    % Mark boundaries
                    spa(Pos(i) + find(sp>0) - 1) = 1;
                end
                
            end
            
        end

        
        fprintf('Interpolate unmapped region\n');
        posn = zeros(length(intraIDs), 1);
        posn(idx0) = spa;
        posn = find(posn > 0);
        % TAD_boundaries = posn;

        
        fprintf('Re-calculate Fiedler number for each TADs\n');
        repos = find(spa > 0);
        Fdvls = zeros(length(posn) - 1, 1);

        % length(posn) == length(repos);
        for i = 1:length(posn)-1
            idx = repos(i):repos(i+1)-1;
            % If block size <= ms0, we will not split again
            if length(idx) > ms0
                % Sub-matrix
                SH = H(idx, idx);
            
                [Fdv,Fdvl] = Fdvectors(SH);

                Fdvls(i) = Fdvl;
            end
        end
        
        
        %% Output
        fprintf('Output\n');
        for i = 1:length(posn)-1
            if Fdvls(i)>sig0+1e-5 & posn(i+1)-posn(i)>=ms0
                fprintf(fid, '%s\t%d\t%d\t%f\n', char(contig), ...
                        posn(i), posn(i + 1) - 1, Fdvls(i));
            end
        end
    end

    fclose(fid);
end


%% ============= Sub-functions =============

%% Sub-function 1: Recusive split
function sp = SubSplit(Ho,sig0, ms0)
% Recursively splitting a connection matrix via Fiedler value and
% vector

% Default threshold
    if nargin < 3
        ms0 = 3;
        if nargin<2
            sig0 = 0.8;
        end
    end


    % Fiedler vector
    [Fdv,Fdvl]=Fdvectors(Ho);
    % Position of sign change (sub-block starting)
    Pos = [1;find(sign(Fdv(2:end))-sign(Fdv(1:end-1))~=0)+1; ...
           size(Ho,1)];


    sp = zeros(size(Ho,1),1);
    sp(Pos) = 1;
    % If Fiedler value is high enough
    if  Fdvl > sig0+1e-5        %   +1e-5 for numerical stability
        sp = 1;
        return;
    end


    % For each sub-block
    for i = 1 :  length(Pos)-1
        % Range
        idx = Pos(i):Pos(i+1)-1;

        % minimum sub-block size
        if length(idx)>ms0
            % Continue to split
            sp1 = SubSplit(Ho(idx,idx),sig0,ms0);
            % Mark bock boundary
            sp(Pos(i)+find(sp1>0)-1)=1;
        end
    end
end


%% Fiedler vector calculation
function [Fdv,Fdvl] = Fdvectors(H,NN)
    H = (H +H')/2;
    N = size(H,1);

    
    if nargin < 2
        NN = 0;
    end
    dgr = sum(H);
    dgr(dgr==0)=1;
    DN = diag(1./sqrt(dgr));
    L = DN*(diag(dgr)-H)*DN;

    
    if NN == 1
        L = diag(dgr)-H;
    end
    
    L = (L+L')/2;
    [V,D] = eigs(L,2,'SA');  % Or we can just use svd / eig
    Fdv = V(:,end);
    Fdvl =(D(end,end));
    %

    if NN == 1;
        Fdvl = Fdvl/size(L,1)^0.3;
    end
end
