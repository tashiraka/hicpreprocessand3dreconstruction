import std.stdio, std.conv, std.string, std.typecons, std.algorithm, std.math;


/*
bool isDup(DSPair ds1, DSPair ds2)
{
  auto dupMargin = 4; // 1 is exactly same.
  return abs(ds1.pos1 - ds2.pos1) <= 4  && abs(ds1.pos2 - ds2.pos2) <= 4 ;
}
*/

void main(string[] args)
{
  auto DSFile = args[1];
  auto perPosFile = args[2];
  auto perFragFile = args[3];

  DSPair[][string][string] ff, fr, rr;
  
  // qname1 rname1 strand1 pos1 fragID1 mapq1 mappedLen1 DNALen1        \
  // qname2 rname2 strand2 pos2 fragID2 mapq2 mappedLen2 DNALen2
  foreach (line; File(DSFile).byLine) {
    auto fields = line.to!string.strip.split;

    auto rname1 = fields[1];
    auto rname2 = fields[9];
    auto str1 = fields[2] == "+" ? Strand.fwd : Strand.rev;
    auto str2 = fields[10] == "+" ? Strand.fwd : Strand.rev;
    auto pos1 = fields[3].to!uint;
    auto pos2 = fields[11].to!uint;
    
    // DS pair must have tha order from "+" to "-".
    if (str1 == Strand.rev && str2 == Strand.fwd) {
      fields = fields[8..16] ~ fields[0..8];
      str1 = Strand.fwd;
      str2 = Strand.rev;
    }
    
    if (str1 == Strand.fwd) {
      if (str2 == Strand.fwd) {
        fields.sortRname(rname1, rname2, pos1, pos2);
        ff[rname1][rname2] ~= new DSPair(fields, line.to!string);
      }
      else
        fr[rname1][rname2] ~= new DSPair(fields, line.to!string);
    }
    else {
      fields.sortRname(rname1, rname2, pos1, pos2);
      rr[rname1][rname2] ~= new DSPair(fields, line.to!string);
    }
  }

  uint[uint][Strand][string] dupPerPos;
  uint[uint][Strand][string] notDupPerPos;
  uint[uint][Strand][string] dupPerFrag;
  uint[uint][Strand][string] notDupPerFrag;
  
  sortDup(ff, dupPerPos, notDupPerPos, dupPerFrag, notDupPerFrag);
  sortDup(fr, dupPerPos, notDupPerPos, dupPerFrag, notDupPerFrag);
  sortDup(rr, dupPerPos, notDupPerPos, dupPerFrag, notDupPerFrag);

  auto foutPerPos = File(perPosFile, "w");
  
  foreach (rname; dupPerPos.byKey) {
    foreach (str; dupPerPos[rname].byKey) {
      foreach (pos; dupPerPos[rname][str].byKey) {
        auto dup = dupPerPos[rname][str][pos];
        auto notDup = notDupPerPos[rname][str][pos];
        foutPerPos.writeln(notDup, "\t", dup, "\t", notDup.to!double / dup);
      }
    }
  }

  auto foutPerFrag = File(perFragFile, "w");
  
  foreach (rname; dupPerFrag.byKey) {
    foreach (str; dupPerFrag[rname].byKey) {
      foreach (fragID; dupPerFrag[rname][str].byKey) {
        auto dup = dupPerFrag[rname][str][fragID];
        auto notDup = notDupPerFrag[rname][str][fragID];
        foutPerFrag.writeln(notDup, "\t", dup, "\t", notDup.to!double / dup);
      }
    }
  }
}


DSPair[] sortPosition(ref DSPair[] pairs)
{
  pairs.sort!((a,b) => a.pos1 < b.pos1);
  pairs.sort!((a,b) => a.pos2 < b.pos2, SwapStrategy.stable);
  return pairs;
}



void sortDup(DSPair[][string][string] hash,
             ref uint[uint][Strand][string] dupPerPos,
             ref uint[uint][Strand][string] notDupPerPos,
             ref uint[uint][Strand][string] dupPerFrag,
             ref uint[uint][Strand][string] notDupPerFrag)
{  
  foreach (rname1; hash.byKey) {
    foreach (rname2; hash[rname1].byKey) {
      auto pairs = hash[rname1][rname2];

      if (pairs.length == 1) {
        dupPerPos[rname1][pairs[0].str1][pairs[0].pos1]++;      
        dupPerPos[rname2][pairs[0].str2][pairs[0].pos2]++;
        notDupPerPos[rname1][pairs[0].str1][pairs[0].pos1]++;
        notDupPerPos[rname2][pairs[0].str2][pairs[0].pos2]++;

        dupPerFrag[rname1][pairs[0].str1][pairs[0].fragID1]++;      
        dupPerFrag[rname2][pairs[0].str2][pairs[0].fragID2]++;
        notDupPerFrag[rname1][pairs[0].str1][pairs[0].fragID1]++;
        notDupPerFrag[rname2][pairs[0].str2][pairs[0].fragID2]++;
      }
      else {
        // Already ordered by other fields from position
        // Sort by paired position
        pairs.sortPosition;
        pairs.sortPosition;
      
        DSPair[] dupPairs = [pairs[0]];
        
        foreach (pair; pairs[1..$]) {
          if (dupPairs[$-1].pos1 != pair.pos1 ||
              dupPairs[$-1].pos2 != pair.pos2) {
            dupPerPos[rname1][dupPairs[0].str1][dupPairs[0].pos1] += dupPairs.length;
            dupPerPos[rname2][dupPairs[0].str2][dupPairs[0].pos2] += dupPairs.length;
            notDupPerPos[rname1][dupPairs[0].str1][dupPairs[0].pos1]++;
            notDupPerPos[rname2][dupPairs[0].str2][dupPairs[0].pos2]++;

            dupPerFrag[rname1][dupPairs[0].str1][dupPairs[0].fragID1] += dupPairs.length;      
            dupPerFrag[rname2][dupPairs[0].str2][dupPairs[0].fragID2] += dupPairs.length;
            notDupPerFrag[rname1][dupPairs[0].str1][dupPairs[0].fragID1]++;
            notDupPerFrag[rname2][dupPairs[0].str2][dupPairs[0].fragID2]++;

            dupPairs = [];
          }
          
          dupPairs ~= pair;
        }

        dupPerPos[rname1][dupPairs[0].str1][dupPairs[0].pos1] += dupPairs.length;   
        dupPerPos[rname1][dupPairs[0].str2][dupPairs[0].pos2] += dupPairs.length;
        notDupPerPos[rname1][dupPairs[0].str1][dupPairs[0].pos1]++;
        notDupPerPos[rname1][dupPairs[0].str2][dupPairs[0].pos2]++;
        
        dupPerFrag[rname1][dupPairs[0].str1][dupPairs[0].fragID1] += dupPairs.length;      
        dupPerFrag[rname2][dupPairs[0].str2][dupPairs[0].fragID2] += dupPairs.length;
        notDupPerFrag[rname1][dupPairs[0].str1][dupPairs[0].fragID1]++;
        notDupPerFrag[rname2][dupPairs[0].str2][dupPairs[0].fragID2]++;
      }
    }      
  }
}


DSPair maxMapq(DSPair[] pairs)
{
  if (pairs.length == 1) return pairs[0];
                                           
  DSPair maxPair = pairs[0];
  
  foreach (pair; pairs[1..$])
    if (maxPair.mapqSum < pair.mapqSum)
      maxPair = pair;
  
  return maxPair;
  
}

void sortRname(ref string[] fields, ref string rname1, ref string rname2,
               uint pos1, uint pos2)
{
  if (rname1 > rname2 || (rname1 == rname2 && pos1 > pos2)) {
    fields = fields[8..16] ~ fields[0..8];
    auto tmp = rname1;
    rname1 = rname2;
    rname2 = rname1;
  }
}


enum Strand: char {fwd=0, rev=1}

class DSPair
{
  uint pos1, pos2, mapq1, mapq2, fragID1, fragID2;
  Strand str1, str2;
  string line;
  
  this (string[] fields, string l)
  {
    if (fields.length != 16) throw new Exception("Fields.length != 16");

    auto s1 = fields[2] == "+" ? Strand.fwd : Strand.rev;
    auto s2 = fields[10] == "+" ? Strand.fwd : Strand.rev;

    // DS pair must have tha order from "+" to "-".
    if (s1 == Strand.rev && s2 == Strand.fwd)
      throw new Exception("Rev to Fwd pair");

    str1 = s1;
    str2 = s2;
    
    // qname1 rname1 strand1 pos1 fragID1 mapq1 mappedLen1 DNALen1      \
    // qname2 rname2 strand2 pos2 fragID2 mapq2 mappedLen2 DNALen2
    pos1 = fields[3].to!uint;
    pos2 = fields[11].to!uint;
    mapq1 = fields[5].to!uint;
    mapq2 = fields[13].to!uint;
    fragID1 = fields[4].to!uint;
    fragID2 = fields[12].to!uint;

    line = l;
  }

  override string toString()
  {
    return line;
  }

  uint mapqSum()
  {
    return mapq1 + mapq2;
  }
}
