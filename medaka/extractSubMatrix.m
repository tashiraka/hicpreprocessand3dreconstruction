function extractSubMatrix(CFMFilename, ssDepthFile, ...
                          binInfoFile, contigNameFile, ...
                          outFile, outSSFile, outBinInfoFile)
% CFM is upper or lower triangle.
% CFM contains the entry of (matrix size, matrix size, value). So
% OK only spconvert.
% binInfoFile fields: binID contigName startCoord endCoord gap
% extract chr1-chr2 matrix

CFM = load(CFMFilename);
CFM(:,1:2) = CFM(:,1:2) + 1;
CFM = spconvert(CFM);

ssDepth = load(ssDepthFile);

fid = fopen(binInfoFile);
binInfo = textscan(fid, '%d %s %d %d %s');
fclose(fid);
binInfo{1} = binInfo{1} + 1;
%contigs = unique(binInfo{2}).';

fid = fopen(contigNameFile);
contigs = textscan(fid, '%s');
fclose(fid);

% Bin IDs corresponding to contigs
IDs = binInfo{1}(ismember(binInfo{2}, contigs{1}));
subMatrixSize = size(IDs, 1);

subCFM = CFM(IDs, IDs);
[i, j, val] = find(subCFM);
i = i - 1;
j = j - 1;
fid = fopen(outFile, 'w');
fprintf(fid, '%d\t%d\t%f\n', vertcat(i', j', val'));
if subCFM(subMatrixSize, subMatrixSize) == 0
    fprintf(fid, '%d\t%d\t%f\n', subMatrixSize - 1, subMatrixSize - 1, 0);
end
fclose(fid);

subSSDepth = ssDepth(IDs);

fid = fopen(outSSFile, 'w');
fprintf(fid, '%f\n', subSSDepth);
fclose(fid);

binInfo{1} = binInfo{1} - 1;
for id = 1:size(binInfo, 2)
    subBinInfo{id} = binInfo{id}(IDs);
end

% Assume binIDs are sorted
%subBinInfo{1} = 1:subMatrixSize;

fid = fopen(outBinInfoFile, 'w');
for id = 1:size(subBinInfo{1}, 1)
    fprintf(fid, '%d\t%s\t%d\t%d\n', subBinInfo{1}(id), cell2mat(subBinInfo{2}(id)),...
            subBinInfo{3}(id), subBinInfo{4}(id));
end
fclose(fid);
exit();
end