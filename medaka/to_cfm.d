// CFM == Contact Frequency Matrix
// upper triangle

import std.stdio, std.conv, std.string, std.algorithm, std.typecons,
       std.array, std.getopt;

/*
  fragID     : ID in [0, #fragments - 1]
  name       : Contig name including this fragment
  start      : Start position
  end        : End position
  fragStart  : Start position as a blunted RE fragment
  fragEnd    : End position as a blunted RE fragment
  startNoOvhg: Start position of a fragment not including overhang
  endNoOvhg  : End position of a fragment not including overhang
  gapStart   : Start of a gap included in the fragment
  gapEnd     : End of a gap included in the fragment
*/

// qname1 rname1 strand1 pos1 fragID1 mapq1 mappedLen1 DNALen1          \
// qname2 rname2 strand2 pos2 fragID2 mapq2 mappedLen2 DNALen2

void main(string[] args)
{
  // ds=ds1.txt,ds2.txt,ds3.txt ss=ss1.txt,ss2.txt,ss3.txt
  string[] dsFiles;
  string[] ssFiles;

  arraySep = ",";

  auto helpInformation = getopt(args, "ds", &dsFiles, "ss", &ssFiles);
  if (helpInformation.helpWanted)
    defaultGetoptPrinter("Sep is ,",
                         helpInformation.options);

  writeln("Reading files");
  writeln(dsFiles);
  writeln(ssFiles);
  
  auto fragFile = args[1];
  auto binSize = args[2].to!uint;
  auto outCfmFile = args[3];
  auto outDepthFile = args[4];
  auto outBinInfoFile = args[5];

  // Fragments
  auto frags = readFrag(fragFile);

  Tuple!(uint[], "fragEndIDToBinID",
         Tuple!(uint, string, uint, uint, bool)[], "binInfo")
    idInfo = mapFragEndIDToBinID(frags, binSize);

  auto foutBinInfo = File(outBinInfoFile, "w");
  foreach (row; idInfo.binInfo) {
    foreach (i, elem; row) {
      if (i < row.length - 1) foutBinInfo.write(elem, "\t");
      else foutBinInfo.write(elem);
    }
    foutBinInfo.writeln;
  }
  
  dsToCfm(dsFiles, outCfmFile, frags, idInfo.fragEndIDToBinID, idInfo.binInfo);
  ssToDepth(ssFiles, outDepthFile, frags, idInfo.fragEndIDToBinID, idInfo.binInfo);
}


void dsToCfm(string[] dsFiles, string outFile,
             Fragment[] frags, uint[] fragEndIDToBinID,
             Tuple!(uint, string, uint, uint, bool)[] binInfo)
{
  uint[Tuple!(uint, uint)] dsCfm;
  foreach (dsFile; dsFiles)
    readDS(File(dsFile), frags, dsCfm);
  
  auto binnedDSCfm = binningDSByCoord(dsCfm, fragEndIDToBinID, binInfo);
  
  auto foutCfm = File(outFile, "w");
  foreach(key; binnedDSCfm.byKey)
    foutCfm.writeln(key[0], "\t", key[1], "\t", binnedDSCfm[key]);
}


void ssToDepth(string[] ssFiles, string outFile,
               Fragment[] frags, uint[] fragEndIDToBinID,
               Tuple!(uint, string, uint, uint, bool)[] binInfo)
{
  auto ssDepth = new uint[](2 * frags.length);
  foreach (ssFile; ssFiles)
    readSS(File(ssFile), frags, ssDepth);
  
  auto binnedSSDepth = binningSSByCoord(ssDepth, fragEndIDToBinID, binInfo);

  auto foutDepth = File(outFile, "w");
  foreach(d; binnedSSDepth) foutDepth.writeln(d);
}

// The format of binInfo: [binID, contigName, startCoord, endCoord, hasGap]
// Assign binID to the fragments such as
// fragStart or fragEnd in [binStart, binEnd].
Tuple!(uint[], "fragEndIDToBinID", Tuple!(uint, string, uint, uint, bool)[], "binInfo")
mapFragEndIDToBinID(Fragment[] frags, uint binSize)
{
  // binInfo
  Tuple!(uint, string, uint, uint, bool)[] binInfo;
  auto fragEndIDToBinID = new uint[](frags.length * 2);
  // both frag ends: Evens are frag.start and  Odds are frag.end.
  uint count=0, binID=0, binStart=1, binEnd=binSize, contigEnd=0;
  string contigName = frags[0].rname;
  bool hasGap = false;
  
  foreach(i, frag; frags) {
    if(contigName != frag.rname) { // Not included the current frag
      // Go to the next contig
      binInfo ~= tuple(binID, contigName, binStart, contigEnd, hasGap);
      count = 0;
      binID++;
      contigName = frag.rname;
      binStart = 1;
      binEnd = binSize;
      hasGap = frag.hasGap;
    }

    if (binEnd < frag.start) { // Not included the current frag
      // Go to the next bin.
      binInfo ~= tuple(binID, contigName, binStart, binEnd, hasGap);
      count++;
      binID++;
      binStart = binSize * count + 1;
      binEnd = binSize * (count + 1);
      hasGap = frag.hasGap;
    }
    // frag.start is in the current bin.
    fragEndIDToBinID[2 * i] = binID;

    if (frag.hasGap) hasGap = true;
    
    while (binEnd < frag.end) { // included the current frag
      // Go to the next bin.
      binInfo ~= tuple(binID, contigName, binStart, binEnd, hasGap);
      count++;
      binID++;
      binStart = binSize * count + 1;
      binEnd = binSize * (count + 1);
    }    
    // frag.end is in the current bin.
    fragEndIDToBinID[2 * i + 1] = binID;

    contigEnd = frag.end;
  }

  binInfo ~= tuple(binID, contigName, binStart, contigEnd, hasGap);

  return Tuple!(uint[], "fragEndIDToBinID",
                Tuple!(uint, string, uint, uint, bool)[], "binInfo")
         (fragEndIDToBinID, binInfo);
}


uint[Tuple!(uint, uint)] binningDSByCoord(uint[Tuple!(uint, uint)] spCfm,
                                          uint[] fragEndIDToBinID,
                                          Tuple!(uint, string, uint, uint, bool)[] binInfo)
{
  // binned sparse contact frequency matrix
  uint[Tuple!(uint, uint)] binnedSpCfm;
  
  foreach (key; spCfm.byKey) {
    auto bin1 = fragEndIDToBinID[key[0]];
    auto bin2 = fragEndIDToBinID[key[1]];
    if (!binInfo[bin1][4] && !binInfo[bin2][4]) {
      if (bin1 < bin2) {
        auto tmp = bin1;
        bin1 = bin2;
        bin2 = tmp;
      }
      binnedSpCfm[tuple(bin1, bin2)] += spCfm[key];
    }
  }

  return binnedSpCfm;
}


uint[] binningSSByCoord(uint[] depth, uint[] fragEndIDToBinID,
                        Tuple!(uint, string, uint, uint, bool)[] binInfo)
{
  auto binnedDepth = new uint[](fragEndIDToBinID[$-1] + 1);

  foreach (i, d; depth) {
    auto bin = fragEndIDToBinID[i];
    if (!binInfo[bin][4]) binnedDepth[bin] += d;
  }
  
  return binnedDepth;
}


class Fragment
{
  uint id, start, end;
  string rname;
  bool hasGap;
 
  this(uint id_, string rname_, uint start_, uint end_, bool g) {
    id=id_; rname=rname_; start=start_; end=end_; hasGap = g;
  }
}


Fragment[] readFrag(string file)
{
  auto app = appender!(Fragment[]);
  
  foreach(line; File(file, "r").byLine) {
    if(line != null) {
      auto fields = line.to!string.split("\t");
      app.put(new Fragment(fields[0].to!uint, fields[1],
                           fields[2].to!uint, fields[3].to!uint,
                           fields[8] == "*" ? false : true));
    }
  }
  
  return app.data;
}


enum Strand: char {fwd=0, rev=1}


uint[Tuple!(uint, uint)] readDS(File fin, Fragment[] frags,
                                ref uint[Tuple!(uint, uint)] spCfm)
{ 
  foreach (line; fin.byLine) {
    auto fields = line.strip().split('\t');
    auto fragID1 = fields[4].to!uint;
    auto fragID2 = fields[12].to!uint;
    auto str1 = (fields[2] == "+" ? Strand.fwd : Strand.rev);
    auto str2 = (fields[10] == "+" ? Strand.fwd : Strand.rev);

    uint fragEndID1, fragEndID2;
    
    if (str1 == Strand.fwd) fragEndID1 = 2 * fragID1 + 1; // frag end
    else fragEndID1 = 2 * fragID1; // frag start

    if (str2 == Strand.fwd) fragEndID2 = 2 * fragID2 + 1; // frag end
    else fragEndID2 = 2 * fragID2; // frag start

    spCfm[tuple(min(fragEndID1, fragEndID2),
                    max(fragEndID1, fragEndID2))] += 1;
  }

  auto maxID = (2 * frags.length - 1).to!uint;
  
  if (tuple(maxID, maxID) !in spCfm)
    spCfm[tuple(maxID, maxID)] = 0;

  return spCfm;
}


uint[] readSS(File fin, Fragment[] frags, ref uint[] depth)
{
  foreach (line; fin.byLine) {
    auto fields = line.strip().split('\t');
    auto fragID1 = fields[4].to!uint;
    auto str1 = (fields[2] == "+" ? Strand.fwd : Strand.rev);

    uint fragEndID1;
    
    if (str1 == Strand.fwd) fragEndID1 = 2 * fragID1 + 1; // frag end
    else fragEndID1 = 2 * fragID1; // frag start

    depth[fragEndID1] += 1;
  }

  return depth;
}
