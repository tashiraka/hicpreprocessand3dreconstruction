function [error, X, q] = floyd_warshall_MBO_search_q(D, H, alpha, ...
                                                     q_min, q_max)
    fprintf('Optimizing q\n');
    Da = D.^alpha;
    log_q = fminbnd(@(x) floyd_warshall_MBO(Da, H, alpha, exp(x)), ...
                    log(q_min), log(q_max), ...
                    optimset('TolX', 0.001, 'Display', 'notify', 'MaxIter', 100));
    q = exp(log_q);
    fprintf('q optimized: q = %f\n', q);
    
    [error, X] = floyd_warshall_MBO(Da, H, alpha, q);        
end