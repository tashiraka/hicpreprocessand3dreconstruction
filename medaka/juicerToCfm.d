// cfm == Contact Frequency Matrix
// lower triangle

import std.stdio, std.conv, std.string, std.algorithm, std.typecons,
       std.array;


/* Juicer long form:     
 * str1 chr1 pos1 frag1 str2 chr2 pos2 frag2 mapq1 cigar1 seq1 mapq2 cigar2 seq2 rname1 rname2
 *
 * Out format
 * bin1 bin2 freq
 */

void main(string[] args)
{
  auto inFile = args[1]; // juicer long form
  auto chromSizeFile = args[2];
  auto binSize = args[3].to!uint; // bp
  auto mapqThreshold = args[4].to!uint;
  auto outCfmFile = args[5];


  uint[string] cumlativeStartPos;
  auto prev = 0;
  foreach (line; File(chromSizeFile).byLineCopy) {
    auto fields = line.strip.split;
    auto chr = fields[0];
    auto size = fields[1].to!uint;

    cumlativeStartPos[chr] = prev;
    prev += size - 1;
  }

  
  uint[Tuple!(uint, uint)] cfm;

  File fin;
  if (inFile == "stdin") fin = stdin;
  else fin = File(inFile);
  
  foreach (line; fin.byLineCopy) {
    auto fields = line.strip.split;
    auto chr1 = fields[1];
    auto chr2 = fields[5];
    auto bp1 = fields[2].to!uint;
    auto bp2 = fields[6].to!uint;
    auto frag1 = fields[3];
    auto frag2 = fields[7];
    auto mapq1 = fields[8].to!uint;
    auto mapq2 = fields[11].to!uint;

    // Filters
    // only increment if not intraFragment and passes the mapq threshold    
    auto mapq = min(mapq1, mapq2);
    if (mapq < mapqThreshold || (chr1 == chr2 && frag1 == frag2)) continue;

    auto pos1 = cumlativeStartPos[chr1] + bp1;
    auto pos2 = cumlativeStartPos[chr2] + bp2;
    uint xBin = pos1 / binSize;
    uint yBin = pos2 / binSize;

    // I'll store lower diagonal only
    auto b1 = min(xBin, xBin);
    auto b2 = max(xBin, yBin);
    xBin = b1;
    yBin = b2;
    cfm[tuple(xBin, yBin)]++;
  }

  fin.close;

  File fout;
  if (outCfmFile == "stdout") fout = stdout;
  else fout = File(outCfmFile, "w");
  foreach (bin; cfm.byKey)
    fout.writeln(bin[0], "\t", bin[1], "\t", cfm[bin]);
}
