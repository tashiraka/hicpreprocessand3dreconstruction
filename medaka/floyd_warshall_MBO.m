function [error, X] = floyd_warshall_MBO(D, H, alpha, q)    
    fprintf('Finding coordinates using MBO\n');
    Hq = H.^(-q);
    options.solver = 'TR';
    options.tolcost = 1e-12;
    options.tolgradnorm = 1e-7;
    options.Delta0 = 10;
    options.Delta_bar = 10*options.Delta0;
    [X, info] = MBO(D, Hq, options);

    fprintf('Scaling to best fit original original distance matrix for 1-norm\n');
    DX = pdist(X);
    [scale, error] = fminbnd(@(x) norm(D - exp(x) * DX, 1), log(0.001), log(1000), ...
                             optimset('TolX', 0.001, 'Display', 'notify', 'MaxIter', 100));
    X = scale * X;
end