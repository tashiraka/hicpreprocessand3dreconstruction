import std.stdio, std.conv, std.string, std.typecons, std.math, std.algorithm;


void main(string[] args)
{
  auto cfmFile = args[1];
  auto binInfoFile = args[2];
  auto snRatio = args[3].to!double;
  auto outFile = args[4];
  auto expectedFile = args[5];

  normalize1d(cfmFile, binInfoFile, snRatio, outFile, expectedFile);
}


void normalize1d(string cfmFile, string binInfoFile, double snRatio,
                  string outFile, string expectedFile)
{
  auto binInfo = readBinInfo(binInfoFile);
  writeln("Calculating intervals");
  auto intraIntervals = splitIntraAndGap(binInfo);
  writeln("Calculating expected");
  auto expected = calcExpected(cfmFile, binInfo, intraIntervals);
  auto threshold = snRatio ^^ 2;

  writeln("Calculating obs/exp");
  auto expectedFout = File(expectedFile, "w");
  foreach (d, e; expected.expected) {
    // 1D-distance expected_value N
    expectedFout.writeln(d, "\t", e, "\t", expected.N[d], "\t", expected.div[d]);
  }
  
  auto fout = File(outFile, "w");
  
  foreach (line; File(cfmFile).byLineCopy) {
    auto fields = line.strip.split;
    auto row = fields[0].to!uint;
    auto col = fields[1].to!uint;
    auto val = fields[2].to!double;

    foreach (i, interval; intraIntervals) {
      if (interval.start <= row && row <= interval.end) {
        if (intraIntervals[i].start <= col &&
            col <= intraIntervals[i].end) {
          auto d = abs(col - row);
          
          if (expected.N[d] >= threshold) { // ~ Poisson process
            if (expected.expected[d] == 0)
              fout.writeln(row, "\t", col, "\t", 0);
            else
              fout.writeln(row, "\t", col, "\t", val / expected.expected[d]);
          }
        }
        
        break;
      }
    }
  }  
}


Tuple!(double[], "expected", uint[], "N", double[], "div")
calcExpected(string cfmFile, BinInfo[] binInfo,
             Tuple!(string, "rname",
                    uint, "start",
                    uint, "end")[] intraIntervals)
{
  // Determine maximum 1D distance
  auto maxD = intraIntervals.map!(x => x.end - x.start).reduce!max;

  auto expected = new double[](maxD + 1);
  auto N = new uint[](maxD + 1); // # non-zero elements
  auto depth = new double[](binInfo.length);

  expected.fill(0);
  N.fill(0);
  depth.fill(0);
  
  foreach (line; File(cfmFile).byLineCopy) {
    auto fields = line.strip.split;
    auto row = fields[0].to!uint;
    auto col = fields[1].to!uint;
    auto val = fields[2].to!double;

    depth[row] += val; // Input is upper triangle matrix
    depth[col] += val;
    
    foreach (i, interval; intraIntervals) {
      if (interval.start <= row && row <= interval.end) {
        if (intraIntervals[i].start <= col &&
            col <= intraIntervals[i].end) {
          auto d = abs(col - row);
          expected[d] += val;
          N[d]++;
          break;
        }
      }
    }
  }


  auto div = new double[](maxD + 1);
  div.fill(0);
  
  foreach (d; 0..(maxD+1)) {
    foreach (interval; intraIntervals) {
      auto possiblePair = (interval.end - interval.start).to!long + 1 - d;
      if (possiblePair > 0) div[d] += possiblePair;
    }
  }
  
  foreach (row, d1; depth) {
    if (d1 == 0 && !binInfo[row].gap) {
      foreach (col, d2; depth[row..$]) {
        if (abs(row - col) > maxD) break;
        
        if (d2 == 0 && !binInfo[row].gap) {
          foreach (i, interval; intraIntervals) {
            if (interval.start <= row && row <= interval.end) {
              if (intraIntervals[i].start <= col &&
                  col <= intraIntervals[i].end) {
                div[abs(row - col)] -= 1;
                break;
              }
            }
          }
        }
      }
    }
  }
  
  foreach (d, ref e; expected) e /= div[d];
  
  
  return Tuple!(double[], "expected", uint[], "N", double[], "div")(expected, N, div);
}


Tuple!(string, "rname", uint, "start", uint, "end")[]
splitIntraAndGap(BinInfo[] binInfo)
{
  Tuple!(string, "rname", uint, "start", uint, "end")[] intervals;

  auto start = binInfo[0].binID;

  foreach (bin; binInfo) {
    if (!bin.gap) {
      start = binInfo[0].binID;
      break;
    }
  }
  
  foreach (i, bin; binInfo[1..$]) { // i is start 0, so it points the previous bin.
    if ((!binInfo[i].gap && bin.gap) || binInfo[i].rname != bin.rname) {
      intervals ~=  Tuple!(string, "rname", uint, "start", uint, "end")
                         (binInfo[i].rname, start, binInfo[i].binID);
      foreach (j, bin2; binInfo[i+1..$]) {
        if (!bin2.gap) {
          start = bin2.binID;
          break;
        }
      }
    }
  }

  if (!binInfo[$-1].gap)
    intervals ~=  Tuple!(string, "rname", uint, "start", uint, "end")
                        (binInfo[$-1].rname, start, binInfo[$-1].binID);
  
  return intervals;
}


BinInfo[] readBinInfo(string filename)
{
  BinInfo[] binInfo;
  auto count = 0;
  foreach (line; File(filename).byLineCopy) {
    auto fields = line.strip.split;
    auto binID = fields[0].to!uint;
    if (count != binID)
      throw new Exception("binID is not sorted");
    binInfo ~= new BinInfo(binID, fields[1],
                           fields[2].to!uint, fields[3].to!uint,
                           fields[0] == "true" ? true : false);
    count++;
  }
  return binInfo;
}


class BinInfo
{
  uint binID, start, end;
  string rname;
  bool gap;

  this(uint id, string r, uint s, uint e, bool g)
  {
    binID = id;
    rname = r;
    start = s;
    end = e;
    gap = g;
  }
}
