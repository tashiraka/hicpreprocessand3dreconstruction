function KRnormalize(dsFile, flag, depthLowerCutOff, depthUpperCutOff, outFile, biasFile)
% DepthCutOff must be > 0, and should be > 2.

    fprintf('Loading the upper triangled contact frequency matrix\n');
    cfm = load(dsFile);
    cfm(:, 1:2) = cfm(:, 1:2) + sparse(1);
    cfm = spconvert(cfm);
    
    fprintf('Creating the lower triangle\n');
    cfm = cfm + cfm.' - diag(diag(cfm));
    n = size(cfm, 1);

    fprintf('Extracting %f % =< depth <= %f %\n', depthLowerCutOff, depthUpperCutOff);
    depth = sum(cfm);
    
    sorted = sort(depth(depth > 0));
    nd = length(sorted);

    valid = 1:1:nd;
    
    if flag
        sorted = sorted(floor(nd * depthLowerCutOff):ceil(nd * depthUpperCutOff));
        minD = sorted(1);
        maxD = sorted(end);
        
        valid = find(minD <= depth & depth <= maxD);
    else
        valid = find(depthLowerCutOff <= depth & depth <= depthUpperCutOff);
    end
    
    validCfm = cfm(valid, valid);

    validNum = size(validCfm, 1);
    originalNum = length(find(depth ~= 0 & depth ~= 1 & depth ~= 2));
    fprintf('Removed # bins except depth=0,1,2: %d, %d, %f\n', ...
            validNum, originalNum, 1 - validNum / originalNum);

    fprintf('Balancing the matrix (%d * %d)\n', size(valid, 2), size(valid,2));
    [x, res, isNotConverge] = bnewt(validCfm);

    if isNotConverge
        fid = fopen(outFile,'w');
        fprintf(fid, 'Not converged\n');
        fclose(fid);
    else
        normedValidCfm = diag(x) * validCfm * diag(x);
        fprintf('Reconstructing an original-sized matrix\n');
        normedCfm = sparse(n, n);
        normedCfm(valid, valid) = normedValidCfm;

        bias = NaN(n, 1);
        bias(valid) = full(x);
        
        fprintf('Writing\n');
        [i, j, val] = find(triu(normedCfm));
        i = i - 1;
        j = j - 1;
        fid = fopen(outFile,'w');
        fprintf(fid, '%d\t%d\t%f\n', vertcat(i', j', val'));
        if normedCfm(n, n) == 0
            fprintf(fid, '%d\t%d\t%f\n', n - 1, n - 1, 0);
        end
        fclose(fid);

        fid = fopen(biasFile,'w');
        fprintf(fid, '%f\n', bias);
        fclose(fid);
        
    end
    