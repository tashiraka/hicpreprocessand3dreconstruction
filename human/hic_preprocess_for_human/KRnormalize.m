function KRnormalize(inFile, outFile, biasFile)

% input a half of symmetric sparse matrix
spCFM = load(inFile);
spCFM(:, 1:2) = spCFM(:, 1:2) + sparse(1);
spCFM = spconvert(spCFM);

% reconstruct the full of symmetric sparse matrix
% diagonal element to 0.
spCFM = spCFM - diag(diag(spCFM));
spCFM = spCFM + spCFM.';

% delete the no-read loci from contact frequency matrix
columnSum = sum(spCFM);
noCoverageIndeces = find(columnSum == 0);
spCFM(:, noCoverageIndeces) = [];
spCFM(noCoverageIndeces, :) = [];

% normalization
[x, res] = bnewt(spCFM);
normedSpCFM = diag(x)*spCFM*diag(x);

% re-insert the no-read loci
for i = noCoverageIndeces
    normedSpCFM = cat(1, ...
                      normedSpCFM(1:i-1, :), ...
                      sparse(zeros(1, size(normedSpCFM, 2))), ...
                      normedSpCFM(i:end, :));
    normedSpCFM = cat(2, ...
                      normedSpCFM(:, 1:i-1), ...
                      sparse(zeros(size(normedSpCFM, 1), 1)), ...
                      normedSpCFM(:, i:end));
    x = cat(1, x(1:i-1, :), sparse(inf), x(i:end, :));
end


%fout = fopen(outFile, 'w');
%[row, col, val] = find(normedSpCFM);
%for i = 1:size(val)
%    if row(i) < col(i)
%        fprintf(fout, '%d\t%d\t%f\n', row(i), col(i), val(i));
%    end
%end
%fclose(fout);
fullcfm = full(normedSpCFM);
size(fullcfm)
save(outFile, '-ascii', '-tabs', 'fullcfm');

fullx = full(x);
save(biasFile, '-ascii', '-tabs', 'fullx');

exit();