import sys, os, logging, subprocess, pysam, tempfile, re


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def removeDup(inStream, outStream):
    prevLine = ''

    for line in inStream:
        if prevLine == '':
            prevLine = line
        elif line.split('\t')[0] == prevLine.split('\t')[0]:
            mapq = int(line.split('\t')[4])
            prevMapq = int(prevLine.split('\t')[4])
            readLen = len(line.split('\t')[9])
            prevReadLen = len(line.split('\t')[9])
            
            if (mapq > prevMapq) or (mapq == prevMapq and readLen > prevReadLen):
                prevLine = line
            else:
                prevLine = prevLine
        else:
            outStream.write(prevLine)
            prevLine = line

    outStream.write(prevLine)



def producePairedBam(inSam1, inSam2, outBam1, outBam2, minMapq):
    logging.info('Pairing {0} and {1} to {2} and {3}'.format(inSam1, inSam2, outBam1, outBam2))
        
    c1 = int(subprocess.Popen(['samtools', 'view', '-Sc', inSam1], stdout=subprocess.PIPE).communicate()[0].strip().split()[0])
    c2 = int(subprocess.Popen(['samtools', 'view', '-Sc', inSam2], stdout=subprocess.PIPE).communicate()[0].strip().split()[0])
    if c1 != c2:
        logging.info('Error: different # records in {0} and {1}'.format(inSam1, inSam2))
        exit()
    logging.info('# records: {0}'.format(c1))

    # produce the paired flag and fields
    inP1 = subprocess.Popen(['samtools', 'view', '-Sh', inSam1], stdout=subprocess.PIPE)
    inP2 = subprocess.Popen(['samtools', 'view', '-Sh', inSam2], stdout=subprocess.PIPE)
    outP1 = subprocess.Popen(['samtools', 'view', '-Sb', '-'], stdin=subprocess.PIPE, stdout=open(outBam1, 'w'))
    outP2 = subprocess.Popen(['samtools', 'view', '-Sb', '-'], stdin=subprocess.PIPE, stdout=open(outBam2, 'w'))

    for line1 in inP1.stdout:
        line2 = inP2.stdout.readline()
        
        fields1 = line1.split('\t')
        fields2 = line2.split('\t')

        if line1[0] == '@' and line2[0] == '@':
            outP1.stdin.write(line1)
            outP2.stdin.write(line2)
            
        elif (line1[0] != '@' and line2[0] == '@') or (line1[0] == '@' and line2[0] != '@'):
            logging.info("ERROR: different # header!!")
            exit()
            
        elif fields1[0] != fields2[0]:
            logging.info("ERROR: Not paired entry!!")
            exit()
            
        else:
            # Produce flags
            flag1 = int(fields1[1])
            flag2 = int(fields2[1])
            
            if flag1 & 0x100 != 0 or flag2 & 0x100 != 0:
                logging.info("ERROR: Not primary alignment!!")
                exit()

            mapq1 = int(fields1[4])
            mapq2 = int(fields2[4])
            newFlag1 = 0x1 + 0x2 + (0x4 if mapq1 < minMapq else 0) + (0x8 if mapq2 < minMapq else 0) + (flag1 & 0x10) + ((flag2 & 0x10) << 1) + 0x40
            newFlag2 = 0x1 + 0x2 + (0x4 if mapq2 < minMapq else 0) + (0x8 if mapq1 < minMapq else 0) + (flag2 & 0x10) + ((flag1 & 0x10) << 1) + 0x80
            fields1[1] = str(newFlag1)
            fields2[1] = str(newFlag2)
            
            # Produce fields
            # RNEXT. samtools automaticaly convert the same RNAME to '='.
            fields1[6] = fields2[2];
            fields2[6] = fields1[2];            
            
            # PNEXT
            fields1[7] = fields2[3];
            fields2[7] = fields1[3];            

            outP1.stdin.write('\t'.join(fields1))
            outP2.stdin.write('\t'.join(fields2))
        
    inP1.communicate()
    inP2.communicate()
    outP1.stdin.close()
    outP2.stdin.close()
    outP1.communicate()
    outP2.communicate()

    
def pairing(bamPrefix, minMapq):
    # The input files of iterative mapping
    bams1 = filter(lambda x: re.match(bamPrefix + '_1.bam.[0-9]+$', x), subprocess.Popen('ls', stdout=subprocess.PIPE).communicate()[0].split())
    bams2 = filter(lambda x: re.match(bamPrefix + '_2.bam.[0-9]+$', x), subprocess.Popen('ls', stdout=subprocess.PIPE).communicate()[0].split())
    logging.info('Input BAM_1 files: %s', ' '.join(bams1))
    logging.info('Input BAM_2 files: %s', ' '.join(bams2))

    # Merge the mapped reads
    mergedBam1 = bamPrefix + '_1.bam.merged'
    mergedBam2 = bamPrefix + '_2.bam.merged'
    cmd1 = ['samtools', 'merge', '-fn', mergedBam1] + bams1
    cmd2 = ['samtools', 'merge', '-fn', mergedBam2] + bams2
    logging.info('Merge BAM files command: {0} > {1}'.format(' '.join(cmd1), mergedBam1))        
    logging.info('Merge BAM files command: {0} > {1}'.format(' '.join(cmd2), mergedBam2))
    p1 = subprocess.Popen(cmd1)
    p2 = subprocess.Popen(cmd2)
    p1.communicate()
    p2.communicate()

    # Filter out the secondary reads
    filteredBam1 = mergedBam1 + '.filtered'
    filteredBam2 = mergedBam2 + '.filtered'
    cmd1 = ['samtools', 'view', '-b', '-F', '0x100', mergedBam1]
    cmd2 = ['samtools', 'view', '-b', '-F', '0x100', mergedBam2]
    logging.info('Filter command: {0} > {1}'.format(' '.join(cmd1), filteredBam1))
    logging.info('Filter command: {0} > {1}'.format(' '.join(cmd2), filteredBam2))
    p1 = subprocess.Popen(cmd1, stdout=open(filteredBam1, 'w'))
    p2 = subprocess.Popen(cmd2, stdout=open(filteredBam2, 'w'))
    p1.communicate()
    p2.communicate()

    os.remove(mergedBam1)
    os.remove(mergedBam2)
    
    # Remove the duplicated reads
    logging.info('Remove duplicated records')

    noDupSam1 = filteredBam1 + '.noDup.sam'
    noDupSam2 = filteredBam2 + '.noDup.sam'
    p1 = subprocess.Popen(['samtools', 'view', '-H', filteredBam1], stdout=open(noDupSam1, 'w'))
    p2 = subprocess.Popen(['samtools', 'view', '-H', filteredBam2], stdout=open(noDupSam2, 'w'))
    p1.communicate()
    p2.communicate()
    
    p1 = subprocess.Popen(['samtools', 'view', filteredBam1], stdout=subprocess.PIPE)
    p2 = subprocess.Popen(['samtools', 'view', filteredBam2], stdout=subprocess.PIPE)
    removeDup(p1.stdout, open(noDupSam1, 'a'))
    removeDup(p2.stdout, open(noDupSam2, 'a'))
    p1.communicate()
    p2.communicate()

    os.remove(filteredBam1)
    os.remove(filteredBam2)
        
    # Produce the paired flags and paired fields in BAM files
    producePairedBam(noDupSam1, noDupSam2,
                     bamPrefix + '_1.paired.bam',
                     bamPrefix + '_2.paired.bam',
                     minMapq)

    os.remove(noDupSam1)
    os.remove(noDupSam2)

    
args = sys.argv
bamPrefix = args[1] # if the pair, SRR123_1.bam and SRR123_2.bam, then SRR123
minMapq = int(args[2])
logging.info('Start of pairing')
logging.info('pairing({0})'.format(bamPrefix))
pairing(bamPrefix, minMapq)
logging.info('End of pairing')
