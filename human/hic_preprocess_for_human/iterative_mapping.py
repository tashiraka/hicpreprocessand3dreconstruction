import os, sys, shutil, subprocess, tempfile, pysam, logging


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def gzipWriter(filename):
    with open(os.path.abspath(filename), 'wb') as fout:
        p = subprocess.Popen(['pigz'], stdin=subprocess.PIPE, stdout=fout, shell=False, bufsize=-1)
    logger.info('Writer created with command pigz')
    return p


def isReadUnmapped(read, minMapq):
    if (read.mapq < minMapq):
        return True
#    for tag in read.tags:
#        if tag[0] == 'XT' and tag[1] == 'N':
#            return True
    return False


def filterUnmappedFastq(mappingFastqStream, inBam, unmappedFastqPath, minMapq, lenStep):
    # Collect the unmapped-primary read IDs.
    unmappedIDs = set()
    for read in pysam.Samfile(inBam, 'rb'):
        if (not read.is_secondary) and isReadUnmapped(read, minMapq):
            unmappedIDs.add(read.qname)

    # Write the unmapped reads to gziped FASTQ file.
    writeProcess = gzipWriter(unmappedFastqPath)
    numFiltered = 0
    numTotal = 0
    while True:
        line = mappingFastqStream.readline()

        try:
            assert line[0] == '@'
        except AssertionError:
            print 'Not fastq'
        except IndexError:
            break

        fastqEntry = (line,
                      mappingFastqStream.readline()[0 : -lenStep - 1] + '\n',
                      mappingFastqStream.readline(),
                      mappingFastqStream.readline()[0 : -lenStep - 1] + '\n')
        readID = line.split()[0][1:]
        
        if readID in unmappedIDs:
            writeProcess.stdin.writelines(fastqEntry)
            numFiltered += 1
        numTotal += 1

    writeProcess.communicate()
        
    if writeProcess.returncode != 0:
        raise RuntimeError("Writing process return code {0}".format(
            writeProcess.returncode))

    return numTotal, numFiltered


def iterativeMapping(bwaPath, readGroupInfo, bwaIndexPath, fastqPath, outBamPath,
                     minSeqLen, lenStep, minMapq, nThreads, **kwargs):
    # Check if input FASTQ file is gzipped.
    bashReader = 'zcat' if fastqPath.split('.')[-1].lower() == 'gz' else 'cat'
    readCmd = bashReader.split() + [fastqPath]

    bamCmd = ['samtools', 'view', '-bS', '-']

    # Check the additional BWA flags.
    bwaFlags = kwargs.get('bwaFlags', '')

    # Get the read length.
    readProcess = subprocess.Popen(readCmd, stdout=subprocess.PIPE)
    readProcess.stdout.readline()
    rawSeqLen = len(readProcess.stdout.readline().strip())
    logger.info('The length of whole sequences in the file: %d', rawSeqLen)
    readProcess.terminate()

    # Current mapping-seaqence length in the iteration.
    currentSeqLen = rawSeqLen

    # Current FASTQ file to map
    currentFastqPath = fastqPath
    
    # Main iteration
    firstFlag = True
    iterativeOutBams = []
    while True:
        currentOutBam = outBamPath + '.' + str(currentSeqLen)
        iterativeOutBams.append(currentOutBam)
        mapCmd = [bwaPath, 'mem'] + bwaFlags.split() + ['-R', readGroupInfo, '-t', str(nThreads), bwaIndexPath, currentFastqPath]
        pipeline = []

        try:
            # Read out FASTQ to BWA
            logger.info('Read command: %s', ' '.join(readCmd))
            pipeline.append(subprocess.Popen(readCmd, stdout=subprocess.PIPE))

            # BWA mem
            logger.info('Map command: %s', ' '.join(mapCmd))
            pipeline.append(subprocess.Popen(mapCmd, stdin=pipeline[-1].stdout, stdout=subprocess.PIPE if bamCmd else open(currentOutBam, 'w')))

            # Compress the out SAM file to BAM
            logger.info('Output formatting command: %s', ' '.join(bamCmd))
            pipeline.append(subprocess.Popen(bamCmd, stdin=pipeline[-1].stdout, stdout=open(currentOutBam, 'w')))

            pipeline[-1].wait()
        finally:
            for process in pipeline:
                if process.poll() is None:
                    process.terminate()

        # Next map length
        nextSeqLen = currentSeqLen - lenStep            
        if nextSeqLen < minSeqLen:
            break
                    
        # Go to the next iteration.
        logger.info('Save the unique aligments and send the non-unique ones to the next iteration')
        unmappedFastqPath = fastqPath + '.%d' % nextSeqLen + ".fastq.gz"
        readCmd = ['zcat', currentFastqPath]
        readProcess = subprocess.Popen(readCmd, stdout=subprocess.PIPE)
        
        # Filtering the unmapped reads.
        numTotal, numFiltered = filterUnmappedFastq(readProcess.stdout, currentOutBam, unmappedFastqPath, minMapq, lenStep)
        readProcess.communicate()

        logger.info(('{0} non-unique reads out of {1} are sent the next iteration.').format(numFiltered, numTotal))

        # Prepare for next iteretion.
        if not firstFlag:
            os.remove(currentFastqPath)
        else:
            firstFlag = False
            
        currentSeqLen = nextSeqLen
        currentFastqPath = unmappedFastqPath

    # remove temp fastq
    if not firstFlag:
        os.remove(currentFastqPath)


args = sys.argv
bwaPath = args[1]
readGroupInfo = args[2]
bwaIndexPath = args[3]
fastqPath = args[4]   
outBamPath = args[5]
minSeqLen = args[6]
lenStep = args[7]
minMapq = args[8]
nThreads = args[9]

logging.info('Start of iterative mapping')
logging.info('iterativeMapping(bwaPath={0}, readGroupInfo={1}, bwaIndexPath={2}, fastqPath={3}, outBamPath={4}, minSeqLen={5}, lenStep={6}, minMapq={7}, nThreads={8}, bwaFlags={9}'.format(bwaPath, readGroupInfo, bwaIndexPath, fastqPath, outBamPath, minSeqLen, lenStep, minMapq, nThreads, '-M'))

iterativeMapping(
    bwaPath=bwaPath,
    readGroupInfo=readGroupInfo,
    bwaIndexPath=bwaIndexPath,
    fastqPath=fastqPath,
    outBamPath=outBamPath,
    minSeqLen=int(minSeqLen),
    lenStep=int(lenStep),
    minMapq=int(minMapq),
    nThreads=nThreads,
    bwaFlags='-M')

logging.info('End of iterative mapping')
