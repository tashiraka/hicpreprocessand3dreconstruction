file_prefix=$1

fastq1=${file_prefix}_1.fastq.gz
fastq2=${file_prefix}_2.fastq.gz
bam1=${file_prefix}_1.bam
bam2=${file_prefix}_2.bam
cmd_path=/home/yuichi/Workspace/Hi-C_workbench/refined_hic_preprocess/human
bwa_path=bwa
bwa_index_path=/work/yuichi/human/genome/hg38/bwa_index/hg38.fa
min_seq_len=25
len_step=5
min_mapq=30
n_threads=24


# iterative mapping
# Produce the files: ${bam1}.[0-9]+$ and ${bam2}.[0-9]+$
python ${cmd_path}/iterative_mapping.py $bwa_path $bwa_index_path \
    $fastq1 $bam1 $min_seq_len $len_step $min_mapq $n_threads
python ${cmd_path}/iterative_mapping.py $bwa_path $bwa_index_path \
    $fastq2 $bam2 $min_seq_len $len_step $min_mapq $n_threads


# Pairing
# Produce the files: ${file_prefix}_1.paired.bam and ${file_prefix}_2.paired.bam
python ${cmd_path}/pairing.py $file_prefix min_mapq
ls | grep -E '${bam1}.[0-9]+$' | xargs rm
ls | grep -E '${bam2}.[0-9]+$' | xargs rm


# Remove unmapped or non-unique pairs
samtools view -b -F 0x8 -q 30 ${file_prefix}_1.paired.bam > ${file_prefix}_1.paired.filtered.bam
rm ${file_prefix}_1.paired.bam
samtools view -b -F 0x8 -q 30 ${file_prefix}_2.paired.bam > ${file_prefix}_2.paired.filtered.bam
rm ${file_prefix}_2.paired.bam


# Map to restriction fragments
