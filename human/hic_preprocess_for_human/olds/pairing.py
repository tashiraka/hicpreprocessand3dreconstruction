import sys, os, logging, subprocess, pysam, tempfile, re


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def filterBam(inBam, outBam, options):
    cmd = ['samtools', 'view', '-b'] + options + [inBam]
    logging.info('Filter command: %s', ' '.join(cmd))
    return subprocess.Popen(cmd, stdout=open(outBam, 'w'))


def mergeBams(outBam, inBams):
    cmd = ['samtools', 'merge', '-fn', outBam] + inBams
    logging.info('Merge BAM files command: %s', ' '.join(cmd))
    return subprocess.Popen(cmd)


def removeDup(bam):
    tmpSam = tempfile.mkstemp(prefix=os.path.abspath(bam) + '.', suffix='.sam')[1]
    cmd = ['samtools', 'view', '-H', bam]
    logging.info('Write the header command: %s', ' '.join(cmd))
    p = subprocess.Popen(cmd, stdout=open(tmpSam, 'w'))
    p.communicate()

    prevLine = ''
    isFirst = True
    isLastDup = False
    fout = open(tmpSam, 'a')
    cmd = ['samtools', 'view', bam]
    logging.info('Remove the duplicated records')
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    
    for line in p.stdout:
        if isFirst:
            isFirst = False
        elif line.split('\t')[0] == prevLine.split('\t')[0]:
            mapq = int(line.split('\t')[4])
            prevMapq = int(prevLine.split('\t')[4])

            if mapq > prevMapq:
                fout.write(line)
            else:
                fout.write(prevLine)

            isLastDup = True
        else:
            fout.write(prevLine)
            isLastDup = False
            
        prevLine = line

    if not isLastDup:
        fout.write(prevLine)
            
    p.communicate()
        
    # Compress 
    cmd = ['samtools', 'view', '-Sb', tmpSam]
    logging.info('Compress command: %s', ' '.join(cmd))
    p = subprocess.Popen(cmd, stdout=open(bam, 'w'))
    p.communicate()
    
#    os.remove(tmpSam)
        

def producePairedBam(inBam1, inBam2, outBam1, outBam2, minMapq):
    cmd1 = ['samtools', 'view', '-c', inBam1]
    logging.info('Count the records in ' + inBam1)
    c1 = int(subprocess.Popen(cmd1, stdout=subprocess.PIPE).\
             communicate()[0].strip().split()[0])
    
    cmd2 = ['samtools', 'view', '-c', inBam2]
    logging.info('Count the records in ' +  inBam2)
    c2 = int(subprocess.Popen(cmd2, stdout=subprocess.PIPE).\
             communicate()[0].strip().split()[0])

    if c1 != c2:
        logging.info('Error: different # records in {0} and {1}'.format(inBam1,
                                                                        inBam2))
        exit()
        
    # Out the headers
    cmd = ['samtools', 'view', '-H', inBam1]
    logging.info('Write the header command: %s', ' '.join(cmd))
    p = subprocess.Popen(cmd, stdout=open(outBam1, 'w'))
    p.communicate()

    cmd = ['samtools', 'view', '-H', inBam2]
    logging.info('Write the header command: %s', ' '.join(cmd))
    p = subprocess.Popen(cmd, stdout=open(outBam2, 'w'))
    p.communicate()

    # produce the paired flag and fields
    fout1 = open(outBam1, 'a')
    fout2 = open(outBam2, 'a')
    cmd1 = ['samtools', 'view', inBam1]
    cmd2 = ['samtools', 'view', inBam2]
    logging.info('Pairing {0} and {1} to {2} and {3}'.format(inBam1, inBam2,
                                                              outBam1, outBam2))
    p1 = subprocess.Popen(cmd1, stdout=subprocess.PIPE)
    p2 = subprocess.Popen(cmd2, stdout=subprocess.PIPE)

    '''
    ===FIELDS===
    1 QNAME String [!-?A-~]{1,255} Query template NAME
    2 FLAG Int [0,216-1] bitwise FLAG
    3 RNAME String \*|[!-()+-<>-~][!-~]* Reference sequence NAME
    4 POS Int [0,231-1] 1-based leftmost mapping POSition
    5 MAPQ Int [0,28-1] MAPping Quality
    6 CIGAR String \*|([0-9]+[MIDNSHPX=])+ CIGAR string
    7 RNEXT String \*|=|[!-()+-<>-~][!-~]* Ref. name of the mate/next read
    8 PNEXT Int [0,231-1] Position of the mate/next read
    9 TLEN Int [-231+1,231-1] observed Template LENgth
    10 SEQ String \*|[A-Za-z=.]+ segment SEQuence
    11 QUAL String [!-~]+ ASCII of Phred-scaled base QUALity+33
    
    ====FLAG====
    0x1 template having multiple segments in sequencing
    0x2 each segment properly aligned according to the aligner
    0x4 segment unmapped
    0x8 next segment in the template unmapped
    0x10 SEQ being reverse complemented
    0x20 SEQ of the next segment in the template being reversed
    0x40 the first segment in the template
    0x80 the last segment in the template
    0x100 secondary alignment
    0x200 not passing quality controls
    0x400 PCR or optical duplicate
    0x800 supplementary alignment
    '''
    for line1 in p1.stdout:
        line2 = p2.stdout.readline()
        fields1 = line1.split('\t')
        fields2 = line2.split('\t')
        
        if fields1[0] != fields2[0]:
            logging.info("ERROR: Not paired entry!!")
            exit()
        else: 
            # Produce flags
            flag1 = int(fields1[1])
            flag2 = int(fields2[1])

            if flag1 & 0x100 != 0 or flag2 & 0x100 != 0:
                logging.info("ERROR: Not primary alignment!!")
                exit()

            mapq1 = int(fields1[4])
            mapq2 = int(fields2[4])

            # CAUTION: The unmapped flag 0x4 is defined by BWA mem.
            #          The pair unmapped flag 0x8 is defined as follows.
            # Is the pair unmapped ?
            flag1 += 0x8 if flag1 & 0x8 == 0 and mapq2 < minMapq else 0x0
            flag2 += 0x8 if flag2 & 0x8 == 0 and mapq1 < minMapq else 0x0
            # Is the pair in the template ?
            flag1 += 0x20 if flag1 & 0x20 == 0 and flag2 & 0x10 != 0 else 0x0
            flag2 += 0x20 if flag2 & 0x20 == 0 and flag1 & 0x10 != 0 else 0x0
            # Def) The inBam1 contains the firsts of the pairs. The inBam2 second.
            flag1 += 0x40 if flag1 & 0x40 == 0 else 0x0
            flag2 -= 0x40 if flag2 & 0x40 != 0 else 0x0
            flag1 -= 0x80 if flag1 & 0x80 != 0 else 0x0
            flag2 += 0x80 if flag2 & 0x80 == 0 else 0x0

            fields1[1] = str(flag1)
            fields2[1] = str(flag2)

            # Produce fields
            # RNEXT
            fields1[6] = fields2[2];
            fields2[6] = fields1[2];            

            # PNEXT
            fields1[7] = fields2[3];
            fields2[7] = fields1[3];            

        fout1.write('\t'.join(fields1) + '\n')
        fout2.write('\t'.join(fields2) + '\n')

    p1.communicate()
    p2.communicate()


def pairing(bamPrefix, minMapq):
    # The input files of iterative mapping
    bams1 = filter(lambda x: re.match(bamPrefix + '_1.bam.[0-9]+$', x),
                   subprocess.Popen('ls', stdout=subprocess.PIPE)\
                   .communicate()[0].split())
    logging.info('Input BAM_1 files: %s', ' '.join(bams1))

    bams2 = filter(lambda x: re.match(bamPrefix + '_2.bam.[0-9]+$', x),
                   subprocess.Popen('ls', stdout=subprocess.PIPE)\
                   .communicate()[0].split())
    logging.info('Input BAM_2 files: %s', ' '.join(bams2))

    # Parse BAMs
    fullLen1 = max([int(x.split('.')[-1]) for x in bams1])
    fullLenBam1 = bamPrefix + '_1.bam.' + str(fullLen1)
    logging.info('BAM_1 file with full length: {0}'.format(fullLenBam1))
    
    fullLen2 = max([int(x.split('.')[-1]) for x in bams2])
    fullLenBam2 = bamPrefix + '_2.bam.' + str(fullLen2)
    logging.info('BAM_2 file with full length: {0}'.format(fullLenBam2))

    truncatedBams1 = list(bams1)
    truncatedBams1.remove(fullLenBam1)
    logging.info('BAM_1 files with truncated length: %s',
                 ' '.join(truncatedBams1))

    truncatedBams2 = list(bams2)
    truncatedBams2.remove(fullLenBam2)
    logging.info('BAM_2 files with truncated length: %s',
                 ' '.join(truncatedBams2))

    # Filter out the secondary reads from full-length-mapped BAMs
    # Filter out the secondary and unmapped reads from truncated-length-mapped BAMs
    filteredFullLenBam1 = bamPrefix + '_1.bam.' + str(fullLen1) + '.filtered'
    filteredFullLenBam2 = bamPrefix + '_2.bam.' + str(fullLen2) + '.filtered'
    filteredTruncatedBams1 = [x + '.filtered' for x in truncatedBams1]
    filteredTruncatedBams2 = [x + '.filtered' for x in truncatedBams2]

    p1 = filterBam(fullLenBam1, filteredFullLenBam1, ['-F', '0x100'])
    p2 = filterBam(fullLenBam2, filteredFullLenBam2, ['-F', '0x100'])
    ps1 = []
    for (inBam, outBam) in zip(truncatedBams1, filteredTruncatedBams1):
        ps1.append(filterBam(inBam, outBam, ['-F', '0x100', '-q', '30']))
    ps2 = []
    for (inBam, outBam) in zip(truncatedBams2, filteredTruncatedBams2):
        ps2.append(filterBam(inBam, outBam, ['-F', '0x100', '-q', '30']))
        
    # run p1, p2, ps1 and ps2 in the same time
    p1.communicate()
    p2.communicate()
    for pp in ps1:
        pp.communicate()
    for pp in ps2:
        pp.communicate()

    # Merge the mapped reads
    mergedBam1 = bamPrefix + '_1.bam.merged'
    mergedBam2 = bamPrefix + '_2.bam.merged'

    p1 = mergeBams(mergedBam1,
                   [filteredFullLenBam1] + filteredTruncatedBams1)
    p2 = mergeBams(mergedBam2,
                [filteredFullLenBam2] + filteredTruncatedBams2)
    p1.communicate()
    p2.communicate()
    
    # Remove the duplicated reads
    removeDup(mergedBam1)
    removeDup(mergedBam2)
    
    # Remove the temporary files
    logging.info('Remove ' \
                 + filteredFullLenBam1 \
                 + ' '.join(filteredTruncatedBams1)\
                 + filteredFullLenBam1 \
                 + ' '.join(filteredTruncatedBams1))
#    os.remove(filteredFullLenBam1)
#    os.remove(filteredFullLenBam2)
#    for bam in filteredTruncatedBams1:
#        os.remove(bam)
#    for bam in filteredTruncatedBams2:
#        os.remove(bam)

    # Produce the paired flags and paired fields in BAM files
    producePairedBam(mergedBam1, mergedBam2,
                     bamPrefix + '_1.paired.bam',
                     bamPrefix + '_2.paired.bam',
                     minMapq)

#    os.remove(mergedBam1)
#    os.remove(mergedBam2)

    
args = sys.argv
bamPrefix = args[1] # if the pair, SRR123_1.bam and SRR123_2.bam, then SRR123
minMapq = args[2]
logging.info('Start of pairing')
logging.info('pairing({0})'.format(bamPrefix))
pairing(bamPrefix, minMapq)
logging.info('End of pairing')
