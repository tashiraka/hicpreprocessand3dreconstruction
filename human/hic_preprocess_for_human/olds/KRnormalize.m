function KRnormalize(inFile, outFile, biasFile)
cfm = load(inFile);

% delete the no-read loci from contact frequency matrix
columnSum = sum(cfm);
noCoverageIndeces = find(columnSum == 0);
cfm(:, noCoverageIndeces) = [];
cfm(noCoverageIndeces, :) = [];

% normalization
[x, res] = bnewt(cfm);
normedCFM = diag(x)*cfm*diag(x);

% re-insert the no-read loci
for i = noCoverageIndeces
    cfm = cat(1, cfm(1:i-1, :), zeros(1, size(cfm, 2)), cfm(i+1:end, :));
    cfm = cat(2, cfm(:, 1:i-1), zeros(size(cfm, 1), 1), cfm(:, i+1:end));
end

save(outFile, normeCFM);
save(biasFile, x);