// CFM == Contact Frequency Matrix

import std.stdio, std.conv, std.string, std.algorithm;


void main(string[] args) {
  auto inFile = args[1];
  auto fragFile = args[2];
  auto outFile = args[3];

  uint maxFragID = 0;
  foreach(line; File(fragFile).byLine) {
    maxFragID = max(maxFragID, line.strip().split('\t')[0].to!uint);
  }

  auto cfm = new uint[][](maxFragID, maxFragID);
  foreach(ref row; cfm) {
    row.fill(0);
  }
  
  foreach(line; File(inFile).byLine) {
    auto fields = line.strip().split('\t');
    auto fragID1 = fields[4].to!uint;
    auto fragID2 = fields[10].to!uint;
    cfm[fragID1][fragID2] += 1;
    cfm[fragID2][fragID1] += 1;
  }


  auto fout = File(outFile, "w");
  foreach(row; cfm) {
    foreach(i, elem; row) {
      fout.write(elem);
      if(i < row.length - 1) {
        fout.write('\t');
      }
      fout.writeln;
    }
  }
}





