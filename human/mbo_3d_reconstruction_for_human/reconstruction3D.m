function reconstruction3D(normedCFMFile, outXFile)
    fprintf('Initialize normalized CFM\n')
    CFM = load(normedCFMFile);
    CFM(:,1:2) = CFM(:,1:2) + 1;
    CFM = spconvert(CFM);
    reducedNbins = size(CFM, 1);

    fprintf('Finding all-pairs shortest path\n');
    %D = CFM;
    H = CFM.';
    clear CFM;
    H(1:reducedNbins+1:reducedNbins*reducedNbins) = 0;
    H = full(squareform(H));
    
    %{
    tic
    D = full(D);
    n = size(D, 1);
    D = D + D.';
    D = D.^(-1);
    D(1:n+1:n*n) = 0;

    for k=1:n
        A = repmat(D(:,k), 1, n);
        A = bsxfun(@plus, A, D(k,:));
        D = min(D, A); 
    end
    toc
    
    D = squareform(D);
    save(strcat(normedCFMFile, '.D.mat'), 'D', '-v7.3');
    %}
    fprintf('Loading D.mat\n');
    load(strcat(normedCFMFile, '.D.mat'));

    fprintf('Finding coordinates using MBO\n');
    a_min = 1;
    a_max = 1;
    [log_a, fval, exitflag, output] = fminbnd(@(a) known_alpha(D, H, exp(a)), ...
                                              log(a_min), log(a_max), ...
                                              optimset('TolX', 0.001, 'Display',...
                                                      'notify', ...
                                                      'MaxIter', 100));

    fprintf('error: %f\n', fval);
    fprintf('exitflag: %d\n', exitflag);
    fprintf('algorithm: %s\n', output.algorithm);
    fprintf('funcCount: %d\n', output.funcCount);
    fprintf('iterations: %d\n', output.iterations);
    fprintf('message: %s\n', output.message);
    alpha = exp(log_a);
    fprintf('alpha: %f\n', alpha);
    D = D.^(alpha);
    Ha = (1 + D.^(-alpha) - H).^(-1);
    fprintf('max(H): %f\n', max(H));
    fprintf('min(H): %f\n', min(H));

    options.solver = 'TR';
    options.tolcost = 1e-12;
    options.tolgradnorm = 1e-7;
    options.Delta0 = 10;
    options.Delta_bar = 10*options.Delta0;
    options.maxiter = 1000;
    [X, info] = MBO(D, H, options);
    clear D;
    clear H;

    fprintf('Writing X\n');
    fid = fopen(outXFile, 'w');
    fprintf(fid, '%f\t%f\t%f\n', X.');
    fclose(fid);    
end    


function cost = known_alpha(D, H, alpha)
    fprintf('alpha in this iteration: %f\n', alpha);
    Da = D.^(alpha);
    Ha = (D.^(-alpha) - H + 1).^(-1);
    fprintf('max(H): %f\n', max(Ha));
    fprintf('min(H): %f\n', min(Ha));
    
    options.solver = 'TR';
    options.tolcost = 1e-12;
    options.tolgradnorm = 1e-7;
    options.Delta0 = 10;
    options.Delta_bar = 10*options.Delta0;
    options.maxiter = 10000;
    [X, info] = MBO(Da, Ha, options);
    fprintf('cost: %f\n', info.cost);
    cost = info.cost;
end