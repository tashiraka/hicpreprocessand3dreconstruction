function [X, s] = pca_1m(normedCFMFile, nonZeroSumFlagsFile,...
                          outXFile, outLatentFile)
    fprintf('Initialize normalized CFM\n')
    CFM = load(normedCFMFile);
    CFM(:,1:2) = CFM(:,1:2) + 1;
    CFM = spconvert(CFM);
    CFM = CFM + CFM.' - diag(diag(CFM));
    
    fprintf('Finding coordinates using MBO\n');
    [X, ~, latent] = pca(full(CFM));
    X = X(:,1:3);
    
    fprintf('interpolate the bins without contacts\n');
    nonZeroSumFlags = load(nonZeroSumFlagsFile);
    nonZeroSumFlags = logical(nonZeroSumFlags);
    
    nbins = size(nonZeroSumFlags, 1);
    bins = (1:nbins).';
    reconstructedBins = bins(nonZeroSumFlags);
    X = interp1(reconstructedBins, X, bins, 'linear', 'extrap');
    
    fprintf('Writing X\n');
    fid = fopen(outXFile, 'w');
    fprintf(fid, '%f\t%f\t%f\n', X.');
    fclose(fid);
    
    fprintf('Writing parameter info\n');
    fid = fopen(outLatentFile, 'w');
    fprintf(fid, '%f\n', latent);
    fclose(fid);
end
