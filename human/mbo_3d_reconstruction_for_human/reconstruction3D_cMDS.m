function reconstruction3D_cMDS(normedCFMFile, outXFile)
    fprintf('Initialize normalized CFM\n')
    CFM = load(normedCFMFile);
    CFM(:,1:2) = CFM(:,1:2) + 1;
    CFM = spconvert(CFM);

    n = size(CFM, 1);
    CFM(1:n+1:n*n) = 0;
    CFM = squareform(CFM.');
    
    fprintf('Finding coordinates using cMDS\n');
    a_min = 0.1;
    a_max = 3;
    [log_a, fval, exitflag, output] = fminbnd(@(a) known_alpha(CFM, exp(a)), ...
                                              log(a_min), log(a_max), ...
                                              optimset('TolX', 0.001, 'Display',...
                                                      'notify', ...
                                                      'MaxIter', 100));
    
    fprintf('error: %f\n', fval);
    fprintf('exitflag: %d\n', exitflag);
    fprintf('algorithm: %s\n', output.algorithm);
    fprintf('funcCount: %d\n', output.funcCount);
    fprintf('iterations: %d\n', output.iterations);
    fprintf('message: %s\n', output.message);
    alpha = exp(log_a);
    fprintf('alpha: %f\n', alpha);

    fprintf('Finding all-pairs shortest path\n');
    tic
    D = full(squareform(CFM));
    D = D.^(-alpha);
    D(1:n+1:n*n) = 0;
    toc
    
    tic
    for k=1:n
        A = repmat(D(:,k), 1, n);
        A = bsxfun(@plus, A, D(k,:));
        D = min(D, A); 
    end
    toc
    
    D = squareform(D);

    X = cmdscale(D, 3);
    clear D;

    fprintf('Writing X\n');
    fid = fopen(outXFile, 'w');
    fprintf(fid, '%f\t%f\t%f\n', X.');
    fclose(fid);    
end    


function cost = known_alpha(CFM, alpha)
    fprintf('alpha in this iteration: %f\n', alpha);
    fprintf('Finding all-pairs shortest path\n');
    tic
    D = full(squareform(CFM));
    n = size(D, 1);
    D = D.^(-alpha);
    D(1:n+1:n*n) = 0;
    toc
    
    tic
    for k=1:n
        A = repmat(D(:,k), 1, n);
        A = bsxfun(@plus, A, D(k,:));
        D = min(D, A); 
    end
    toc
    
    D = squareform(D);

    X = cmdscale(D, 3);
    clear D;
    
    cost = sum(abs(pdist(X).^(-alpha) - CFM));
    fprintf('cost: %f\n', cost);
end