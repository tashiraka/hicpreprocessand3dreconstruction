function [X, s] = shortest_path_weight_MBO_1m(normedCFMFile, nonZeroSumFlagsFile,...
                                           outXFile, outParamFile, ...
                                           CFMImageFile, DImageFile, ...
                                              recoveredCFMImageFile)
    tic
    fprintf('Initialize normalized CFM\n')
    CFM = load(normedCFMFile);
    CFM(:,1:2) = CFM(:,1:2) + 1;
    CFM = spconvert(CFM);
    reducedNbins = size(CFM, 1);
    
    %fprintf('Plot the heatmap of normalized CFM\n');
    %saveas(plot(HeatMap(log10(CFM + CFM.' - diag(diag(CFM))), 'Colormap', 'redbluecmap')), CFMImageFile, 'png');
    
    fprintf('Finding all-pairs shortest path\n');
    D = CFM;
    H = CFM.';
    clear CFM;
    
    %D(find(D)) = D(find(D)).^(-1);
    %D = D.'; % This script assumes CFM is upper triangle.
    %D = squareform(graphallshortestpaths(D, 'Directed', false));
    D = full(D);
    n = size(D, 1);
    D = D + D.';
    D = D.^(-1);
    D(1:n+1:n*n) = 0;
    D = squareform(FastFloyd(D));
    save(strcat(normedCFMFile, '.D.mat'), 'D', '-v7.3');
    toc

    H(1:reducedNbins+1:reducedNbins*reducedNbins) = 0;
    H = full(squareform(H));
    alpha = 0.5;
    q = 1;

    %load(strcat(normedCFMFile, '.D.mat'));
    D(find(D)) = D(find(D)).^(alpha);
    
    %fprintf('Plot the heatmap of the distance matrix by shortest path\n');
    %saveas(plot(HeatMap(log10(squareform(D)), 'Colormap', 'redbluecmap')), DImageFile, 'png');

    fprintf('Calculate weights\n');
    H = (1 + D.^(-alpha) - H).^(-q);
    fprintf('max(H) = %f\n', max(H));
    fprintf('min(H) = %f\n', min(H));
    
    fprintf('Finding coordinates using MBO\n');
    options.solver = 'TR';
    options.tolcost = 1e-12;
    options.tolgradnorm = 1e-7;
    options.Delta0 = 10;
    options.Delta_bar = 10*options.Delta0;
    options.maxiter = 10000;
    [X, info] = MBO(D, H, options);
    %X = cmdscale(D, 3);
    clear D;
    clear H;
    
    fprintf('interpolate the bins without contacts\n');
    nonZeroSumFlags = load(nonZeroSumFlagsFile);
    nonZeroSumFlags = logical(nonZeroSumFlags);
    
    nbins = size(nonZeroSumFlags, 1);
    bins = (1:nbins).';
    reconstructedBins = bins(nonZeroSumFlags);
    X = interp1(reconstructedBins, X, bins, 'linear', 'extrap');
    
    fprintf('Writing X\n');
    fid = fopen(outXFile, 'w');
    fprintf(fid, '%f\t%f\t%f\n', X.');
    fclose(fid);
    
    fprintf('Writing parameter info\n');
    fid = fopen(outParamFile, 'w');
    fprintf(fid, 'alpha %f\n', alpha);
    fprintf(fid, 'q %f\n', q);
    fclose(fid);
    
    %fprintf('Plot the heatmap of the recovered CFM\n');
    %recoveredCFM = pdist(X).^(-alpha);
    %saveas(plot(HeatMap(squareform(log10(recoveredCFM)), 'Colormap', 'redbluecmap')), recoveredCFMImageFile, 'png');
end
