function interpolation(XFile, subBinInfoFile, nonZeroSumFlagsFile, ...
                       contigFile, outXFile, outBinInfoFile)

    fprintf('Initialize BinInfo\n');
    fid = fopen(subBinInfoFile);
    binInfo = textscan(fid, '%d %s %d %d');
    fclose(fid);
    
    fprintf('Initialize contig names\n');
    fid = fopen(contigFile);
    contigs = textscan(fid, '%s');
    fclose(fid);

    fprintf('Initializing X\n');
    X = load(XFile);

    fprintf('Initializing nonZeroSumFlags\n');
    nonZeroSumFlags = load(nonZeroSumFlagsFile);
    nonZeroSumFlags = logical(nonZeroSumFlags);

    fid = fopen(outXFile, 'w');
    fid2 = fopen(outBinInfoFile, 'w');

    for contig = contigs{1}.'
        fprintf('interpolate the missing bins except the ends: %s\n', char(contig));
        subX = X(strcmp(binInfo{2}(nonZeroSumFlags), contig), :);

        contigMask = strcmp(binInfo{2}, contig);
        subNonZeroSumFlags = nonZeroSumFlags(contigMask);
        bins = 1:size(subNonZeroSumFlags, 1);
        reconstructedBins = bins(subNonZeroSumFlags);

        fprintf('# 3D reconstructed bins: %d / %d \n', ...
                size(reconstructedBins, 2), size(bins, 2));
        if size(reconstructedBins, 2) == 0
            continue
        end
        
        subX = interp1(reconstructedBins, subX, bins, 'linear');

        endMask = ~isnan(subX(:, 1));
        subX = subX(endMask, :);
        fprintf(fid, '%f\t%f\t%f\n', subX.');
        
        fprintf('Writing binInfo\n');
        % nonZeroSumFlag is no need because X has been already
        % interpolated.
        ids = binInfo{1}(contigMask);
        ids = ids(endMask);
        c = binInfo{2}(contigMask);
        c = c(endMask);
        starts = binInfo{3}(contigMask);
        starts = starts(endMask);
        ends = binInfo{4}(contigMask);
        ends = ends(endMask);
        
        for i = 1:size(ids, 1)
            fprintf(fid2, '%d\t%s\t%d\t%d\n', ids(i), char(c(i)), starts(i), ends(i));
        end        
    end
    
    fclose(fid);
    fclose(fid2);
end