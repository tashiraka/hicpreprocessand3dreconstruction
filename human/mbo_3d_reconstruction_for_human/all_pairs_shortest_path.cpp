#include <boost/config.hpp>
#include <fstream>
#include <iostream>
#include <vector>
#include <cstdlib>
#include <string>
#include <boost/algorithm/string.hpp> // for boost:split
#include <boost/lexical_cast.hpp>
#include <boost/property_map/property_map.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/johnson_all_pairs_shortest.hpp>
#include <limits>

int main(int argc, char *argv[])
{
  using namespace boost;

  char* cfmFile = argv[1];
  char* outFile = argv[2];
  
  typedef adjacency_list<vecS, vecS, undirectedS, no_property,
			 property<edge_weight_t, double> > Graph;
  typedef boost::graph_traits<Graph>::vertex_descriptor vertex_descriptor;

  // Construct the graph
  typedef std::pair<int, int> Edge;
  std::string line;
  std::vector<Edge> edges;
  std::vector<double> weights;
  int vertexNum = 0;

  std::cout << "Loading the contact frequency file" << std::endl;
  std::ifstream ifs(cfmFile);
  
  while (getline(ifs, line)) {
    std::vector<std::string> fields;
    split(fields, line, is_any_of("\t"));
    int i = lexical_cast<int>(fields[0]);
    int j = lexical_cast<int>(fields[1]);
    double contactFrequency = lexical_cast<double>(fields[2]);
    if (contactFrequency != 0 && i != j) {
      edges.push_back(Edge(i, j));
      weights.push_back(1.0 / contactFrequency);
    }
    vertexNum = i; // The last entry indicates the matrix size.
  }
  vertexNum++;
  
  std::cout << "Constructing thr graph with # vetecies " << vertexNum << std::endl;    
  Graph g(edges.begin(), edges.end(), weights.begin(), vertexNum);

  std::vector<double> d(vertexNum, std::numeric_limits<double>::infinity());
  double **D = new double*[vertexNum];
  for(int i = 0; i < vertexNum; i++) D[i] = new double[vertexNum];

  std::cout << "Running johnson's algorithm" << std::endl;
  johnson_all_pairs_shortest_paths(g, D, distance_map(&d[0]));

  std::cout << "Write the distace matrix" << std::endl;
  std::ofstream ofs(outFile);
  
  for (int i = 0; i < vertexNum; i++) {
    bool first = true;
    for (int j = 0; j < vertexNum; j++) {
      if (first) ofs << D[i][j];
      else ofs << "\t" << D[i][j];
      first = false;
    }
    ofs << "\n";
  }
  
  return 0;
}
