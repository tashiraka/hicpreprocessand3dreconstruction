function KRnormalize(CFMFile, outFile, biasFile)
    fprintf('Loading the upper triangled contact frequency matrix from the file.\n');
    CFM = load(CFMFile);
    CFM(:, 1:2) = CFM(:, 1:2) + 1;
    CFM = spconvert(CFM);

    fprintf('Creating the full matrix from loaded upper (or lower) triangle matrix.\n');
    CFM = CFM + CFM.' - diag(diag(CFM));
    nbin = size(CFM, 1);

    fprintf(['Removing the rows and columns with all zeros or sum one ' ...
             'or sum two.\n']);
    sumCFM = sum(CFM, 1);
    nonZeroSumIDs = find(sumCFM ~= 0 & sumCFM ~= 1 & sumCFM ~=2);
    % Sum=2 prevent the convergence of bnewt.
    clear sumCFM;
    reducedCFM = CFM(nonZeroSumIDs, nonZeroSumIDs);
    clear CFM;

    %fprintf('Write nunZeroSumIDs to %s\n', nonZeroSumIDsFile);
    %fid = fopen(nonZeroSumIDsFile, 'w');
    %fprintf(fid, '%d\n', (nonZeroSumIDs - 1).');
    %fclose(fid);
    matrixSize = size(reducedCFM, 1);
    fprintf(['Normalizing (balancing) the contact frequency matrix (%d ' ...
             '* %d).\n'], size(nonZeroSumIDs, 2), size(nonZeroSumIDs,2));
    [x, res, notConvergeFlag] = bnewt(reducedCFM);
    clear res;

if notConvergeFlag
    fid = fopen(outFile,'w');
    fprintf(fid, 'Not converged\n');
    fclose(fid);
else
    normedReducedCFM = diag(x)*reducedCFM*diag(x);
    clear reducedCFM;
    
    %fprintf('Inserting the rows and columns with all zeros.\n');
    %normedCFM = sparse(nbin, nbin);
    %normedCFM(nonZeroSumIDs, nonZeroSumIDs) = normedReducedCFM;
    %clear normedReducedCFM;

    %fprintf('Writing the normalized constact frequency matrix to the file.\n');
    %[i, j, val] = find(triu(normedCFM));
    %i = i - 1;
    %j = j - 1;
    %fid = fopen(outFile,'w');
    %fprintf(fid, '%d\t%d\t%f\n', vertcat(i', j', val'));
    %fclose(fid);
    %clear normedCFM;
    
    fprintf(['Output the normalized matrix without rows and columns ' ...
             'with all zeros.\n']);
    [i, j, val] = find(triu(normedReducedCFM));
    i = i - 1;
    j = j - 1;
    fid = fopen(outFile,'w');
    fprintf(fid, '%d\t%d\t%f\n', vertcat(i', j', val'));
    if normedReducedCFM(matrixSize, matrixSize) == 0
        fprintf(fid, '%d\t%d\t%f\n', matrixSize - 1, matrixSize - 1, 0);
    end
    
    fclose(fid);
    clear normedReducedCFM;

    %bias = NaN(nbin);
    %bias(nonZeroSumIDs) = full(x);
    %clear nonZeroSumIDs;
    %clear x;    

    %fid = fopen(biasFile,'w');
    %fprintf(fid, '%f\n', bias);
    %fclose(fid);
    %clear bias;

    clear nonZeroSumIDs;
    
    fid = fopen(biasFile,'w');
    fprintf(fid, '%f\n', full(x));
    fclose(fid);
    clear x;
end