function [X, s] = shortest_path_weight_MBO_1m_param_test(normedCFMFile, nonZeroSumFlagsFile,...
                                                      outXFile, outParamFile, ...
                                                      CFMImageFile, DImageFile, ...
                                                      recoveredCFMImageFile)
    fprintf('Initialize normalized CFM\n')
    CFM = load(normedCFMFile);
    CFM(:,1:2) = CFM(:,1:2) + 1;
    CFM = spconvert(CFM);
    reducedNbins = size(CFM, 1);
    
    fprintf('Plot the heatmap of normalized CFM\n');
    saveas(plot(HeatMap(log10(CFM + CFM.' - diag(diag(CFM))), 'Colormap', 'redbluecmap')), CFMImageFile, 'png');
    
    fprintf('Finding all-pairs shortest path\n');
    %D = CFM;
    H = CFM.';
    clear CFM;
    H(1:reducedNbins+1:reducedNbins*reducedNbins) = 0;
    H = full(squareform(H));

    %D(find(D)) = D(find(D)).^(-1);
    %D = D.'; % This script assumes CFM is upper triangle.
    %D = squareform(graphallshortestpaths(D, 'Directed', false));
    %save(strcat(normedCFMFile, '.D.mat'), 'D', '-v7.3');
    load(strcat(normedCFMFile, '.D.mat'));


    fprintf('3D riconstruction with optimization of alpha and q\n');
    a_min = 0.1;
    a_max = 3;
    q_min = 1;
    q_max = 3;
    %log_a = fminbnd(@(a) known_a(D, H, exp(a), q_min, q_max), ...
    %                log(a_min), log(a_max), ...
    %                optimset('TolX', 0.001, 'Display',...
    %                         'notify', 'MaxIter', 100));
    
    %alpha = exp(log_a);
    alpha = 2;
    D(find(D)) = D(find(D)).^(alpha);
    fprintf('Plot the heatmap of the distance matrix by shortest path\n');
    saveas(plot(HeatMap(log10(squareform(D)), 'Colormap', ...
                        'redbluecmap')), DImageFile, 'png');

    fprintf('Calculate weights');
    %log_q = fminbnd(@(q) known_q(Da, H, exp(q)), ...
    %                log(q_min), log(q_max), ...
    %                optimset('TolX', 0.001, 'Display', 'notify', ...
    %                         'MaxIter', 100));
    %q = exp(log_q);
    q = 1;
    H = (1 + (H - D.^(-alpha))).^q;
    fprintf('max(H) = %f\n', max(H));
    fprintf('min(H) = %f\n', min(H));
    
    fprintf('Finding coordinates using MBO\n');
    options.solver = 'TR';
    options.tolcost = 1e-12;
    options.tolgradnorm = 1e-7;
    options.Delta0 = 10;
    options.Delta_bar = 10*options.Delta0;
    options.maxiter = 10000;
    [X, info] = MBO(D, H, options);
    clear D;
    clear H;
    
    fprintf('interpolate the bins without contacts\n');
    nonZeroSumFlags = load(nonZeroSumFlagsFile);
    nonZeroSumFlags = logical(nonZeroSumFlags);
    
    nbins = size(nonZeroSumFlags, 1);
    bins = (1:nbins).';
    reconstructedBins = bins(nonZeroSumFlags);
    X = interp1(reconstructedBins, X, bins, 'linear', 'extrap');
    
    fprintf('Writing X\n');
    fid = fopen(outXFile, 'w');
    fprintf(fid, '%f\t%f\t%f\n', X.');
    fclose(fid);
    
    fprintf('Writing parameter info\n');
    fid = fopen(outParamFile, 'w');
    fprintf(fid, 'alpha %f\n', alpha);
    fprintf(fid, 'q %f\n', q);
    fclose(fid);
    
    fprintf('Plot the heatmap of the recovered CFM\n');
    recoveredCFM = pdist(X).^(-alpha);
    saveas(plot(HeatMap(squareform(log10(recoveredCFM)), 'Colormap', 'redbluecmap')), recoveredCFMImageFile, ...
           'png');
end


function cost = known_a(D, H, alpha, q_min, q_max)
    Da = D;
    Da(find(Da)) = Da(find(Da)).^(alpha);

    fprintf('Calculate weights');
    H = (1 + (H - D.^(-alpha)));

    log_q = fminbnd(@(q) known_q(Da, H, exp(q)), ...
                    log(q_min), log(q_max), ...
                    optimset('TolX', 0.001, 'Display', 'notify', ...
                             'MaxIter', 100));
    q = exp(log_q);
    Hq = H.^q;
    fprintf('max(H) = %f\n', max(Hq));
    fprintf('min(H) = %f\n', min(Hq));

    options.solver = 'TR';
    options.tolcost = 1e-12;
    options.tolgradnorm = 1e-7;
    options.Delta0 = 10;
    options.Delta_bar = 10*options.Delta0;
    options.maxiter = 1000;
    [X, info] = MBO(Da, Hq, options);
    cost = info.cost;
end


function cost = known_q(D, H, q)
    Hq = H.^q;
    options.solver = 'TR';
    options.tolcost = 1e-12;
    options.tolgradnorm = 1e-7;
    options.Delta0 = 10;
    options.Delta_bar = 10*options.Delta0;
    options.maxiter = 1000;
    [X, info] = MBO(D, Hq, options);
    cost = info.cost;
end