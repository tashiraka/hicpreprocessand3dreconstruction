function speed_test(CFMFile)
    fprintf('Initialize normalized CFM\n')
    CFM = load(CFMFile);
    CFM(:,1:2) = CFM(:,1:2) + 1;
    CFM = spconvert(CFM);
    reducedNbins = size(CFM, 1);    
    D = CFM;
    clear CFM;    
    D = full(D);
    n = size(D, 1);
    D = D + D.';
    D = D.^(-1);
    D(1:n+1:n*n) = 0;

    tic
    for k=1:n
        %A = zeros(n);
        %A = bsxfun(@plus, A, D(:,k));
        %A = bsxfun(@plus, zeros(n), D(:,k)); % 26.3 s
        %A = repmat(D(:,k), 1, n); % 24.2
        %A = bsxfun(@plus, A, D(k,:));
        %D = min(D, A); % 25.6
        %D = bsxfun(@min, D, A); % 28 s
        %i2k = repmat(D(:,k), 1, n);
        %k2j = repmat(D(k,:), n, 1);
        %D = min(D, i2k+k2j); % 27 s
        %D = bsxfun(@min, D, i2k+k2j); %42 s
    end
    toc
end