function [X, s] = shortest_path_speed_test(normedCFMFile)
    fprintf('Initialize normalized CFM\n')
    CFM = load(normedCFMFile);
    CFM(:,1:2) = CFM(:,1:2) + 1;
    CFM = spconvert(CFM);
    
    fprintf('Finding all-pairs shortest path\n');
    D = CFM;

    D(find(D)) = D(find(D)).^(-1);
    tic
    DD = D.'; % This script assumes CFM is upper triangle.
    DD = squareform(full(graphallshortestpaths(DD, 'Directed', false)));
    toc
    
    tic
    D = D + D.';
    n = size(D, 1);
    D = full(D);
    D(D == 0) = Inf;
    D(1:n+1:n*n) = 0;
    D = squareform(FastFloyd(D));
    toc

    isequal(D, DD)
end
