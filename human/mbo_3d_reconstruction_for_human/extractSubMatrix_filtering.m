function extractSubMatrix_filtering(CFMFile, normedCFMFile, binInfoFile, contigNameFile,...
                                    lowerLimit,...
                                    outNormedCFMFile, outBinInfoFile, outSubNonZeroSumFlagsFile)
% CFM is upper or lower triangle.
% CFM contains the entry of (matrix size, matrix size, value). So
% OK only spconvert.
% binInfoFile fields: binID contigName startCoord endCoord
% extract chr1-chr2 matrix

    fprintf('Initialize BinInfo\n');
    fid = fopen(binInfoFile);
    binInfo = textscan(fid, '%d %s %d %d');
    fclose(fid);
    binInfo{1} = binInfo{1} + 1;

    fprintf('Initialize contig names\n');
    fid = fopen(contigNameFile);
    contigs = textscan(fid, '%s');
    fclose(fid);

    fprintf('Bin IDs corresponding to contigs\n');
    IDs = binInfo{1}(ismember(binInfo{2}, contigs{1}));

    fprintf('Write binInfo\n');
    binInfo{1} = binInfo{1} - 1;
    for id = 1:size(binInfo, 2)
        subBinInfo{id} = binInfo{id}(IDs);
    end
    
    fid = fopen(outBinInfoFile, 'w');
    for id = 1:size(subBinInfo{1}, 1)
        fprintf(fid, '%d\t%s\t%d\t%d\n', subBinInfo{1}(id), ...
                cell2mat(subBinInfo{2}(id)), ...
                subBinInfo{3}(id), subBinInfo{4}(id));
    end
    fclose(fid);
    clear subBinInfo;
    binInfo{1} = binInfo{1} + 1;


    fprintf('Initialize CFM\n');
    CFM = load(CFMFile);
    CFM(:,1:2) = CFM(:,1:2) + 1;
    CFM = spconvert(CFM);
    sumCFM = sum(CFM + CFM.' - diag(diag(CFM)));
    nonZeroSumFlags = (sumCFM >= lowerLimit);
    clear sumCFM;    
    fprintf('extract subCFM\n');
    subCFM = CFM(IDs, IDs);
    clear CFM;
    subNonZeroSumFlags = nonZeroSumFlags(IDs);
    clear IDs;
    sumCFM = sum(subCFM, 1) + sum(subCFM, 2).';
    clear subCFM;
    additionalSubNonZeroSumFlags = (sumCFM ~= 0);
    clear sumCFM;
    subNonZeroSumFlags = subNonZeroSumFlags & ...
        additionalSubNonZeroSumFlags;
    clear additionalSubNonZeroSumFlags;
 
    % nonZeroSumFlags for all bins
    % subNonZeroSumFlags for sub bins corresponding contigs
    % subNonZeroSumFlags will be used for interp1 in 3D reconstruction.

    fid = fopen(outSubNonZeroSumFlagsFile, 'w');
    fprintf(fid, '%d\n', full(subNonZeroSumFlags));
    fclose(fid);
    
    fprintf('Initialize normalized CFM\n');
    normedCFM = load(normedCFMFile);
    normedCFM(:,1:2) = normedCFM(:,1:2) + 1;
    normedCFM = spconvert(normedCFM);

    %{
    % normedCFM contains the bins only with nonZeroSumFlags == true.
    % I have to do re-indexing for normedCFM from full CFM.
    %}
    for col = 1:4
        binInfo{col} = binInfo{col}(nonZeroSumFlags);
    end
    binInfo{1} = (1:size(binInfo{1}, 1)).';
    normedIDs = binInfo{1}(ismember(binInfo{2}, contigs{1}));
    clear binInfo;

    % normedIDs for IDs of normedCFM corresponding to contigs 
    
    fprintf('extract normalized subCFM\n');
    subNormedCFM = normedCFM(normedIDs, normedIDs);
    clear normedCFM;
    % not remove sum=1 and sum=2, CAUSION that diag(subNormedCFM) is double-count. 
    sumCFM = sum(subNormedCFM, 1) + sum(subNormedCFM, 2).';
    nonZeroSumIDs = find(sumCFM ~= 0);
    clear sumCFM;
    subNormedCFM = subNormedCFM(nonZeroSumIDs, nonZeroSumIDs);
    subMatrixSize = size(nonZeroSumIDs, 2);
    clear nonZeroSumIDs;    
    [i, j, val] = find(subNormedCFM);
    i = i - 1;
    j = j - 1;

    fprintf('Write normalized subCFM\n');
    fid = fopen(outNormedCFMFile, 'w');
    fprintf(fid, '%d\t%d\t%f\n', vertcat(i', j', val'));
    if subNormedCFM(subMatrixSize, subMatrixSize) == 0
        fprintf(fid, '%d\t%d\t%f\n', subMatrixSize - 1, subMatrixSize - 1, 0);
    end
    fclose(fid);
    clear subNormedCFM subMatrixSize i j val;
end