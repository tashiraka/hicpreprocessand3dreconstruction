function calc_contact_probability(CFMFile, binInfoFile, outFile)
% CFM is upper or lower triangle.
% CFM contains the entry of (matrix size, matrix size, value). So
% OK only spconvert.
% binInfoFile fields: binID contigName startCoord endCoord

    fprintf('Initialize BinInfo\n');
    fid = fopen(binInfoFile);
    binInfo = textscan(fid, '%d %s %d %d');
    fclose(fid);
    binInfo{1} = binInfo{1} + 1;

    fprintf('Initialize contig names\n');
    fid = fopen(contigNameFile);
    contigs = textscan(fid, '%s');
    fclose(fid);

    fprintf('Bin IDs corresponding to contigs\n');
    IDs = binInfo{1}(ismember(binInfo{2}, contigs{1}));

    fprintf('Initialize normalized CFM\n')
    CFM = load(CFMFile);
    CFM(:,1:2) = CFM(:,1:2) + 1;
    CFM = spconvert(CFM);
    CFM = CFM + CFM.' - diag(diag(CFM));

    fprintf('Inserting the bins without contacts\n');
    nonZeroSumFlags = load(nonZeroSumFlagsFile);
    nonZeroSumFlags = logical(nonZeroSumFlags);
    
    fid = fopen(outFile, 'w');

    for contig = contigs
        intraChrIDs = binInfo(find(binInfo(:, 2) == contig), 1);
        intraChrCFM = logical(CFM(intraChrIDs, intraChrIDs));
        intraZeroSumFlags = zeroSumFlags(intraChrIDs, intraChrIDs);
        N = size(intraChrCFM, 1);
        
        if N < 2
            continue
        end
        
        for s = 1:(N - 1)
            % s-th off diagonal
            count = full(sum(diag(intraChrCFM, s)));
            % Ignore bins with all zeros
            zeroSumCount = full(sum(diag(intraZeroSumFlags, s)));;
            
            %n = N - s;
            n = N - s - zeroSumCount;
            if n > 0
                fprintf(fid, '%d\t%d\t%f\n', contig, s, count / n);
            else
                fprintf(fid, '%d\t%d\t%f\n', contig, s, 0);
            end
        end
    end

    fclose(fid);
end;
    