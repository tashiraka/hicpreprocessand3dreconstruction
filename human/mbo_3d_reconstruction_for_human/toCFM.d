// CFM == Contact Frequency Matrix
// upper triangle

import std.stdio, std.conv, std.string, std.algorithm, std.typecons,
  std.getopt, std.array;


void main(string[] args)
{
  uint fragBinSize = 0;
  uint coordBinSize = 0;
  GetoptResult helpInfo;

  try {
    helpInfo = getopt(args,
                      "frag", "The size of bin of fragments", &fragBinSize,
                      "coord", "The size of bin of coordinates", &coordBinSize);
  }
  catch(GetOptException e) {
    writeln(e.msg ~ "\nCheck -h/--help option.");
    return;
  }
  
  if(helpInfo.helpWanted) {
      defaultGetoptPrinter("Some information about the program.",
                           helpInfo.options);
      return;
  }

  if(fragBinSize != 0 && coordBinSize != 0) {
    // either --frag or --coord
    defaultGetoptPrinter("Choose either --frag  or --coord\n" ~
                         "Some information about the program.",
                         helpInfo.options);
    return;
  }
  else if(fragBinSize == 0 && coordBinSize == 0) {
    // Default is binning by # fragments and the size is 1.
    fragBinSize = 1;
  }

  assert(fragBinSize != 0 && coordBinSize != 0, "binByFrag XOR binByCoord");

  if(args.length != 5) {
    throw new Exception("\nArgments are wrong: \n" ~
                        reduce!((a, b) => a ~ " " ~ b)("", args)  ~ "\n" ~
                        "Expect: \n" ~ args[0] ~ 
                        " [options] inFile fragFile " ~
                        "outSparseCfmFile outBinInfoFile");
  }

  auto inFile = args[1];
  auto fragFile = args[2];
  auto outCfmFile = args[3];
  auto outBinInfoFile = args[4];

  auto frags = readFrag(fragFile);
  auto sparseCFM = readSparseCFM(inFile, (frags.length - 1).to!uint);
  
  Tuple!(uint[Tuple!(uint, uint)], "cfm",
         Tuple!(uint, string, uint, uint)[], "binInfo") result;
  
  if(fragBinSize != 0) {
    result = binningByFrag(sparseCFM, frags, fragBinSize);    
  }
  else {
    result = binningByCoord(sparseCFM, frags, coordBinSize);
  }

  // output
  auto fout = File(outCfmFile, "w");
  foreach(key; result.cfm.byKey) {
    fout.writeln(key[0], "\t", key[1], "\t", result.cfm[key]);
  }

  fout = File(outBinInfoFile, "w");
  foreach(row; result.binInfo) {
    foreach(i, elem; row) {
      if(i < row.length - 1) {
        fout.write(elem, "\t");
      }
      else {
        fout.write(elem);
      }
    }
    fout.writeln;
  }
}


// The format of binInfo: [binID, contigName, startCoord, endCoord]
Tuple!(uint[Tuple!(uint, uint)], "cfm",
       Tuple!(uint, string, uint, uint)[], "binInfo")
binningByFrag(uint[Tuple!(uint, uint)] sparseCFM, Fragment[] frags, int binSize)
{
  // binInfo
  Tuple!(uint, string, uint, uint)[] binInfo;
  auto fragIDToBinID = new uint[](frags.length);
  uint count = 0, binID = 0, start = frags[0].start, end = frags[0].end;
  string contigName = frags[0].rname;
  
  foreach(i, frag; frags) {
    if(contigName != frag.rname || count == binSize) {
      // Go to the next contig or the next bin
      binInfo ~= tuple(binID, contigName, start, end);
      count = 0;
      binID++;
      contigName = frag.rname;
      start = frag.start;
    }
    else {
      count++;
    }
    
    fragIDToBinID[i] = binID;
    end = frag.end;
  }

  binInfo ~= tuple(binID, contigName, start, end);
  fragIDToBinID[frags.length - 1] = binID;

  // binned sparse contact frequency matrix
  uint[Tuple!(uint, uint)] binnedSparseCFM;
  
  foreach(key; sparseCFM.byKey) {
    //uint.init == 0, so first increment success.
    binnedSparseCFM[tuple(fragIDToBinID[key[0]],
                          fragIDToBinID[key[1]])] += sparseCFM[key];
  }

  return Tuple!(uint[Tuple!(uint, uint)], "cfm",
                Tuple!(uint, string, uint, uint)[], "binInfo")(binnedSparseCFM,
                                                               binInfo);
}


// The format of binInfo: [binID, contigName, startCoord, endCoord]
// Assign binID to the fragments such as
// (fragStart + fragEnd) / 2 in [binStart, binEnd].
Tuple!(uint[Tuple!(uint, uint)], "cfm",
       Tuple!(uint, string, uint, uint)[], "binInfo")
binningByCoord(uint[Tuple!(uint, uint)] sparseCFM, Fragment[] frags, int binSize)
{
  // binInfo
  Tuple!(uint, string, uint, uint)[] binInfo;
  auto fragIDToBinID = new uint[](frags.length);
  uint count = 0, binID = 0, start = 1, end = binSize, contigEnd = 0;
  string contigName = frags[0].rname;

  foreach(i, frag; frags) {
    if(contigName != frag.rname) {
      // Go to the next contig
      binInfo ~= tuple(binID, contigName, start, contigEnd);
      count = 0;
      binID++;
      contigName = frag.rname;
      start = 1;
      end = binSize;
    }
    else if(frag.mid > end){
      // Go to the next bin.
      binInfo ~= tuple(binID, contigName, start, end);
      count++;
      binID++;
      start = binSize * count + 1;
      end = binSize * (count + 1);
    }
 
    fragIDToBinID[i] = binID;
    contigEnd = frag.end;
  }

  binInfo ~= tuple(binID, contigName, start, contigEnd);
  fragIDToBinID[frags.length - 1] = binID;

  // binned sparse contact frequency matrix
  uint[Tuple!(uint, uint)] binnedSparseCFM;
  
  foreach(key; sparseCFM.byKey) {
    //uint.init == 0, so first increment success.
    binnedSparseCFM[tuple(fragIDToBinID[key[0]],
                          fragIDToBinID[key[1]])] += sparseCFM[key];
  }

  return Tuple!(uint[Tuple!(uint, uint)], "cfm",
                Tuple!(uint, string, uint, uint)[], "binInfo")(binnedSparseCFM,
                                                               binInfo);
}


class Fragment
{
  uint id, start, end;
  string rname;
  bool gapFlag;
  
  this(uint id_, string rname_, uint start_, uint end_, bool gapFlag_) {
    id=id_; rname=rname_; start=start_; end=end_; gapFlag = gapFlag_;
  }

  uint mid() {
    return (start + end) / 2;
  }
}


Fragment[] readFrag(string file)
{
  auto app = appender!(Fragment[]);
  
  foreach(line; File(file, "r").byLine) {
    if(line != null) {
      auto fields = line.to!string.split("\t");
      app.put(new Fragment(fields[0].to!uint, fields[1],
                           fields[2].to!uint, fields[3].to!uint,
                           fields[4] == "1" ? true : false));
    }
  }
  
  return app.data;
}


uint[Tuple!(uint, uint)] readSparseCFM(string inFile, uint size)
{
  uint[Tuple!(uint, uint)] sparseCFM;

  foreach(line; File(inFile).byLine) {
    auto fields = line.strip().split('\t');
    auto fragID1 = fields[4].to!uint;
    auto fragID2 = fields[10].to!uint;

    sparseCFM[tuple(min(fragID1, fragID2), max(fragID1 ,fragID2))] += 1;
  }

  sparseCFM[tuple(size, size)] = 0;

  return sparseCFM;
}
